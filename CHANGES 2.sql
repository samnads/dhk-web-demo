CREATE TABLE `customer_support` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
---------------------------------------------------------------------------------------------
ALTER TABLE `package_subscription_payment`
ADD `transaction_charge` double(16,2) NULL AFTER `amount`;
---------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `mail_booking_confirmation_to_customer_status` tinyint NULL COMMENT 'NULL - not sent, 0 - send failed - 1 - send success',
ADD `mail_booking_confirmation_to_admin_status` tinyint NULL COMMENT 'NULL - not sent, 0 - send failed - 1 - send success' AFTER `mail_booking_confirmation_to_customer_status`;
---------------------------------------------------------------------------------------------
ALTER TABLE `package_subscription`
ADD `mail_package_confirmation_to_customer_status` tinyint(1) NULL COMMENT 'NULL - not sent, 0 - send failed - 1 - send success',
ADD `mail_package_confirmation_to_admin_status` tinyint(1) NULL COMMENT 'NULL - not sent, 0 - send failed - 1 - send success' AFTER `mail_package_confirmation_to_customer_status`;
---------------------------------------------------------------------------------------------
ALTER TABLE `customer_payments`
ADD `online_payment_id` bigint(20) NULL AFTER `payment_id`,
ADD FOREIGN KEY (`online_payment_id`) REFERENCES `online_payments` (`payment_id`);
---------------------------------------------------------------------------------------------
ALTER TABLE `customers`
ADD `email_verified_at` datetime NULL DEFAULT NOW();
---------------------------------------------------------------------------------------------
ALTER TABLE `customers`
CHANGE `email_verified_at` `email_verified_at` datetime NULL;
---------------------------------------------------------------------------------------------
ALTER TABLE `package_subscription_payment`
ADD `total_amount` double(16,2) NULL AFTER `transaction_charge`;
---------------------------------------------------------------------------------------------
update `package_subscription_payment` SET `total_amount` = (`amount` + `transaction_charge`);
---------------------------------------------------------------------------------------------
CREATE TABLE `week_days` (
  `week_day_id` int(11) NOT NULL,
  `weekend` tinyint(1) NOT NULL DEFAULT 0,
  `order_id` int(11) NOT NULL,
  `week_name` varchar(50) NOT NULL,
  PRIMARY KEY (`week_day_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `week_days` (`week_day_id`, `weekend`, `order_id`, `week_name`) VALUES
(0,	1,	1,	'Sunday'),
(1,	0,	2,	'Monday'),
(2,	0,	3,	'Tuesday'),
(3,	0,	4,	'Wednesday'),
(4,	0,	5,	'Thursday'),
(5,	0,	6,	'Friday'),
(6,	0,	7,	'Saturday');
---------------------------------------------------------------------------------------------
CREATE TABLE `holidays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
---------------------------------------------------------------------------------------------
ALTER TABLE `holidays`
ADD `holiday_name` varchar(255) NOT NULL AFTER `date`;
---------------------------------------------------------------------------------------------
ALTER TABLE `holidays`
ADD `created_by_user` int(11) NULL AFTER `holiday_name`,
ADD FOREIGN KEY (`created_by_user`) REFERENCES `users` (`user_id`);
---------------------------------------------------------------------------------------------
ALTER TABLE `crew_in_options`
ADD `sort_order` int(5) NULL AFTER `text`;