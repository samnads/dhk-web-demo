<?php
return [
    'package_payment_success_slug_prefix' => 'package/payment/success/',
    'package_payment_failed_slug_prefix' => 'package/payment/failed/',
    'service_payment_success_slug_prefix' => 'payment-success/',
    'service_payment_failed_slug_prefix' => 'payment-error/',
];