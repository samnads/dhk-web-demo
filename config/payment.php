<?php
return [
    'ccavenue_merchant_id' => env('CCAVENUE_MERCHANT_ID'),
    'ccavenue_req_handler' => env('CCAVENUE_REQ_HANDLER'),
    'ccavenue_resp_handler' => env('CCAVENUE_RESP_HANDLER'),
    'ccavenue_resp_handler_online' => env('CCAVENUE_RESP_HANDLER_ONLINE'),
];
