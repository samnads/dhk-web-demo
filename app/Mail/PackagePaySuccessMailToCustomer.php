<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class PackagePaySuccessMailToCustomer extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $customer;
    public $bookings;
    public $customerAddress;
    public $area;
    public $services;
    public $online;
    public $package;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$customer,$bookings,$customerAddress,$area,$services,$online,$package)
    {
        $this->email = $email;
        $this->customer = $customer;
        $this->bookings = $bookings;
        $this->customerAddress = $customerAddress;
        $this->area = $area;
        $this->services = $services;
        $this->online = $online;
        $this->package = $package;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'), Config::get('values.mail_name'))
            ->to($this->email,@$this->customer['customer_name'])
            ->subject('Package Booking Confirmation - '.$this->bookings['reference_id'])
            ->view('emails.package_pay_confirmation_mail_to_customer_template');  
    }
}
