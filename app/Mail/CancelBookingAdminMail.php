<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class CancelBookingAdminMail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$data)
    {
        $this->email = $email;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'), Config::get('values.mail_name'))
            ->to(Config::get('values.mail_admin'),'Admin')
            ->subject('Booking cancellation - '.$this->data['reference_id'])
            ->view('emails.booking_cancel_admin'); 
    }
}
