<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class OnlineSuccessMailAdmin extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $customer;
    public $customerAddress;
    public $areaName;
    public $services;
    public $online;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$customer,$customerAddress,$areaName,$services,$online)
    {
        $this->email = $email;
        $this->customer = $customer;
        $this->customerAddress = $customerAddress;
        $this->areaName = $areaName;
        $this->services = $services;
        $this->online = $online;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'), Config::get('values.mail_name'))
            ->to('accounts@dubaihousekeeping.com','Admin')
            ->subject('Cleaning Payment Confirmation - '.$this->online['reference_id'])
            ->view('emails.online-success-admin');  
    }
}
