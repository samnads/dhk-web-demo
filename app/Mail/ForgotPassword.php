<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;


class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $password;
    public $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$password,$name)
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'), Config::get('values.mail_name'))
            ->to($this->email,@$this->name)
            ->subject('Password Recovery - Dubai Housekeeping Account Password')
            ->view('emails.forgot_password');    
    }
}
