<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Mail\PackagePaySuccessMailToCustomer;
use Illuminate\Support\Facades\Mail;
use App\Mail\PackagePaySuccessMailToAdmin;
use Redirect;

class PackagePaymentController extends Controller
{
    public function ccavenue_response_handler(Request $request)
    {
        $workingKey = Config::get('values.ccavenue_workingkey'); //Working Key should be provided here.
        $encResponse = $_POST["encResp"]; //This is the response sent by the CCAvenue Server
        $rcvdString = ccavDecrypt($encResponse, $workingKey); //Crypto Decryption used as per the specified working key.
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        echo "<center>";
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 3) {
                $order_status = $information[1];
            }

        }
        parse_str($rcvdString, $params);
        $query_params = [];
        if (isset($_GET['mobile'])) {
            $query_params['mobile'] = "";
        }
        $query_params = http_build_query($query_params);
        if ($params['order_status'] == 'Success') {
            /************************************************************** */
            // update status
            $data['order_id'] = $params['order_id'];
            $data['transaction_id'] = $params['tracking_id'];
            $data['status'] = $params['order_status'];
            $data['post_data'] = json_encode($params);
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('post', Config::get('api.api_url') . 'change-package-payment-status', [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => $data,
            ]);
            /************************************************************** */
            // merchant_param1 have success url slug
            $redirect = url('') . '/' . $params['merchant_param1'] . '/' . $params['order_id'] . '/' . $params['tracking_id'] . ($query_params ? ('?' . $query_params) : '');
        } else {
            /************************************************************** */
            // update status
            $data['order_id'] = $params['order_id'];
            $data['transaction_id'] = $params['tracking_id'];
            $data['status'] = $params['order_status'];
            $data['post_data'] = json_encode($params);
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('post', Config::get('api.api_url') . 'change-package-payment-status', [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => $data,
            ]);
            /************************************************************** */
            // merchant_param2 have failed url slug
            $redirect = url('') . '/' . $params['merchant_param2'] . '/' . $params['order_id'] . '/' . $params['tracking_id'] . ($query_params ? ('?' . $query_params) : '');
        }
        return Redirect::to($redirect);
    }
    public function paymentSuccess(Request $request, $reference_id)
    {
        /************************************************************************************************* */
        if(isWebView() && !$request->has('user_id')){
            // if mobile add user_id and token
            return Redirect::to(request()->fullUrlWithQuery(['user_id' => session('customerId'),'token' => session('token')]));
        }
        /************************************************************************************************* */
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('GET', Config::get('api.api_url') . 'package/payment/' . $reference_id, [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        /************************************************************************************************* */
        if ($responseBody['subscription']['mail_package_confirmation_to_customer_status'] != 1 && session('mail_package_confirmation_to_customer_status_'.$responseBody['subscription']['id']) != 1) {
            // send confirmation mail to customer
            try {
                Mail::send(new PackagePaySuccessMailToCustomer($responseBody['customer']['email_address'], $responseBody['customer'], $responseBody['subscription'], $responseBody['customer_address'], @$responseBody['area'], $responseBody['service'], $responseBody['payment'], $responseBody['package']));
                session(['mail_package_confirmation_to_customer_status_'.$responseBody['subscription']['id'] => 1]);
                $mail_package_confirmation_to_customer_status = 1;
            } catch (\Exception $e) {
                $mail_package_confirmation_to_customer_status = 0;
            }
        }
        /************************************************************************************************* */
        if ($responseBody['subscription']['mail_package_confirmation_to_admin_status'] != 1 && session('mail_package_confirmation_to_admin_status_'.$responseBody['subscription']['id']) != 1) {
            // send confirmation mail to admin
            try {
                Mail::send(new PackagePaySuccessMailToAdmin(Config::get('values.mail_admin'), $responseBody['customer'], $responseBody['subscription'], $responseBody['customer_address'], @$responseBody['area'], $responseBody['service'], $responseBody['payment'], $responseBody['package']));
                session(['mail_package_confirmation_to_admin_status_'.$responseBody['subscription']['id'] => 1]);
                $mail_package_confirmation_to_admin_status = 1;
            } catch (\Exception $e) {
                $mail_package_confirmation_to_admin_status = 0;
            }
        }
        /************************************************************************************************* */
        try {
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('POST', Config::get('api.api_url') . 'package-mail-status', [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                    'package_subscription_id' => $responseBody['subscription']['id'],
                    'mail_package_confirmation_to_customer_status' => @$mail_package_confirmation_to_customer_status,
                    'mail_package_confirmation_to_admin_status' => @$mail_package_confirmation_to_admin_status,
                ],
            ]);
        } catch (\Exception $e) {
        }
        /************************************************************************************************* */
        if (strtolower($responseBody['payment']['payment_status']) == 'success' || strtolower($responseBody['payment']['payment_mode']) == 'cash') {
            return view('Packages.payment_success', ['data' => $responseBody]);
        } else {
            return view('Packages.payment_failed', ['data' => $responseBody]);
        }
    }
    public function paymentFailed(Request $request, $reference_id)
    {
        /************************************************************************************************* */
        if(isWebView() && !$request->has('user_id')){
            // if mobile add user_id and token
            return Redirect::to(request()->fullUrlWithQuery(['user_id' => session('customerId'),'token' => session('token')]));
        }
        /************************************************************************************************* */
        // get details
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $response = $client->request('get', Config::get('api.api_url') . 'package/payment/' . $reference_id, [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                ],
            ]);
            $responseBody = json_decode((string) $response->getBody(), true);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }
        /********************************************************************
        $transaction_id = $responseBody['payment']['transaction_id'] ?: $request->transaction_id;
        try {
            if ($responseBody['payment']['payment_mode'] != 'cash' && $responseBody['payment']['payment_status'] != 'success' && $transaction_id) {
                // get payment status officially from api
                $checkout_header = array("Authorization: Bearer " . Config::get('values.checkout_secret_key'), "Content-Type: application/json", "Accept: application/json");
                $checkout_data = $this->curlGet(Config::get('values.checkout_service_url') . $transaction_id, $checkout_header);
                $checkout = json_decode($checkout_data, true);
                // end
                $data['order_id'] = $reference_id;
                $data['transaction_id'] = $transaction_id;
                $data['status'] = (isset($checkout["approved"]) && $checkout["approved"]) == true ? 'success' : $checkout["status"];
                $client = new \GuzzleHttp\Client(['verify' => false]);
                $response = $client->request('post', Config::get('api.api_url') . 'change-package-payment-status', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'form_params' => $data,
                ]);

            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }
        /******************************************************************** */
        // get again
        /*try {
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $response = $client->request('get', Config::get('api.api_url') . 'package/payment/' . $reference_id, [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                ],
            ]);
            $responseBody = json_decode((string) $response->getBody(), true);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }*/
        if (strtolower($responseBody['payment']['payment_status']) != 'success') {
            return view('Packages.payment_failed', ['data' => $responseBody]);
        } else {
            return view('Packages.payment_success', ['data' => $responseBody]);
        }
    }
    public function cardCheckout(Request $request)
    {
        $data = array();
        $data['source'] = array("type" => "token", "token" => $request['token_req']);
        $data['customer'] = array("email" => $request['customer_email'], "name" => $request['customer_name']);
        $data['3ds'] = array("enabled" => true);
        $data['amount'] = (int) ($request['amount'] * 100);
        $data['currency'] = 'AED';
        $data['reference'] = $request['order_id'];
        $data['success_url'] = url('package/payment/success/' . $request['order_id']);
        $data['failure_url'] = url('package/payment/failed/' . $request['order_id']);
        $data['processing_channel_id'] = Config::get('values.checkout_channel_id');
        //dd($data);
        $post_datas = json_encode($data);
        $secret_key = Config::get('values.checkout_secret_key');
        $ServiceURL = Config::get('values.checkout_service_url');
        $Headers = array("Authorization: Bearer " . $secret_key, "Content-Type: application/json", "Accept: application/json");
        /************************************************************************* */
        // check already paid or not (check on db only)
        try {
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('get', Config::get('api.api_url') . 'package/payment/status/success/' . $request['order_id'], [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [],
            ]);
            $responseBody = json_decode((string) $response->getBody(), true);
            if ($responseBody['data']) {
                return Redirect::to('package/payment/success/' . $request['order_id'] . '?status=already-verified');
            }
        } catch (\Exception $e) {
            return 'Current Payment Status Check Failed !';
        }
        /************************************************************************* */
        $orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_datas);
        $result = json_decode($orderCreateResponse, true);
        /************************************************************************* */
        // update with real payment status
        try {
            $result['payment_id'] = $request['payment_id'];
            $result['order_id'] = $request['order_id'];
            $result['transaction_id'] = $result['id'];
            if (isset($result['approved']) && $result['approved'] == true) {
                $result['status'] = 'success';
            }
            //dd($result);
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('post', Config::get('api.api_url') . 'change-package-payment-status', [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => $result,
            ]);
            $responseBody = json_decode((string) $response->getBody(), true);
        } catch (\Exception $e) {
        }
        /************************************************************************* */
        if (isset($result['approved']) && $result['approved'] == true) {
            // payment success
            return Redirect::to('package/payment/success/' . $request['order_id']);
        } else {
            // payment failed
            if (isset($result['status']) && $result['status'] == 'Pending') {
                return Redirect::to($result['_links']['redirect']['href']);
            } else {
                return Redirect::to('package/payment/failed/' . $request['order_id']);
            }
        }
    }
    public function gpayCheckout(Request $request)
    {
        $paymentToken = $request['paymentToken'];
        $secret_key = Config::get('values.checkout_primary_key');
        $ServiceURL = Config::get('values.checkout_token_url');
        $Headers = array("Authorization: Bearer " . $secret_key, "Content-Type: application/json", "Accept: application/json");
        $post_fields = '{"type":"googlepay","token_data":' . $paymentToken . '}';
        $orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
        $result = json_decode($orderCreateResponse, true);
        if ($result != null) {
            $data = array();
            $data['source'] = array("type" => "token", "token" => $result['token']);
            $data['customer'] = array("email" => $request['email'], "name" => $request['name']);
            $data['3ds'] = array("enabled" => true);
            $data['amount'] = (int) ($request['amount'] * 100);
            $data['currency'] = 'AED';
            $data['processing_channel_id'] = Config::get('values.checkout_channel_id');
            $data['reference'] = $request['reference_id'];
            $data['success_url'] = url('package/payment/success/' . $request['reference_id']);
            $data['failure_url'] = url('package/payment/failed/' . $request['reference_id']);
            $post_datas = json_encode($data);
            $secret_key_new = Config::get('values.checkout_secret_key');
            $ServiceURL_new = Config::get('values.checkout_service_url');
            $Headers_new = array("Authorization: Bearer " . $secret_key_new, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
            $result_new = json_decode($orderCreateResponse_new, true);
            //dd($orderCreateResponse_new);
            if (!isset($result_new['error_type'])) {
                $status = $result_new['status'];
                if ($status == 'Pending') {
                    /******************************************************* */
                    // update payment status
                    try {
                        $data['order_id'] = $request['reference_id'];
                        $data['transaction_id'] = $result_new['id'];
                        $data['status'] = $result_new['status'];
                        $client = new \GuzzleHttp\Client(['verify' => false]);
                        $response = $client->request('post', Config::get('api.api_url') . 'change-package-payment-status', [
                            'headers' => [
                                'cache-control' => 'no-cache',
                                'Content-Type' => 'application/x-www-form-urlencoded',
                            ],
                            'form_params' => $data,
                        ]);
                    } catch (\Exception $e) {
                    }
                    /******************************************************* */
                    return response()->json(
                        [
                            'status' => 'success',
                            'url' => $result_new['_links']['redirect']['href'],
                        ],
                        200
                    );
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'url' => $data['failure_url'],
                        ],
                        200
                    );
                }
            } else {
                return response()->json(
                    [
                        'status' => 'failed',
                        'url' => $data['failure_url'],
                    ],
                    200
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'url' => $data['failure_url'],
                ],
                200
            );

        }
    }
    public function applepayCheckout(Request $request)
    {
        $refId = $request['ap_order_id'];
        //$amount = $request['amount'];
        $paymentToken = $request['paymentToken'];
        $secret_key = Config::get('values.checkout_primary_key');
        $ServiceURL = Config::get('values.checkout_token_url');
        $Headers = array("Authorization: Bearer " . $secret_key, "Content-Type: application/json", "Accept: application/json");
        $post_fields = '{"type":"applepay","token_data":' . $paymentToken . '}';
        $orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
        $result = json_decode($orderCreateResponse, true);
        if ($result != null) {
            $data = array();
            $data['source'] = array("type" => "token", "token" => $result['token']);
            $data['customer'] = array("email" => $request['ap_billing_email'], "name" => $request['ap_billing_name']);
            $data['3ds'] = array("enabled" => true);
            $data['amount'] = (int) ($request['ap_amount'] * 100);
            $data['currency'] = 'AED';
            $data['processing_channel_id'] = Config::get('values.checkout_channel_id');
            $data['reference'] = $request['ap_order_id'];
            $data['success_url'] = Config::get('values.checkout_success');
            $data['failure_url'] = Config::get('values.checkout_fail');
            $post_datas = json_encode($data);
            $secret_key_new = Config::get('values.checkout_secret_key');
            $ServiceURL_new = Config::get('values.checkout_service_url');
            $Headers_new = array("Authorization: Bearer " . $secret_key_new, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
            $result_new = json_decode($orderCreateResponse_new, true);
            // echo '<pre>';
            // print_r($result_new);
            if (!isset($result_new['error_type'])) {
                $approved = $result_new['approved'];
                $status = $result_new['status'];
                /******************************************************* */
                // update payment status on db
                try {
                    $data['order_id'] = $refId;
                    $data['transaction_id'] = $result_new['id'];
                    $data['status'] = $status;
                    $client = new \GuzzleHttp\Client(['verify' => false]);
                    $response = $client->request('post', Config::get('api.api_url') . 'change-package-payment-status', [
                        'headers' => [
                            'cache-control' => 'no-cache',
                            'Content-Type' => 'application/x-www-form-urlencoded',
                        ],
                        'form_params' => $data,
                    ]);
                } catch (\Exception $e) {
                }
                /******************************************************* */
                if ($approved == true) {
                    //$redirectUrl = $result_new['_links']['redirect']['href'];
                    //return redirect($redirectUrl);
                    return response()->json(
                        [
                            'status' => 'success',
                            'transaction_id' => $result_new['id'],
                            'reference_id' => $refId,
                        ],
                        200
                    );
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'reference_id' => $refId,
                        ],
                        200
                    );
                }
            } else {
                return response()->json(
                    [
                        'status' => 'failed',
                        'reference_id' => $refId,
                    ],
                    200
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'reference_id' => $refId,
                ],
                200
            );
        }
    }
    public function retryPayment(Request $request, $reference_id)
    {
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $response = $client->request('get', Config::get('api.api_url') . 'package/payment/' . $reference_id, [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                ],
            ]);
            $responseBody = json_decode((string) $response->getBody(), true);
            if ($responseBody['payment']['payment_status'] == 'success') {
                return response()->json(['status' => 'error', 'message' => 'Payment Already Verified !'], 200);
            } else if ($responseBody['payment']['payment_mode'] == 'cash') {
                return response()->json(['status' => 'error', 'message' => 'Online Payment Not Possible !'], 200);
            }
            return response()->json(['status' => 'success', 'data' => $responseBody], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }
    }
    public static function curlFun($url, $headers, $data)
    {
        try {
            $ch = curl_init();
            //$certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
            //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if ($response === false) {
                die(curl_error($ch));
            }
            curl_close($ch);
            return $response;
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
        }
    }
    public static function curlGet($url, $Headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $Headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if ($response === false) {
            die(curl_error($ch));
            // echo 'no';
            // echo 'Request Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $response;
    }
}
