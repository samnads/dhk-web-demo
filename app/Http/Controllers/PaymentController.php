<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\OnlineSuccessMailAdmin;
use App\Mail\OnlineSuccessMail;
use App\Mail\CancelBookingAdminMail;
use App\Mail\CancelBookingUserMail;
use App\Mail\SuccessMailAdmin;
use App\Mail\SuccessMail;
use Redirect;
use Illuminate\Support\Facades\Storage;

class PaymentController extends Controller
{
    public function payLink(Request $request)
    {
        if(isset($request->amount)){
            $amount = $request->amount;
        } else {
            $amount = 0;
        }
        if(isset($request->id)){
            $customerId = $request->id;
        } else {
            $customerId = 0;
        }
        if(isset($request->message)){
            $message = $request->message;
        } else {
            $message = 0;
        }
        return view('Payment.make-payment',['customerId'=>$customerId,'amount'=>$amount,'message'=>$message]);
    }

    public function saveOnlinePay(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'save-online-pay', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId'=>$request->input('customerId'),
                'amount'=>$request->input('amount'),
                'description'=>$request->input('description')
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return view('Payment.confirm-payment',['data'=>$responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }
    }

    public function onlinePaymentSuccess($payId,$trackId)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'online-payment-success', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'payId' => $payId, // hard coded service id for cleaning
                'trackId' => $trackId,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);

        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            try {
                $customer = $responseBody['data']['customer'];
                $customerAddress =  $responseBody['data']['customer_address'];
                $areaName = $responseBody['data']['areaName'];
                $services = $responseBody['data']['serviceName'];
                $paymentData = $responseBody['data']['paymentData'];
                Mail::send(new OnlineSuccessMail($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
                Mail::send(new OnlineSuccessMailAdmin($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
                return view('common.success',['data'=>$responseBody['data']]);
            }  catch (\Exception $e) {
                return view('common.success',['data'=>$responseBody['data']]);
            }
        } else if($responseStatus == 'refresh_success') {
            return redirect('/');
        }
        else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
        
    }
    public function onlinePaymentFailed($payId,$trackId)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'online-payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'payId' => $payId, // hard coded service id for cleaning
                'trackId' => $trackId,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            return view('common.fail',['data'=>$responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
       
    }
    public function onlineErrorFailed($ref,$id)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'online-payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'payId' => $ref, // hard coded service id for cleaning
                'trackId' => $id,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return view('common.fail',['data'=>$responseBody['data']]);
        } else {
             return view('NotFound.not_found');
        }
    }
    public function bookinglistPayment(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'save-bookinglist-pay', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'is_device'=>'web', 
                'bookingId'=>$request->input('bookingId')
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => $responseBody['messages'],
                    'data' => $responseBody['data']
                ]
            );
            // return view('Payment.confirm-payment',['data'=>$responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }   
    }
    public function cancelBooking(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'cancel-booking', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId'=>$request->input('bookingId'),
                'customerId'=>session('customerId'),
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
               $data = $responseBody['data'];
            try {
                Mail::send(new CancelBookingAdminMail($data['customer_email'],$data));
                Mail::send(new CancelBookingUserMail($data['customer_email'],$data));
    
                return response()->json(
                    [
                        'status' => 'success',
                        'messages' => $responseBody['messages'],
                    ]
                );
            } catch (\Exception $e) {
                return response()->json(
                    [
                        'status' => 'success',
                        'messages' => $responseBody['messages'],
                    ]
                );      
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        } 
    }

    public function payment(Request $request)
    { 
        $retryOnline = $request['retry_online'];
        $mobileParam = $request['mobile_view'];
        $data=array();
        $data['source']= array("type" => "token","token" => $request['token_req']);
        $data['customer']= array("email" => $request['customer_email'],"name" => $request['customer_name']);
        $data['3ds']= array("enabled" => true);
        // $data['amount']= $request['amount'];
        // $amt = 1;
        $data['amount']= (int)($request['amount'] * 100);
        $data['currency']= 'AED';
        $data['reference']= $request['order_id'];
		$data['processing_channel_id']= Config::get('values.checkout_channel_id');
        if($retryOnline == 'not_online') {
            $data['success_url']= Config::get('values.checkout_success').$mobileParam;
            $data['failure_url']= Config::get('values.checkout_fail').$mobileParam;
            $urlVal = 'payment-failed';
        } else if($retryOnline == 'invoice') {
            $data['success_url']= Config::get('values.checkout_invoice_success').$mobileParam;
            $data['failure_url']= Config::get('values.checkout_invoice_fail').$mobileParam;
            $urlVal = 'payment-invoice-failed';
        } else {
			$data['success_url']= Config::get('values.checkout_online_success').$mobileParam;
            $data['failure_url']= Config::get('values.checkout_online_fail').$mobileParam;
            $urlVal = 'online-payment-failed';
		}
        $post_datas=json_encode($data);
        $secret_key=Config::get('values.checkout_secret_key');
        $ServiceURL=Config::get('values.checkout_service_url');
        $Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
        $orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_datas);
        $result=json_decode($orderCreateResponse,true);
        if(!isset($result['error_type'])) {
            $status=$result['status'];
            if ($status == 'Pending') { 
                $redirectUrl = $result['_links']['redirect']['href'];
                return Redirect::to($redirectUrl);
            } else {
                if($mobileParam != '') {
                    return Redirect::to($urlVal.'/'.$result['reference'].'/'.$result['id'].'?mobile=active');
                } else {
                    return Redirect::to($urlVal.'/'.$result['reference'].'/'.$result['id']);
                }
            }
        } else{
            if($mobileParam != '') {
                return Redirect::to($urlVal.'/'.$request['order_id'].'/'.$result['request_id'].'?mobile=active');
            } else {
                return Redirect::to($urlVal.'/'.$request['order_id'].'/'.$result['request_id']);
            }
        }
    }

    public static function curlFun($url,$Headers,$post_datas)
    {
        $ch = curl_init();
        $certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_datas);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if($response === FALSE){
            die(curl_error($ch));
			// echo 'no';
			// echo 'Request Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $response;
    }
    public function paymentSuccess(Request $request)
    {
        if(isset($_GET['cko-session-id'])) {
            $sessionID = $_GET['cko-session-id'];
            $secret_key=Config::get('values.checkout_secret_key');
            $ServiceURL=Config::get('values.checkout_service_url').$sessionID;
            $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
			
            $result=json_decode($orderCreateResponse,true);
            if($result != null) { 
                $trackId = $result['id'];
                $referenceNo = $result['reference'];
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'payment-success', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'ref_no' => $referenceNo, // hard coded service id for cleaning
                        'trackId' => $trackId,
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == "success" || $responseStatus == 'success') {
                    try {
                        $customer = $responseBody['data']['customer'];
                        $customerAddress =  $responseBody['data']['customer_address'];
                        $bookings =  $responseBody['data']['bookings'];
                        $areaName = $responseBody['data']['areaName'];
                        $services = $responseBody['data']['services'];
                        $online = $responseBody['data']['online'];
                        Mail::send(new SuccessMail($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
                        Mail::send(new SuccessMailAdmin($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
                        return view('common.success',['data'=>$responseBody['data']]);
                    } catch (\Exception $e) {
                        return view('common.success',['data'=>$responseBody['data']]);
                    }
                } else if($responseStatus == 'refresh_success') {
                    return redirect('/');
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
            } else{
                return redirect('house-cleaning-dubai');
            }
        } else {
            return view('NotFound.not_found');
        }
    }
    public function errorFailed($ref,$id)
    {
        $trackId = $id;
        $referenceNo = $ref;
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'ref_no' => $referenceNo, // hard coded service id for cleaning
                'trackId' => $trackId,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            return view('common.fail',['data'=>$responseBody['data']]);
        } else {
            return view('NotFound.not_found');
        }
    }
	
	public function gpayPaymentFailed($ref,$id)
    {
        $trackId = $id;
        $referenceNo = $ref;
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'ref_no' => $referenceNo, // hard coded service id for cleaning
                'trackId' => $trackId,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            return view('common.gpayfail',['data'=>$responseBody['data']]);
        } else {
            return view('NotFound.not_found');
        }
    }
	
	public function invoiceGpayPaymentFailed($ref,$id)
    {
        $trackId = $id;
        $referenceNo = $ref;
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'invoice-payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'payId' => $referenceNo, // hard coded service id for cleaning
                'trackId' => $trackId,
                'payment_type' => 'gpay',
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            return view('common.invoicegpayfail',['data'=>$responseBody['data']]);
        } else {
            return view('NotFound.not_found');
        }
    }
	
    public function paymentFailed(Request $request)
    {
        if(isset($_GET['cko-session-id'])) {
            $sessionID = $_GET['cko-session-id'];
            $secret_key=Config::get('values.checkout_secret_key');
            $ServiceURL=Config::get('values.checkout_service_url').$sessionID;
            $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
            $result=json_decode($orderCreateResponse,true);
            if($result != null) { 
                $trackId = $result['id'];
                $referenceNo = $result['reference'];
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'payment-failed', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'ref_no' => $referenceNo, // hard coded service id for cleaning
                        'trackId' => $trackId,
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == 'success') {
                    return view('common.fail',['data'=>$responseBody['data']]);
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
            } else{
                return redirect('house-cleaning-dubai');
            }
        } else {
            return view('NotFound.not_found');
        }
    }
	
	public function paymentTamaraSuccess(Request $request)
    {
		if(isset($_GET['paymentStatus'])) {
            $trackId = $_GET['orderId'];
			$client = new \GuzzleHttp\Client([
				'verify' => false
			]);
			$response = $client->request('post', Config::get('api.api_url').'payment-tamara-success', [
				'headers' => [
					'cache-control' => 'no-cache',
					'Content-Type' => 'application/x-www-form-urlencoded'
				],
				'form_params' => [ // hard coded service id for cleaning
					'trackId' => $trackId,
				],
			]);
			$responseBody = json_decode((string) $response->getBody(), true);
			$responseStatus = $responseBody['status'];
	
			if($responseStatus == 'success') {
				try {
					$customer = $responseBody['data']['customer'];
					$customerAddress =  $responseBody['data']['customer_address'];
					$bookings =  $responseBody['data']['bookings'];
					$areaName = $responseBody['data']['areaName'];
					$services = $responseBody['data']['services'];
					$online = $responseBody['data']['online'];
					Mail::send(new SuccessMail($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
					Mail::send(new SuccessMailAdmin($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
					return view('common.success',['data'=>$responseBody['data']]);
				} catch (\Exception $e) {
					return view('common.success',['data'=>$responseBody['data']]);
				}
			} else if($responseStatus == 'refresh_success') {
				return redirect('/');
			} else {
				return response()->json(
					[
						'status' => 'failed',
						'messages' => $responseBody['message'],
					]
				);
			}
        } else {
            return view('NotFound.not_found');
        }
    }
	
	public function paymentTamaraError(Request $request)
    {
        if(isset($_GET['orderId'])) {
			$orderId = $_GET['orderId'];
			$status = $_GET['paymentStatus'];
			
			$client = new \GuzzleHttp\Client([
				'verify' => false
			]);
			$response = $client->request('post', Config::get('api.api_url').'tamara-payment-failed', [
				'headers' => [
					'cache-control' => 'no-cache',
					'Content-Type' => 'application/x-www-form-urlencoded'
				],
				'form_params' => [
					'trackId' => $_GET['orderId'],
					'status' => $_GET['paymentStatus'],
				],
			]);
			$responseBody = json_decode((string) $response->getBody(), true);
			
			$responseStatus = $responseBody['status'];

			if($responseStatus == 'success') {
				return view('common.tamarafail',['data'=>$responseBody['data']]);
			} else {
				return response()->json(
					[
						'status' => 'failed',
						'messages' => $responseBody['message'],
					]
				);
			}
		} else {
			return redirect('house-cleaning-dubai');
		}
    }
	
	public function makeGpayPayment(Request $request)
    {
		//$refId = $request['referenceId'];
		//$amount = $request['amount'];
		$paymentToken = $request['paymentToken'];
        $secret_key=Config::get('values.checkout_primary_key');
		$ServiceURL=Config::get('values.checkout_token_url');
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$post_fields = '{"type":"googlepay","token_data":'.$paymentToken.'}';
		
		//$dataa = array('country'=>'AE','currency'=>'AED','phone'=>$phone,'order_value'=>$ordervalue);
		$orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
		$result=json_decode($orderCreateResponse,true);
		if($result != null) {
			$data=array();
			$data['source']= array("type" => "token","token" => $result['token']);
			$data['customer']= array("email" => $request['email'],"name" => $request['name']);
			$data['3ds']= array("enabled" => true);
			$data['amount']= (int)($request['amount'] * 100);
			$data['currency']= 'AED';
			$data['processing_channel_id']= Config::get('values.checkout_channel_id');
			$data['reference']= $request['referenceId'];
			$data['success_url']= Config::get('values.checkout_success');
            $data['failure_url']= Config::get('values.checkout_fail');
            $urlVal = 'gpay-payment-failed';
			
			$post_datas=json_encode($data);
			$secret_key_new=Config::get('values.checkout_secret_key');
			$ServiceURL_new=Config::get('values.checkout_service_url');
			$Headers_new  = array("Authorization: Bearer ".$secret_key_new, "Content-Type: application/json", "Accept: application/json");
			$orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
			$result_new=json_decode($orderCreateResponse_new,true);
			// echo '<pre>';
			// print_r($result_new);
			if(!isset($result_new['error_type'])) {
				$status=$result_new['status'];
				if ($status == 'Pending') { 
					$redirectUrl = $result_new['_links']['redirect']['href'];
					//return redirect($redirectUrl);
					return response()->json(
						[
							'status' => 'success',
							'url' =>$redirectUrl,
						],
						200
					);
				} else {
					return Redirect::to($urlVal.'/'.$result_new['reference'].'/'.$result_new['id']);
				}
			} else{
				return Redirect::to($urlVal.'/'.$request['referenceId'].'/'.$result_new['request_id']);
			}
		} else {
			return redirect('house-cleaning-dubai');
		}
    }
	
	public function makeInvoiceGpayPayment(Request $request)
    {
		//$refId = $request['referenceId'];
		//$amount = $request['amount'];
		$paymentToken = $request['paymentToken'];
        $secret_key=Config::get('values.checkout_primary_key');
		$ServiceURL=Config::get('values.checkout_token_url');
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$post_fields = '{"type":"googlepay","token_data":'.$paymentToken.'}';
		
		//$dataa = array('country'=>'AE','currency'=>'AED','phone'=>$phone,'order_value'=>$ordervalue);
		$orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
		$result=json_decode($orderCreateResponse,true);
		if($result != null) {
			$data=array();
			$data['source']= array("type" => "token","token" => $result['token']);
			$data['customer']= array("email" => $request['email'],"name" => $request['name']);
			$data['3ds']= array("enabled" => true);
			$data['amount']= (int)($request['amount'] * 100);
			$data['currency']= 'AED';
			$data['processing_channel_id']= Config::get('values.checkout_channel_id');
			$data['reference']= $request['referenceId'];
			$data['success_url']= Config::get('values.checkout_gpay_invoice_success');
            $data['failure_url']= Config::get('values.checkout_gpay_invoice_fail');
            $urlVal = 'invoice-gpay-payment-failed';
			
			$post_datas=json_encode($data);
			$secret_key_new=Config::get('values.checkout_secret_key');
			$ServiceURL_new=Config::get('values.checkout_service_url');
			$Headers_new  = array("Authorization: Bearer ".$secret_key_new, "Content-Type: application/json", "Accept: application/json");
			$orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
			$result_new=json_decode($orderCreateResponse_new,true);
			// echo '<pre>';
			// print_r($result_new);
			if(!isset($result_new['error_type'])) {
				$status=$result_new['status'];
				if ($status == 'Pending') { 
					$redirectUrl = $result_new['_links']['redirect']['href'];
					//return redirect($redirectUrl);
					return response()->json(
						[
							'status' => 'success',
							'url' =>$redirectUrl,
						],
						200
					);
				} else {
					return Redirect::to($urlVal.'/'.$result_new['reference'].'/'.$result_new['id']);
				}
			} else{
				return Redirect::to($urlVal.'/'.$request['referenceId'].'/'.$result_new['request_id']);
			}
		} else {
			return redirect('house-cleaning-dubai');
		}
    }
	
	public function appleProcessPay(Request $request)
    {
		$refId = $request['ap_order_id'];
		//$amount = $request['amount'];
		$paymentToken = $request['paymentToken'];
        $secret_key=Config::get('values.checkout_primary_key');
		$ServiceURL=Config::get('values.checkout_token_url');
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$post_fields = '{"type":"applepay","token_data":'.$paymentToken.'}';
		//$dataa = array('country'=>'AE','currency'=>'AED','phone'=>$phone,'order_value'=>$ordervalue);
		$orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
		$result=json_decode($orderCreateResponse,true);
		if($result != null) {
			$data=array();
			$data['source']= array("type" => "token","token" => $result['token']);
			$data['customer']= array("email" => $request['ap_billing_email'],"name" => $request['ap_billing_name']);
			$data['3ds']= array("enabled" => true);
			$data['amount']= (int)($request['ap_amount'] * 100);
			$data['currency']= 'AED';
			$data['processing_channel_id']= Config::get('values.checkout_channel_id');
			$data['reference']= $request['ap_order_id'];
			$data['success_url']= Config::get('values.checkout_success');
            $data['failure_url']= Config::get('values.checkout_fail');
            $urlVal = 'apple-payment-failed';
			
			$post_datas=json_encode($data);
			$secret_key_new=Config::get('values.checkout_secret_key');
			$ServiceURL_new=Config::get('values.checkout_service_url');
			$Headers_new  = array("Authorization: Bearer ".$secret_key_new, "Content-Type: application/json", "Accept: application/json");
			$orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
			$result_new=json_decode($orderCreateResponse_new,true);
			// echo '<pre>';
			// print_r($result_new);
			if(!isset($result_new['error_type'])) {
				$status=$result_new['approved'];
				if ($status == true) { 
					//$redirectUrl = $result_new['_links']['redirect']['href'];
					//return redirect($redirectUrl);
					return response()->json(
						[
							'status' => 'success',
							'id' =>$result_new['id'],
							'refId' =>$refId,
						],
						200
					);
				} else {
					return Redirect::to($urlVal.'/'.$refId);
				}
			} else{
				return Redirect::to($urlVal.'/'.$refId);
			}
		} else {
			return redirect('house-cleaning-dubai');
		}
    }
	
	public function applePaymentSuccess($ref,$id)
    {
		$client = new \GuzzleHttp\Client([
			'verify' => false
		]);
		$response = $client->request('post', Config::get('api.api_url').'payment-success', [
			'headers' => [
				'cache-control' => 'no-cache',
				'Content-Type' => 'application/x-www-form-urlencoded'
			],
			'form_params' => [
				'ref_no' => $ref,
				'trackId' => $id,
			],
		]);
		$responseBody = json_decode((string) $response->getBody(), true);
		$responseStatus = $responseBody['status'];

		if($responseStatus == "success") {
			try {
				$customer = $responseBody['data']['customer'];
				$customerAddress =  $responseBody['data']['customer_address'];
				$bookings =  $responseBody['data']['bookings'];
				$areaName = $responseBody['data']['areaName'];
				$services = $responseBody['data']['services'];
				$online = $responseBody['data']['online'];
				Mail::send(new SuccessMail($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
				Mail::send(new SuccessMailAdmin($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
				return view('common.success',['data'=>$responseBody['data']]);
			} catch (\Exception $e) {
				return view('common.success',['data'=>$responseBody['data']]);
			}
		} else if($responseStatus == 'refresh_success') {
			return redirect('/');
		} else {
			return response()->json(
				[
					'status' => 'failed',
					'messages' => $responseBody['message'],
				]
			);
		}
    }
	
	public function applePaymentFailed($ref)
    {
        $referenceNo = $ref;
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'ref_no' => $referenceNo, // hard coded service id for cleaning
                'trackId' => 0,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            return view('common.applepayfail',['data'=>$responseBody['data']]);
        } else {
            return view('NotFound.not_found');
        }
    }
	
	public function appleInvoicePaymentFailed($ref)
    {
        $referenceNo = $ref;
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'invoice-payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'payId' => $referenceNo, // hard coded service id for cleaning
                'trackId' => 0,
				'payment_type' => 'applepay',
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            return view('common.applepayfail',['data'=>$responseBody['data']]);
        } else {
            return view('NotFound.not_found');
        }
    }
	
	public function appleInvoiceProcessPay(Request $request)
    {
		$refId = $request['ap_order_id'];
		//$amount = $request['amount'];
		$paymentToken = $request['paymentToken'];
        $secret_key=Config::get('values.checkout_primary_key');
		$ServiceURL=Config::get('values.checkout_token_url');
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$post_fields = '{"type":"applepay","token_data":'.$paymentToken.'}';
		//$dataa = array('country'=>'AE','currency'=>'AED','phone'=>$phone,'order_value'=>$ordervalue);
		$orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
		$result=json_decode($orderCreateResponse,true);
		if($result != null) {
			$data=array();
			$data['source']= array("type" => "token","token" => $result['token']);
			$data['customer']= array("email" => $request['ap_billing_email'],"name" => $request['ap_billing_name']);
			$data['3ds']= array("enabled" => true);
			$data['amount']= (int)($request['ap_amount'] * 100);
			$data['currency']= 'AED';
			$data['processing_channel_id']= Config::get('values.checkout_channel_id');
			$data['reference']= $request['ap_order_id'];
			$data['success_url']= Config::get('values.checkout_success');
            $data['failure_url']= Config::get('values.checkout_fail');
            $urlVal = 'apple-invoice-payment-failed';
			
			$post_datas=json_encode($data);
			$secret_key_new=Config::get('values.checkout_secret_key');
			$ServiceURL_new=Config::get('values.checkout_service_url');
			$Headers_new  = array("Authorization: Bearer ".$secret_key_new, "Content-Type: application/json", "Accept: application/json");
			$orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
			$result_new=json_decode($orderCreateResponse_new,true);
			// echo '<pre>';
			// print_r($result_new);
			if(!isset($result_new['error_type'])) {
				$status=$result_new['approved'];
				if ($status == true) { 
					//$redirectUrl = $result_new['_links']['redirect']['href'];
					//return redirect($redirectUrl);
					return response()->json(
						[
							'status' => 'success',
							'id' =>$result_new['id'],
							'refId' =>$refId,
						],
						200
					);
				} else {
					return Redirect::to($urlVal.'/'.$refId);
				}
			} else{
				return Redirect::to($urlVal.'/'.$refId);
			}
		} else {
			return redirect('house-cleaning-dubai');
		}
    }
	
    public static function curlFunget($url,$Headers)
    {
        $ch = curl_init();
        $certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if($response === FALSE){
            die(curl_error($ch));
        }
        curl_close($ch);

        return $response;
    }
	
	public static function curlFungetData($url,$Headers,$postdatas)
    {
        $ch = curl_init();
        //$certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postdatas);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        echo $response = curl_exec($ch);
        if($response === FALSE){
            //die(curl_error($ch));
			echo 'no';
			echo 'Request Error:' . curl_error($ch);
        }
        curl_close($ch);
		exit();
        return $response;
    }
	
	public function testcall()
	{
		echo Config::get('values.checkout_success');
		exit();
		
		
		
		$postdatas = '{"type":"googlepay","token_data":{"signature":"MEUCIQCQNiUeWmH6CzV8Huk3sfXck3M80tHpmaiahHkinz1Z0wIgcnl7v8bAJQQTVlD/p0CbwjiqVZED8DTv8Cg9137xksw\u003d","protocolVersion":"ECv1","signedMessage":"{\"encryptedMessage\":\"yXVWg82+eNvWrEVKSY5NJVGhWH90w1dRElVZswK0waG5dxvRotEySyOF1q9hnwShjR1KhV+B94x/hMPBp5PyEVNfBQ6mv+Lb/ZiIRhmDafZ3Or2GHhFzcAUuvV2u8p+oKX03UDZCYsP5dtM+e5YlmgC6Z9aRPeC+qlXl9FgINaRnnXvclh/BXMJ7bEfKPoig/nSxNBHjE0A6xmuP/+iCf5Bn5SkacAAaVWXeevQ3zp/1rqxwJZon0NHIRb9DwTN1aBOI4PurqfjXSgZ4Lz7/uJsSK6CgHc5yh95a3nkynie6JyUhFiVr48vmzIkAfNDIvJSmJ+0XCQ91tsXgJDEAES2OVKVRf3wIdoUoch2t45XZ5CXzuE76Bu8xyBP89ZNItS+3E6DSO969zvL5spWn/dmILP/5Pun+FsoVFYZEDfGcLrpIsiIFZjwrysdhs8SQ6qEqJbnsSQJJKcxiqxBlaiFvCqO6ixGGsCieDTfp\",\"ephemeralPublicKey\":\"BDQJBsryo2cFybwt9MA7ZUP/VZgnUIi7TVlPFMmA2AQzL8SyFDP0RdKP5aHsY2rOaFVAM7TAsO63ksRqiK3zoww\\u003d\",\"tag\":\"g+8svM5Uf3Mkwh/ggBmWLLxEM7aDAGxi4hBdByrJR2Y\\u003d\"}"}}';
		// $secret_key = "pk_test_7b3235b1-ad25-4061-85df-09e851fe87b4";
		$secret_key="pk_sbox_rmpjrc4nra5yliwneajdk7s4vyt";
		$url="https://api.sandbox.checkout.com/tokens";
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		// $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
		
		$ch = curl_init();
        $certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postdatas);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        echo $response = curl_exec($ch);
		print_r($response);
        // if($response === FALSE){
            // die(curl_error($ch));
        // }
		if(curl_errno($ch)){
			echo 'no';
			echo 'Request Error:' . curl_error($ch);
		}
		echo 'hai';
        curl_close($ch);
		exit();
        return $response;
	}
}
