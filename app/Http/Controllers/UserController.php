<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Config;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\RequestException;
use Redirect;
use \Crypt;


class UserController extends Controller
{
    public function updateCustomerAvatar(Request $request)
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('POST', Config::get('api.api_url') . 'update-customer-avatar', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'customerId' => session('customerId'),
                'customer_image_base64' => $request->input('customer_image_base64'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['status'] == 'success') {
            session(['customer_photo_url' => $responseBody['data']['customer_photo_url']]);
        }
        return response()->json($responseBody);
    }
    public function getUserData()
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-user-data', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' =>session('customerId')
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['status'] == 'success') {
            return view('User.personal',['data'=>$responseBody['data']]);
        }
    }
    public function editUserData(Request $request)
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'edit-user-data', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' =>$request->input('customerId'),
                'phone'=>$request->input('phone'),
                'email'=>$request->input('email'),
                'fullName'=>$request->input('fullName'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['status'] == 'success') {
            session(['fullName' => $request->input('fullName'),'phone'=>$request->input('phone')]);
			if(isset($_GET['mobile']))
			{
				return redirect('my-accounts?mobile=active&user_id='.$_GET['user_id'].'&token='.$_GET['token'])->with('message',$responseBody['messages']);
			} else {
				return redirect('my-accounts')->with('message',$responseBody['messages']);
			}
        } else {
            	return redirect('my-accounts')->with('message',$responseBody['messages']);
        }
    }
    public function myAccounts(Request $request)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /********************************************************** */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('GET', Config::get('api.api_url') . 'my-account-data', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'query' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        /********************************************************** */
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1'],'customer_photo_url'=>$responseBody['data']['customer']['customer_photo_url']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
            return isWebView() ? Redirect::to('/') : Redirect::to('/');
        }
        /********************************************************** */
        return view('User.accounts',['customerId'=>session('customerId')]);
    }
    public function manageAddress()
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-user-address', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' =>session('customerId')
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['status'] == 'success') {
            return view('User.address_list',['data'=>$responseBody['data'],'customerId'=>session('customerId')]);
        }
    }
    public function editAddressData($addressId='')
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        try {
            // $addressId = Crypt::decrypt($addressId);
            $client = new \GuzzleHttp\Client([
                'verify' => false
            ]);
            $response = $client->request('POST', Config::get('api.api_url').'edit-address-data', [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'addressId' =>$addressId,
                ],
            ]);
            $responseBody = json_decode((string) $response->getBody(), true);
            if($responseBody['status'] == 'success') {
                if($responseBody['data']['customerId'] == session('customerId')) {
                    return view('User.address_edit',['data'=>$responseBody['data']]);
                } else {
                    return view('NotFound.not_found');
                }
            } else {
                return view('NotFound.not_found');
            }
        }
        catch (RequestException $ex) {
            abort(500);
        }
    }
    public function saveAddressData(Request $request)
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'save-address-data', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'address_id' =>$request->input('address_id'),
                'area' =>$request->input('area'),
                'address' =>$request->input('address'),
                'customerId' =>$request->input('customerId'),
                'address_type' =>$request->input('address_opt'), 
                'selected_address' =>$request->input('selected_address'),
                'latitude' =>$request->input('latitude'),
                'longitude' =>$request->input('longitude'),
            ],
        ]);

        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        if($responseBody['status'] == 'success') {
            return redirect('manage-address')->with('message',$responseBody['messages']);
        }
    }
    public function addAddressData()
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'add-address-data', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' =>session('customerId'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['status'] == 'success') {
            return view('User.address_edit',['data'=>$responseBody['data']]);
        }
    }
    public function deleteCustomerAddress(Request $request)
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'delete-address', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'addressId' =>$request->input('customerAddresId'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['status'] == 'success') {
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Address deleted successfully.',
                ],
                200
            );
        }
    }
    public function setDefault(Request $request)
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'set-default', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'addressId' =>$request->input('address_opt'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if($responseBody['status'] == 'success') {
            return redirect('manage-address')->with('message',$responseBody['messages']);
        } 
    }
    public function updateUserPassword(Request $request)
    {
        // $this->userSessioncheck();
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'update-password', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'old_password' =>$request->input('old_password'),
                'new_password'=>$request->input('new_password'),
                'confirm_password'=>$request->input('confirm_password'),
                'customerId'=>$request->input('customerId'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        return response()->json(
            [
                'status' => $responseBody['status'],
                'messages' => $responseBody['messages'],
            ],
            200
        );
    }
    public function viewBookings()
    {
        // $this->userSessioncheck();
        if (isset($_GET['user_id'])) {
            session(['customerId' => $_GET['user_id']]);
        }
        if((session('customerId') =='')) {
            Session::flush();
            return Redirect::to('house-cleaning-dubai');
        }
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'view-bookings', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' =>session('customerId'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        if($responseBody['status'] == 'success') {
            return view('Bookings.booking',['data'=>$responseBody['data'],'customerId'=>session('customerId')]);
        } 
    }
    public function logOut()
    {
        Session::flush(); 
        return response()->json(
            [
                'status' => 'success',
                'messages' =>'Logged out successfully.',
            ],
            200, array(), JSON_PRETTY_PRINT
        );
    }
}
