<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use DateTime;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword;
use App\Mail\SuccessMailAdmin;
use App\Mail\SuccessMail;
use App\Mail\BookingConfirmationToCustomerMail;
use App\Mail\BookingConfirmationToAdminMail;
use DateInterval;
use App\Mail\RegistrationSuccessMail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class ServiceController extends Controller
{
	public function termsAndConditions()
	{
		return Redirect::to('https://dubaihousekeeping.com/terms-conditions');
	}
	
    public function getServices(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('POST', Config::get('api.api_url') . 'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'service_type_id' => 1,
                'customerId' => @$customerId,
                'token' => @$token,
            ],

        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        $data = $responseBody['data'];
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours' => 2, 'numberMaids' => 1, 'cleaningStatus' => 'N', 'extraServices' => 0, 'customerId' => '', 'bookedDate' => '', 'monthDuration' => '', 'price_per_hr' => $responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
        $service_charge = $amountResponse['serv_rate'];
        $net_cleaning_fee = $amountResponse['cleaningrate'];
        $net_vat_charge = $amountResponse['vat_charge'];
        $net_service_charge = $amountResponse['gross_amount'];
        $summaryArray = ['service_charge' => $service_charge, 'net_cleaning_fee' => $net_cleaning_fee, 'net_vat_charge' => $net_vat_charge, 'net_service_charge' => $net_service_charge];
        if ($responseStatus == 'success') {
            if (!empty($responseBody['data']['address'])) {
                $addressName = $responseBody['data']['address']['addressName'] ?: "";
                $areaName = $responseBody['data']['address']['AreaName'] ?: "";
            } else {
                $addressName = '';
                $areaName = '';
            }
            session(['addressName' => $addressName, 'areaName' => $areaName]);
            return view('HouseCleaning.home', ['data' => $data, 'addressName' => $addressName, 'areaName' => $areaName, 'AllServices' => $AllServices, 'summaryArray' => $summaryArray, 'area' => $area, 'couponCode' => $couponCode]);
        }
    }
    public function getDeepCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 53,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('DeepCleaningNew.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getSteamCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 56,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('SteamCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getSofaCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 57,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('SofaCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getMattressCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 58,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('MattressCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getCarpetCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 59,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('CarpetCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getFloorScrubbing(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 60,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('FloorScrubbing.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getSanitization(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 61,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('Sanitization.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getDeclutterAndOrganize(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 62,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('DeclutterOrganize.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getMoldCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 63,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('MoldCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getHolidayHomeCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 64,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('HolidayHomeCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getMoveOutCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 54,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('MoveOutCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getMoveInCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 55,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('MoveInCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getIroning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 5,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('Ironing.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getBabySitting(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 52,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('BabySitting.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getOrganicCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 47,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
        //dd($amountResponse);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('OrganicCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    public function getOfficeCleaning(Request $request,$couponCode = null)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 4,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $data = $responseBody['data'];
        //dd($data);
        $responseStatus = $responseBody['status'];
        $AllServices = '';
        $area = $responseBody['data']['area'];
        $request = new \Illuminate\Http\Request();
        $requestArray = $request->replace(array('numberHours'=> 2,'numberMaids'=>1,'cleaningStatus'=>'N','extraServices'=>0,'customerId'=>'','bookedDate'=>'','monthDuration'=>'','price_per_hr'=>$responseBody['data']['service']['service_rate']));
        $amountResponse = ServiceController::calculateAmount($requestArray);
        //dd($amountResponse);
       
            $service_charge = $amountResponse['serv_rate'];
            $net_cleaning_fee = $amountResponse['cleaningrate'];
            $net_vat_charge = $amountResponse['vat_charge'];
            $net_service_charge = $amountResponse['gross_amount'];
            $summaryArray = ['service_charge'=>$service_charge,'net_cleaning_fee'=>$net_cleaning_fee,'net_vat_charge'=>$net_vat_charge,'net_service_charge'=>$net_service_charge];
        if ($responseStatus == 'success' ) {
                    if (!empty($responseBody['data']['address'])) {
                        $addressName = $responseBody['data']['address']['addressName']?:"";
                        $areaName = $responseBody['data']['address']['AreaName']?:"";
                    } else {
                        $addressName = '';
                        $areaName = '';
                    }
                    session(['addressName' => $addressName,'areaName'=>$areaName]);
           
            return view('OfficeCleaning.home',['data'=>$data,'addressName'=>$addressName,'areaName'=>$areaName,'AllServices'=>$AllServices,'summaryArray'=>$summaryArray,'area'=>$area,'couponCode'=>$couponCode]);
        }
    }
    
    /**
     * Function to call api for save step 1
     * Author : Karthika
     * Date : 27/08/2020
     */
    public function saveStep1(Request $request)
    {
        // dd($request->input('extraServices'));
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);

        $response = $client->request('POST', Config::get('api.api_url').'save-booking-one', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'serviceId' => 1, // hard coded service id for cleaning
                'noOfHours' => $request->input('noOfHours'),
                'cleaningMaterialStatus' => $request->input('cleaningMaterialStatus'),
                'noOfMaids' => $request->input('noOfMaids'),
                'extraServices' => $request->input('extraServices'),
                'cleaning_material_fee' => 100,
                'bookingId' => $request->input('bookingId')
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData = $responseBody['data']['id'];
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking time details saved successfully.',
                'data' => [
                    'id' =>  $responseData 
                ]
            ],
            200
        );
        // if ($responseStatus == 'success' ) {
        //     return view('common.step2',['responseData'=>$responseData]);
        // }
    }
    /**
     * function to show second step
     * Author:Karthika
     * Date:28/08/2020
     */
    public function cleaningSecond($bookingId)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-bookingtwo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData = $responseBody['data'];
        // dd($responseData);
        // $responseDataBookId = $responseBody['data']['bookingId'];
       
        return view('common.step2',['bookingId'=>$bookingId,'responseData'=>$responseData]);
    }
    /**
     * function to show third step
     * Author:Karthika
     * Date:02/09/2020
     */
    public function cleaningThird($bookingId)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-bookingtwo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData = $responseBody['data'];
        return view('common.step3',['bookingId'=>$bookingId,'responseData'=>$responseData]);
    }

     /**
     * Function to call api for save step 1
     * Author : Karthika
     * Date : 27/08/2020
     */
    public function saveStep2(Request $request)
    {
        // dd($request->input('extraServices'));
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);

        $response = $client->request('POST', Config::get('api.api_url').'save-booking-two', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'how_often' => $request->input('how_often'), // hard coded service id for cleaning
                'cleaning_date' => $request->input('cleaning_date'),
                'timeSelected' => $request->input('timeSelected'),
                'monthDurations' => $request->input('monthDurations'),
                'bookingId' => $request->input('bookingId'), 
                'hour_rate' => $request->input('hour_rate'), 
                'discount' => $request->input('discount'), 
                'service_charge' => $request->input('service_charge'), 
                'vat_charge' => $request->input('vat_charge'), 
                'total_amount' => $request->input('total_amount'), 
                'net_cleaning_fee' => $request->input('net_cleaning_fee'), 
                'net_service_charge' => $request->input('net_service_charge'), 
                'net_discount' => $request->input('net_discount'), 
                'net_vat_charge' => $request->input('net_vat_charge'), 
                'total_net_amount' => $request->input('total_net_amount'), 
                'cleaning_material_fee' => $request->input('cleaning_material_fee'), 
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData = $responseBody['data']['id'];
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking time details saved successfully.',
                'data' => [
                    'id' =>  $responseData 
                ]
            ],
            200
        );
        // if ($responseStatus == 'success' ) {
        //     return view('common.step2',['responseData'=>$responseData]);
        // }
    }
    /**
     * function to save a user
     * Author:Karthika
     * Date:02/09/2020
     */
    public function loginUser(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'sign-in', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'emailId' => $request->input('emailUser'),
                'password' => $request->input('passwordUser'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseMessage = $responseBody['messages'];
        if($responseStatus == 'success') {
            $responseDataCustomer = $responseBody['data']['id'];
            $responseDataAddress = $responseBody['data']['address'];
            session(['customerId' => $responseDataCustomer,'token' => $responseBody['data']['token'],'fullName'=>$responseBody['data']['name'],'phone'=>$responseBody['data']['phone'],'customer_photo_url'=>$responseBody['data']['customer_photo_url']]);

            return response()->json(
            $responseBody,
            200
            );
        } else {
            return response()->json(
                [
            'status' => $responseStatus,
                        'data' => $responseBody['data'],
            'messages' =>$responseMessage ,
                      ]);
        }
    }
    /**
     * function to save customer address
     * Author:Karthika
     * Date:02/0/20202
     */
    public function cleaningFourth($bookingId)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-address', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData2 = $responseBody['data'];
        $responseArea = $responseBody['area'];
        // dd($responseArea);

        $client1 = new \GuzzleHttp\Client();
        $response1 = $client1->request('POST', Config::get('api.api_url').'get-bookingtwo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody1 = json_decode((string) $response1->getBody(), true);
        // dd($responseBody);
        $responseStatus1 = $responseBody1['status'];
        $responseData = $responseBody1['data'];


        // dd($responseData);
        // $responseDataBookId = $responseBody['data']['bookingId'];
       
        return view('common.step4',['bookingId'=>$bookingId,'responseData'=>$responseData,'responseData2'=>$responseData2,'responseArea'=>$responseArea]);
        // return view('common.step4',['bookingId'=>$bookingId]);
    }
    /**
     * function to register new user
     * Author:Karthika
     * Date:02/09/2020
     */
    public function registerUser(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $fullName = $request->input('firstName').' '. $request->input('lastName');

        $response = $client->request('POST', Config::get('api.api_url').'sign-up', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'bookingId' => $request->input('bookingId'), 
                'firstName' => $request->input('firstName'), 
                'lastName' => $request->input('lastName'), 
                'phoneNumber' => $request->input('phoneNumber'), 
                'countryCode' =>$request->input('countryCode'),
                //'countryCode' =>'+971',
                'fullName'=>$fullName,

            ],
        ]);
        $fullName = $request->input('firstName').' '. $request->input('lastName');
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseDataCustomer = $responseBody['data'];
        $responseMeassage = $responseBody['messages'];
        if($responseStatus == 'success') {
            //session(['customerId' => $responseDataCustomer['id']]);
            session(['fullName'=>$fullName,'phone'=>$request->input('phoneNumber')]);
            try {
                //Mail::send(new RegistrationSuccessMail($request->input('email'),$fullName,$request->input('password')));
                        return response()->json(
                    [
                        'status' => $responseStatus,
                        'messages' => $responseMeassage,
                        'data' =>$responseDataCustomer
                    ],
                    200
                ); 
            } catch (\Exception $e) {
               return response()->json(
                [
                    'status' => $responseStatus,
                    'messages' => $responseMeassage,
                    'data' =>$responseDataCustomer
                ],
                200
            ); 
            }
            
        }
        return response()->json(
            [
                'status' => $responseStatus,
                'messages' => $responseMeassage,
                'data' =>$responseDataCustomer
            ],
            200
        ); 
    }
    /**
     * function to save address details
     * Author:Karthika
     * Date:03/09/2020
     */
    public function saveAddress(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);

        $response = $client->request('POST', Config::get('api.api_url').'save-address', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'area' => $request->input('area'),
                'address' => $request->input('address'),
                'address_type' => $request->input('address_opt'), 
                'customerId' =>$request->input('customerId'),
                'latitude' =>$request->input('latitude'),
                'longitude' =>$request->input('longitude'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseDataCustomer = $responseBody['data']['id'];
        $responseDataAddress = $responseBody['data']['address'];
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Customer address saved successfully.',
                'data' => [
                    'customerId' =>  $responseDataCustomer ,
                    'address' =>  $responseDataAddress ,
                ]
            ],
            200
        ); 
    }
    /**
     * function to show fifth step
     * Author:Karthika
     * Date:03/09/2020
     */
    public function cleaningFifth($bookingId)
    {
        $client1 = new \GuzzleHttp\Client();
        $response1 = $client1->request('POST', Config::get('api.api_url').'get-bookingtwo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody1 = json_decode((string) $response1->getBody(), true);
        // dd($responseBody);
        $responseStatus1 = $responseBody1['status'];
        $responseData = $responseBody1['data'];

        return view('common.step5',['bookingId'=>$bookingId,'responseData'=>$responseData]);
    }
    /**
     * function to show previous fourth step
     * Author:Karthika
     * Date:03/09/2020
     */
    public function goFour($bookingId,Request $request)
    {   
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-address', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData2 = $responseBody['data'];
        $responseArea = $responseBody['area'];
        // dd($responseArea);

        $client1 = new \GuzzleHttp\Client();
        $response1 = $client1->request('POST', Config::get('api.api_url').'get-bookingtwo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody1 = json_decode((string) $response1->getBody(), true);
        // dd($responseBody);
        $responseStatus1 = $responseBody1['status'];
        $responseData = $responseBody1['data'];


        // dd($responseData);
        // $responseDataBookId = $responseBody['data']['bookingId'];
       
        return view('common.step4',['bookingId'=>$bookingId,'responseData'=>$responseData,'responseData2'=>$responseData2,'responseArea'=>$responseArea]);
       
    }
    /**
     * function to show thrid step
     * Author : Karthika
     * Date : 03/09/2020
     */
    public function goThree($bookingId)
    {  
        return view('common.step3',['bookingId'=>$bookingId]);
    }
    /**
     * function to go step 2
     * Author:Karthika
     * Date:03/09/2020
     */
    public function goTwo($bookingId) {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-bookingtwo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData = $responseBody['data'];
        // dd($responseData);
        // $responseDataBookId = $responseBody['data']['bookingId'];
       
        return view('common.step2',['bookingId'=>$bookingId,'responseData'=>$responseData]);
    }
    /**
     * function to go step 1
     * Author:Karthika
     * Date:03/09/2020
     */
    public function goOne($bookingId) {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-booking-one', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId' =>$bookingId
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        $responseData = $responseBody['data'];
        // dd($responseData);
        // $responseDataBookId = $responseBody['data']['bookingId'];
       
        return view('home',['bookingId'=>$bookingId,'responseData'=>$responseData]);
    }
    /**
     * forgot password api
     * Author:Karthika
     * Date:04/09/2020
     */
    public function forgotPassword(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'forgot-password', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'email' =>$request->input('emailId')
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            try{
               Mail::send(new ForgotPassword($request->input('emailId'),$responseBody['data']['password'],$responseBody['data']['name']));
                return response()->json(
                    [
                        'status' => 'success',
                        'messages' => 'Mail send successfully.',
                    ],
                    200
                );
            } catch (\Exception $e) {
                // dd($e);die;
                return response()->json(
                    [
                        'status' => 'failed',
                        'messages' => 'Mail send failed..',
                        'execption' => $e,
                    ],
                    200
                );   
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Mail send failed...',
                ],
                200
            );
        }
        
    }

    /**
     * verify mobile number
     * Author:Karthika
     * Date:04/09/2020
     */
    public function verifyMobile(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'check-otp', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'otp1' =>$request->input('first'),
                'otp2' =>$request->input('second'),
                'otp3' =>$request->input('third'),
                'otp4' =>$request->input('fourth'),
                // 'bookingId' =>$request->input('booking_id'),
                'customerId'=>$request->input('customerId'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            $responseData = $responseBody['data'];
            try {
                Mail::send(new RegistrationSuccessMail($responseData['emailId'],$responseData['name'],$responseData['password']));
            } catch (\Exception $e) {
            }
            //dd($responseData);
            session(['customerId' => $responseData['id']]);
            session(['fullName'=> $responseData['name'],'phone'=>$responseData['phone']]);
            return response()->json(
                [
                    'status' => 'success',
                    'data'=>$responseData,
                    'messages' => $responseBody['messages'],
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }
    }
    /**
     * function to resend otp
     * Author:Karthika
     * Date:04/09/2020
     */
    public function resendOTP(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'resend-otp', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId'=>$request->input('customerId'),
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => $responseBody['messages'],
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }
    }
    /**
     * function to save payment details
     * Author:Karthika
     * Date:04/09/2020
     */
    public function makePayment(Request $request)
    {
        try {
        // dd($request->input('payment_mode'));
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $form_params = [
                'is_device'=> isWebView() ? 'mobile' : 'web',
                'customerId'=>$request->input('customerId'),
                'payment_mode' =>$request->input('payment_mode'),
                'instructions'=>$request->input('instructions'),
                'crew_in'=>$request->input('crew_in'),
                'serviceId' => $request->input('serviceId'), // hard coded service id for cleaning
                'noOfHours' => $request->input('noOfHours'),
                'cleaningMaterialStatus' => $request->input('cleaningMaterialStatus'),
                'noOfMaids' => $request->input('noOfMaids'),
                'how_often' => $request->input('visitType'), 
                'cleaning_date' => $request->input('cleaning_date'),
                'toTime' => $request->input('toTime'),
                'fromTime' => $request->input('fromTime'),
                'monthDurations' => $request->input('monthDurationId'),
                'hour_rate' => $request->input('hour_rate'), 
                'coupon_code' => $request->input('coupon_code'), 
                'service_charge' => $request->input('service_charge'), 
                'vat_charge' => $request->input('vat_charge'), 
                'total_amount' => $request->input('total_amount'), 
                // 'net_cleaning_fee' => $request->input('net_cleaning_fee'), 
                // 'net_service_charge' => $request->input('net_service_charge'), 
                // 'net_discount' => $request->input('net_discount'), 
                // 'net_vat_charge' => $request->input('net_vat_charge'), 
                // 'total_net_amount' => $request->input('total_net_amount'), 
                'cleaning_material_fee' => $request->input('cleaning_material_fee'), 
                'discount_price'=>$request->input('discount_price'),
                'interior'=>$request->input('interior'),
                'ironing'=>$request->input('ironing'),
                'fridge'=>$request->input('fridge'),
                'oven'=>$request->input('oven'),
                // 'category_id'=>$request->input('category_id'),
                // 'sub_category_id'=>$request->input('sub_category_id'),
                // 'service_sub_funish_id'=>$request->input('service_sub_funish_id'),
                // 'is_scrubbing'=>$request->input('is_scrubbing'),
                // 'name'=>$request->input('name'),
                // 'service_cost'=>$request->input('service_cost'),
                'officesqure'=>$request->input('officesqure'), 
                'cate_first'=>$request->input('cate_first'), 
                'cate_second'=>$request->input('cate_second'), 
                'cate_third'=>$request->input('cate_third'), 
                'cate_fourth'=>$request->input('cate_fourth'), 
                'cate_fifth'=>$request->input('cate_fifth'), 
                'cate_sixth'=>$request->input('cate_sixth'),
                /*************************************************** */
                'custom_supplies' => $request->input('custom_supplies') ?: [],
                'custom_supplies_rate' => $request->input('custom_supplies_rate') ?: [],
                /*************************************************** */
                'plan_based_supply' => @$request->input('plan_based_supply'),
                'plan_based_supply_hourly_rate' => @$request->input('plan_based_supply_hourly_rate'),
                'plan_based_supply_total_amount' => @$request->input('plan_based_supply_total_amount'),
                /*************************************************** */
                // extra services
                'extra_services' => $request->input('extra_services') ?: [],
                'extra_services_cost' => $request->input('extra_services_cost') ?: [],
                'extra_services_duration' => $request->input('extra_services_duration') ?: [],
                /*************************************************** */
                'supervisor_status' => $request->input('supervisor_status'),
                'supervisor_charge_hourly' => $request->input('supervisor_charge_hourly'),
                'supervisor_charge' => $request->input('supervisor_charge'),
                /*************************************************** */
                '_service_category' => $request->input('_service_category'),
                '_service_category_rate' => $request->input('_service_category_rate'),
                '_service_category_quantity' => $request->input('_service_category_quantity'),
                /*************************************************** */
                '_category' => $request->input('_category'),
                '_sqft' => $request->input('_sqft'),
                '_sqft_rate' => $request->input('_sqft_rate'),
                /*************************************************** */
                '_property_details' => $request->input('property_details') ?: null,
                /*************************************************** */
                '_service_week_days' => $request->input('service_week_days') ?: null,
                /*************************************************** */
				'_noOfBedrooms' => $request->input('noOfBedrooms') ?: null,
				'_noOfBathrooms' => $request->input('noOfBathrooms') ?: null,
				'_noOfTerraces' => $request->input('noOfTerraces') ?: null,
        ];
        $response = $client->request('POST', Config::get('api.api_url').'make-payment', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => $form_params, 
        ]);
        //dd($response);
        $responseBody = json_decode((string) $response->getBody(), true);
        
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
			if($responseBody['data']['payment_mode'] == "tamara")
			{
				$mobb = substr($responseBody['data']['customer']['mobile_number_1'], -9);
				$description = "Payment against service dated ".$request->input('cleaning_date');
				
				$postdata["order_reference_id"] = $responseBody['data']['bookings']['reference_id'];
				$postdata["total_amount"]["amount"] = $request->input('total_amount');
				$postdata["total_amount"]["currency"] = "AED";
				$postdata["description"] = $description;
				$postdata["country_code"] = "AE";
				$postdata["payment_type"] = "PAY_BY_INSTALMENTS";
				
				$pages_array[0]['reference_id'] = $request->input('serviceId');
				$pages_array[0]['type'] = "service";
				$pages_array[0]['name'] = $request->input('serviceNamePay');
				$pages_array[0]['sku'] = $request->input('serviceId');
				$pages_array[0]['quantity'] = 1;
				$amount_detail = array(
					"amount" => $request->input('total_amount'),
					"currency" => "AED",
				);
				$pages_array[0]['total_amount'] = $amount_detail;
				
				$postdata["items"] = $pages_array;
				
				// $postdata["items"]["reference_id"] = $responseBody['data']['order_reference_id'];
				// $postdata["items"]["type"] = "service";
				// $postdata["items"]["name"] = $request->input('serviceNamePay');
				// $postdata["items"]["sku"] = $request->input('serviceId');
				// $postdata["items"]["quantity"] = 1;
				// $postdata["items"]["total_amount"]["amount"] = $request->input('total_amount');
				// $postdata["items"]["total_amount"]["currency"] = "AED";
				$name = explode(' ', $responseBody['data']['customer']['customer_name'], 2);
				$postdata["consumer"]["first_name"] = $name[0];
				$postdata["consumer"]["last_name"] = empty($name[1])? $name[0] : $name[1];
				$postdata["consumer"]["phone_number"] = $mobb;
				$postdata["consumer"]["email"] = $responseBody['data']['customer']['email_address'];
				$postdata["shipping_address"]["first_name"] = $name[0];
				$postdata["shipping_address"]["last_name"] = empty($name[1])? $name[0] : $name[1];
				$postdata["shipping_address"]["line1"] = $responseBody['data']['customer_address']['customer_address'];
				$postdata["shipping_address"]["city"] = session('areaName');
				$postdata["shipping_address"]["country_code"] = "AE";
				$postdata["tax_amount"]["amount"] = $request->input('vat_charge');
				$postdata["tax_amount"]["currency"] = "AED";
				$postdata["shipping_amount"]["amount"] = $request->input('total_amount');
				$postdata["shipping_amount"]["currency"] = "AED";
				$postdata["merchant_url"]["success"] = Config::get('values.tamara_success');
				$postdata["merchant_url"]["failure"] = Config::get('values.tamara_fail');
				$postdata["merchant_url"]["cancel"] = Config::get('values.tamara_fail');
				$postdata["merchant_url"]["notification"] = Config::get('values.tamara_hook');
				
				$secret_key=Config::get('values.tamara_secret_key');
				$ServiceURL=Config::get('values.tamara_checkout_url');
				$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
				//$orderCreateResponse = $this->curlFunget($ServiceURL, $Headers, $post_datas);
				$ordercheckoutResponse = $this->execFunc("POST", $ServiceURL, $postdata, $Headers);
				
				// $resultsss=json_decode($orderCreateResponse,true); 
				if($ordercheckoutResponse['data']->order_id != "")
				{
					$payresponse = $client->request('POST', Config::get('api.api_url').'update-tamara-payment', [
						'headers' => [
							'cache-control' => 'no-cache',
							'Content-Type' => 'application/x-www-form-urlencoded'
						],
						'form_params' => [
							'order_id'=>$ordercheckoutResponse['data']->order_id, 
							'checkout_id'=>$ordercheckoutResponse['data']->checkout_id,
							'reference_id'=>$responseBody['data']['bookings']['reference_id'],
						], 
					]);
					$payresponseBody = json_decode((string) $payresponse->getBody(), true);
					$payresponseStatus = $payresponseBody['status'];
					if($payresponseStatus == 'success')
					{
						return response()->json(
							[
								'status' => 'success',
								'data' =>$responseBody['data'],
								'url' =>$ordercheckoutResponse['data']->checkout_url,
								'messages' => $payresponseBody['messages'],
                                'form_params' => $form_params
							],
							200
						);
					} else {
						return response()->json(
							[
								'status' => 'failed',
								'messages' => $payresponseBody['messages'],
							]
					);
					}
				} else {
					return response()->json(
						[
							'status' => 'failed',
							'messages' => $responseBody['messages'],
						]
					);
				}
			} else {
				return response()->json(
					[
						'status' => 'success',
						'data' =>$responseBody['data'],
						'messages' => $responseBody['messages'],
                        'form_params' => $form_params
					],
					200
				);
			}
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }
    }
    catch (\Exception $e) {
    return response()->json(
						[
							'status' => 'failed',
							'messages' => $e->getMessage(),
						]
					);
}
    }
	
	public function makeTamaraPayment(Request $request)
    {
        $mobb = substr($request['phn'], -9);
		$phone = '971'.$mobb;
		$ordervalue = $request['amount'];
		$dataa = array('country'=>'AE','currency'=>'AED','phone'=>$phone,'order_value'=>$ordervalue);
		$secret_key=Config::get('values.tamara_secret_key');
		$ServiceURL=Config::get('values.tamara_payment_type_url');
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$orderCreateResponse = $this->execFunc("GET", $ServiceURL, $dataa, $Headers);
		// $resultsss=json_decode($orderCreateResponse,true); 
		if(!empty($orderCreateResponse['data']))
		{
			$min_amount = $orderCreateResponse['data'][0]->min_limit->amount;
			$max_amount = $orderCreateResponse['data'][0]->max_limit->amount;
			
			if($ordervalue >= $min_amount && $ordervalue <= $max_amount)
			{
				$payServiceURL=Config::get('values.tamara_precheck_url');
				$postdata["country"] = "AE";
				$postdata["order_value"]["amount"] = $ordervalue;
				$postdata["order_value"]["currency"] = "AED";
				$postdata["phone_number"] = $phone;
				$availabilitycheck = $this->execFunc("POST", $payServiceURL, $postdata, $Headers);
				
				if(!empty($availabilitycheck['data']))
				{
					if(!empty($availabilitycheck['data']->errors))
					{
						return response()->json(
							[
								'status' => 'failed',
								'messages' => 'Not eligible for payment.',
							]
						);
						exit();
					} else {
						if($availabilitycheck['data']->has_available_payment_options == true)
						{
							return response()->json(
								[
									'status' => 'success',
									'messages' => 'Eligible for payment',
								]
							);
							exit();
						} else {
							return response()->json(
								[
									'status' => 'failed',
									'messages' => 'Not eligible for payment.',
								]
							);
							exit();
						}
					}
				} else {
					return response()->json(
						[
							'status' => 'failed',
							'messages' => 'Not eligible for payment.',
						]
					);
					exit();
				}
			} else {
				return response()->json(
					[
						'status' => 'failed',
						'messages' => 'Not eligible for payment.',
					]
				);
				exit();
			}
		} else {
			return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Not eligible for payment.',
                ]
            );
			exit();
		}
    }
	
	public function makeRetryTamaraPayment(Request $request)
    {
        // dd($request->input('payment_mode'));
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
		
		$mobb = substr($request->input('tamaraCustMob'), -9);
		$description = "Payment against service dated ".$request->input('tamaraServiceDate');
		
		$postdata["order_reference_id"] = $request->input('tamaraRefId');
		$postdata["total_amount"]["amount"] = $request->input('tamaraTotalAmount');
		$postdata["total_amount"]["currency"] = "AED";
		$postdata["description"] = $description;
		$postdata["country_code"] = "AE";
		$postdata["payment_type"] = "PAY_BY_INSTALMENTS";
		
		$pages_array[0]['reference_id'] = $request->input('tamaraServiceId');
		$pages_array[0]['type'] = "service";
		$pages_array[0]['name'] = $request->input('tamaraServiceName');
		$pages_array[0]['sku'] = $request->input('tamaraServiceId');
		$pages_array[0]['quantity'] = 1;
		$amount_detail = array(
			"amount" => $request->input('tamaraTotalAmount'),
			"currency" => "AED",
		);
		$pages_array[0]['total_amount'] = $amount_detail;
		
		$postdata["items"] = $pages_array;
		
		// $postdata["items"]["reference_id"] = $responseBody['data']['order_reference_id'];
		// $postdata["items"]["type"] = "service";
		// $postdata["items"]["name"] = $request->input('serviceNamePay');
		// $postdata["items"]["sku"] = $request->input('serviceId');
		// $postdata["items"]["quantity"] = 1;
		// $postdata["items"]["total_amount"]["amount"] = $request->input('total_amount');
		// $postdata["items"]["total_amount"]["currency"] = "AED";
		$name = explode(' ', $request->input('tamaraCustName'), 2);
		$postdata["consumer"]["first_name"] = $name[0];
		$postdata["consumer"]["last_name"] = empty($name[1])? $name[0] : $name[1];
		$postdata["consumer"]["phone_number"] = $mobb;
		$postdata["consumer"]["email"] = $request->input('tamaraCustEmail');
		$postdata["shipping_address"]["first_name"] = $name[0];
		$postdata["shipping_address"]["last_name"] = empty($name[1])? $name[0] : $name[1];
		$postdata["shipping_address"]["line1"] = $request->input('tamaraCustAddress');
		$postdata["shipping_address"]["city"] = $request->input('tamaraAreaName');
		$postdata["shipping_address"]["country_code"] = "AE";
		$postdata["tax_amount"]["amount"] = $request->input('tamaraVatCharge');
		$postdata["tax_amount"]["currency"] = "AED";
		$postdata["shipping_amount"]["amount"] = $request->input('tamaraTotalAmount');
		$postdata["shipping_amount"]["currency"] = "AED";
		$postdata["merchant_url"]["success"] = Config::get('values.tamara_success');
		$postdata["merchant_url"]["failure"] = Config::get('values.tamara_fail');
		$postdata["merchant_url"]["cancel"] = Config::get('values.tamara_fail');
		$postdata["merchant_url"]["notification"] = Config::get('values.tamara_hook');
		
		$secret_key=Config::get('values.tamara_secret_key');
		$ServiceURL=Config::get('values.tamara_checkout_url');
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		//$orderCreateResponse = $this->curlFunget($ServiceURL, $Headers, $post_datas);
		$ordercheckoutResponse = $this->execFunc("POST", $ServiceURL, $postdata, $Headers);
		
		// $resultsss=json_decode($orderCreateResponse,true); 
		if($ordercheckoutResponse['data']->order_id != "")
		{
			$payresponse = $client->request('POST', Config::get('api.api_url').'update-tamara-payment', [
				'headers' => [
					'cache-control' => 'no-cache',
					'Content-Type' => 'application/x-www-form-urlencoded'
				],
				'form_params' => [
					'order_id'=>$ordercheckoutResponse['data']->order_id, 
					'checkout_id'=>$ordercheckoutResponse['data']->checkout_id,
					'reference_id'=>$request->input('tamaraRefId'),
				], 
			]);
			$payresponseBody = json_decode((string) $payresponse->getBody(), true);
			$payresponseStatus = $payresponseBody['status'];
			if($payresponseStatus == 'success')
			{
				return response()->json(
					[
						'status' => 'success',
						'url' =>$ordercheckoutResponse['data']->checkout_url,
						'messages' => $payresponseBody['messages'],
					],
					200
				);
			} else {
				return response()->json(
					[
						'status' => 'failed',
						'messages' => $payresponseBody['messages'],
					]
			);
			}
		} else {
			return response()->json(
				[
					'status' => 'failed',
					'messages' => 'Something went wrng. Try again.',
				]
			);
		}
    }
	
    /**
     * function to calculate price
     * Author:Karthika
     * Date:05/09/2020
     */
    public function calculatePrice(Request $request)
    {
        $no_hrs = $request['numberHours'];
        $no_maids = $request['numberMaids'];
        $cleaningStatus = $request['cleaningStatus'];
        // $extraServices = $request['extraServices'];
        // $coupon = ucfirst($request['coupon']);
        $customerId = $request['customerId'];
        $bookDate = $request['bookedDate'];
       // print_r($bookDate);
        $monthDuration = $request['monthDuration'];
        // dd($monthDuration);
        $service_types = $request['serviceId'];
        $visitType = $request['visitType'];
        $interior=$request['interior'];
        $ironing=$request['ironing'];
        $fridge=$request['fridge'];
        $oven=$request['oven'];
		$toolRate = $request['tool_rate'];
		$supplyRate = $request['supply_rate'];
		$supervisorVal = $request['supervisorVal'];
        
        if($bookDate != '') {           
            $weekday = date('w', strtotime($bookDate));
            $startDate = new DateTime($bookDate);
            if ($visitType != "OD") { 
                $dateIntervel = new DateTime($bookDate);
                   if($monthDuration == '') {
                        $monthDuration = 1;
                    } 
                    if($monthDuration == 'Continue') {
                        $actual_ending_date = $startDate;
                        $ending_date = $startDate;  
                    } else {
                        $dateIntervel->add(new DateInterval('P'.$monthDuration.'M'));
                        $actual_ending_date = $dateIntervel->format('Y-m-d'); 
                        $ending_date = $dateIntervel->format('Y-m-d');  // 2016-01-02
                        $endDate = new DateTime($ending_date);    
                        $resultDays = array('0' => 0, 
                            '1' => 0, 
                            '2' => 0, 
                            '3' => 0, 
                            '4' => 0, 
                            '5' => 0, 
                            '6' => 0); 
                            // iterate over start to end date 
                            while($startDate <= $endDate ){ 
                                //find the timestamp value of start date 
                                $timestamp = strtotime($startDate->format('d-m-Y')); 
                                // find out the day for timestamp and increase particular day 
                                $weekDay = date('w', $timestamp); 
                                $resultDays[$weekDay] = $resultDays[$weekDay] + 1; 
                                // increase startDate by 1 
                                $startDate->modify('+1 day'); 
                            } 
                            $number_of_weeks = $resultDays[$weekday];  
                    }
                              
            } else {
                $actual_ending_date = $startDate;
                $endDate = $startDate; 
            }
            // dd($startDate,$endDate);
            
            $no_visits =1;
            // if ($visitType == 'OD') {
            //     $no_visits = 1;
            // } elseif ($visitType == 'WE') {
            //     $no_visits = $number_of_weeks;
            // } elseif ($visitType == 'BW') {
            //     $no_visits = round($number_of_weeks/2);
            // }
        }

        $vat_percentage = Config::get('values.vat_amount');
        if ($customerId != "") 
        {
            $cust_id=$customerId;
        } else {
            $cust_id = "";
        }
		
		$perHour = 40;
		if($no_hrs == 2)
		{
			$totPrice = (110 * $no_maids);
		} else if($no_hrs == 3)
		{
			$totPrice = (140 * $no_maids);
		} else if($no_hrs == 4)
		{
			$totPrice = (160 * $no_maids);
		} else if($no_hrs == 5)
		{
			$totPrice = (200 * $no_maids);
		} else if($no_hrs == 6)
		{
			$totPrice = (240 * $no_maids);
		} else {
			$totPrice = (($no_hrs * $perHour) * $no_maids);
		}
		
		if($supervisorVal == "Y")
		{
			$supervisorrate = (60 * $no_hrs);
			$totPrice = ($totPrice + $supervisorrate);
		} else {
			$supervisorrate = 0;
		}
      
        if($cleaningStatus == "N")
        {
            $cleaning_material_rate = ($toolRate);
			$totPrice = ($totPrice + $cleaning_material_rate);
        } else {
			$cleaning_material_rate = (($supplyRate * $no_hrs) * $no_maids);
			$totPrice = ($totPrice + $cleaning_material_rate);
        }
		$vat_charge = ($totPrice * 5) / (100 + 5);
		$service_rate_with_material = ($totPrice - $vat_charge);
		$service_rate = ($service_rate_with_material - $cleaning_material_rate);
		
		$per_hour_rate = ($service_rate/$no_hrs);
		$per_hour_rate = round($per_hour_rate, 2);
		$vat_charge = round($vat_charge, 2);
		$gross_amount = round($totPrice, 2);
		$a_service_rate = $service_rate;
		$cleaning_rates = $cleaning_material_rate;
		$service_rate = round($service_rate, 2);
		
        $total_service_rate = 0;
        $discount = 0;			
        // if($coupon == "")
        // { 
            $data = array('vat_charge' => $vat_charge ,'service_rate' => $service_rate,'gross_amount' => $gross_amount,'hour_rate' => $per_hour_rate,'net_amount'=>$total_service_rate,'discount'=>$discount,'price_to_show'=>$per_hour_rate,'cleaningrate'=>$cleaning_rates,'serv_rate'=>$a_service_rate,'supervisorrate'=>$supervisorrate);
            return response()->json([
                'status' => "success",
                'data' => $data,
                'message' =>'Amount details fetched!'
                ], 200);
        // }
    }
    public function calculateAvailability(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'time-availability', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => $request->all(), 
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
        
        if($responseStatus == 'success') {
            $data = $responseBody['data'];
            return response()->json(
                    [
                        'status' => 'success',
                        'data' =>$data,
                        'messages' => $responseBody['message'],
                    ],
                    200
                );
            // $html = "";
            // $times = array();
            // $current_hour_index = 0;
            // $time = '07:00 AM';  
            // $time_stamp = strtotime($time);
            // $err =0;
            // // $html .= '<p>Available time <strong class="selecteddate">'.$request->input('bookedDate').'</strong><span class="hiddenfromtime_error val_errr" style="display:none;"></span></p>';
            // for ($i = 0; $i < 10; $i++)
            // {
            //     $oneDimensionalArray = array_map('current', $data['available_times']);
    
            //     $time_stamp = strtotime('+60mins', strtotime($time));
            //     $timess = date('H:i:s', $time_stamp);
            //     $time = date('g:i A', $time_stamp);
            //     // dump($time);
            //     if( in_array( $timess ,$oneDimensionalArray ) )
            //     {
    
            //     } else {
            //         if($data['date'] == date('Y-m-d'))
            //         {
            //             $t_shrt = date('H:i:s', strtotime($time)); 
            //             $cur_shrt = date('H:i:s');
            //             $hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
            //             // dump($hours);
            //             if($hours >= 2)
            //             {
            //                 $err = 0;
            //                 // dd($data['s_hour']);
            //                 $to_time = date('H', strtotime($timess.'+'.$request->input('numberHours').' hour'));
            //                 // dump($to_time);
            //                 // dump($time);
            //                 if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
            //                     $html.= '<li>';
            //                     $html.= '<div class="tick-mark">&nbsp;</div>';
            //                     $html .= '<div class="tick-text" data-id="'.$timess.'">'.$time.'</div>';
            //                     $html .= '<div class="clear"></div></li>';
            //                     //$html .= '<li data-id="'.$timess.'">'.$time.'</li>';
            //                 }
            //             } else {
            //                 $err = 1;
            //             }
            //         } else {
            //             $to_time = date('H', strtotime($timess.'+'.$request->input('numberHours').' hour'));
            //             if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
            //                 // dd("iiii");
            //                 $html.= '<li>';
            //                 $html.= '<div class="tick-mark"></div>';
            //                 $html.= '<div class="tick-text" data-id="'.$timess.'">'.$time.'</div>';
            //                 $html.= '<div class="clear"></div></li>';
            //             } else{
            //                 $err =1;
            //             }
            //         }
                
            //     }
            // }
            // if($err == 1)
            // {
            //     $html.='<div class="col-md-12 col-sm-12 comment-box no-left-right-padding">
            //                                     <p id="no-shift-message" class="black">Currently No Shift Available...</p>
            //                                     </div>';
            // }
            // $html .= '<div class="clear"></div>';

            // return response()->json(
            //     [
            //         'status' => 'success',
            //         'data' =>$html,
            //         'messages' => $responseBody['message'],
            //     ],
            //     200
            // );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }
    }
    public function redirectVal()
    {
        return redirect('house-cleaning-dubai');
    }
    public function calculateAmount(Request $request)
    {
        // return $request->numberHours;
        $no_hrs = $request['numberHours'];
        $no_maids = $request['numberMaids'];
        $cleaningStatus = $request['cleaningStatus'];
        // $extraServices = $request['extraServices'];
        // $coupon = ucfirst($request['coupon']);
        $customerId = $request['customerId'];
        $bookDate = $request['bookedDate'];
        $monthDuration = $request['monthDuration'];
       

        if($bookDate != '') {
            $weekday = date('w', strtotime($bookDate));
            $startDate = new DateTime($bookDate);
            if($monthDuration == 'Continue') {
                $actual_ending_date = $startDate;
                $ending_date = $startDate;  
            } else {
                $dateIntervel = new DateTime($bookDate);
            $dateIntervel->add(new DateInterval('P'.$monthDuration.'M'));
            $actual_ending_date = $dateIntervel->format('Y-m-d'); 
            $ending_date = $dateIntervel->format('Y-m-d');  // 2016-01-02
            $endDate = new DateTime($ending_date); 
            $resultDays = array('0' => 0, 
            '1' => 0, 
            '2' => 0, 
            '3' => 0, 
            '4' => 0, 
            '5' => 0, 
            '6' => 0); 
            // iterate over start to end date 
            while($startDate <= $endDate ){ 
                // find the timestamp value of start date 
                $timestamp = strtotime($startDate->format('d-m-Y')); 
                // find out the day for timestamp and increase particular day 
                $weekDay = date('w', $timestamp); 
                $resultDays[$weekDay] = $resultDays[$weekDay] + 1; 
                // increase startDate by 1 
                $startDate->modify('+1 day'); 
            } 
            $number_of_weeks = $resultDays[$weekday];
            }
        }
        $service_types = $request['serviceId'];
        $visitType = $request['visitType'];
        $vat_percentage = Config::get('values.vat_amount');
        $no_visits =1;
        // if ($visitType == 'OD') {
        //     $no_visits = 1;
        // } elseif ($visitType == 'WE') {
        //     $no_visits = $number_of_weeks;
        // } elseif ($visitType == 'BW') {
        //     $no_visits = round($number_of_weeks/2);
        // }



        if ($customerId != "") 
        {
            $cust_id=$customerId;
        } else {
            $cust_id = "";
        }
        // // $coupon = ucfirst($this->input->post('coupon_code'));
        // $booktype = $this->input->post('hiddenbooktype');
        // // $no_hrs = $this->input->post('select_hours');
        // $no_maids = $this->input->post('select_maids');
        // $no_visits = $this->input->post('no_of_visits');
        // $service_types = $this->input->post('service_types');
       
        
        //$interior_cost = $this->input->post('interiorcost');
        //$fridge_cost = $this->input->post('fridgecost');
        //$iron_cost = $this->input->post('ironingcost');
        //$oven_cost = $this->input->post('ovencost');
        // $clean_val = $this->input->post('hiddencleaningval');
        // $get_fee_details = $this->default_model->get_servicefee_details($service_types);
//        $get_fee_details = $this->default_model->get_fee_details();
        $bookedDate = date("Y-m-d", strtotime($bookDate));
        if($cleaningStatus == "N")
        {
			if($no_hrs == 2)
			{
				$per_hour_rate = 52.38;
			} else if($no_hrs == 3)
			{
				$per_hour_rate = 44.44;
			} else if($no_hrs == 4){
				$per_hour_rate = 38.10;
			} else {
				$per_hour_rate = 38.10;
			}
			$cleaning_material_rate = 0;
			$price_to_show = (float)$per_hour_rate;
        } else {
			// if($no_hrs == 2)
			// {
				// $per_hour_rate = 50;
			// } else if($no_hrs == 3)
			// {
				// $per_hour_rate = 40;
			// } else {
				// if($bookedDate == "2023-04-20" || $bookedDate == "2023-04-23" || $bookedDate == "2023-04-24")
				// {
					// $per_hour_rate = 40;
				// } else {
					// $per_hour_rate = 35;
				// }
			// }
            // if($service->material_incl == "Y")
            // {
                // $cleaning_material_rate = 0;
            // } else {
                // $cleaning_material_rate = Config::get('values.cleaning_amount');
            // }
            // $price_to_show = (int)$per_hour_rate;
        }
        if($no_visits != 0 ) {
            $a_service_rate = ((($no_hrs * $per_hour_rate) * $no_maids) * $no_visits);
            $cleaning_rates = ((($no_hrs * $cleaning_material_rate) * $no_maids) * $no_visits);
        } else {
            $a_service_rate = (($no_hrs * $per_hour_rate) * $no_maids);
            $cleaning_rates = (($no_hrs * $cleaning_material_rate) * $no_maids);
        }
        $service_rate = number_format(($a_service_rate + $cleaning_rates),2,'.', '');
        $vat_charge = number_format(($service_rate*($vat_percentage/100)),2,'.', '');
        $gross_amount = number_format(($service_rate + $vat_charge),2,'.', '');
        $total_service_rate = 0;
        $discount = 0;				
        // if($coupon == "")
        // { 
            $data = array('vat_charge' => $vat_charge ,'service_rate' => $service_rate,'gross_amount' => $gross_amount,'hour_rate' => $per_hour_rate,'net_amount'=>$total_service_rate,'discount'=>$discount,'price_to_show'=>$price_to_show,'cleaningrate'=>$cleaning_rates,'serv_rate'=>$a_service_rate);
            return $data;
            return response()->json([
                'status' => "success",
                'data' => $data,
                'message' =>'Amount details fetched!'
                ], 200);
        // } 
    }

    public function checkCoupon(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'check-coupon', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'couponFrom'=>'WEB',
                'numberHours'=>$request->input('numberHours'),
                'no_maids' =>$request->input('no_maids'),
                'cleaningStatus' =>$request->input('cleaningStatus'),
                // 'extraServices' =>$request->input('extraServices'),
                'coupon' =>$request->input('coupon'),
                'customerId' =>$request->input('customerId'),
                'bookedDate' =>$request->input('bookedDate'),
                // 'monthDuration' =>$request->input('monthDuration'),
                'serviceId'=>$request->input('serviceId'),
                'selectedAmount'=>$request->input('selectedAmount'),
                // 'visitType' => $request->input('visitType'),
                // 'interior'=>$request->input('interior'),
                // 'ironing'=>$request->input('ironing'),
                // 'fridge'=>$request->input('fridge'),
                // 'oven'=>$request->input('oven'),
                'booking_type'=>$request->input('booking_type') ?: 'OD',
            ], 
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return response()->json(
                [
                    'status' => 'success',
                    'data' =>$responseBody['data'],
                    'message' => $responseBody['message'],
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'data' =>$responseBody['data'],
                    'message' => $responseBody['message'],
                ]
            );
        }
    }
    /**
     * function to load details of disinfection service
     */
    public function loadDisinfection(Request $request,$couponCode="")
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'get-disinfection', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            if (!empty($responseBody['data']['address'])) {
                $addressName = $responseBody['data']['address']['addressName']?:"";
                $areaName = $responseBody['data']['address']['AreaName']?:"";
            } else {
                $addressName = '';
                $areaName = '';
            }
            session(['addressName' => $addressName,'areaName'=>$areaName]);
            return view('Disinfection.home',['service'=>$responseBody['data']['service'],'area'=>$responseBody['data']['area'],'allAmount'=>$responseBody['data']['allServices'],'couponCode'=>$couponCode]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    /**
     * function to load details of deep cleaning service
     */
    public function loadDeep(Request $request,$couponCode="")
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        //get-deep-cleaning
        $response = $client->request('post', Config::get('api.api_url').'load-service-by-id', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'service_type_id' => 53,
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        //dd($responseBody);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            if (!empty($responseBody['data']['address'])) {
                $addressName = $responseBody['data']['address']['addressName']?:"";
                $areaName = $responseBody['data']['address']['AreaName']?:"";
            } else {
                $addressName = '';
                $areaName = '';
            }
            session(['addressName' => $addressName,'areaName'=>$areaName]);
            return view('DeepCleaning.home',['data'=>$responseBody['data'],'service'=>$responseBody['data']['service'],'area'=>$responseBody['data']['area'],'allAmount'=>@$responseBody['data']['allServices'],'couponCode'=>$couponCode]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    /**
     * function to load details of deep cleaning service
     */
    public function loadCarpet(Request $request,$couponCode="")
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'get-carpet-cleaning', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            if (!empty($responseBody['data']['address'])) {
                $addressName = $responseBody['data']['address']['addressName']?:"";
                $areaName = $responseBody['data']['address']['AreaName']?:"";
            } else {
                $addressName = '';
                $areaName = '';
            }
            session(['addressName' => $addressName,'areaName'=>$areaName]);
            return view('Carpet.home',['service'=>$responseBody['data']['service'],'area'=>$responseBody['data']['area'],'allAmount'=>$responseBody['data']['allServices'],'couponCode'=>$couponCode]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    /**
     * function to load details of mattress cleaning service
     */
    public function mattressCleaning(Request $request,$couponCode="")
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'get-mattress-cleaning', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            if (!empty($responseBody['data']['address'])) {
                $addressName = $responseBody['data']['address']['addressName']?:"";
                $areaName = $responseBody['data']['address']['AreaName']?:"";
            } else {
                $addressName = '';
                $areaName = '';
            }
            session(['addressName' => $addressName,'areaName'=>$areaName]);
            return view('Mattress.home',['service'=>$responseBody['data']['service'],'area'=>$responseBody['data']['area'],'allAmount'=>$responseBody['data']['allServices'],'couponCode'=>$couponCode]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    /**
     * function to load details of mattress cleaning service
     */
    public function floorScrubbing(Request $request,$couponCode="")
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'get-floor-scrubbing', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            if (!empty($responseBody['data']['address'])) {
                $addressName = $responseBody['data']['address']['addressName']?:"";
                $areaName = $responseBody['data']['address']['AreaName']?:"";
            } else {
                $addressName = '';
                $areaName = '';
            }
            session(['addressName' => $addressName,'areaName'=>$areaName]);
            return view('Floor.home',['service'=>$responseBody['data']['service'],'area'=>$responseBody['data']['area'],'allAmount'=>$responseBody['data']['allServices'],'couponCode'=>$couponCode]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    /**
     * function to load details of sofa cleaning service
     */
    public function sofaCleaning(Request $request,$couponCode="")
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'get-sofa-cleaning', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            if (!empty($responseBody['data']['address'])) {
                $addressName = $responseBody['data']['address']['addressName']?:"";
                $areaName = $responseBody['data']['address']['AreaName']?:"";
            } else {
                $addressName = '';
                $areaName = '';
            }
            session(['addressName' => $addressName,'areaName'=>$areaName]);
            return view('Sofa.home',['service'=>$responseBody['data']['service'],'area'=>$responseBody['data']['area'],'allAmount'=>$responseBody['data']['allServices'],'couponCode'=>$couponCode]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    public function calculateLoad(Request $request)
    {
        $selectedArray = $request->input('selectedArray');
        $intialArray = $request->input('intialArray');  
        $unserArray = unserialize($intialArray);
        $category = $request->input('category');
        
        foreach($unserArray as $arr)
        {
            $result = array_intersect_assoc($arr, $selectedArray[0]);
            if(count($selectedArray[0]) == count($result)) {
                
                $amount = $arr['service_cost'];
                $category_id =$arr['category_id'];
                $sub_category_id =$arr['sub_category_id'];
                $service_sub_funish_id =$arr['service_sub_funish_id'];
                $is_scrubbing =$arr['is_scrubbing'];
                $name =$arr['name'];
                $service_cost =$arr['service_cost'];
                $status =$arr['status'];

            }
        }
        if($category == 2  || $category ==4){
            $officesqure = $request->input('officesqure');
            $amount = number_format(($officesqure * $amount),2,'.', '');
        }
        $vatAmount =  number_format(($amount*(Config::get('values.vat_amount')/100)),2,'.', '');
        $total = number_format(($vatAmount + $amount),2,'.', '');
        $arrayVal = ['category_id1'=>$category_id,'sub_category_id1'=>$sub_category_id,'service_sub_funish_id1'=>$service_sub_funish_id,'is_scrubbing1'=>$is_scrubbing,'name1'=>$name,'service_cost1'=>$service_cost,'status1'=>$status];

        return response()->json(
            [
                'status' => 'success',
                'data' =>['totalAmount'=>$total,'vat_amount'=>$vatAmount,'amount'=>$amount,'arrayVal'=>serialize($arrayVal)],
                'messages' => 'Success',
            ]
        );    
    } 
    public function calculateSofa(Request $request)
    {
        $selectedArray = $request->input('selectedArray');
      
        $intialArray = $request->input('intialArray');  
        $unserArray = unserialize($intialArray);
        $category = $request->input('category');
        $category_id1 =0;
        

     
        $sub_category_id1 =0;
        $service_sub_funish_id1 =0;
     

        $is_scrubbing1 =1;
        $name1 =0;
       
      
        $service_cost1 =0;
        $status1 =0;
       

        $amount = 0;
        foreach($unserArray as $arr)
        {   
            if($selectedArray!=0){
                $result = array_intersect_assoc($arr, $selectedArray[0]);
                if(count($selectedArray[0]) == count($result)) {
                    $amount = $arr['service_cost'] + $amount;
                    $category_id1 =$arr['category_id'];
                    $sub_category_id1 =$arr['sub_category_id'];
                    $service_sub_funish_id1 =$arr['service_sub_funish_id'];
                    $is_scrubbing1 =$arr['is_scrubbing'];
                    $name1 =$arr['name'];
                    $service_cost1 =$arr['service_cost'];
                    $status1 =$arr['status'];
                }
            }
            
        }
      
        $vatAmount =  number_format(($amount*(Config::get('values.vat_amount')/100)),2,'.', '');
        $total = number_format(($vatAmount + $amount),2,'.', '');
       
        $cat1seat = ['category_id1'=>$category_id1,'sub_category_id1'=>$sub_category_id1,'service_sub_funish_id1'=>$service_sub_funish_id1,'is_scrubbing1'=>$is_scrubbing1,'name1'=>$name1,'service_cost1'=>$service_cost1,'status1'=>$status1];

        return response()->json(
            [
                'status' => 'success',
                'data' =>['totalAmount'=>$total,'vat_amount'=>$vatAmount,'amount'=>$amount,'cat1seat'=>serialize($cat1seat)],
                'messages' => 'Success',
            ]
        );    
    }  
    public function calculateMattress(Request $request)
    {
        $singleArray = $request->input('singleArray');
        $kingArray = $request->input('kingArray');
        $queenArray = $request->input('queenArray');
        $intialArray = $request->input('intialArray');  
        $unserArray = unserialize($intialArray);
        $category_id1 =0;
        $category_id2 =0;
        $category_id3 =0;
        $sub_category_id3 =0;
        $sub_category_id2 =0;
        $sub_category_id1 =0;
        $service_sub_funish_id1 =0;
        $service_sub_funish_id2 =0;
        $service_sub_funish_id3 =0;
        $is_scrubbing3 =0;
        $is_scrubbing2 =0;
        $is_scrubbing1 =0;
        $name1 =0;
        $name2 =0;
        $name3 =0;
        $service_cost3 =0;
        $service_cost2 =0;
        $service_cost1 =0;
        $status1 =0;
        $status2 =0;
        $status3 =0;
        $amount = 0;
        foreach($unserArray as $arr)
        {   
            if($singleArray!=0){
                $result = array_intersect_assoc($arr, $singleArray[0]);
                if(count($singleArray[0]) == count($result)) {
                    $amount = $arr['service_cost'] + $amount;
                    $category_id1 =$arr['category_id'];
                    $sub_category_id1 =$arr['sub_category_id'];
                    $service_sub_funish_id1 =$arr['service_sub_funish_id'];
                    $is_scrubbing1 =$arr['is_scrubbing'];
                    $name1 =$arr['name'];
                    $service_cost1 =$arr['service_cost'];
                    $status1 =$arr['status'];
                }
            }
            if($kingArray!=0){
                $result2 = array_intersect_assoc($arr, $kingArray[0]);
                if(count($kingArray[0]) == count($result2)) {
                    $amount = $arr['service_cost'] + $amount;
                    $category_id2 =$arr['category_id'];
                    $sub_category_id2 =$arr['sub_category_id'];
                    $service_sub_funish_id2 =$arr['service_sub_funish_id'];
                    $is_scrubbing2 =$arr['is_scrubbing'];
                    $name2 =$arr['name'];
                    $service_cost2 =$arr['service_cost'];
                    $status2 =$arr['status'];
                } 
            }
            if($queenArray!=0){
                $result3 = array_intersect_assoc($arr, $queenArray[0]);
                if(count($queenArray[0]) == count($result3)) {
                    $amount = $arr['service_cost'] + $amount;
                    $category_id3 =$arr['category_id'];
                    $sub_category_id3 =$arr['sub_category_id'];
                    $service_sub_funish_id3 =$arr['service_sub_funish_id'];
                    $is_scrubbing3 =$arr['is_scrubbing'];
                    $name3 =$arr['name'];
                    $service_cost3 =$arr['service_cost'];
                    $status3 =$arr['status'];
                }
            }
           
          
        }
        $vatAmount =  number_format(($amount*(Config::get('values.vat_amount')/100)),2,'.', '');
        $total = number_format(($vatAmount + $amount),2,'.', '');
        $single = ['category_id1'=>$category_id1,'sub_category_id1'=>$sub_category_id1,'service_sub_funish_id1'=>$service_sub_funish_id1,'is_scrubbing1'=>$is_scrubbing1,'name1'=>$name1,'service_cost1'=>$service_cost1,'status1'=>$status1];
        $king = ['category_id2'=>$category_id2,'sub_category_id2'=>$sub_category_id2,'service_sub_funish_id2'=>$service_sub_funish_id2,'is_scrubbing2'=>$is_scrubbing2,'name2'=>$name2,'service_cost2'=>$service_cost2,'status2'=>$status2];
        $queen = ['category_id3'=>$category_id3,'sub_category_id3'=>$sub_category_id3,'service_sub_funish_id3'=>$service_sub_funish_id3,'is_scrubbing3'=>$is_scrubbing3,'name3'=>$name3,'service_cost3'=>$service_cost3,'status3'=>$status3];
        return response()->json(
            [
                'status' => 'success',
                'data' =>['totalAmount'=>$total,'vat_amount'=>$vatAmount,'amount'=>$amount,'single'=>serialize($single),'king'=>serialize($king),'queen'=>serialize($queen)],
                'messages' => 'Success',
            ]
        ); 
    }

    public function calculateCarpet(Request $request)
    {
        // $coupon = $request->input('coupon');
        $carpet1Array = $request->input('carpet1Array');
      
        $intialArray = $request->input('intialArray');  
        $unserArray = unserialize($intialArray);
        $category_id1 =0;
      
        $sub_category_id1 =0;
        $service_sub_funish_id1 =0;
        
     
        $is_scrubbing1 =0;
        $name1 =0;
       
       
        $service_cost1 =0;
        $status1 =0;
       
        $amount = 0;
        foreach($unserArray as $arr)
        {   
            if($carpet1Array!=0){
                $result = array_intersect_assoc($arr, $carpet1Array[0]);
                if(count($carpet1Array[0]) == count($result)) {
                    $amount = $arr['service_cost'] + $amount;
                    $category_id1 =$arr['category_id'];
                    $sub_category_id1 =$arr['sub_category_id'];
                    $service_sub_funish_id1 =$arr['service_sub_funish_id'];
                    $is_scrubbing1 =$arr['is_scrubbing'];
                    $name1 =$arr['name'];
                    $service_cost1 =$arr['service_cost'];
                    $status1 =$arr['status'];
                }
            }
        }
       
       
        $vatAmount =  number_format(($amount*(Config::get('values.vat_amount')/100)),2,'.', '');
        $total = number_format(($vatAmount + $amount),2,'.', '');
        $carpetsize1 = ['category_id1'=>$category_id1,'sub_category_id1'=>$sub_category_id1,'service_sub_funish_id1'=>$service_sub_funish_id1,'is_scrubbing1'=>$is_scrubbing1,'name1'=>$name1,'service_cost1'=>$service_cost1,'status1'=>$status1];
      
        return response()->json(
            [
                'status' => 'success',
                'data' =>['totalAmount'=>$total,'vat_amount'=>$vatAmount,'amount'=>$amount,'carpetsize1'=>serialize($carpetsize1)],
                'messages' => 'Success',
            ]
        ); 
    }
    public function cashPaymentSuccess(Request $request,$referenceNo)
    {
        /************************************************************************************************* */
        if(isWebView() && !$request->has('user_id')){
            // if mobile add user_id and token
            return Redirect::to(request()->fullUrlWithQuery(['user_id' => session('customerId'),'token' => session('token')]));
        }
        /************************************************************************************************* */
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', Config::get('api.api_url').'cash-payment-success', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'ref_no' => $referenceNo,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        if ($responseBody['status'] == 'success') {
            $customer = $responseBody['data']['customer'];
            $customerAddress = $responseBody['data']['customer_address'];
            $bookings = $responseBody['data']['bookings'];
            $areaName = $responseBody['data']['areaName'];
            $services = $responseBody['data']['services'];
            /************************************************************************************************* */
            if ($responseBody['data']['bookings']['mail_booking_confirmation_to_customer_status'] != 1 && session('mail_booking_confirmation_to_customer_status_'.$responseBody['data']['bookings']['booking_id']) != 1) {
                // send confirmation mail to customer
                try {
                    Mail::send(new BookingConfirmationToCustomerMail($customer['email_address'], $customer, $bookings, $customerAddress, $areaName, $services, null, $responseBody['data']));
                    session(['mail_booking_confirmation_to_customer_status_'.$responseBody['data']['bookings']['booking_id'] => 1]);
                    $mail_booking_confirmation_to_customer_status = 1;
                } catch (\Exception $e) {
                    $mail_booking_confirmation_to_customer_status = 0;
                }
            }
            /************************************************************************************************* */
            if ($responseBody['data']['bookings']['mail_booking_confirmation_to_admin_status'] != 1 && session('mail_booking_confirmation_to_admin_status_'.$responseBody['data']['bookings']['booking_id']) != 1) {
                // send confirmation mail to admin
                try {
                    Mail::send(new BookingConfirmationToAdminMail($customer['email_address'], $customer, $bookings, $customerAddress, $areaName, $services, null, $responseBody['data']));
                    session(['mail_booking_confirmation_to_admin_status_'.$responseBody['data']['bookings']['booking_id'] => 1]);
                    $mail_booking_confirmation_to_admin_status = 1;
                } catch (\Exception $e) {
                    $mail_booking_confirmation_to_admin_status = 0;
                }
            }
            /************************************************************************************************* */
            // update mail send status
            try {
                $client = new \GuzzleHttp\Client(['verify' => false]);
                $response = $client->request('POST', Config::get('api.api_url') . 'booking-mail-status', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'form_params' => [
                        'booking_id' => $bookings['booking_id'],
                        'mail_booking_confirmation_to_customer_status' => @$mail_booking_confirmation_to_customer_status,
                        'mail_booking_confirmation_to_admin_status' => @$mail_booking_confirmation_to_admin_status,
                    ],
                ]);
            } catch (\Exception $e) {
            }
            /************************************************************************************************* */
            return view('common.success', ['data' => $responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    public function paymentSuccess(Request $request,$refId,$trackId)
    {
        /************************************************************************************************* */
        if(isWebView() && !$request->has('user_id')){
            // if mobile add user_id and token
            return Redirect::to(request()->fullUrlWithQuery(['user_id' => session('customerId'),'token' => session('token')]));
        }
        /************************************************************************************************* */
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', Config::get('api.api_url') . 'payment-success', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'ref_no' => $refId,
                'trackId' => $trackId,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if (@strtolower($responseBody['data']['online']['payment_status']) == "success") {
            $customer = $responseBody['data']['customer'];
            $customerAddress = $responseBody['data']['customer_address'];
            $bookings = $responseBody['data']['bookings'];
            $areaName = $responseBody['data']['areaName'];
            $services = $responseBody['data']['services'];
            $online = $responseBody['data']['online'];
            /************************************************************************************************* */
            if ($responseBody['data']['bookings']['mail_booking_confirmation_to_customer_status'] != 1 && session('mail_booking_confirmation_to_customer_status_'.$responseBody['data']['bookings']['booking_id']) != 1) {
                // send confirmation mail to customer
                try {
                    Mail::send(new BookingConfirmationToCustomerMail($customer['email_address'], $customer, $bookings, $customerAddress, $areaName, $services, $online, $responseBody['data']));
                    session(['mail_booking_confirmation_to_customer_status_'.$responseBody['data']['bookings']['booking_id'] => 1]);
                    $mail_booking_confirmation_to_customer_status = 1;
                } catch (\Exception $e) {
                    $mail_booking_confirmation_to_customer_status = 0;
                }
            }
            /************************************************************************************************* */
            if ($responseBody['data']['bookings']['mail_booking_confirmation_to_admin_status'] != 1 && session('mail_booking_confirmation_to_admin_status_'.$responseBody['data']['bookings']['booking_id']) != 1) {
                // send confirmation mail to admin
                try {
                    Mail::send(new BookingConfirmationToAdminMail($customer['email_address'], $customer, $bookings, $customerAddress, $areaName, $services, $online, $responseBody['data']));
                    session(['mail_booking_confirmation_to_admin_status_'.$responseBody['data']['bookings']['booking_id'] => 1]);
                    $mail_booking_confirmation_to_admin_status = 1;
                } catch (\Exception $e) {
                    $mail_booking_confirmation_to_admin_status = 0;
                }
            }
            /************************************************************************************************* */
            // update mail send status
            try {
                $client = new \GuzzleHttp\Client(['verify' => false]);
                $response = $client->request('POST', Config::get('api.api_url') . 'booking-mail-status', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'form_params' => [
                        'booking_id' => $bookings['booking_id'],
                        'mail_booking_confirmation_to_customer_status' => @$mail_booking_confirmation_to_customer_status,
                        'mail_booking_confirmation_to_admin_status' => @$mail_booking_confirmation_to_admin_status,
                    ],
                ]);
            } catch (\Exception $e) {
            }
            /************************************************************************************************* */
            return view('common.success', ['data' => $responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
    public function paymentFailed(Request $request,$refId,$trackId)
    {
        /************************************************************************************************* */
        if(isWebView() && !$request->has('user_id')){
            // if mobile add user_id and token
            return Redirect::to(request()->fullUrlWithQuery(['user_id' => session('customerId'),'token' => session('token')]));
        }
        /************************************************************************************************* */
        //dd($trackId);
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'ref_no' => $refId,
                'trackId' => $trackId,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        $responseStatus = $responseBody['status'];
        if($responseStatus == "success") {
            return view('common.fail',['data'=>$responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['message'],
                ]
            );
        }
    }
	
	public static function curlFunget($url,$Headers,$postdatas)
    {
        $ch = curl_init();
        //$certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postdatas);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if($response === FALSE){
            die(curl_error($ch));
        }
        curl_close($ch);

        return $response;
    }
	
	public static function execFunc($method, $url, $obj = array(),$headers) {
     
    $curl = curl_init();
     
    switch($method) {
      case 'GET':
        if(strrpos($url, "?") === FALSE) {
          $url .= '?' . http_build_query($obj);
        }
        break;

      case 'POST': 
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($obj));
        break;

      case 'PUT':
      case 'DELETE':
      default:
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method)); // method
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($obj)); // body
    }

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers); 
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
    
    // Exec
    $response = curl_exec($curl);
    $info = curl_getinfo($curl);
    curl_close($curl);
    
    // Data
    $header = trim(substr($response, 0, $info['header_size']));
    $body = substr($response, $info['header_size']);
     
    return array('data' => json_decode($body));
  }
}
