<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class PackageController extends Controller
{
    public function list_packages(Request $request)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /********************************************************** */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('GET', Config::get('api.api_url') . 'packages', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'query' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        /********************************************************** */
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1'],'customer_photo_url'=>$responseBody['data']['customer']['customer_photo_url']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        /********************************************************** */
        return view('Packages.list_packages', ['packages'=>$responseBody['data']]);
    }
	public function list_packages_web(Request $request)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /********************************************************** */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('GET', Config::get('api.api_url') . 'packages-web', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'query' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        /********************************************************** */
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1'],'customer_photo_url'=>$responseBody['data']['customer']['customer_photo_url']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        /********************************************************** */
        return view('Packages.list_packages', ['packages'=>$responseBody['data']]);
    }
    public function view_package(Request $request,$package_id)
    {
        /******************************************************* */
        if($request->has('user_id')){
            if ($request->token) {
                $customerId = $request->user_id;
                $token = $request->token;
            } else {
                $customerId = null;
            }
        }
        else if (session('customerId') != '') {
            $customerId = session('customerId');
            $token = session('token');
        }
        /******************************************************* */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('GET', Config::get('api.api_url') . 'packages/'.$package_id, [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'query' => [
                'customerId' => @$customerId,
                'token' => @$token,
            ],
        ]);
        /******************************************************* */
        $responseBody = json_decode((string) $response->getBody(), true);
        //dd($responseBody);
        if (@$responseBody['data']['customer']) {
            session(['customerId' => $responseBody['data']['customer']['customer_id'],'token' => $responseBody['data']['customer']['oauth_token'], 'fullName' => $responseBody['data']['customer']['customer_name'], 'phone' => $responseBody['data']['customer']['mobile_number_1']]);
        } else {
            Session::forget('customerId');
            Session::forget('token');
            Session::forget('fullName');
            Session::forget('phone');
        }
        /******************************************************* */
        //dd($responseBody['data']);
        return view('Packages.view_package', $responseBody['data']);
    }
}
