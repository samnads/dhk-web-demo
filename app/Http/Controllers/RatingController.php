<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\RatingConfirmationMail;

class RatingController extends Controller
{
    public function rating($date,$serviceId,$bookingId)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'get-rate-review', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'date'=>$date,
                'day_service_id'=>$serviceId,
                'booking_id'=>$bookingId,
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
           $rate = $responseBody['data']['rating'];
           $review = $responseBody['data']['comments'];
        } else {
            $rate = 0;
            $review = '';
        }
        // dd($responseBody);
        return view('Ratings.rating',['date'=>$date,'serviceId'=>$serviceId,'bookingId'=>$bookingId,'rate'=>$rate,'review'=>$review]);
    }
    public function submitRate(Request $request)
    {
        // dd($request->all());
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'submit-rate', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'date'=>$request['date'],
                'day_service_id'=>$request['day_service_id'],
                'booking_id'=>$request['booking_id'],
                'ratingValue'=>$request['ratingValue'],
                'review'=>$request['review'],
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            try{
            Mail::send(new RatingConfirmationMail($responseBody['data'],$request['date'],$request['ratingValue'],$request['review']));
            } catch (\Exception $e) {
            return response()->json(
            $responseBody
        );            }
        }
        return response()->json(
            $responseBody
        );
    }
}

