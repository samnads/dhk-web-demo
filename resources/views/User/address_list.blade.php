@extends('layouts.app')
@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
      <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
               <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                    <div class="col-sm-12 em-step-head no-left-right-padding">
                         <h2>Address</h2>
                    </div>
               </div>
               <form id="address_list_form" method="post" action="{{url('set-default')}}">
               {{ csrf_field() }}
               <div class="col-sm-12 my-account-main">
                    <div class="col-sm-12 em-booking-det-cont my-account-content-main">
                         <ul>
                             <li>
                                 <div class="row my-acc-cont-main p-0 m-0">
                                      <div class="col-sm-12 my-adr-left-cont p-0"><h5>Your saved addresses <a  href="{{URL::to('add-address')}}">Add new</a></h5></div>
                                      <!--<div class="col-sm-6 my-adr-right-cont p-0"></div>-->
                                 </div>
                             </li>
                             <?php $i =0;?>
                             @foreach($data as $address)
                             <?php $i++;?>
                              <li>
                                   <div class="col-sm-12 em-field-main address-opt-main p-0">
                                        <div class="my-acc-edit"><a href="{{url('edit-address-data/'.$address['id'])}}"><i class="fa fa-pencil"></i></a></div>
                                        @if($address['default'] != 1) <div class="my-acc-delete"><a onclick="viewDeletePopup(<?php echo $address['id'];?>);" href="#"><i class="fa fa-trash"></i></a></div>@endif
                                        <input id="address-opt{{$i}}" @if($address['default'] == 1) checked @endif value="{{$address['id']}}" name="address_opt" class="" title="Default Address" type="radio">
                                        <label for="address-opt{{$i}}"> <span></span> &nbsp;         
                                             <div class="col-sm-12 my-acc-cont-main address-opt  p-0">
                                                  <?php 
                                                  $addressType = $address['AddressType'];
                                                  if($addressType == 'HO')
                                                  {
                                                       $type = 'Home';
                                                  }
                                                  else if($addressType == 'OF')
                                                  {
                                                       $type = 'Office';
                                                  }
                                                  else if($addressType == 'OT')
                                                  {
                                                       $type = 'Other';
                                                  } 
                                                  else {
                                                       $type ='';
                                                  }
                                                  ?>
                                                  <div class="col-sm-12 my-acc-que-cont p-0">{{$type}}</div>
                                                  <div class="col-sm-12 my-acc-ans-cont p-0">
                                                       <input name="" class="text-field" type="text" value="{{$address['addressName']}},{{$address['Area']}}" disabled>
                                                  </div>
                                             </div>
                                        </label>
                                  </div>
                              </li>
                              @endforeach  
                         </ul>
                    </div>
                    <div class="col-12 em-booking-det-cont em-next-btn">
                         <div class="row em-next-btn-set ml-0 mr-0">
                              <div class="col-md-6 col-sm-12 col-6 em-next-btn-left pl-0 pr-0">
                              <a class="cursor" href="{{URL::to('my-accounts')}}"> <span class="em-back-arrow back-to4-btn " title="My Account"></span></a>
                              </div>
                              @if($data)
                              <div class="col-sm-6 col-12 em-next-btn-right pl-0 pr-0">
                                   <input value="Set Default Address" class="text-field-button show6-step" type="submit">
                              </div>
                              @endif
                         </div>
                    </div>
               </div>
               </form>
          </div>
      </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
<div class="col-sm-12 popup-main remove-address-popup" id="conform_delete">
    <div class="row min-vh-100 d-flex flex-column justify-content-center">
         <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
               <h5 class="">Remove Address<span class="remove-addresss-close-btn"><img src="{{asset('images/el-close-black.png')}}"
 title="" style=""></span></h5>
               <input type="hidden" id="customerAddresId" value="">
               <p class="text-left">Are you sure want to delete this address.</p>
               <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                    <input value="Delete" class="text-field-button show6-step delete" onclick="deleteAddress()" type="button">
              </div>      
         </div>
    </div>
</div><!--popup-main end-->
<div class="col-sm-12 popup-main forgot-password-popup" id="success_delete">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
               <span id="message_delete">Address deleted successfully.</span> 
          </div>
     </div>
</div><!--popup-main end-->
@endsection
@push('scripts')
<script>
$(document).ready(function() {
    $('.logo').addClass('logo_left');
});  
$('.remove-addresss-close-btn').click(function(){
$('.remove-address-popup').hide(500);
});
function deleteAddress()
{
    var customerAddresId = $('#customerAddresId').val();
    $(".preloader").fadeIn(1000);
    $.ajax({
        method: 'post',
        url: '{{url('delete-customer-address')}}',
        data:{'customerAddresId':customerAddresId,_token:'{{csrf_token()}}'},
        success: function (result) {
          if (result.status == 'success') {
               location.reload(); 
               $('#success_delete').show();
          }
        }
    }); 
}
function viewDeletePopup(customerAddressId)
{
     $('#conform_delete').show(500);
     $('#customerAddresId').val(customerAddressId);
}
$.validator.addMethod("customemail",
        function(value, element) {
            return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
        },
        "Please enter a valid email id. "
    );
$("#address_list_form").validate({
    ignore: [],
     rules: {
          address_opt: {
               required:true,
          },
     },
    messages: {
     address_opt: "Please select your default address.",
    },
    errorPlacement: function (error, element)
    {
    //     if(element.attr("name") == "timeSelected") {
    //     error.insertAfter(element.parent());
    // } else {
        error.insertAfter(element.parent().parent());
    // }
        },
    submitHandler: function (form,event) {
        document.getElementById('address_list_form').submit();
    }

});
</script>
@endpush