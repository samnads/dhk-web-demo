@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
    <script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
    <div class="col-sm-12 popup-main otp-popup" id="crop-popup">
        <div class="row min-vh-100 d-flex flex-column justify-content-center">
            <div class="col-md-5 col-sm-12 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <h5 class="">Image Cropper <span class="em-login-close-btn">
                        <img src="{{ asset('images/el-close-black.png') }}" title="" style=""
                            onclick="closeCropper()">
                    </span>
                </h5>
                <div class="popup-main-box">
                    <div id="" class="col-12 p-0">
                        <div class="modal-body p-0 pb-3">
                            <div class="img-container">
                                <img id="image" src="#">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 next-but mb-1">
                                <button type="button" class="text-field-button" onclick="closeCropper()">Cancel</button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 next-but mb-1">
                                <button type="button" class="text-field-button" id="crop"><i class="fa fa-floppy-o"
                                        aria-hidden="true"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0">

                    <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                        <div class="col-sm-12 em-step-head no-left-right-padding">
                            @if (!isWebView())
                                <h2>My Account</h2>
                            @endif
                            @if (session('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif

                        </div>
                    </div>

                    <div class="col-sm-12 my-account-main">

                        <div class="col-sm-12 my-account-title-main mb-4">

                            <div class="my-account-title-photo-main">
                                <div class="my-account-title-photo">
                                    @if(!isWebView())
                                    <span title="Update profile image"></span>
                                    @else
                                    <span title="Update profile image"></span>
                                    @endif
                                    @if(!isWebView())
                                    <img id="customer_image_show" src="{{ session('customer_photo_url') ?: asset('images/man-icon.png') }}" />
                                    @else
                                    <img id="customer_image_show" src="{{ session('customer_photo_url') ?: asset('images/man-icon.png') }}" />
                                    <!--<img id="customer_image_show" src="{{ asset('images/man-icon.png') }}" />-->
                                    @endif
                                </div>
                            </div>
                            <input type="file" class="input-image d-none" accept="image/*" data-selector="customer_image"
                                data-crop-width="512" data-crop-height="512">
                            <input type="hidden" name="customer_image_base64" id="customer_image_base64">
                            <div class="col-sm-12 my-account-title-cont">
                                <h5 class="text-center">
                                    @if (session('fullName'))
                                        {{ session('fullName') }}
                                    @endif
                                </h5>
                                <p class="text-center">
                                    @if (session('phone'))
                                        {{ session('phone') }}
                                    @endif
                                </p>
                            </div>
                        </div>



                        <div class="col-sm-12 em-booking-det-cont my-account-content-main">
                            <ul class="link-set">
                                <li><a href="{{ url('get-user-data') }}">Edit Personal Details</a></li>
                                <li><a href="{{ url('manage-address') }}">Manage Address</a></li>
                                <li><a onclick="viewChangePasswordPopup(<?php echo session('customerId'); ?>);" href="#">Change
                                        Password</a></li>

                                <!-- <li><a href="{{ url('manage-cards/' . session('customerId')) }}">Manage Cards</a></li> -->
                            </ul>
                        </div>




                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
    <div class="col-sm-12 popup-main otp-popup" id="password_pop">
        <form id="password_form" method="post">
            <div class="row min-vh-100 d-flex flex-column justify-content-center">
                <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                    <h5 class="">Reset Password <span class="em-login-close-btn"><img
                                src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>
                    <span id="pass_error" style="display:none;"></span>
                    <input type="hidden" id="customerId" name="customerId">
                    <div class="row ml-0 mr-0">
                        <div class="em-field-main pl-0 pr-0" style="width:100%">
                            <div class="col-sm-12 em-field-main pl-0 pr-0">
                                <p>Old Password</p>
                                <div class="col-12 em-text-field-main pl-0 pr-0">
                                    <input name="old_password" class="text-field" type="password" onclick="hideError()">
                                    <div class="password-set-btn fa fa-fw fa-eye-slash"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 em-field-main pl-0 pr-0">
                                <p>New Password</p>
                                <div class="col-12 em-text-field-main password-set pl-0 pr-0">
                                    <input name="new_password" id="new_password" class="text-field" type="password"
                                        onclick="hideError()">
                                    <div class="password-set-btn fa fa-fw fa-eye-slash"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 em-field-main pl-0 pr-0">
                                <p>Confirm Password</p>
                                <div class="col-12 em-text-field-main password-set pl-0 pr-0">
                                    <input name="confirm_password" class="text-field" type="password"
                                        onclick="hideError()">
                                    <div class="password-set-btn fa fa-fw fa-eye-slash"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                        <input value="Reset" class="text-field-button show6-step" id="button_submit_login"
                            type="submit">
                    </div>
                </div>
            </div>
        </form>
    </div><!--popup-main end-->
    <style>
        .password-set-btn {
            display: flex !important;
            align-items: center;
            justify-content: center;
            padding: 0;
            width: 36px !important;
        }
    </style>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.logo').addClass('logo_left');
        });

        function hideError() {
            $('#pass_error').hide();
        }
        $(".password-set-btn").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            input = $(this).parent().find("input");
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        function viewChangePasswordPopup(customerId) {
            $('#customerId').val(customerId);
            $('#pass_error').hide();
            $('#password_pop').show(300);
        }
        $('.em-login-close-btn').click(function() {
            $('#password_pop').hide(500);
        });
        $.validator.addMethod("customemail",
            function(value, element) {
                return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    .test(value);
            },
            "Please enter a valid email id. "
        );
        $("#password_form").validate({
            ignore: [],
            rules: {
                old_password: {
                    required: true,
                    minlength: 6
                },
                new_password: {
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    minlength: 6,
                    equalTo: "#new_password"
                }
            },
            messages: {
                old_password: {
                    required: "Please enter old password.",
                    minlength: "Password must contain atleast 6 characters."
                },
                new_password: {
                    required: "Please enter new password.",
                    minlength: "Password must contain atleast 6 characters."
                },
                confirm_password: {
                    required: "Please re enter password.",
                    minlength: "Password must contain atleast 6 characters.",
                    equalTo: "Confirm password must equal to new password."
                }
            },
            errorPlacement: function(error, element) {
                //     if(element.attr("name") == "timeSelected") {
                //     error.insertAfter(element.parent());
                // } else {
                error.insertAfter(element.parent().parent());
                // }
            },
            submitHandler: function(form, event) {
                $('#pass_error').hide();
                var form_data = new FormData($("#password_form")[0]);
                form_data.append('_token', '{{ csrf_token() }}');
                $(".preloader").show();
                $.ajax({
                    method: 'post',
                    url: '{{ url('update-user-password') }}',
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result) {
                        $(".preloader").hide();
                        if (result.status == 'success') {
                            $('#pass_error').show();
                            $("#pass_error").css("color", "green");
                            $('#pass_error').html(result.messages);
                            setTimeout(function() {
                                $('#password_pop').hide(300);
                            }, 3000);
                            $('#password_form')[0].reset();
                        } else {
                            $('#pass_error').show();
                            $("#pass_error").css("color", "red");
                            $('#pass_error').html(result.messages);
                            setTimeout(function() {
                                $('#password_pop').show(300);
                            }, 2000);
                            $('#password_form')[0].reset();
                        }
                    }
                });
            }
        });
        $('.my-account-title-photo > span').click(function() {
            $('[data-selector="customer_image"]').trigger("click");
        });
        var cropper;

        function showCropper(cropWidth, cropHeight) {
            cropper = new Cropper(image, {
                aspectRatio: cropWidth / cropHeight,
                viewMode: 0,
            });
            $('#crop-popup').show();
        }
        window.addEventListener('DOMContentLoaded', function() {
            var selector = null;
            var selectorWidth = null;
            var selectorHeight = null;
            var image = document.getElementById('image');
            [...document.querySelectorAll('.input-image')].forEach(function(item) {
                item.addEventListener('change', function(e) {
                    selector = e.target.getAttribute('data-selector');
                    selectorWidth = e.target.getAttribute('data-crop-width');
                    selectorHeight = e.target.getAttribute('data-crop-height');
                    var files = e.target.files;
                    var done = function(url) {
                        item.value = '';
                        image.src = url;
                        showCropper(selectorWidth, selectorHeight);
                    };
                    var reader;
                    var file;
                    var url;
                    if (files && files.length > 0) {
                        file = files[0];
                        if (URL) {
                            done(URL.createObjectURL(file));
                        } else if (FileReader) {
                            reader = new FileReader();
                            reader.onload = function(e) {
                                done(reader.result);
                            };
                            reader.readAsDataURL(file);
                        }
                    }
                });
            });
            document.getElementById('crop').addEventListener('click', function() {
                var canvas;
                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        width: selectorWidth,
                        height: selectorHeight,
                        fillColor: '#fff',
                        imageSmoothingEnabled: true,
                        imageSmoothingQuality: 'high',
                    });
                    var src = canvas.toDataURL();
                    document.getElementById(selector + "_base64").value = src; // reflect changed value
                    // upload
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('update-customer-avatar') }}",
                        data: {
                            customer_image_base64: src
                        },
                        success: function(data) {
                            document.getElementById(selector + "_show").src = src; // show changed image
                        },
                        error: function(response) {
                        },
                    });
                }
                closeCropper();
            });
        });

        function closeCropper() {
            cropper.destroy();
            cropper = null;
            $('#crop-popup').hide();
        }
    </script>
@endpush
