@extends('layouts.app')
@section('content')
<section class="em-booking-content-section">
    <div class="container em-booking-content-box">
        <div class="row em-booking-content-main ml-0 mr-0">
            <div class="col-12 em-booking-content make_payment_wrapper pl-0 pr-0">
                <div class="col-12 pl-0 pr-0">
                    <form id="payment_link_form" method="post" action="{{url('save-online-pay')}}">
                        {{ csrf_field() }}
                        <div class="col-12 em-booking-det-cont pl-0 pr-0">
                            <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
                                <h5>Make Payment</h5>
                                <div class="col-sm-12 em-field-main-set"> 
                                    <div class="row m-0">


                                        <div class="col-sm-12 em-field-main">
                                            <p>Amount</p>
                                            <input type="hidden" id="customerId" name="customerId" value="{{$customerId}}">
                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                <input name="amount" class="text-field" value="{{@$amount?:''}}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 em-field-main">
                                            <p>Description</p>
                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                <textarea name="description" cols="" rows="" class="text-field-big">{{@$message?:""}}</textarea>
                                            </div>
                                        </div>
                                        
                                        
                                    </div> 
                                </div>
                            </div>
                        </div> 
                        <div class="col-12 em-booking-det-cont em-next-btn">
                            <div class="row em-next-btn-set ml-0 mr-0">
                                <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                    <a class="cursor" href="{{URL::to('house-cleaning-dubai')}}"><span class="em-back-arrow back-to1-btn" title="Home"></span></a>
                                </div>
                                <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                <input value="Make Payment" id="payment_button" class="text-field-button show6-step reset-button" type="submit">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>

<style>
.make_payment_wrapper {
    max-width: 500px;
    margin: 0px auto;
}
.make_payment_wrapper form{
    width: 100%;
}
.reset-button {
    width: 100% !important;
    padding-left: 0px !important;
    padding-right: 0px !important;
}
</style>

@endsection
@push('scripts')
<script>
$(document).ready(function() {
    $('.logo').addClass('logo_left');

    if($('#customerId').val() == 0 ) {
        var customerId = localStorage.getItem('customerId');
        if ((customerId) && customerId != '' && customerId != 0 ) {
            $('.login').hide();
            $('.logout').show();
            $('.userPersonel').show();
            $('.make_pay_head').show();
            $('#customerId').val(customerId);
        } else{
            $('.login').show();
            $('.logout').hide();
            $('.userPersonel').hide();
            $('.make_pay_head').hide();
        }
    } else {
        localStorage.setItem('customerId',$('#customerId').val());
        $('.login').hide();
        $('.logout').show();
        $('.userPersonel').show();
        $('.make_pay_head').show();
    }  
});

$("#payment_link_form").validate({
    ignore: [],
    rules: {
    amount: {
    required:true
    },
    description: {
        required:true
    },
    },
    messages: {
    amount: "Please enter the amount.",
    description: "Please enter the description.",
    },
    errorPlacement: function (error, element)
    {
    //     if(element.attr("name") == "timeSelected") {
    //     error.insertAfter(element.parent());
    // } else {
        error.insertAfter(element.parent().parent());
    // }
        },
    submitHandler: function (form,event) {
        document.getElementById('payment_link_form').submit();
    }

});
</script>
@endpush