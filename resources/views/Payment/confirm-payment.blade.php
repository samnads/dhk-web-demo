@extends('layouts.app')
@section('content')
<section class="em-booking-content-section">
    <div class="container em-booking-content-box">
        <div class="row em-booking-content-main ml-0 mr-0">
            <div class="col-12 em-booking-content make_payment_wrapper pl-0 pr-0">
                <div class="col-12 pl-0 pr-0">
					<form id="payment_confirm_form" method="post"  action="{{config('payment.ccavenue_req_handler')}}?{{@$query_params}}">
                     {{ csrf_field() }}
						<div class="col-12 em-booking-det-cont pl-0 pr-0">
							<div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
								<h5>Confirm Payment</h5>
									<div class="col-sm-12 em-field-main-set"> 
										<div class="row m-0">
											<?php
											$pay_id = ($data['pay_details']['payment_id']);
											$name_new = preg_replace("/[^a-zA-Z0-9\s]/", "", $data['customer_name']);
											$address_new = preg_replace("/[^a-zA-Z0-9\s]/", "", $data['address']);
											$clientemail = $data['customer_email'];
											if($clientemail == "")
											{
												$clientemail = "dhkccavenue@gmail.com";
											}
											?>
											<input type="hidden" id="customerId" name="customerId" value="{{$data['pay_details']['customer_id']}}">
											<input type="hidden" name="merchant_id" value="{{config('payment.ccavenue_merchant_id')}}"/>
											<input type="hidden" name="order_id" id="order_id" value="<?php echo 'DHK-ON/'.date("Y").'/'.$pay_id; ?>"/>
											<input type="hidden" name="amount" id="amount" value="<?php echo number_format($data['gross_amount'],2,'.','');  ?>"/>
											<input type="hidden" name="currency" value="AED"/>
											<input type="hidden" name="redirect_url" value="{{config('payment.ccavenue_resp_handler_online')}}?{{@$query_params}}"/>
											<input type="hidden" name="cancel_url" value="{{config('payment.ccavenue_resp_handler_online')}}?{{@$query_params}}"/>
											<input type="hidden" name="language" value="EN"/>
											<input type="hidden" name="integration_type" value="iframe_normal"/>
											<input type="hidden" name="merchant_param1" value="<?php echo $clientemail; ?>"/>
											<input type="hidden" name="merchant_param2" value="<?php echo $data['pay_details']['payment_id']; ?>"/>
											<input type="hidden" name="billing_zip" value=""/>
											<input type="hidden" name="billing_country" value="UAE"/>
											<input type="hidden" name="billing_name" id="billing_name" value="<?php echo $name_new; ?>"/>
											<input type="hidden" name="billing_tel" id="billing_tel" value="<?php echo $data['phone_number']; ?>"/>
											<input type="hidden" name="billing_email" id="billing_email" value="<?php echo $clientemail; ?>"/>
											<input type="hidden" name="billing_address" id="billing_address" value="<?php echo $address_new; ?>"/>
											<input type="hidden" name="billing_state" id="billing_state" value="Dubai"/>
											<input type="hidden" name="billing_city" id="billing_city" value="Dubai"/>
											<div class="col-sm-12 em-field-main">
												<h4>Hi {{$data['customer_name']}},</h4>
											</div>

											<div class="col-sm-12 em-field-main">
												<div class="col-12 em-text-field-main pl-0 pr-0">
													<span>Please confirm your requested payment of <strong>AED {{number_format($data['amount'],2,'.','')}}</strong></span>
												</div>
											</div>
											<div class="col-sm-12 em-field-main">
												<table class="payment_table">
													<tbody>
														<tr class="borderless">
															<th colspan="2">Address</th>
														</tr>

														<tr>
															<td colspan="2">{{$address_new}} , {{$data['area']}}</td>
														</tr>

														<tr>
															<th>Service Amount</th>
															<td>AED {{number_format($data['amount'],2,'.','')}}</td>
														</tr>

														<tr>
															<th>Convenience Fee</th>
															<td>AED {{number_format($data['transaction_charge'],2,'.','')}}</td>
														</tr>

														<tr>
															<th>Payable Amount</th>
															<td>AED {{number_format($data['gross_amount'],2,'.','')}}</td>
														</tr>
													</tbody>
												</table> 
											</div>
											
										</div> 
									</div>
                        </div>
                    </div>  
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <a class="cursor" href="{{URL::to('house-cleaning-dubai')}}"><span class="em-back-arrow back-to1-btn" title="Home"></span></a>
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                            <button id="book_payment_button" class="text-field-button show6-step reset-button" type="submit">Confirm Payment</button>
                            </div>
                        </div>
                    </div>
					</form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
<style>
.make_payment_wrapper {
    max-width: 500px;
    margin: 0px auto;
}
.make_payment_wrapper form{
    width: 100%;
}
table.payment_table {
    width: 100%;
    border: 1px solid #eee;
}
table.payment_table td, table.payment_table th {
    padding: 6px 10px;
}
.reset-button {
    width: 100% !important;
    padding-left: 0px !important;
    padding-right: 0px !important;
}
</style>

@endsection
@push('scripts')
<script>
$(document).ready(function() {
    $('.logo').addClass('logo_left');
    if($('#customerId').val() == 0 ) {
        var customerId = localStorage.getItem('customerId');
        if ((customerId) && customerId != '' && customerId != 0 ) {
            $('.login').hide();
            $('.logout').show();
            $('.userPersonel').show();
            $('.make_pay_head').show();
            $('#customerId').val(customerId);
        } else{
            $('.login').show();
            $('.logout').hide();
            $('.userPersonel').hide();
            $('.make_pay_head').hide();
        }
    } else {
        localStorage.setItem('customerId',$('#customerId').val());
        $('.login').hide();
        $('.logout').show();
        $('.userPersonel').show();
        $('.make_pay_head').show();
    }  
});

// $('#book_payment_button').click(function() {
    // $('#payment_popup').show(500);
   // });
   // $('.em-forgot-close-btn').click(function(){
// $('#payment_popup').hide(500);
// });
</script>
@endpush