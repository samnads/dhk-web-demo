@extends('layouts.app')

@section('title') {{'Disinfection Services Dubai'}} @endsection

@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
     <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
               <input type="hidden" id="all_amount" value="{{serialize($allAmount)}}">
               <input type="hidden" id="number_hours" value="0">			   			   <input type="hidden" id="serviceNamePay" value="Disinfection">
               @include('Disinfection.steps')
          </div>
     </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')
<script  type="text/javascript" src="{{URL::asset('js/select2.full.min.js')}}"></script>
<script>
 $.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});
$(document).ready(function() {
    $('#noOfHours').val(1);
	getAvailability();
    $(".js-select2").select2();


});
$('#officesqure_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     var target=  $(this).children('.tick-text').attr("id");
     $('#officesqure').val(target);
     amountCalculation();

}); 
function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement; 
}
var ul = document.getElementById('service_for_secondr');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#service_category').val(target.id);
    amountCalculation();
};
var ul = document.getElementById('service_for');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#service_category').val(target.id);
    amountCalculation();
};
var ul = document.getElementById('service_for_secondr2');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#service_category').val(target.id);
    amountCalculation();
};
var ul = document.getElementById('bedroom_ul');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#bedroom_count').val(target.innerHTML);
    amountCalculation();
};
$('#bedroom_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
}); 
$('#villa_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
});
var ul = document.getElementById('villa_ul');
ul.onclick = function(event) { 
    var target = getEventTarget(event);
    $('#villa_id').val(target.innerHTML);
    amountCalculation();
};
$('#car_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
});  
var ul = document.getElementById('car_ul');
ul.onclick = function(event) { 
    var target = getEventTarget(event);
    $('#car_id').val(target.innerHTML);
    amountCalculation();
};
// var ul = document.getElementById('officesqure_ul');
// ul.onclick = function(event) {
//     var target = getEventTarget(event);
//     $('#officesqure').val(target.id);
// };

// function openDis()
// {
//     $.ajax({
//           method: 'get',
//           url: '{{url('load-disinfection')}}',
//           data:{_token:'{{csrf_token()}}'},  
//           cache : false,
//           contentType: false,
//           processData: false,
//           success: function (result) {
//                if (result.status == 'success') {
//                     $("#submit_button").attr( "disabled", false );
//                     window.location.href = '{{url('cleaning-second')}}'+'/'+result.data['id'];
//                }
//           }
//      });   
//    $('#disinfection').show();  
//    $('#houseCleaning').hide();  
//    $('#deepCleaning').hide();  
//    $('#carpet').hide();  
//    $('#mattress').hide();  
//    $('#sofa').hide();  

// }

$(document).ready(function() {
        amountCalculation()
    // $('.frequency_p').text('One Time');
    // $('.maid_id_p').text($('#noOfMaids').val());
    // $('.hrs_p').text($('#noOfHours').val());
    // var clean_status = $('#cleaningMaterialStatus').val();
    // if(clean_status == 'N') 
    // {
    //       $('.cleaning_material_p').text('NO');
    //       $('#mat_success_p').text('without cleaning materials');
    // } else if(clean_status == 'Y') 
    // {
    //       $('.cleaning_material_p').text('YES');
    //       $('#mat_success_p').text('with cleaning materials');
    // }
    // $('.hrs_p').text($('#noOfHours').val());
});

function amountCalculation()
{
    var category = $('#service_category').val();
    if(category == 2) {
        var subCategory = 0;
        var bedroom =  1; 
        var furnished = 0;
        var is_scrubbing = 0;
    }else if(category == 13) {
        var subCategory = 0;
        var bedroom =  $('#car_id').val();
        var furnished = 0;
        var is_scrubbing = 0;
    }
     else {
        var subCategory =  $("input[name='building_type']:checked").val();
        if(subCategory == 2) {
            var bedroom = $('#villa_id').val();
        } else {
            var bedroom = $('#bedroom_count').val();
        }
        var furnished = 0;
        var is_scrubbing = 0;
    }
    var serviceId = $('#service_id').val();
    var selectedArray = [];
    var coupon = $('.couponClass').val();
    selectedArray.push({service_id : serviceId,category_id : category,sub_category_id : subCategory,name : bedroom,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
    var intialArray = $('#all_amount').val();
    // console.log(selectedArray);
    // result2 =JSON.parse(array2);
 
    $.ajax({
          method: 'post',
          url: '{{url('calculate-load')}}',
          data:{'selectedArray':selectedArray,'intialArray':intialArray,'officesqure':$('#officesqure').val(),'category':category,_token:'{{csrf_token()}}'},  
         
          success: function (result) {
               if (result.status == 'success') {
                   $('.total_p').text(result.data['amount']);
                   if(coupon != '' && coupon != 'undefined') {
                        checkCoupon();
                    } else {
                    $('.vat_p').text(result.data['vat_amount']);
                    $('.net_p').text(result.data['totalAmount']);
                    $('.mobile_net_p').text(result.data['totalAmount']);

                    $('.discount_div').hide();
                        $('#coupon_message').hide();
                    }
                   $('#arrayVal').val(result.data['arrayVal']);
                //    $('#sub_category_id').val(result.data['sub_category_id']);
                //    $('#service_sub_funish_id').val(result.data['service_sub_funish_id']);
                //    $('#is_scrubbing').val(result.data['is_scrubbing']);
                //    $('#name').val(result.data['name']);
                //    $('#service_cost').val(result.data['service_cost']);
                //    $('#status').val(result.data['status']);
                    // $("#submit_button").attr( "disabled", false );
                    // window.location.href = '{{url('cleaning-second')}}'+'/'+result.data['id'];
               }
          }
     }); 
   
    

}
    
    function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement; 
    }



function toggleClass( element ) {
    var classe = 'col-lg-6 col-md-6 col-sm-6 col-6 unselect';

    if ( element.className == classe ){
        element.className = classe.replace('unselect', 'selected');
    } else {
        element.className = classe;
    }
}

    

    
</script>
@endpush