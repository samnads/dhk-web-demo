<?php if(!isset($_GET['mobile'])) {

?>

<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">

    <div class="col-md-12 col-sm-12 em-step-head em-head no-left-right-padding">

        <h2>Book Your Service</h2>

        <ul>

            <li class="active"><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

        </ul>

    </div>

</div>

<?php } ?>

<form id="step1Disinfection" method="post">

    {{ csrf_field() }}

    <input type="hidden" value="" id="noOfHours" name="noOfHours">

    <div class="col-12 em-booking-content-set pl-0 pr-0">

        <div class="row em-booking-content-set-main ml-0 mr-0">

            <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">

                <div class="col-12 step1 pl-0 pr-0">

                    @include('common.services')

                    <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0">

                        <div class="col-12 book-det-cont-set-main disinfection-cleaning-service  ser-cont2 ser-cont" style="display:block;"  id="cont2">

                            <h5>Disinfection Services</h5>

                            <div class="col-sm-12 disinfection-cleaning-home pl-0 pr-0">

                                <div class="col-sm-12 em-field-main-set"> 

                                    <div class="row m-0">

                                        <div class="col-sm-6 em-field-main">

                                            <p>Disinfection Service for</p>

                                            <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                <input type="hidden" id="service_category" value="1">

                                                <ul class="clearfix border-bottom-0  border-right-0 service_for" id="service_for_secondr">



                                                    <li class="selected" id="1">HOME</li>

                                                    <li class="show-disinfection-for-office-btn border-bottom" id="2">OFFICE</li>

                                                    <li class="show-disinfection-for-car-btn border-bottom  border-right" id="13">CAR</li>

                                                </ul> 

                                            </div>

                                        </div>

                                        <div class="col-sm-6 em-field-main how-often em-width-auto">

                                            <p>What is your building type?</p>

                                            <ul>

                                                <li>

                                                    <input id="building-type1" value="1" name="building_type" class="buildingType" checked="" type="radio">

                                                    <label for="building-type1"> <span></span> &nbsp; Apartment</label>

                                                </li>

                                                <li>

                                                    <input id="building-type2" value="2" name="building_type" class="buildingType" type="radio">

                                                    <label for="building-type2"> <span></span> &nbsp; Villa</label>

                                                </li>

                                            </ul>

                                        </div>

                                    </div> 

                                </div>

                                <div class="col-sm-12 em-field-main-set pt-1"><!--em-often-section-box -->

                                    <div class="row m-0">

                                        <div class="col-sm-6 em-field-main" id="apart_div">

                                            <p>How many bedroom home you have?</p>

                                            <div class="col-12 em-text-field-main em-box-1-5 how-many-bedroom pl-0 pr-0">

                                                <input type="hidden" id="bedroom_count" value="STUDIO">

                                                <ul id="bedroom_ul" class="clearfix border-0">

                                                   <li class="border border-right-0 selected">STUDIO</li>

                                                   <li class="border  border-right-0">1</li>

                                                   <li class="border  border-right-0">2</li>

                                                   <li class="border">3</li>

                                                   <!-- <li>4</li>

                                                   <li>5</li> -->

                                               </ul>

                                            </div>

                                        </div>

                                        <div class="col-sm-6 em-field-main" id="villa_div" style="display:none;">

                                            <p>How many room you have?</p>

                                            <div class="col-12 em-text-field-main number-of-hours em-box-8 pl-0 pr-0">

                                                <input type="hidden" id="villa_id" value="2">

                                                <ul class="clearfix border-0" id="villa_ul">

                                                    <li class="border border-right-0 selected">2</li>

                                                    <li class="border border-right-0">3</li>

                                                    <li class="border border-right-0">4</li>

                                                    <li class="border">5</li>

                                                    <!-- <li>6</li>

                                                    <li>7</li>

                                                    <li>8</li>

                                                    <li>9</li> -->

                                                </ul>

                                            </div>

                                        </div>

                                    </div>     

                                </div>

                            </div><!--disinfection-cleaning-home end-->

                            <div class="col-sm-12 disinfection-cleaning-car pl-0 pr-0">

                                <div class="col-sm-12 em-field-main-set"> 

                                    <div class="row m-0">

                                        <div class="col-sm-6 em-field-main">

                                            <p>Disinfection Service for</p>

                                            <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                <input type="hidden" id="service_category" value="1">

                                                <ul class="clearfix border-bottom-0  border-right-0 service_for" id="service_for_secondr2">

                                                    <li class="show-disinfection-for-home-btn" id="1">HOME</li>

                                                    <li class="show-disinfection-for-office-btn border-bottom" id="2">OFFICE</li>

                                                    <li class="selected" id="13">CAR</li>

                                                </ul> 

                                            </div>

                                        </div>

                                        <div class="col-sm-6 em-time-section-box pt-3">

                                        <p>How many cars?</p>

                                            <div class="col-12 em-text-field-main number-of-hours em-box-8 pl-0 pr-0">

                                                <input type="hidden" id="car_id" value="1">

                                                <ul class="clearfix" id="car_ul">

                                                    <li class="selected">1</li>

                                                    <li>2</li>

                                                    <li>3</li>

                                                    <li>4</li>

                                                    <li>5</li>

                                                    <li>6</li>

                                                    <li>7</li>

                                                    <li>8</li>

                                                    <!-- <li>9</li> -->

                                                </ul>

                                            </div>

                                        </div>

                                    </div> 

                                </div>

            

                            </div><!--disinfection-cleaning-home end-->

                            <div class="col-sm-12 disinfection-cleaning-office pl-0 pr-0">

                                <div class="col-sm-12 em-field-main-set"> 

                                    <div class="row m-0">

                                        <div class="col-sm-6 em-field-main">

                                            <p>Disinfection Service for</p>

                                            <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                <ul class="clearfix border-bottom-0  border-right-0 service_for" id="service_for">

                                                    <li class="show-disinfection-for-home-btn" id="1">HOME</li>

                                                    <li class="selected" id="2">OFFICE</li>

                                                    <li class="show-disinfection-for-car-btn border-bottom  border-right" id="13">CAR</li>

                                                </ul>

                                            </div>

                                        </div>

                                        <div class="col-sm-6 em-time-section-box pt-3">

                                            <p>What is the size of your office?</p>

                                            <!-- <input type="hidden" id="officesqure" value="600"> -->

                                            <select class="js-select2" id="officesqure" onChange="amountCalculation();">

                                                <option>Select Square Feet</option>

                                                @for ($i = 300; $i <= 25000; $i+=10)

                                                <option value="{{$i}}" @if($i == '300') selected @endif>{{$i}} square feet</option>

                                                @endfor

                                            </select>

                                        </div>

                                    </div> 

                                </div>

                            </div><!--disinfection-cleaning-office end-->

                        </div>

                        <div class="col-sm-12 em-terms-and-condition-main"> 

                            <h6>What to expect from the service?</h6>

                            <div class="col-sm-12 em-terms-and-condition">

                                <ul class="">

                                    <li>

                                        <div class="em-num">1</div>

                                        <div class="em-num-cont">
                                        <p>Please note that we will do our very best to arrive on the given time nonetheless due to traffic conditions please anticipate maximum 30 minutes delay. Please note that we will complete the full hours of booking upon arrival.</p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>

                                    <li>

                                        <div class="em-num">2</div>

                                        <div class="em-num-cont">
                                        <!-- <p>We have invested heavily in the latest cleaning technologies to ensure we are able to deliver cleaning and decontamination services to the highest level and at the quickest speed.</p> -->
                                        <p>Any cancellations with less than 24 hours’ notice is non-refundable.</p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>

                                    <li>

                                        <div class="em-num">3</div>

                                        <div class="em-num-cont">
                                        <!-- <p>Once the environment has been contained we remove all waste safely and dispose it as clinical waste keeping the whole area clean and infection free.</p> -->
                                        <p>For any questions and clarifications on your booking please whatsApp us at <a target="_blank" href="https://api.whatsapp.com/send?phone=971543502510">+971 543502510</a> or email us at <a href="mailto:office@dubaihousekeeping.com">office@dubaihousekeeping.com</a></p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>
                                    <li>

                                    <div class="em-num">4</div>

                                    <div class="em-num-cont">
                                    <p>We have invested heavily in the latest cleaning technologies to ensure we are able to deliver cleaning and decontamination services to the highest level and at the quickest speed.</p>
                                    </div>

                                    <div class="clear"></div>

                                    </li>	
                                    <li>

                                    <div class="em-num">5</div>

                                    <div class="em-num-cont">
                                    <p>Your Emergency is our Everyday. If there are reported COVID-19 cases at your premises, our team will respond immediately to your needs.</p>
                                    </div>

                                    <div class="clear"></div>

                                    </li>	
                                    <li>

                                    <div class="em-num">6</div>

                                    <div class="em-num-cont">
                                    <p>Once the environment has been contained we remove all waste safely and dispose it as clinical waste keeping the whole area clean and infection free.</p>
                                    </div>

                                    <div class="clear"></div>

                                    </li>	
                                    <!-- <li>

                                    <div class="em-num">7</div>

                                    <div class="em-num-cont">
                                    <p>Your Emergency is our Everyday. If there are reported COVID-19 cases at your premises, our team will respond immediately to your needs.</p>
                                    </div>

                                    <div class="clear"></div>

                                    </li>	 -->
                                    <li>

                                        <div class="em-num">7</div>

                                        <div class="em-num-cont">
                                        <!-- <p>Your Emergency is our Everyday. If there are reported COVID-19 cases at your premises, our team will respond immediately to your needs.</p> -->
                                        <p>Dubai Municipality approved service for home Sanitization using 100% safe chemical to sanitize the apartment/villa against known viruses and bacteria.</p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>		
                                    <li>

                                        <div class="em-num">8</div>

                                        <div class="em-num-cont">
                                        <p> This service includes: spraying the apartment using spraying machines with approved Dubai Municipality chemical covering the following: Walls, Floors, Furniture, Wall fixtures, Kitchen surfaces and appliances, Bathroom, Spraying the following with hand held spray: Doors door knobs, Switches, touch points, Wiping all residues of chemical</p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>	
                                    <li>

                                        <div class="em-num">9</div>

                                        <div class="em-num-cont">
                                        <p> This service does not include: House cleaning</p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>				
			

                                </ul>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 em-booking-det-cont em-next-btn">

                        <div class="row em-next-btn-set ml-0 mr-0">

                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">

                                <!--<span class="em-back-arrow " title="Previous Step"></span>-->

                                <div class="col-12 sp-total-price-set pl-0 pr-0">

                                    Total<br>

                                    <span>AED <b class="mobile_net_p"></b></span> 

                                </div>

                                

                            </div>

                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">

                                <input value="Next" class="text-field-button show2-step" id="submit_button" type="button">

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            @include('includes.steps.summary')

        </div>

    </div>

</form>

<style>

    .disinfection-cleaning-car { padding-bottom: 15px;}

    .disinfection-cleaning-car { width: 100% !important; display: none;}

#applePaybtn {  
	width: 150px;  
	height: 45px;  
	display: none;   
	border-radius: 5px;    
	margin-left: auto;
	margin-right: auto;
	background-image: -webkit-named-image(apple-pay-logo-white); 
	background-position: 50% 50%;
	background-color: black;
	background-size: 60%; 
	background-repeat: no-repeat;  
}

</style>

@push('scripts')

<script>

$('.show2-step').click(function(){

    $('#commonStep1').fadeOut(300);

    $('#commonStep2').fadeIn(300);

    $('.timeSelection li:first').trigger("click");

    $('.date_time').show();

    $('header .logo').addClass('logo_left');

    $("html").animate({ scrollTop: 0 }, "slow");

}); 

</script>

@endpush



























