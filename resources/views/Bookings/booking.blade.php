@extends('layouts.app')
@section('content')
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0">
                    <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                        <div class="col-sm-12 em-step-head no-left-right-padding">
                            <h2>My Bookings</h2>
                            <span id="success_info" style="display:none;"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 em-booking-content-set ">
                        <div class="row my-book-tab-hed m-0">
                            <div class="col-4 my-book-tab em-tab1 active pl-0 pr-0 ser-but" data-id="cont1">
                                <p>Scheduled</p>
                            </div>
                            <div class="col-4 my-book-tab em-tab2 pl-0 pr-0 ser-but" data-id="cont2">
                                <p>Past</p>
                            </div>
                            <div class="col-4 my-book-tab em-tab2 pl-0 pr-0 ser-but" data-id="cancelled">
                                <p>Cancelled</p>
                            </div>
                        </div>
                        <div class="col-sm-12 my-book-content-tab-main pl-0 pr-0">
                            <div id="cont1" class="col-sm-12 my-book-cont-tab-main em-tab-cont1 ser-cont p-0">
                                <div class="row bg-white m-0">
                                    <input type="hidden" id="checkout_secret"
                                        value="{{ Config::get('values.checkout_primary_key') }}"> <input type="hidden"
                                        id="apple_merchant_id" value="{{ Config::get('values.apple_merchant_id') }}">
                                    @foreach ($data['upcoming'] as $booking)
                                        <div class="col-sm-6 my-book-tmbnil-main p-0">
                                            <div class="col-sm-12 my-book-tmbnil-cont-main">
                                                <h5>{{ $booking['service'] }} <span>Ref: {{ $booking['reference_id'] ?: $booking['booking_id']}}
                                                    </span>
                                                </h5>
                                                <p>{{ $booking['service_start_date'] }} {{ $booking['time_from'] }} (
                                                    {{ $booking['type'] }} ) </p>
                                                <p>Amount : AED <b>{{number_format($booking['total_amount'] + ($booking['total_amount']*.03),2,'.',',')}}</b></p>
                                                 @if ($booking['payButton'] == 'active')
                                                <p>Amount includes Convenience Fee AED <b>{{number_format($booking['total_amount']*.03,2,'.',',')}}</b></p>
                                                @endif
                                                <div class="col-sm-12 my-book-tmbnil-btn pl-0 pr-0">
                                                    <ul class="clearfix">
                                                        @if ($booking['payButton'] == 'active')
                                                            <li><a onclick="paynow(<?php echo $booking['booking_id']; ?>);"
                                                                    class="paynow cursor">PAY NOW</a></li>
                                                        @endif
                                                        <!-- <li><a href="javascript:void(0);" class="invoice em-bok-his-show-btn">INVOICE</a></li> -->
                                                        <li><a onclick="cancelPopUp(<?php echo $booking['booking_id']; ?>);"
                                                                class="cancel cursor">CANCEL</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @if ($data['upcoming'] == [])
                                    <div class="col-12">
                                        <p class="text-center p-3" style="color: #62a6c9;font-size: 19px;">
                                            <span>No Scheduled bookings</span>
                                        </p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div id="cont2" class="col-sm-12 my-book-cont-tab-main em-tab-cont2 ser-cont p-0">
                                <div class="row bg-white m-0">
                                    @foreach ($data['past'] as $booking)
                                        <div class="col-sm-6 my-book-tmbnil-main p-0">
                                            <div class="col-sm-12 my-book-tmbnil-cont-main">
                                                <h5>{{ $booking['service'] }} <span>Ref: @if (isset($booking['reference_id']))
                                                            {{ $booking['reference_id'] }}
                                                        @endif
                                                    </span></h5>
                                                <p>{{ $booking['service_date'] }} {{ $booking['time_from'] }} (
                                                    {{ $booking['type'] }})</p>
                                            </div>
                                        </div>
                                    @endforeach
                                    @if ($data['past'] == [])
                                        <div class="col-12">
                                        <p class="text-center p-3" style="color: #62a6c9;font-size: 19px;">
                                            <span>No past bookings</span>
                                        </p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            {{-- --}}
                            <div id="cancelled" class="col-sm-12 my-book-cont-tab-main em-tab-cont2 ser-cont p-0">
                                <div class="row bg-white m-0">
                                    @foreach ($data['cancelled'] as $cancelled)
                                        <div class="col-sm-6 my-book-tmbnil-main p-0">
                                            <div class="col-sm-12 my-book-tmbnil-cont-main">
                                                <h5>{{ $cancelled['service_type_name'] }} <span>Ref: {{ $cancelled['reference_id'] ?: $cancelled['booking_id'] }}</span></h5>
                                                <p>{{ date('j M, Y', strtotime($cancelled['service_start_date'])) }} {{ date("g:i A", strtotime($cancelled['time_from'])) }} (
                                                    {{ $cancelled['booking_type_name'] }})</p>
                                                <p>Amount : AED <b>{{number_format($cancelled['total_amount'],2,'.',',')}}</b></p>
                                                <p>Payment Mode : <b>{{$cancelled['pay_by'] ?: 'Cash'}}</b></p>
                                                <div class="col-sm-12 my-book-tmbnil-btn pl-0 pr-0">
                                                    <ul class="clearfix">
                                                        @if($cancelled['online_payment_payment_status'] == "success")
                                                        <li><a onclick="return false;" class="cancel bg-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;PAID</a></li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @if ($data['cancelled'] == [])
                                        <div class="col-12">
                                        <p class="text-center p-3" style="color: #62a6c9;font-size: 19px;">
                                            <span>No cancelled bookings</span>
                                        </p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            {{-- --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
    <div class="col-sm-12 popup-main booking-history-popup">
        <div class="row min-vh-100 d-flex flex-column justify-content-center">
            <div class="col-lg-3 col-md-4 col-sm-6 popup-content em-booking-det-cont mx-auto book-his-pop-main shadow">
                <div class="em-booking-content-right">
                    <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">
                        <h5>BOOKING SUMMARY <span class="em-bok-his-close-btn"><img src="images/el-close-black.png"
                                    title=""></span></h5>
                        <div class="col-sm-12 book-details-main service-type">
                            <div class="row ml-0 mr-0 text-capital">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p class="text-black"><strong>Status</strong></p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p class="text-red"><strong>Cancelled</strong></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 book-details-main">
                            <div class="row ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>Service Type</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>House Cleaning</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 book-details-main">
                            <div class="row ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>Material</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>Yes</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 book-details-main">
                            <div class="row ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>Duration</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>3 hours</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 book-details-main">
                            <div class="row ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>Frequency</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>One-time</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 book-details-main mb-3">
                            <div class="row ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>Number of Cleaners</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>5</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                        <h6>Date & Time </h6>
                        <div class="col-sm-12 book-details-main">
                            <div class="row ml-0 mr-0">
                                <div class="col-5 book-det-left pl-0 pr-0">
                                    <p>Date</p>
                                </div>
                                <div class="col-7 book-det-right pl-0 pr-0">
                                    <p>Tuesday, 15th August</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 book-details-main mb-3">
                            <div class="row ml-0 mr-0">
                                <div class="col-5 book-det-left pl-0 pr-0">
                                    <p>Time</p>
                                </div>
                                <div class="col-7 book-det-right pl-0 pr-0">
                                    <p>10:00 am - 01:00 pm</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                        <h6>Address</h6>

                        <div class="col-sm-12 book-details-main date-time-det mb-3">
                            <div class="row ml-0 mr-0">
                                <div class="col-sm-12 book-det-left pl-0 pr-0">
                                    <p>Discovery Gardens, test building<br />11, test street</p>
                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                        <h6>Price Details</h6>


                        <div class="col-sm-12 book-details-main">
                            <div class="row ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>Price</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>AED 93.33</p>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12 book-details-main">
                            <div class="row ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>VAT 5%</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>AED 4.67</p>
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-12 book-details-main">
                            <div class="row total-price ml-0 mr-0">
                                <div class="col-7 book-det-left pl-0 pr-0">
                                    <p>Total</p>
                                </div>
                                <div class="col-5 book-det-right pl-0 pr-0">
                                    <p>AED 98.00</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div><!--popup-main end-->
    <div class="col-sm-12 popup-main remove-address-popup" id="confirm_cancel">
        <div class="row min-vh-100 d-flex flex-column justify-content-center">
            <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <h5 class="">Cancel Booking<span class="remove-addresss-close-btn"><img
                            src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>
                <input type="hidden" id="bookingId" value="">
                <p class="text-left">Are you sure want to cancel this booking.</p>
                <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                    <input value="Cancel" class="text-field-button show6-step delete" onclick="cancelBooking()"
                        type="button">
                </div>
            </div>
        </div>
    </div><!--popup-main end-->
    <div class="col-sm-12 popup-main" id="payment_popup">
        <div class="row min-vh-100 d-flex flex-column justify-content-center">
            <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <form id="payment-form" method="POST" action="{{ url('make-pay-checkout') }}">
                    {{ csrf_field() }}
                    <h5 class="">Make Payment <span class="em-forgot-close-btn"><img
                                src="{{ asset('images/el-close-black.png') }}" title="" style=""></span>
                    </h5>
                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                        <div class="row m-0">
                            <input type="hidden" id="token_req" name="token_req" value=''>
                            <input type="hidden" name="order_id" id="order_id">
                            <input type="hidden" name="amount" id="amount">
                            <input type="hidden" name="currency" value="AED" />
                            <input type="hidden" id="retry_online" name="retry_online" value="online">
                            <input type="hidden" name="billing_zip" value="" />
                            <input type="hidden" name="billing_country" value="UAE" />
                            <input type="hidden" name="customer_name" id="billing_name">
                            <input type="hidden" name="billing_tel" id="billing_tel">
                            <input type="hidden" name="customer_email" id="billing_email">
                            <input type="hidden" name="billing_address" id="billing_address">
                            <input type="hidden" name="billing_state" id="billing_state" value="Dubai" />
                            <input type="hidden" name="billing_city" id="billing_city" value="Dubai" />
                            <?php if (isset($_GET['mobile'])) {
                                $mobileParam = '?mobile=active';
                            }
                            ?>
                            <?php if (!isset($_GET['mobile'])) {
                                $mobileParam = '';
                            }
                            ?>
                            <input type="hidden" name="mobile_view" id="mobile_view" value="{{ $mobileParam }}" />
                            <div class="one-liner">
                                <div class="card-frame">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                        <button id="pay-button" disabled>
                            PAY AED <span id="amount_span"></span> </button>
                    </div>
                </form>
            </div>
        </div>
    </div><!--popup-main end-->
    <form method="post" id="ccavenue_form"
        action="{{config('payment.ccavenue_req_handler')}}?{{ @$query_params }}"
        style="display: none">
        {!! csrf_field() !!}
        <table width="40%" height="100" border='1' align="center">
            <caption>
                <font size="4" color="blue"><b>Integration Kit</b></font>
            </caption>
        </table>
        <table width="40%" height="100" border='1' align="center">
            <tr>
                <td>Parameter Name:</td>
                <td>Parameter Value:</td>
            </tr>
            <tr>
                <td colspan="2"> Compulsory information</td>
            </tr>
            <tr>
                <td>Merchant Id :</td>
                <td><input type="text" name="merchant_id" value="{{config('payment.ccavenue_merchant_id')}}" /></td>
            </tr>
            <tr>
                <td>Order Id :</td>
                <td><input type="text" name="order_id" value="" /></td>
            </tr>
            <tr>
                <td>Amount :</td>
                <td><input type="text" name="amount" value="" /></td>
            </tr>
            <tr>
                <td>Currency :</td>
                <td><input type="text" name="currency" value="AED" /></td>
            </tr>
            <tr>
                <td>Redirect URL :</td>
                <td><input type="text" name="redirect_url"
                        value="{{config('payment.ccavenue_resp_handler')}}?{{ @$query_params }}" />
                </td>
            </tr>
            <tr>
                <td>Cancel URL :</td>
                <td><input type="text" name="cancel_url"
                        value="{{config('payment.ccavenue_resp_handler')}}?{{ @$query_params }}" />
                </td>
            </tr>
            <tr>
                <td>Language :</td>
                <td><input type="text" name="language" value="EN" /></td>
            </tr>
            <tr>
                <td colspan="2">Billing information(optional):</td>
            </tr>
            <tr>
                <td>Billing Name :</td>
                <td><input type="text" name="billing_name" value="" /></td>
            </tr>
            <tr>
                <td>Billing Address :</td>
                <td><input type="text" name="billing_address" value="" /></td>
            </tr>
            <tr>
                <td>Billing City :</td>
                <td><input type="text" name="billing_city" value="" /></td>
            </tr>
            <tr>
                <td>Billing State :</td>
                <td><input type="text" name="billing_state" value="" /></td>
            </tr>
            <tr>
                <td>Billing Zip :</td>
                <td><input type="text" name="billing_zip" value="" /></td>
            </tr>
            <tr>
                <td>Billing Country :</td>
                <td><input type="text" name="billing_country" value="United Arab Emirates" /></td>
            </tr>
            <tr>
                <td>Billing Tel :</td>
                <td><input type="text" name="billing_tel" value="" /></td>
            </tr>
            <tr>
                <td>Billing Email :</td>
                <td><input type="text" name="billing_email" value="" /></td>
            </tr>
            <tr>
                <td></td>
                <td><INPUT TYPE="submit" value="CheckOut"></td>
            </tr>
        </table>
    </form>
@endsection
@push('scripts')
    <script>
        $('.em-forgot-close-btn').click(function() {
            $('#payment_popup').hide(500);
        });
        $('.remove-addresss-close-btn').click(function() {
            $('#confirm_cancel').hide(500);
        });

        function cancelPopUp(bookingId) {
            $('#confirm_cancel').show(300);
            $('#bookingId').val(bookingId);
        }

        function cancelBooking() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var bookingId = $('#bookingId').val();
            $(".preloader").show(100);
            $.ajax({
                method: 'post',
                url: '{{ url('cancel-booking') }}',
                data: {
                    'bookingId': bookingId,
                    _token: '{{ csrf_token() }}'
                },

                success: function(result) {
                    if (result.status == 'success') {
                        $('#confirm_cancel').show(300);
                        $('#success_info').show();
                        $("#success_info").css("color", "green");
                        $('#success_info').html(result.messages);
                        setTimeout(function() {
                            $('#success_info').hide();
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }

        function paynow(bookingId) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".preloader").show();
            $.ajax({
                method: 'post',
                url: '{{ url('bookinglist-payment') }}',
                data: {
                    'bookingId': bookingId,
                    _token: '{{ csrf_token() }}'
                },
                success: function(result) {
                    if (result.status == 'success') {
                        console.table(result.data['bookings']);
                        $(".preloader").hide();
                        $('#order_id').val(result.data['payment_id']);
                        $('#amount').val(result.data['gross_amount']);
                        $('#billing_name').val(result.data['customer_name']);
                        $('#billing_tel').val(result.data['phone_number']);
                        $('#billing_email').val(result.data['customer_email']);
                        $('#merchant_param1').val(result.data['customer_email']);
                        $('#merchant_param2').val(result.data['payment_id']);
                        $('#billing_address').val(result.data['address']);
                        $('#amount_span').html(result.data['gross_amount']);
                        //$('#payment_popup').show(500);
                        // document.getElementById('payment_now_form').submit();
                        //  $(".preloader").fadeOut(100);
                        /******************************************************************/
                        // for cc-avenue form only
                        $("#ccavenue_form input[name='order_id']").val(result.data['bookings']['reference_id'] || result.data['bookings']['booking_id']);
                        $("#ccavenue_form input[name='amount']").val((result.data['bookings']['total_amount'] + ((3/100)*result.data['bookings']['total_amount']))*result.data['bookings']['no_of_maids']);
                        $("#ccavenue_form input[name='redirect_url']").val();
                        $("#ccavenue_form input[name='cancel_url']").val();
                        $("#ccavenue_form input[name='billing_name']").val(result.data['customer']['customer_name']);
                        $("#ccavenue_form input[name='billing_address']").val(result.data['customer_address']['customer_address']);
                        //$("#ccavenue_form input[name='billing_city']").val();
                        //$("#ccavenue_form input[name='billing_state']").val();
                        //$("#ccavenue_form input[name='billing_zip']").val();
                        $("#ccavenue_form input[name='billing_country']").val("United Arab Emirates");
                        $("#ccavenue_form input[name='billing_tel']").val(result.data['customer']['phone_number']);
                        $("#ccavenue_form input[name='billing_email']").val(result.data['customer']['email_address']);
                        $("#ccavenue_form").submit();
                        /******************************************************************/
                    }
                }
            });
        }
    </script>
@endpush
