@extends('layouts.app')
@section('title')
    {{ 'Payment Failed' }}
@endsection
@section('content')
<form method="post" id="ccavenue_form" action="{{config('payment.ccavenue_req_handler')}}?{{@$query_params}}" style="display: none">
    {!! csrf_field() !!}
    <table width="40%" height="100" border='1' align="center">
        <caption>
            <font size="4" color="blue"><b>Integration Kit</b></font>
        </caption>
    </table>
    <table width="40%" height="100" border='1' align="center">
        <tr>
            <td>Parameter Name:</td>
            <td>Parameter Value:</td>
        </tr>
        <tr>
            <td colspan="2"> Compulsory information</td>
        </tr>
        <tr>
            <td>Merchant Id :</td>
            <td><input type="text" name="merchant_id" value="{{config('payment.ccavenue_merchant_id')}}" /></td>
        </tr>
        <tr>
            <td>Order Id :</td>
            <td><input type="text" name="order_id" value="" /></td>
        </tr>
        <tr>
            <td>Amount :</td>
            <td><input type="text" name="amount" value="" /></td>
        </tr>
        <tr>
            <td>Currency :</td>
            <td><input type="text" name="currency" value="AED" /></td>
        </tr>
        <tr>
            <td>Redirect URL :</td>
            <td><input type="text" name="redirect_url"
                    value="{{config('payment.ccavenue_resp_handler')}}?{{@$query_params}}" /></td>
        </tr>
        <tr>
            <td>Cancel URL :</td>
            <td><input type="text" name="cancel_url"
                    value="{{config('payment.ccavenue_resp_handler')}}?{{@$query_params}}" /></td>
        </tr>
        <tr>
            <td>Language :</td>
            <td><input type="text" name="language" value="EN" /></td>
        </tr>
        <tr>
            <td colspan="2">Billing information(optional):</td>
        </tr>
        <tr>
            <td>Billing Name :</td>
            <td><input type="text" name="billing_name" value="" /></td>
        </tr>
        <tr>
            <td>Billing Address :</td>
            <td><input type="text" name="billing_address" value="" /></td>
        </tr>
        <tr>
            <td>Billing City :</td>
            <td><input type="text" name="billing_city" value="" /></td>
        </tr>
        <tr>
            <td>Billing State :</td>
            <td><input type="text" name="billing_state" value="" /></td>
        </tr>
        <tr>
            <td>Billing Zip :</td>
            <td><input type="text" name="billing_zip" value="" /></td>
        </tr>
        <tr>
            <td>Billing Country :</td>
            <td><input type="text" name="billing_country" value="" /></td>
        </tr>
        <tr>
            <td>Billing Tel :</td>
            <td><input type="text" name="billing_tel" value="" /></td>
        </tr>
        <tr>
            <td>Billing Email :</td>
            <td><input type="text" name="billing_email" value="" /></td>
        </tr>
        <tr>
            <td>Merchant Param 1 :</td>
            <!-- success url prefix -->
            <td><input type="text" name="merchant_param1" value="" /></td>
        </tr>
        <tr>
            <td>Merchant Param 2 :</td>
            <!-- failed url prefix -->
            <td><input type="text" name="merchant_param2" value="" /></td>
        </tr>
        <tr>
            <td></td>
            <td><INPUT TYPE="submit" value="CheckOut"></td>
        </tr>
    </table>
</form>
    <div class="success">
        <section class="em-booking-content-section">
            <div style="display:none;">
                <input type="hidden" name="order_id" id="ap_order_id" value="{{ @$data['subscription']['reference_id'] }}">
                <input type="hidden" name="amount" id="ap_amount"
                    value="{{ $data['payment']['amount'] + $data['payment']['transaction_charge'] }}">
                <input type="hidden" name="customer_name" id="ap_billing_name"
                    value="{{ @$data['customer']['customer_name'] }}">
                <input type="hidden" name="customer_tel" id="ap_billing_tel"
                    value="{{ @$data['customer']['mobile_number_1'] }}">
                <input type="hidden" name="customer_email" id="ap_billing_email"
                    value="{{ @$data['customer']['email_address'] }}">
                <input type="hidden" name="customer_address" id="ap_billing_address"
                    value="{{ @$data['customer_address']['customer_address'] }}">
            </div>
            <div class="container em-booking-content-box">
                <div class="row em-booking-content-main ml-0 mr-0">
                    <div class="col-12 em-booking-content pl-0 pr-0 min-vh-90 d-flex flex-column justify-content-center">
                        <div class="col-12 em-booking-content-set pl-0 pr-0 mx-auto">
                            <div class="row em-booking-content-set-main ml-0 mr-0">

                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pl-0 pr-0 d-sm-block d-lg-none">
                                    &nbsp;
                                </div>

                                <div class="col-lg-5 col-md-12 col-sm-12 success-left payment-fail pl-0">
                                    <div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0">

                                        <div class="col-sm-12 success-message pr-0">
                                            <h3>Your payment failed. <br><span>please try again.</span><br>
                                                <span>Ref Number :
                                                    <i>{{ @$data['subscription']['reference_id'] }}</i></span>
                                            </h3>
                                        </div>
                                        <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">
                                            <h6>PERSONAL DETAILS</h6>
                                            <div class="col-sm-12 book-details-main service-type">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Name</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="name_p">{{ @$data['customer']['customer_name'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Email ID</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="mail_p">{{ @$data['customer']['email_address'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Contact Number</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">{{ @$data['customer']['mobile_number_1'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Address</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p><span class="address_p">
                                                                @if ($data['customer_address'] != '')
                                                                    {{ @$data['customer_address']['customer_address'] }}
                                                                @else
                                                                    NA
                                                                @endif
                                                            </span>, <span
                                                                class="area_p">{{ @$data['area']['areaName'] }}</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Payment Mode</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">{{ ucfirst(@$data['payment']['payment_mode']) }}
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Payment Status</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">
                                                            {{ ucfirst(@$data['payment']['payment_status']) }}
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <input type="hidden" id="checkout_secret"
                                            value="{{ Config::get('values.checkout_primary_key') }}">
                                        <div class="col-sm-12 book-details-main-set pl-0 pr-0">

                                            <h6>Service Details</h6>

                                            <div class="col-sm-12 book-details-main date-time-det mb-1">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-sm-12 book-det-left pl-0 pr-0">

                                                        <p>You've booked your package
                                                            <strong>{{ @$data['package']['package_name'] }}</strong> on

                                                            <?php
                                                            
                                                            $date = @$data['subscription']['service_date'];
                                                            
                                                            $old_date_timestamp = strtotime($date);
                                                            
                                                            $dateFormat = date('l jS, F', $old_date_timestamp);
                                                            
                                                            ?>

                                                            <strong class="date_p">{{ $dateFormat }}.</strong>

                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-sm-12 book-details-main-set pl-0 pr-0">

                                            <h6>Price Details</h6>

                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-7 book-det-left pl-0 pr-0">
                                                        <p>Price</p>
                                                    </div>

                                                    <div class="col-5 book-det-right pl-0 pr-0">
                                                        <p>AED <span
                                                                class="total_p text-success">{{ number_format(@$data['payment']['amount'], 2, '.', '') }}</span>
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>

                                            @if (
                                                $data['subscription']['coupon_id'] != '' &&
                                                    $data['subscription']['coupon_id'] != null &&
                                                    $data['subscription']['coupon_id'] != 0)
                                                <div class="col-sm-12 book-details-main">

                                                    <div class="row ml-0 mr-0">

                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>Discount Price</p>
                                                        </div>

                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p>AED
                                                                <span>{{ number_format(@$data['subscription']['discount'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>

                                                    </div>

                                                </div>
                                            @endif

                                            @if(@$data['payment']['transaction_charge'] > 0)
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-7 book-det-left pl-0 pr-0">
                                                        <p>Convenience Fee</p>
                                                    </div>
                                                    <div class="col-5 book-det-right pl-0 pr-0">
                                                        <p>AED <span
                                                                class="vat_p text-success">{{ number_format(@$data['payment']['transaction_charge'], 2, '.', '') }}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="col-sm-12 book-details-main">

                                                <div class="row total-price ml-0 mr-0">

                                                    <div class="col-7 book-det-left pl-0 pr-0">
                                                        <p>Total</p>
                                                    </div>

                                                    <div class="col-5 book-det-right pl-0 pr-0">
                                                        <p>AED <span
                                                                class="net_p text-success">{{ number_format(@$data['payment']['total_amount'], 2, '.', '') }}</span>
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row total-price ml-0 mr-0">
                                                    <div class="col-sm-12 em-field-main text-center">
                                                        @if ($data['payment']['payment_mode'] == 'applepay')
                                                            <button type="button" id="applePaybtn"
                                                                class="text-field-button col-sm-6"
                                                                style="display:non;"></button>
                                                        @elseif ($data['payment']['payment_mode'] != 'cash')
                                                            <button id="retry-payment"
                                                                data-refid="{{ $data['payment']['reference_id'] }}"
                                                                data-transid="{{ $data['payment']['transaction_id'] }}"
                                                                data-mode="{{ $data['payment']['payment_mode'] }}"
                                                                class="text-field-button show6-step" type="button">
                                                                Retry Payment
                                                            </button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pr-0 d-none d-md-none d-lg-block">
                                    &nbsp;</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 em-bottom-space">&nbsp;</div>
            <!--page bottom white space-->
        </section>
    </div>
    <div class="col-sm-12 popup-main" id="payment_popup">
        <div class="row min-vh-100 d-flex flex-column justify-content-center">
            <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <form id="payment-form" method="POST" action="{{ url('package/payment/card/checkout') }}">
                    {{ csrf_field() }}
                    <h5 class="">Retry Payment <span class="em-forgot-close-btn"><img
                                src="{{ asset('images/el-close-black.png') }}" title="" style=""></span>
                    </h5>
                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                        <div class="row m-0">
                            <input type="hidden" id="token_req" name="token_req" value=''>
                            <input type="hidden" name="currency" value="AED">
                            @if (@$data['serviceName'] != 'Other Payments')
                                <?php $amount = $data['payment']['amount'];
                                $name_new = preg_replace('/[^a-zA-Z0-9\s]/', '', $data['customer']['customer_name']);
                                ?>
                                <input type="hidden" name="order_id" id="order_id"
                                    value="{{ $data['payment']['reference_id'] }}">
                            @else
                                <?php $amount = $data['payment']['amount'];
                                $name_new = preg_replace('/[^a-zA-Z0-9\s]/', '', $data['customer']['customer_name']);
                                ?>
                                <input type="hidden" name="order_id" id="order_id"
                                    value="{{ $data['payment']['reference_id'] }}">
                            @endif
                            <?php if (isset($_GET['mobile'])) {
                                $mobileParam = '?mobile=active';
                            }
                            ?>
                            <?php if (!isset($_GET['mobile'])) {
                                $mobileParam = '';
                            }
                            ?>
                            <input type="hidden" id="retry_online" name="retry_online">
                            <input type="hidden" name="amount" id="amount"
                                value="{{ number_format(@$amount, 2, '.', '') }}">
                            <input type="hidden" name="merchant_param2" id="merchant_param2"
                                value="{{ $mobileParam }}">
                            <input type="hidden" name="customer_name" id="billing_name" value="{{ $name_new }}">
                            <input type="hidden" name="customer_tel" id="billing_tel"
                                value="{{ $data['customer']['mobile_number_1'] }}">
                            <input type="hidden" name="customer_email" id="billing_email"
                                value="{{ $data['customer']['email_address'] }}">
                            <?php if (isset($_GET['mobile'])) {
                                $mobileParam = '?mobile=active';
                            }
                            ?>
                            <?php if (!isset($_GET['mobile'])) {
                                $mobileParam = '';
                            }
                            ?>
                            <input type="hidden" name="mobile_view" id="mobile_view" value="{{ $mobileParam }}" />
                            <div class="one-liner">
                                <div class="card-frame">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                        <button id="pay-button" disabled>
                            PAY AED {{ number_format(@$amount, 2, '.', '') }} </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--popup-main end-->
@endsection
@push('styles')
    <link href="{{ url('css/gijgo.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .bg-color {
            background: #fff;
        }

        #applePaybtn {
            width: 150px;
            height: 45px;
            /*display: none;*/
            border-radius: 5px;
            margin-left: auto;
            margin-right: auto;
            background-image: -webkit-named-image(apple-pay-logo-white);
            background-position: 50% 50%;
            background-color: black;
            background-size: 60%;
            background-repeat: no-repeat;
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript" src="https://cdn.checkout.com/js/framesv2.min.js"></script>
    <script async src="https://pay.google.com/gp/p/js/pay.js"></script>
    <script src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"></script>
    <script>
        var debug = true;
        var checkoutpkey = "{{ Config::get('values.checkout_primary_key') }}";
        //var merchantIdentifier = "{{ Config::get('values.apple_merchant_id') }}"; // Live
        var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser.live'; // Demo
        var customer_id = {{ session('customerId') ?: 'null' }};
        var errorStack = [];
        if (window.ApplePaySession) {
            //var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser';
            var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
            promise.then(function(canMakePayments) {
                if (canMakePayments) {
                    logit('ApplePay Supported !');
                } else {
                    logit('ApplePay is possible on this browser, but not currently activated !');
                }
            });
        } else {
            $("#applePaybtn").hide();
            logit('ApplePay is not available on this browser !');
        }
        $.validator.addMethod("customemail",
            function(value, element) {
                return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    .test(value);
            },
            "Please enter a valid email id. "
        );
        $(document).ready(function() {
            $('.login').hide();
            $('.logout').show();
            $('.make_pay_head').show();
            $('.userPersonel').show();
            // $('#preLoader').fadeOut(300);
        });
        // var payButton = document.getElementById("payment_button");
        var payButton = document.getElementById("pay-button");

        var form = document.getElementById("payment-form");
        Frames.init($('#checkout_secret').val());
        Frames.addEventHandler(
            Frames.Events.CARD_VALIDATION_CHANGED,
            function(event) {
                payButton.disabled = !Frames.isCardValid();
            }
        );
        Frames.addEventHandler(
            Frames.Events.CARD_TOKENIZED,
            function(event) {
                $('#token_req').val(event.token);
                $("#payment-form").submit();
            }
        );
        if (form) {
            form.addEventListener("submit", function(event) {
                $(".preloader").show();
                event.preventDefault();
                Frames.submitCard();
            });
        }
        $('#book_payment_button').click(function() {
            $('#payment_popup').show(500);
            $('#retry_online').val('not_online');
        });
        $('#book_payment_online_button').click(function() {
            $('#payment_popup').show(500);
            $('#retry_online').val('payment');
        });
        $('.em-forgot-close-btn').click(function() {
            $('#payment_popup').hide(500);
        });
        $('#retry-payment').click(function() {
            var reference_id = $(this).data("refid");
            var transaction_id = $(this).data("transid");
            var payment_mode = $(this).data("mode");
            $(".preloader").fadeIn(100);
            $.ajax({
                method: 'get',
                url: '{{ url('package/payment/retry') }}/' + reference_id,
                data: {},
                success: function(response) {
                    console.log(response);
                    var data = response.data;
                    if (response.status == 'success') {
                        //window.location.href = result.url;
                        if (data.payment.payment_mode == 'card') {
                            //$(".preloader").fadeOut(100);
                            //$('#payment_popup').show(0);
                            // for cc-avenue form only
                            $("#ccavenue_form input[name='order_id']").val(response.data['subscription']['reference_id']);
                            $("#ccavenue_form input[name='amount']").val(response.data['payment']['total_amount']);
                            $("#ccavenue_form input[name='redirect_url']").val("{{ url('package/payment/ccavenue_response_handler') }}");
                            $("#ccavenue_form input[name='cancel_url']").val("{{ url('package/payment/ccavenue_response_handler') }}");
                            $("#ccavenue_form input[name='billing_name']").val(response.data['customer']['customer_name']);
                            $("#ccavenue_form input[name='billing_address']").val(response.data['customer_address']['customer_address']);
                            //$("#ccavenue_form input[name='billing_city']").val();
                            //$("#ccavenue_form input[name='billing_state']").val();
                            //$("#ccavenue_form input[name='billing_zip']").val();
                            $("#ccavenue_form input[name='billing_country']").val("United Arab Emirates");
                            $("#ccavenue_form input[name='billing_tel']").val(response.data['customer']['phone_number']);
                            $("#ccavenue_form input[name='billing_email']").val(response.data['customer']['email_address']);
                            $("#ccavenue_form input[name='merchant_param1']").val("{{ Config::get('url.package_payment_success_slug_prefix') }}"); // success url prefix
                            $("#ccavenue_form input[name='merchant_param2']").val("{{ Config::get('url.package_payment_failed_slug_prefix') }}"); // fail url prefix
                            $("#ccavenue_form").submit();
                        } else if (data.payment.payment_mode == 'gpay') {
                            var total_amount_payable = (data.payment.amount).toString();
                            var refId = data.payment.reference_id;
                            var email = data.customer.email_address;
                            var name = data.customer.customer_name;
                            const baseRequest = {
                                apiVersion: 2,
                                apiVersionMinor: 0
                            };

                            const tokenizationSpecification = {
                                type: 'PAYMENT_GATEWAY',
                                parameters: {
                                    'gateway': 'checkoutltd',
                                    'gatewayMerchantId': checkoutpkey
                                }
                            };

                            const allowedCardNetworks = ["AMEX", "DISCOVER",
                                "INTERAC", "JCB", "MASTERCARD", "VISA"
                            ];

                            const allowedCardAuthMethods = ["PAN_ONLY",
                                "CRYPTOGRAM_3DS"
                            ];

                            const baseCardPaymentMethod = {
                                type: 'CARD',
                                parameters: {
                                    allowedAuthMethods: allowedCardAuthMethods,
                                    allowedCardNetworks: allowedCardNetworks
                                }
                            };

                            const cardPaymentMethod = Object.assign({
                                    tokenizationSpecification: tokenizationSpecification
                                },
                                baseCardPaymentMethod
                            );

                            const paymentsClient = new google.payments.api.PaymentsClient({
                                environment: '{{ Config::get('values.package_gpay_environment') }}'
                            });

                            const isReadyToPayRequest = Object.assign({},
                                baseRequest);
                            isReadyToPayRequest.allowedPaymentMethods = [
                                baseCardPaymentMethod
                            ];

                            paymentsClient.isReadyToPay(isReadyToPayRequest)
                                .then(function(response) {
                                    if (response.result) {
                                        const button =
                                            paymentsClient.createButton({
                                                onClick: () => console.log(
                                                    'TODO: click handler'
                                                ),
                                                allowedPaymentMethods: []
                                            }); // make sure to provide an allowed payment method
                                        //document.getElementById('gpaybtn').appendChild(button);
                                    }
                                })
                                .catch(function(err) {
                                    $(".preloader").fadeOut(1000);
                                    $("#success_pay_message").text(
                                        "Something went wrong. Try again.");
                                    $("#paymenterrormessagepopup").show();
                                });

                            const paymentDataRequest = Object.assign({},
                                baseRequest);
                            paymentDataRequest.allowedPaymentMethods = [
                                cardPaymentMethod
                            ];
                            paymentDataRequest.transactionInfo = {
                                totalPriceStatus: 'FINAL',
                                totalPrice: total_amount_payable,
                                currencyCode: 'AED',
                                countryCode: 'AE'
                            };
                            paymentDataRequest.merchantInfo = {
                                merchantName: 'Elite Advance Building Cleaning LLC',
                                merchantId: 'BCR2DN4TXLQ3FHS6'
                            };

                            paymentsClient.loadPaymentData(paymentDataRequest).then(
                                function(paymentData) {
                                    console.log(paymentData);
                                    paymentToken = paymentData.paymentMethodData
                                        .tokenizationData.token;
                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $(
                                                'meta[name="csrf-token"]'
                                            ).attr('content')
                                        }
                                    });

                                    $.ajax({
                                        method: 'post',
                                        url: '{{ url('package/payment/gpay/checkout') }}',
                                        data: {
                                            'amount': total_amount_payable,
                                            'reference_id': refId,
                                            'paymentToken': paymentToken,
                                            'email': email,
                                            'name': name,
                                            _token: '{{ csrf_token() }}'
                                        },
                                        success: function(result) {
                                            console.log(result);
                                            $(".preloader").show();
                                            if (result.status == 'success') {
                                                window.location.href = result.url;
                                            } else {
                                                window.location.href = result.url;
                                            }
                                        }
                                    });
                                }).catch(function(err) {
                                console.log(err);
                                $(".preloader").fadeOut(100);
                            });
                            //paymentsClient.prefetchPaymentData(paymentDataRequest);
                        } else if (data.payment.payment_mode == 'applepay') {
                            alert();
                        }
                    } else {
                        $('#payment_popup').hide();
                        $("#success_pay_message").text(response.message);
                        $("#paymenterrormessagepopup").show();
                    }
                },
                error: function(response) {
                    $(".preloader").fadeOut(100);
                    $("#success_pay_message").text("An error occured !");
                    $("#paymenterrormessagepopup").show();
                }
            });
        });
        document.getElementById("applePaybtn").onclick = function(evt) {
            //$("#package-payment-basic-form").submit();
            //$("#payment_popup_applepay").hide();
            var ap_amount = $("#ap_amount").val();

            var runningAmount = ap_amount;
            var runningPP = 0;
            getShippingCosts('domestic_std', true);
            var runningTotal = function() {
                return runningAmount + runningPP;
            }
            var shippingOption = "";

            var subTotalDescr = "Payment against Cleaning Service";

            function getShippingOptions(shippingCountry) {
                logit('getShippingOptions: ' + shippingCountry);
                if (shippingCountry.toUpperCase() == "AE") {
                    shippingOption = [{
                        label: 'Standard Shipping',
                        amount: getShippingCosts('domestic_std', true),
                        detail: '3-5 days',
                        identifier: 'domestic_std'
                    }, {
                        label: 'Expedited Shipping',
                        amount: getShippingCosts('domestic_exp', false),
                        detail: '1-3 days',
                        identifier: 'domestic_exp'
                    }];
                } else {
                    shippingOption = [{
                        label: 'International Shipping',
                        amount: getShippingCosts('international', true),
                        detail: '5-10 days',
                        identifier: 'international'
                    }];
                }
            }

            function getShippingCosts(shippingIdentifier, updateRunningPP) {
                var shippingCost = 0;

                switch (shippingIdentifier) {
                    case 'domestic_std':
                        shippingCost = 0;
                        break;
                    case 'domestic_exp':
                        shippingCost = 0;
                        break;
                    case 'international':
                        shippingCost = 0;
                        break;
                    default:
                        shippingCost = 0;
                }

                if (updateRunningPP == true) {
                    runningPP = shippingCost;
                }

                logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost + "|" + runningPP);

                return shippingCost;
            }

            var paymentRequest = {
                currencyCode: 'AED',
                countryCode: 'AE',
                requiredShippingContactFields: ['postalAddress'],
                lineItems: [{
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    label: 'P&P',
                    amount: runningPP
                }],
                total: {
                    label: 'Elitemaids',
                    amount: runningTotal()
                },
                supportedNetworks: ['amex', 'masterCard', 'visa'],
                merchantCapabilities: ['supports3DS', 'supportsCredit', 'supportsDebit']
            };

            var session = new ApplePaySession(1, paymentRequest);

            // Merchant Validation
            session.onvalidatemerchant = function(event) {
                logit(event);
                var promise = performValidation(event.validationURL);
                promise.then(function(merchantSession) {
                    session.completeMerchantValidation(merchantSession);
                });
            }


            function performValidation(valURL) {
                return new Promise(function(resolve, reject) {
                    var xhr = new XMLHttpRequest();
                    xhr.onload = function() {
                        var data = JSON.parse(this.responseText);
                        logit(data);
                        resolve(data);
                    };
                    xhr.onerror = reject;
                    xhr.open('GET', 'https://elitemaids.emaid.info/applepay/apple_pay_comm.php?u=' +
                        valURL);
                    xhr.send();
                });
            }

            session.onshippingcontactselected = function(event) {
                logit('starting session.onshippingcontactselected');
                logit(
                    'NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.'
                );
                logit(event);

                getShippingOptions(event.shippingContact.countryCode);

                var status = ApplePaySession.STATUS_SUCCESS;
                var newShippingMethods = shippingOption;
                var newTotal = {
                    type: 'final',
                    label: 'Elitemaids',
                    amount: runningTotal()
                };
                var newLineItems = [{
                    type: 'final',
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    type: 'final',
                    label: 'P&P',
                    amount: runningPP
                }];

                session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems);
            }

            session.onshippingmethodselected = function(event) {
                logit('starting session.onshippingmethodselected');
                logit(event);

                getShippingCosts(event.shippingMethod.identifier, true);

                var status = ApplePaySession.STATUS_SUCCESS;
                var newTotal = {
                    type: 'final',
                    label: 'Elitemaids',
                    amount: runningTotal()
                };
                var newLineItems = [{
                    type: 'final',
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    type: 'final',
                    label: 'P&P',
                    amount: runningPP
                }];

                session.completeShippingMethodSelection(status, newTotal, newLineItems);


            }

            session.onpaymentmethodselected = function(event) {
                logit('starting session.onpaymentmethodselected');
                logit(event);

                var newTotal = {
                    type: 'final',
                    label: 'Elitemaids',
                    amount: runningTotal()
                };
                var newLineItems = [{
                    type: 'final',
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    type: 'final',
                    label: 'P&P',
                    amount: runningPP
                }];

                session.completePaymentMethodSelection(newTotal, newLineItems);


            }
            session.onpaymentauthorized = function(event) {
                logit('starting session.onpaymentauthorized');
                logit(
                    'NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object'
                );
                logit(event.payment.token);
                var eventToken = JSON.stringify(event.payment.token.paymentData);
                var ap_amount = $("#ap_amount").val();
                var ap_billing_name = $("#ap_billing_name").val();
                var ap_billing_tel = $("#ap_billing_tel").val();
                var ap_billing_email = $("#ap_billing_email").val();
                var ap_order_id = $("#ap_order_id").val();
                $.ajax({
                    method: 'post',
                    url: '{{ url('package/payment/applepay/checkout') }}',
                    data: {
                        'paymentToken': eventToken,
                        'ap_order_id': ap_order_id,
                        'ap_amount': ap_amount,
                        'ap_billing_name': ap_billing_name,
                        'ap_billing_tel': ap_billing_tel,
                        'ap_billing_email': ap_billing_email,
                        _token: '{{ csrf_token() }}'
                    },

                    success: function(result) {
                        logit(result);
                        if (result.status == 'success') {
                            session.completePayment(ApplePaySession.STATUS_SUCCESS);
                            window.location.href = '{{ url('package/payment/success') }}/' +
                                _payment_ref_id + '?transaction_id=' + result.transaction_id;
                        } else {
                            session.completePayment(ApplePaySession.STATUS_FAILURE);
                            window.location.href = '{{ url('package/payment/failed') }}/' +
                                _payment_ref_id;
                        }
                    }
                });
            }

            function sendPaymentToken(paymentToken) {
                return new Promise(function(resolve, reject) {
                    logit('starting function sendPaymentToken()');

                    logit(
                        "this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;"
                    );
                    logit(
                        "defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like"
                    );
                    if (debug == true)
                        resolve(true);
                    else
                        reject;
                });
            }

            session.oncancel = function(event) {
                window.location.href = '{{ url('package/payment/failed') }}/' + _payment_ref_id;
            }

            session.begin();

        };
        function logit(data) {
            if (debug == true) {
                console.log(data);
            }
        };
        function closepaymenterrorpop() {
            $("#success_pay_message").text("");
            $("#paymenterrormessagepopup").hide();
        }
    </script>
@endpush
