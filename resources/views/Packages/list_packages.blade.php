@extends('layouts.app')
@section('title')
    {{ 'Packages' }}
@endsection
@section('content')
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0 min-vh-90 d-flex flex-column justify-content-center">
                    <div class="row package-listing-main m-0">
                        @foreach ($packages as $package)
                            <div class="col-lg-4 col-md-6 package-list-tmb">
                                <div class="col-sm-12 package-list-tmb-image-main">
                                    <div class="col-12 package-list-tmb-image pl-0 pr-0">
                                        <img src="{{ $package['promotion_image_url'] }}" />
                                    </div>
                                </div>
                                <div class="col-sm-12 package-list-tmb-cont">
                                    <h5>{{ $package['package_name'] }}</h5>

                                    <div class="n-pice-set">
                                        <label
                                            class="n-real-price">&nbsp;AED&nbsp;<span>{{ $package['strikethrough_amount'] }}</span>
                                            &nbsp; </label>
                                        <label>&nbsp;AED&nbsp;<span>{{ $package['amount'] }}</span> &nbsp; </label>
                                    </div>

                                    <a href="#" class="text-field-button package-book"
                                        data-id="{{ $package['package_id'] }}">Book
                                        Now</a>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div>
        <!--page bottom white space-->
    </section>
@endsection
@push('styles')
    <style>
        .bg-color {
            background: #fff;
        }
    </style>
@endpush
@push('scripts')
    <script src="{{ url('js/packages.js?v=').Config::get('version.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var customer_id = {{ session('customerId') ?: 'null' }};
        $.validator.addMethod("customemail",
            function(value, element) {
                return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    .test(value);
            },
            "Please enter a valid email id. "
        );
        $(".package-book").click(function() {
            if (customer_id == null) {
                redirect_to_url = _base_url + "package/view/" + $(this).data("id");
                viewLogin();
                return;
            }
            window.location.href = _base_url + "package/view/" + $(this).data("id");
        });
    </script>
@endpush
