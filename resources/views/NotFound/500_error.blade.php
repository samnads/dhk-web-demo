
@extends('layouts.app')
@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
      <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0 min-vh-90 d-flex flex-column justify-content-center">
               <div class="col-12 em-booking-content-set pl-0 pr-0 mx-auto">
               	    <div class="row em-booking-content-set-main ml-0 mr-0">
                         
                       <div class="em-404-page mx-auto">
                       <p>Oops! Sorry, something went wrong, <a href="{{url('house-cleaning-dubai')}}">Click Here</a> and go to home page.</p></div>
                       
                    </div>
               </div>
          </div>
      </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
<style>
.bg-color {
    background: #fff;
}
</style>
@endsection