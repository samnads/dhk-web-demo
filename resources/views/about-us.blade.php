@extends('layouts.app')
@section('content')
    <div class="success">
        <section class="em-booking-content-section">
            <div class="container em-booking-content-box">
                <div class="col-md-12 col-lg-12">
                        <p>Welcome to Dubai Housekeeping, where quality meets convenience in Dubai. Since 2014, we've been your local go-to for cleaning and housekeeping services. Situated near Downtown Dubai, we cater to a discerning clientele across a wide areas of New Dubai. Our dedicated team, primarily comprising Filipina lady cleaners and housekeepers, brings professionalism and reliability to your doorstep. We firmly believe in fair pricing practices, ensuring that our services deliver unmatched value. Discover our diverse range of offerings, including one-time cleaning, deep cleaning, moving cleaning, decluttering, and recurring housekeeping for both homes and offices.</p>
                        <br><br><br>
                        <h6>Goals</h6><hr>
                        <p>We are committed to being the reliable cornerstone of Housekeeping & Cleaning Services for the vibrant expatriate community in the bustling city of Dubai. Our mission extends beyond providing top-notch services; it's about fostering an environment of respect, job stability, continuous learning, empowerment, and unwavering growth opportunities for our esteemed team members. We believe that by nurturing our team, we empower them to deliver excellence to our valued clientele.</p>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
@endpush
