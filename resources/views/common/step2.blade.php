<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">

    <div class="col-md-12 col-sm-12 em-step-head em-head2 no-left-right-padding">

        <h2>Date and Time</h2>

        <ul>

            <li class="active"><span></span></li>

            <li class="active"><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

        </ul>

    </div>

</div>



<div class="col-12 em-booking-content-set pl-0 pr-0">

    <div class="row em-booking-content-set-main ml-0 mr-0">

        <input type="hidden" id="extraServices" name="extraServices" value="0" >

            <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">

            <form id="secondForm" method="post">

                {{ csrf_field() }}

                <div class="col-12 step2 pl-0 pr-0">

                    <div class="col-12 em-booking-det-cont book-det-cont-set " >


                 
@php
$how_often_exclude_service_ids = [53,54,55,56,57,58,59,60,61,62,63]
@endphp

                        @if(!in_array($data['service']['service_type_id'],$how_often_exclude_service_ids))
                        <div class="col-lg-12 col-md-12 col-sm-12 em-often-section-box how-often" >

                            <p>How often do you need your cleaner?</p>

                            <ul>

                                <li>

                                    <input id="how-often1" checked  value="OD" name="how_often" class="howOften" type="radio"  >

                                    <label for="how-often1"> <span></span> &nbsp; Once</label>

                                </li>

                                

                                

                                <li>

                                    <input id="how-often2" value="WE" name="how_often" class="howOften" type="radio"  >

                                    <label for="how-often2"> <span></span> &nbsp; Weekly</label>

                                </li>

                                

                                

                                <!--<li>

                                    <input id="how-often3"  value="BW" name="how_often" class="howOften" type="radio"  >

                                    <label for="how-often3"> <span></span> &nbsp; Bi-Weekly</label>

                                </li>-->

                            </ul>

                        </div>
                        @else
                        <input checked  value="OD" name="how_often" class="howOften" type="radio">
                        @endif
                        



                        <span id="need_cleaner_error" style="margin-left: 15px;font-size: 14px;color: darkmagenta;display:none;" ></span>

                        <div class="col-lg-12 col-md-12 col-sm-12 em-date-section-box calendar-box">

                            <p>Select service start date</p>

                            <div id="toggle1" class="article">

                                <input type="hidden" id="cleaning_date" class="cleaning_date" name="cleaning_date">

                                <div class="toggle-calendar1"></div>

                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 mt-2 checkbox_multi" id="week_selection" style="display: none">
                            <p>Select week days</p>
                                <div class="row m-0">
                                <div class="col-2 p-0" style="display:none;">
                                    <input id="service_week_days0" data-day="0" data-day-name="Sunday" value="0" name="service_week_days[]" class="" type="checkbox" disabled>
                                    <label for="service_week_days0" style="width:95%"> <span></span> Sun</label>
                                </div>
                                <div class="col-2 p-0">
                                    <input id="service_week_days1" data-day="1" data-day-name="Monday" value="1" name="service_week_days[]" class="" type="checkbox" disabled>
                                    <label for="service_week_days1" style="width:95%"> <span></span> Mon</label>
                                </div>
                                <div class="col-2 p-0">
                                    <input id="service_week_days2" data-day="2" data-day-name="Tuesday" value="2" name="service_week_days[]" class="" type="checkbox" disabled>
                                    <label for="service_week_days2" style="width:95%"> <span></span> Tue</label>
                                </div>
                                <div class="col-2 p-0">
                                    <input id="service_week_days3" data-day="3" data-day-name="Wednesday" value="3" name="service_week_days[]" class="" type="checkbox" disabled>
                                    <label for="service_week_days3" style="width:95%"> <span></span> Web</label>
                                </div>
                                <div class="col-2 p-0">
                                    <input id="service_week_days4" data-day="4" data-day-name="Thursday" value="4" name="service_week_days[]" class="" type="checkbox" disabled>
                                    <label for="service_week_days4" style="width:95%"> <span></span> Thu</label>
                                </div>
                                <div class="col-2 p-0">
                                    <input id="service_week_days5" data-day="5" data-day-name="Friday" value="5" name="service_week_days[]" class="" type="checkbox" disabled>
                                    <label for="service_week_days5" style="width:95%"> <span></span> Fri</label>
                                </div>
                                <div class="col-2 p-0">
                                    <input id="service_week_days6" data-day="6" data-day-name="Saturday" value="6" name="service_week_days[]" class="" type="checkbox" disabled>
                                    <label for="service_week_days6" style="width:95%"> <span></span> Sat</label>
                                </div>
                                </div>
                        </div>

                        

                        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box time-set">

                        <p>Available time <strong id="availableTime" style="text-transform: uppercase;"></strong></p>

                        <input id="timeSelected" name="timeSelected" type="hidden"> 

                            <ul id="timeSelection" class="timeSelection">

                                

                            </ul>

                        </div>

                        

                        

                        <div class="col-sm-12 em-field-main" style="display:none;" id="month_div">

                            <p>Recurring for how many months ?</p>

                            <div class="col-12 em-text-field-main duration-list em-box-8 pl-0 pr-0">

                                <ul class="clearfix" id="monthDuration">

                                <input type="hidden" id="monthDurationId" name="monthDurations" value="1">

                                @for($count = 1;$count <=12;$count++)

                                @if($count == 1)

                                    <li class="selected" >{{$count}}</li>

                                @else

                                    <li >{{$count}}</li>    

                                @endif

                                @endfor

                                <li class="continue_btn">Continue</li>

                                </ul>

                            </div>

                        </div>    

                    </div>

                        

                    <div class="col-12 em-booking-det-cont em-next-btn">

                        <div class="row em-next-btn-set ml-0 mr-0">
                            
                            
                              

                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">

                                <a class="cursor" id="back_houseCleaningStep1"><span class="em-back-arrow back-to1-btn" title="Previous Step"></span></a>

                                <div class="col-12 sp-total-price-set pl-0 pr-0">

                                    Total<br>

                                    <span>AED <b class="mobile_net_p"></b></span> 

                                </div>

                            

                            </div>

                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">

                                <input value="Next" class="text-field-button show3-step" id="submit_form" type="submit">

                            </div>

                        </div>

                    </div>

            </form>

            </div><!--step2 end-->

        </div>

        @include('includes.steps.summary')

    </div>

</div>



<style>

/* .duration-list li{

    width: auto;

    flex: 1;

}

.duration-list ul {

    display: flex;

    justify-content: space-between;

    align-items: center;

}

.continue_btn{

    padding: 0 10px

} */

</style>