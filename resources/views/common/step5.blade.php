@php
$query_params = [];
if (isset($_GET['mobile']) || Session::get('webview') == 1) {
  $query_params['mobile'] = "";
}
$query_params = http_build_query($query_params);
@endphp
<form method="post" id="ccavenue_form" action="{{config('payment.ccavenue_req_handler')}}?{{@$query_params}}" style="display: none">
    {!! csrf_field() !!}
    <table width="40%" height="100" border='1' align="center">
        <caption>
            <font size="4" color="blue"><b>Integration Kit</b></font>
        </caption>
    </table>
    <table width="40%" height="100" border='1' align="center">
        <tr>
            <td>Parameter Name:</td>
            <td>Parameter Value:</td>
        </tr>
        <tr>
            <td colspan="2"> Compulsory information</td>
        </tr>
        <tr>
            <td>Merchant Id :</td>
            <td><input type="text" name="merchant_id" value="{{config('payment.ccavenue_merchant_id')}}" /></td>
        </tr>
        <tr>
            <td>Order Id :</td>
            <td><input type="text" name="order_id" value="" /></td>
        </tr>
        <tr>
            <td>Amount :</td>
            <td><input type="text" name="amount" value="" /></td>
        </tr>
        <tr>
            <td>Currency :</td>
            <td><input type="text" name="currency" value="AED" /></td>
        </tr>
        <tr>
            <td>Redirect URL :</td>
            <td><input type="text" name="redirect_url"
                    value="{{config('payment.ccavenue_resp_handler')}}?{{@$query_params}}" /></td>
        </tr>
        <tr>
            <td>Cancel URL :</td>
            <td><input type="text" name="cancel_url"
                    value="{{config('payment.ccavenue_resp_handler')}}?{{@$query_params}}" /></td>
        </tr>
        <tr>
            <td>Language :</td>
            <td><input type="text" name="language" value="EN" /></td>
        </tr>
        <tr>
            <td colspan="2">Billing information(optional):</td>
        </tr>
        <tr>
            <td>Billing Name :</td>
            <td><input type="text" name="billing_name" value="" /></td>
        </tr>
        <tr>
            <td>Billing Address :</td>
            <td><input type="text" name="billing_address" value="" /></td>
        </tr>
        <tr>
            <td>Billing City :</td>
            <td><input type="text" name="billing_city" value="" /></td>
        </tr>
        <tr>
            <td>Billing State :</td>
            <td><input type="text" name="billing_state" value="" /></td>
        </tr>
        <tr>
            <td>Billing Zip :</td>
            <td><input type="text" name="billing_zip" value="" /></td>
        </tr>
        <tr>
            <td>Billing Country :</td>
            <td><input type="text" name="billing_country" value="" /></td>
        </tr>
        <tr>
            <td>Billing Tel :</td>
            <td><input type="text" name="billing_tel" value="" /></td>
        </tr>
        <tr>
            <td>Billing Email :</td>
            <td><input type="text" name="billing_email" value="" /></td>
        </tr>
        <tr>
            <td></td>
            <td><INPUT TYPE="submit" value="CheckOut"></td>
        </tr>
    </table>
</form>
<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">
    <div class="col-md-12 col-sm-12 em-step-head em-head5 no-left-right-padding">
        <h2>Payment Details</h2>
        <ul>
            <li class="active"><span></span></li>
            <li class="active"><span></span></li>
            <li class="active"><span></span></li>
            <li class="active"><span></span></li>
            <li class="active"><span></span></li>
        </ul>
    </div>
</div>

<div class="col-12 em-booking-content-set pl-0 pr-0">
    <div class="row em-booking-content-set-main ml-0 mr-0">
        <input type="hidden" id="timeSpan1">
        <input type="hidden" id="timeSpan2">

        <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
            <form id="payment_form" method="post">
                {{ csrf_field() }}

                <div class="col-12 step5 pl-0 pr-0">
                    <div class="col-12 em-booking-det-cont pl-0 pr-0">
                        <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
                            <h5>Payment Details</h5>
                            <div class="col-sm-12 em-field-main-set">
                                <div class="row m-0">
                                    <div class="col-sm-12 em-field-main">
                                        <p>Do you have any specific cleaning instructions?</p>
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                            <textarea name="instructions" cols="" rows="" class="text-field-big"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 em-field-main-set">
                                <div class="row m-0">
                                    <div class="col-sm-12 em-field-main">
                                        <p>How to let the crew in?</p>
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                            <select name="crew_in" type="text" placeholder="" class="el-text-field">
                                                <option value="">-- Select --</option>
                                                <!--<option value="At home">At home</option>
                                                <option value="Key is with security">Key is with security</option>
                                                <option value="Key under the mat">Key under the mat</option>
                                                <option value="Buzz the intercom">Buzz the intercom</option>
                                                <option value="Door open">Door open</option>
                                                <option value="Others">Others</option>-->
                                                @foreach($data['crew_in_options'] as $key => $crew_in_option)
                                                <option value="{{$crew_in_option['text']}}">{{$crew_in_option['text']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 em-field-main pl-0 pr-0">
                                <div class="col-sm-12 em-extra-ser-set">
                                    <p>Payment Mode</p>
                                    <div class="row" id="payment_div">
                                        <input type="hidden" id="payment_mode" name="payment_mode">
                                        <input type="hidden" id="phonetamarapay" name="phonetamarapay"
                                            value="{{ session('phone') }}">
                                        <style>
                                            .n-upi-set.em-extra-ser-img img {
                                                width: 80%;
                                                margin: 0 auto;
                                            }
                                        </style>

                                        <div class="col-lg-4 col-sm-6 col-6" id="cash"
                                            onclick="getPayment(this.id)">
                                            <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                <div class="n-upi-set em-extra-ser-img"
                                                    style="padding-left: 5px !important; padding-right: 5px !important;">
                                                    <img src="{{ asset('images/cash-payment.jpg') }}" alt="">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-4 col-sm-6 col-6" id="card"
                                            onclick="getPayment(this.id)">
                                            <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                <div class="n-upi-set em-extra-ser-img"
                                                    style="padding-left: 5px !important; padding-right: 5px !important;">
                                                    <img src="{{ asset('images/card-payment.jpg') }}" alt="">
                                                </div>
                                            </div>
                                        </div>


                                        <!--<div class="col-lg-4 col-sm-6 col-6" id="applepay" onclick="getApplePayment(this.id)">
                                                <div class="row ml-0 mr-0 em-extra-ser-tmb" id="applepaytmb">
                                                    <div class="n-upi-set em-extra-ser-img" style="padding-left: 5px !important; padding-right: 5px !important;"><img src="{{ asset('images/n-applepay.jpg') }}" alt=""></div>
                                                </div>
                                        </div>

                                        <div class="col-lg-4 col-sm-6 col-6" id="gpay" onclick="getPayment(this.id)">
           <div class="row ml-0 mr-0 em-extra-ser-tmb">
            <div class="n-upi-set em-extra-ser-img" style="padding-left: 5px !important; padding-right: 5px !important;"><img src="{{ asset('images/n-gpay.jpg') }}" alt=""></div>
           </div>
                                        </div>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 em-terms-and-condition-main">
                                <h6>Terms & Conditions</h6>
                                <div class="col-sm-12 em-terms-and-condition">
                                    <ul class="">
                                        <li>
                                            <div class="em-num">1</div>
                                            <div class="em-num-cont">
                                                <p>In case the scope of the job is found to be more extensive after the inspection upon arrival, we might need extra equipment / time and the pricing might differ.</p>
                                            </div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">2</div>
                                            <div class="em-num-cont">
                                                <p>Client may cancel or adjust without any charges:  the time of a cleaning visit/s by giving at least 24 hours advanced notice.</p>
                                            </div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">3</div>
                                            <div class="em-num-cont">
                                                <p>Kindly take note that a cancellation fee amounting to 50% shall be levied should you opt to cancel your booking within 12 hours of the pre-scheduled appointment. In the event of an on the spot cancellation, an entire 100% cancellation fee shall be applied.</p>
                                            </div>
                                            <div class="clear"></div>
                                        </li>
										</li>
											<div class="em-num">4</div>
											<div class="em-num-cont">
												<p>You agree to pay the full price of the cleaning visit in the event of an on the spot cancellation, lock-out, no one home to let them in or a problem with your keys.</p>
											</div>
											<div class="clear"></div>
										</li>
                                        <li>
                                            <div class="em-num">5</div>
                                            <div class="em-num-cont">
                                                <p>Proceeding with your booking confirms your acceptance to Dubai Housekeeping terms and conditions.</p>
                                            </div>
                                            <div class="clear"></div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12 em-field-main log-remb terms-check">
                                <input value="1" id="terms" name="terms" class="" type="checkbox">
                                <label for="terms"> <span></span> &nbsp; I accept all the <a
                                                        href="https://dubaihousekeeping.com/terms-conditions/"
                                                        target="_blank">terms and
                                    conditions</a></label>
                            </div>

                        </div>
                    </div>
                    <div id="payment_div">
                    </div>
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <a id="back_houseCleaningStep4"><span class="em-back-arrow back-to4-btn"
                                        title="Previous Step"></span></a>

                                <div class="col-12 sp-total-price-set pl-0 pr-0">
                                    Total<br>
                                    <span>AED <b class="mobile_net_p"></b></span>
                                </div>


                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                <input value="Make Payment" id="payment_button" class="text-field-button show6-step"
                                    type="submit">
                                <button type="button" id="applePaybtn" class="text-field-button"
                                    style="display:none;"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        @include('includes.steps.summary')
    </div>
</div>
<div class="col-sm-12 popup-main" id="payment_popup">
    <div class="row min-vh-100 d-flex flex-column justify-content-center">
        <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
            <form id="payment-form" method="POST" action="{{ url('make-pay-checkout') }}">
                {{ csrf_field() }}
                <h5 class="">Make Payment <span class="em-forgot-close-btn close-pay-poup"><img
                            src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>
                <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                    <div class="row m-0">
                        <input type="hidden" id="token_req" name="token_req" value=''>
                        <input type="hidden" name="order_id" id="order_id">
                        <input type="hidden" name="amount" id="amount">
                        <input type="hidden" name="currency" value="AED">
                        <input type="hidden" id="retry_online" name="retry_online" value="not_online">
                        <?php if (isset($_GET['mobile'])) {
                            $mobileParam = '?mobile=active';
                        }
                        ?>
                        <?php if (!isset($_GET['mobile'])) {
                            $mobileParam = '';
                        }
                        ?>
                        <input type="hidden" name="mobile_view" id="mobile_view" value="{{ $mobileParam }}" />
                        <input type="hidden" name="merchant_param2" id="merchant_param2"
                            value="{{ $mobileParam }}">
                        <input type="hidden" name="customer_name" id="billing_name">
                        <input type="hidden" name="customer_tel" id="billing_tel">
                        <input type="hidden" name="customer_email" id="billing_email">
                        <div class="one-liner">
                            <div class="card-frame">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                    <button id="pay-button" disabled>
                        PAY AED <span id="amount_span"></span> </button>
                </div>
            </form>
        </div>
    </div>
</div><!--popup-main end-->

<div class="col-sm-12 popup-main" id="payment_popup_applepay">
    <div class="row min-vh-100 d-flex flex-column justify-content-center">
        <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
            <h5 class="">Make Payment <span class="em-forgot-close-btn close-pay-poup"><img
                        src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>
            <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                <div class="row m-0">
                    <input type="hidden" name="order_id" id="ap_order_id">
                    <input type="hidden" name="amount" id="ap_amount">
                    <input type="hidden" name="customer_name" id="ap_billing_name">
                    <input type="hidden" name="customer_tel" id="ap_billing_tel">
                    <input type="hidden" name="customer_email" id="ap_billing_email">
                    <input type="hidden" name="customer_address" id="ap_billing_address">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                <button type="button" id="applePaybtn1"></button>
            </div>
        </div>
    </div>
</div><!--popup-main end-->
@push('scripts')
<script>
$(document).ready(function() {
    $('select[name="crew_in"]').select2();
    $(document.body).on("change",'select[name="crew_in"]',function(){
        $(this).valid();
    });
});
</script>
@endpush