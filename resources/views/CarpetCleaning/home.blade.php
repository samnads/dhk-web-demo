@extends('layouts.app')

@section('title'){{'Book carpet cleaning services in Dubai | maid service Dubai'}}@endsection
@section('description'){{'Book carpet cleaning services in Dubai from the best cleaning company in Dubai at an affordable rate. Top maid cleaning service company in Dubai'}}@endsection
@section('canonical'){{'https://booking.dubaihousekeeping.com/carpet-cleaning-service'}}@endsection
@section('tagimage'){{'https://booking.dubaihousekeeping.com/images/carpet-cleaning-new.jpg?v=1.5'}}@endsection

@section('content')
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0">
                    <input type="hidden" id="number_hours" value="2">
                    @include('CarpetCleaning.steps')
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
@endsection
@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $('#noOfHours').val(2);
            getAvailability();
            $('.frequency_p').text('-');
            $('.maid_id_p').text($('#noOfMaids').val());
            $('.hrs_p').text($('#noOfHours').val());
            var clean_status = $('#cleaningMaterialStatus').val();
            if (clean_status == 'N') {
                $('.cleaning_material_p').text('No');
                $('#mat_success_p').text('without cleaning materials');
            } else if (clean_status == 'Y') {
                $('.cleaning_material_p').text('Yes');
                $('#mat_success_p').text('with cleaning materials');
            }
            $('.hrs_p').text($('#noOfHours').val());
            setTimeout(function() {
                $('.mob-coupon_message').hide(500);
            }, 7000);

            $('.small-close-btn').click(function() {
                $('.mob-coupon_message').hide(500);
            });
        });
        /***********************************************************/
        // step 1 form validation
        $("#step1Home").validate({
            ignore: [],

            rules: {
                noOfHours: {
                    required: true
                },
                noOfMaids: {
                    required: true,
                    min: 0,
                    max: 10
                },
                cleaningMaterialStatus: {
                    required: true
                },
                apartment_room: {
                    required: function(element) {
                        return $('input[name="category"][data-category-name="Home"]').is(':checked') && $(
                            'input[name="building_type"][data-sub-category-name="Apartment"]').is(
                            ':checked');
                    }
                },
                villa_room: {
                    required: function(element) {
                        return $('input[name="category"][data-category-name="Home"]').is(':checked') && $(
                            'input[name="building_type"][data-sub-category-name="Villa"]').is(':checked');
                    }
                },
                office_sqft: {
                    required: function(element) {
                        return $('input[name="category"][data-category-name="Office"]').is(':checked');
                    },
                },
            },
            messages: {
                noOfHours: "Please select number of hours.",
                noOfMaids: {
                    required: "Please select number of maids.",
                    min: "Please enter a value greater than 0.",
                    max: "Please enter a value less than 10.",
                    number: "Only numbers are allowed.",
                },
                cleaningMaterialStatus: "Please select about cleaning material.",
                apartment_room: "Select carpet size.",
                villa_room: "Select villa room.",
                office_sqft: "Select office Sq.Ft. .",
            },
            errorPlacement: function(error, element) {
                console.log(error);
                if (element.attr("name") == "office_sqft") {
                    error.insertAfter(element);
                } else if (element.attr("name") == "apartment_room") {
                    error.insertAfter(element.parent().parent().parent());
                } else if (element.attr("name") == "villa_room") {
                    error.insertAfter(element.parent().parent().parent());
                } else {
                    error.insertAfter(element.parent().parent());
                }
            },
            submitHandler: function(form, event) {
                var matstatus = $('#cleaningMaterialStatus').val();
                var tolstat = $('#cleaningSupplytatus').val();
                if (matstatus == 'Y' && $('input[name="plan_based_supply"]').is(':checked') == false) {
                    $("#cleaningSupplytatus-error").css('display', 'block');
                    $("#cleaningSupplytatus-error").text('Please select cleaning supply.');
                    return false;
                } else {
                    $("#cleaningSupplytatus-error").css('display', 'none');
                }
                var form_data = new FormData($("#step1Home")[0]);
                form_data.append('_token', '{{ csrf_token() }}');

                $('#houseCleaningStep1').fadeOut(300);
                $('#commonStep2').fadeIn(300);
                $('.timeSelection li:first').trigger("click");

                $('header .logo').addClass('logo_left');
                $('.date_time').show();
                $('.frequency_p').text('One Time');
                $("html").animate({
                    scrollTop: 0
                }, "slow");
            }
        });
        /***********************************************************/
        // trigger handlers for category
        $('input[name="category"]').change(function() {
            var category_name = $(this).attr('data-category-name');
            if (category_name == "Home") {
                $('select[name="office_sqft"]').val("");
                $('.office_sqfts').hide();
                //
                $('.home-types').show();
                var sub_category_name = $('input[name="building_type"]:checked').attr("data-sub-category-name");
                if (sub_category_name == "Apartment") {
                    $('.apartment_rooms').show();
                } else if (sub_category_name == "Villa") {
                    $('.villa_rooms').show();
                }
                localCalculate();
            } else if (category_name == "Office") {
                $('.home-types').hide();
                $('.apartment_rooms').hide();
                $('.villa_rooms').hide();
                //
                $('.office_sqfts').show();
                localCalculate();
            }
        });
        /***********************************************************/
        // trigger handler for sub category
        $('input[name="building_type"]').change(function() {
            $('.apartment_rooms').hide();
            $('.villa_rooms').hide();
            $('.office_sqfts').hide();
            sub_category_name = $(this).attr("data-sub-category-name");
            _total_payable = 0;
            _supervisor_fee = 0;
            _vat_charge = 0;
            _service_only_charge = 0;
            $('input[name="apartment_room"]').prop('checked', false);
            $('input[name="villa_room"]').prop('checked', false);
            $('select[name="office_sqft"]').val("");
            if (sub_category_name == "Apartment") {
                $('.apartment_rooms').show();
            } else if (sub_category_name == "Villa") {
                $('.villa_rooms').show();
            }
            _no_of_hours = 2;
            $('input[name="noOfHours"]').val(_no_of_hours).trigger("change");
            no_of_maids = parseInt($('#noOfMaids').val());
            localCalculate();
        });
        /***********************************************************/
        // trigger handlers
        $('input[name="apartment_room"]').change(function() {
            $('input[name="noOfHours"]').val($(this).attr("data-hours")).trigger("change");
            $("div[class*='service_category_qty_']").hide();
            var service_category_id = $(this).val();
            $('.service_category_qty_'+service_category_id).show(200);
            localCalculate();
        });
        $('[id^="service_category_quantity_"]').change(function() {
            // quantity changed
            localCalculate();
        });
        $('input[name="villa_room"]').change(function() {
            $('input[name="noOfHours"]').val($(this).attr("data-hours")).trigger("change");
            localCalculate();
        });
        $('select[name="office_sqft"]').change(function() {
            $('input[name="noOfHours"]').val($('select[name="office_sqft"]').find(':selected').attr("data-hours"))
                .trigger("change");
            localCalculate();
        });
        $('input[name="supervisor_status"]').change(function() {
            localCalculate();
        });
        /***********************************************************/
        // first load views
        $('#summary_supervisor').hide();
        $('#summary_material').hide();
        $('#summary_tools').hide();
        /***********************************************************/
        // first load select and trigger
        $('input[name="category"][data-category-name="Home"]').trigger("click");
        $('input[name="building_type"][data-sub-category-name="Apartment"]').trigger("click");
        $('input[name="apartment_room"][data-room-name="STUDIO"]').trigger("click");
        /***********************************************************/
        // cost calculate
        function localCalculate() {
            $('#noOfMaids').val(0);
            $('#noOfHours').val(0);
            _no_of_maids = parseInt($('#noOfMaids').val());
            _no_of_hours = parseInt($('#noOfHours').val());
            /**********************************************************/
            // supervisor option handling
            if (_no_of_hours > 2 && _no_of_maids > 1) {
                // supervisor allowed
                $('input[name="supervisor_status"]').removeAttr("disabled");
                $('#mainSupervisorDiv').show();
                $('#summary_supervisor').show();
            } else {
                // supervisor not allowed
                $("input[name=supervisor_status][value=N]").prop('checked', true);
                $('input[name="supervisor_status"]').attr("disabled", true);
                $('#mainSupervisorDiv').hide();
                $('#summary_supervisor').hide();
            }
            var supervisor_status = $('input[name="supervisor_status"]:checked').val();
            $('.supervisorcharge_div').hide();
            if (supervisor_status == "Y") {
                _supervisor_fee = _hourly_supervisor_fee * _no_of_hours;
                $('.supervisorcharge_div').show();
                $('.super_visor_p').html('Yes');
            } else {
                _supervisor_fee = 0;
                $('.super_visor_p').html('No');
            }
            /**********************************************************/
            var category_name = $('input[name="category"]:checked').attr('data-category-name');
            var service_fixed_rate_with_vat = 0;
            if (category_name == "Home") {
                var sub_category_name = $('input[name="building_type"]:checked').attr("data-sub-category-name");
                if (sub_category_name == "Apartment") {
                    service_fixed_rate_with_vat = parseFloat($('input[name="apartment_room"]:checked').attr("data-rate")) ||
                        0;
                } else if (sub_category_name == "Villa") {
                    service_fixed_rate_with_vat = parseFloat($('input[name="villa_room"]:checked').attr("data-rate")) || 0;
                }
            } else if (category_name == "Office") {
                var sqft = parseFloat($('select[name="office_sqft"]').find(':selected').val()) || 0;
                var sqft_rate = parseFloat($('select[name="office_sqft"]').find(':selected').attr("data-sqft-rate")) || 0;
                service_fixed_rate_with_vat = sqft * sqft_rate;
            }
            var service_category_id = $('input[name="apartment_room"]:checked').val();
            var quantity = $('input[name="service_category_quantity['+service_category_id+'][]"]:checked').val();
            service_fixed_rate_with_vat = service_category_id ? (service_fixed_rate_with_vat * quantity) : 0; 
            service_fixed_rate_vat = (service_fixed_rate_with_vat * _vat_percentage) / (100 + _vat_percentage);
            service_fixed_rate_without_vat = service_fixed_rate_with_vat - service_fixed_rate_vat;
            _taxable_amount = service_fixed_rate_without_vat + _supervisor_fee;
            _coupon_discount = 0;
            //let response = await applyCoupon();
            // do coupon check here
            _discount = _coupon_discount;
            _taxable_amount = _taxable_amount - _coupon_discount;
            _vat_charge = (_vat_percentage / 100) * _taxable_amount;
            _total_payable = _taxable_amount + _vat_charge;
            var hour_rate = (_total_payable / _no_of_hours) || 0;
            $('#hour_rate').val(hour_rate || 35);
            $('.hrs_p').html(parseFloat(_no_of_hours));
            $('.total_p').html(parseFloat(_taxable_amount).toFixed(2));
            /**********************************************************/
            var tdate = moment().format('YYYY-MM-DD');
            $('#cleaning_date').val(tdate);
            if (_coupon_code) {
                applyCoupon();
            } else {
                $('.net_p,.mobile_net_p').html(parseFloat(_total_payable).toFixed(2));
                $('.supervisorcharge_div_p').html(_supervisor_fee);
                $('.vat_p').html(parseFloat(_vat_charge).toFixed(2));
            }
            /**********************************************************/
        }
        /**********************************************************/
        function amountCalculation() {
            return false;
            var no_hrs = parseInt($('#noOfHours').val());
            var no_maids = parseInt($('#noOfMaids').val());
            var cleaningStatus = $('#cleaningMaterialStatus').val();
            /*if(no_hrs >= 3 && no_maids >= 2)
            {
            	$('#mainSupervisorDiv').show();
            } else {
            	$('#mainSupervisorDiv').hide();
            }*/
            var supervisorVal = $('#supervisorStatus').val();
            var cleaningVacuumStatus = $('#cleaningVacuumStatus').val();
            var cleaningLadderStatus = $('#cleaningLadderStatus').val();
            var cleaningMopStatus = $('#cleaningMopStatus').val();
            var cleaningIronStatus = $('#cleaningIronStatus').val();
            var cleaningSupplytatus = $('#cleaningSupplytatus').val();
            var interior = $('#interior').val();
            var ironing = $('#ironing').val();
            var fridge = $('#fridge').val();
            var oven = $('#oven').val();
            var coupon = $('.couponClass').val();
            // console.log(coupon)
            var customerId = '';
            var bookedDate = $('#cleaning_date').val();
            //console.log(bookedDate);
            var monthDuration = '';
            var serviceId = $('#service_id').val();
            var visitType = 0;
            var service_types = parseInt('1');
            var visitType = 'OD';
            var vat_percentage = parseInt('5');
            var no_visits = 0;
            var bookingdatess = $('#cleaning_date').val();
            var day = new Date(bookingdatess).getDay();
            if (visitType == 'OD') {
                no_visits = 1;
            }
            if (customerId != "") {
                var cust_id = customerId;
            } else {
                var cust_id = "";
            }
            var time_from = $('.time_span1').text();
            var perHour = 40;
            if (no_hrs == 2) {
                var totPrice = (110 * no_maids);
            } else if (no_hrs == 3) {
                var totPrice = (140 * no_maids);
            } else if (no_hrs == 4) {
                var totPrice = (160 * no_maids);
            } else if (no_hrs == 5) {
                var totPrice = (200 * no_maids);
            } else if (no_hrs == 6) {
                var totPrice = (240 * no_maids);
            } else {
                var totPrice = ((no_hrs * perHour) * no_maids);
            }
            if (supervisorVal == "Y") {
                //var supervisorrate = (60 * no_hrs);
                //totPrice = (totPrice + supervisorrate);
                //$(".supervisorcharge_div_p").text(supervisorrate);
                //$(".supervisorcharge_div").show();
            } else {
                //var supervisorrate = 0;
                //$(".supervisorcharge_div").hide();
            }
            totPrice = totPrice + _supervisor_fee;
            $(".supervisorcharge_div_p").text(_supervisor_fee);
            if (cleaningStatus == "N") {
                if (cleaningVacuumStatus == 'Y' || cleaningLadderStatus == 'Y' || cleaningMopStatus == 'Y' ||
                    cleaningIronStatus == 'Y') {
                    //var toolRate = 10;
                    //$('.cleaningtoolsdiv_p').text(toolRate);
                    //$('.cleaningtools_div').show();
                } else {
                    //var toolRate = 0;
                    //$('.cleaningtoolsdiv_p').text('');
                    //$('.cleaningtools_div').hide();
                    //$('.cleaning_tools_p').text('No');
                }
                var cleaning_material_rate = (tools_rate);
                var totPrice = (totPrice + cleaning_material_rate);
                $('.cleaningmaterial_div').hide();
            } else {
                //$('.cleaningtools_div').hide();
                if (cleaningSupplytatus == 0) {
                    var supplyRate = 10;
                } else if (cleaningSupplytatus == 1) {
                    var supplyRate = 20;
                } else {
                    var supplyRate = 0;
                }
                var cleaning_material_rate = (_materials_rate);
                var totPrice = (totPrice + cleaning_material_rate);
                //$('.cleaningmaterialdiv_p').text(cleaning_material_rate);
                //$('.cleaningmaterial_div').show();
            }

            var vat_charge = (totPrice * 5) / (100 + 5);
            var service_rate_with_material = (totPrice - vat_charge);
            var service_rate = (service_rate_with_material - cleaning_material_rate);

            var per_hour_rate = (service_rate / no_hrs);
            per_hour_rate = per_hour_rate.toFixed(2);
            vat_charge = vat_charge.toFixed(2);
            var gross_amount = totPrice.toFixed(2);
            var a_service_rate = service_rate;
            var cleaning_rates = cleaning_material_rate;
            service_rate = service_rate.toFixed(2);
            var price_to_show = per_hour_rate;
            var total_service_rate = 0;
            var discount = 0;
            $('.total_p').text(service_rate);
            if (coupon != '' && coupon != 'undefined') {
                //checkCoupon();
            } else {
                $('.vat_p').text(vat_charge);
                $('.net_p').text(gross_amount);
                $('.mobile_net_p').text(gross_amount);
                $('.discount_div').hide();
                $('#coupon_message').hide();
            }
            localCalculate();
        }

        function getEventTarget(e) {
            e = e || window.event;
            return e.target || e.srcElement;
        }
        var ul = document.getElementById('noHours');
        ul.onclick = function(event) {
            var target = getEventTarget(event);
            $('#noOfHours').val(target.innerHTML).trigger('change');
            $('#number_hours').val(target.innerHTML);
            $('.hrs_p').text(target.innerHTML);
            $('#noOfHours-error').hide();
            //amountCalculation();
            getAvailability();
        };
        // var increment = 2;
        function addNumber() {
            increment = $('#noOfMaids').val();
            if (increment <= 9) {
                increment++;
                $('#noOfMaids').val(increment).trigger('change');
                $('.maid_id_p').text(increment);
                $('#noOfMaids-error').hide();
                //amountCalculation();
            }
        }
        // console.log(dec);
        function noofmaidchange() {
            var change = $('#noOfMaids').val();
            if (change < 1 && change != '')
                change = 1;
            else if (change > 10)
                change = 10;
            $('#noOfMaids').val(change).trigger('change');
            $('.maid_id_p').text(change);
            $('#noOfMaids-error').hide();
            //if (change != '')
            //amountCalculation();
        }

        function removeNumber() {
            var dec = $('#noOfMaids').val();
            if (dec > 1) {
                dec--;
                $('#noOfMaids').val(dec).trigger('change');
                $('.maid_id_p').text(dec);
                $('#noOfMaids-error').hide();
                //amountCalculation();
            }
        }
    </script>
@endpush
