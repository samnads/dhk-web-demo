<?php if(!isset($_GET['mobile'])) {
?>
<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">
    <div class="col-md-12 col-sm-12 em-step-head em-head no-left-right-padding">
        <h2>Book Your Service</h2>
        <ul>
            <li class="active"><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
        </ul>
    </div>
</div>
<?php } ?>
<form id="step1SofaCleaning" method="post">
{{ csrf_field() }}
<input type="hidden" value="2" id="noOfHours" name="noOfHours">
    <div class="col-12 em-booking-content-set pl-0 pr-0">
        <div class="row em-booking-content-set-main ml-0 mr-0">
            <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
                <div class="col-12 step1 pl-0 pr-0">
                    @include('common.services')
                    <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0">
                        <div class="col-12 book-det-cont-set-main sofa-cleaning-service  ser-cont5 ser-cont"  style="display:block;" id="cont5">
                            <h5>Sofa Cleaning Services</h5>
                                <div class="col-sm-12 normal-sofa pl-0 pr-0">
                                    <div class="col-sm-12 em-field-main-set"> 
                                        <div class="row m-0">
                                               <input type="hidden" id="sofa_main_id" value="14">
                                               <div id="office_div" class="col-sm-6 pt-3">
                                                        <p>What is the seat size of sofa?</p>
                                                        <select class="js-select2" name="sofa_count" id="sofa_count" onChange="amountCalculation();">
                                                            <option value="">Select Seat</option>
                                                            @for ($i = 1; $i <= 30; $i++)
                                                            <option value="{{$i}}" @if($i == '10') selected @endif>{{$i}} Seat</option>
                                                            @endfor
                                                        </select>
                                                </div>
                                           
                                            </div> 
                                        </div>
                                      
                                       
                                    </div><!--soafa-cleaning-home end-->
                            
                            </div>
                            
                            <div class="col-sm-12 em-terms-and-condition-main"> 
                                <h6>What to expect from the service?</h6>
                                <div class="col-sm-12 em-terms-and-condition">
                                    <ul class="">
                                        <li>
                                            <div class="em-num">1</div>
                                            <div class="em-num-cont"><p>Onsite Survey</p></div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">2</div>
                                            <div class="em-num-cont"><p>Thorough dry vacuuming</p></div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">3</div>
                                            <div class="em-num-cont"><p>Sofa shampooing</p></div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">4</div>
                                            <div class="em-num-cont"><p>Cleaning with the Upholstery Brush to reach the difficult parts of the sofa especially the backside</p></div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">5</div>
                                            <div class="em-num-cont"><p>Sofa steaming</p></div>
                                            <div class="clear"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <!--<span class="em-back-arrow " title="Previous Step"></span>-->
                                <div class="col-12 sp-total-price-set pl-0 pr-0">
                                    Total<br>
                                    <span>AED <b class="net_p"></b></span> 
                                </div>
                            
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                <input value="Next" class="text-field-button show2-step" id="submit_button" type="button">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             
            @include('includes.steps.summary')

        </div>
    </div>
</form>
<style>
#applePaybtn {  
	width: 150px;  
	height: 45px;  
	display: none;   
	border-radius: 5px;    
	margin-left: auto;
	margin-right: auto;
	background-image: -webkit-named-image(apple-pay-logo-white); 
	background-position: 50% 50%;
	background-color: black;
	background-size: 60%; 
	background-repeat: no-repeat;  
}
</style>
@push('scripts')
<script>
   $('.show2-step').click(function(){
    //   if ($('#sofa_main_id').val() == 14 ) {
    //     var subCategory3Seat =  $("input[name='seat1']:checked").val();
    //     var subCategory4Seat =  $("input[name='seat2']:checked").val();
    //     var subCategory1Seat =  $("input[name='one_seat1']:checked").val();
    //     var subCategory2Seat =  $("input[name='two_seat1']:checked").val();

    //         if ((subCategory3Seat == '0' || subCategory3Seat ==undefined) && (subCategory4Seat == '0' || subCategory4Seat ==undefined) &&
    //         (subCategory1Seat == '0' || subCategory1Seat ==undefined) && (subCategory2Seat == '0' || subCategory2Seat ==undefined)) {
    //             $('#categoryerror').show();
    //             return false;
    //         } else {
                $('#categoryerror').hide();
                $('#sofaStep1').fadeOut(300);
                $('#commonStep2').fadeIn(300);
                $('header .logo').addClass('logo_left');
                $('.date_time').show();
                $('.timeSelection li:first').trigger("click");
                $("html").animate({ scrollTop: 0 }, "slow");
    //         }
    //   }
    //   if ($('#sofa_main_id').val() == 8 ) {
    //     var subCategory3SeatL =  $("input[name='seatL1']:checked").val();
    //     var subCategory4SeatL =  $("input[name='seatL2']:checked").val();
    //         if ((subCategory3SeatL == '0' || subCategory3SeatL ==undefined) && (subCategory4SeatL == '0' || subCategory4SeatL ==undefined)) {
    //             $('#categoryLerror').show();
    //             return false;
    //         } else {
    //             $('#categoryLerror').hide();
    //             $('#sofaStep1').fadeOut(300);
    //             $('#commonStep2').fadeIn(300);
    //             $('.timeSelection li:first').trigger("click");
    //             $('header .logo').addClass('logo_left');
    //             $('.date_time').show();
    //             $("html").animate({ scrollTop: 0 }, "slow");   
    //         }

    //   }
       
    });
</script>
@endpush