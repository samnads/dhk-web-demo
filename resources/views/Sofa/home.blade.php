@extends('layouts.app')

@section('title') {{'Sofa Cleaning Services in Dubai'}} @endsection

@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
     <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
          <input type="hidden" id="all_amount" value="{{serialize($allAmount)}}">
          <input type="hidden" id="number_hours" value="0">		  		  <input type="hidden" id="serviceNamePay" value="SofaCleaning">
               @include('Sofa.steps')
          </div>
     </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')
<script>
 $.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});
$(document).ready(function() {
    $('#noOfHours').val(2);
	getAvailability();
    amountCalculation();
    $('.show-L-sofa-btn').click(function(){
		$('.normal-sofa').hide(500);
		$('.L-shape-sofa').show(500);
		$('#sofa_main_id').val('8');
		$("#seatL1").prop("checked", false);
        $("#seatL2").prop("checked", true);
		// console.log("innnnnn");
		checkCheckBox3('seatL1');
		checkCheckBox4('seatL2');
        // if ($("#seatL1").prop(":checked")) { 
		// 	$('#threeSeatL_div').show();
		// }
        // if ($("#seatL2").prop(":checked")) { 
		// 	$('#fourSeatL_div').show();
		// }
		$('#categoryLerror').hide();
		amountCalculation();
	});
	$('.show-normal-sofa-btn').click(function(){
		$('.normal-sofa').show(500);
		$('#sofa_main_id').val('7');
		$('.L-shape-sofa').hide(500);
        $("#seat1").prop("checked", true);
        $("#seat2").prop("checked", false);
        $("#one_seat1").prop("checked", false);
        $("#two_seat1").prop("checked", false);

		checkCheckBox('seat1');
		checkCheckBox2('seat2');
        checkCheckBox5('one_seat1');
		checkCheckBox6('two_seat1');

        // if ($("#seat1").prop(":checked")) { 
        // 	$('#threeSeat_div').show();
  		// }
        // if ($("#seat2").prop(":checked")) { 
		// 	$('#fourSeat_div').show();
		// }
        // if ($("#one_seat1").prop(":checked")) { 

        // // if($('#one_seat1').prop('checked', true)) {
		// 	$('#oneSeat_div').show();
		// }
        // if ($("#two_seat1").prop(":checked")) { 

        // // if($('#two_seat1').prop('checked', true)) {
		// 	$('#twoSeat_div').show();
		// }
		$('#categoryerror').hide();
		amountCalculation();
	});
});
function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement; 
}

function amountCalculation()
{
    var furnished =0;
    var is_scrubbing =1;
    var serviceId = $('#service_id').val();
    var category = $('#sofa_main_id').val();
    var selectedArray = [];
    var coupon = $('.couponClass').val();

           var sofaCount = $('#sofa_count').val();
           selectedArray.push({service_id : serviceId,category_id : category,sub_category_id : 14,name : sofaCount,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
       
    var intialArray = $('#all_amount').val();
 
    $.ajax({
          method: 'post',
          url: '{{url('calculate-sofa')}}',
          data:{'selectedArray':selectedArray,'intialArray':intialArray,'officesqure':0,'category':category,_token:'{{csrf_token()}}'},  
         
          success: function (result) {
               if (result.status == 'success') {
                   $('.total_p').text(result.data['amount']);
                   if(coupon != '') {
                        checkCoupon();
                    } else {
                        $('.vat_p').text(result.data['vat_amount']);
                        $('.net_p').text(result.data['totalAmount']);
                        $('.discount_div').hide();
                        $('#coupon_message').hide();
                    $('.mobile_net_p').text(result.data['totalAmount']);

                    }
                   
                  
                   $('#cat1seat').val(result.data['cat1seat']);
        
               }
          }
     }); 
}
    function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement; 
    }



function toggleClass( element ) {
    var classe = 'col-lg-6 col-md-6 col-sm-6 col-6 unselect';

    if ( element.className == classe ){
        element.className = classe.replace('unselect', 'selected');
    } else {
        element.className = classe;
    }
}
</script>
@endpush