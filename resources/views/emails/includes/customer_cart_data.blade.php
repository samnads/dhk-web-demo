<!------------------------ ------------------------------------------------->
{{--
    @if ($data['service_data']['service_mappings'])
    <tr style="background: #FFF;">
        <td colspan="3">
            <table border="1"
                style="width: 100%;border-color: #e5e2e2;font-family: Tahoma, Geneva, sans-serif;color: #555;text-align: center;">
                <tr style="background: #efefef;">
                    <th colspan="4" style="font-weight: bold;">Service Details</th>
                </tr>
                <tr style="background: #fafafa;">
                    <th>Name</th>
                    <th>Rate</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                </tr>
                @foreach ($data['service_data']['service_mappings'] as $key => $service_mapping)
                    <tr>
                        <td>{{ $service_mapping['name'] }}</td>
                        <td>{{ $service_mapping['service_cost'] }}</td>
                        <td>{{ $service_mapping['quantity'] }}</td>
                        <td>{{ $service_mapping['service_cost'] * $service_mapping['quantity'] }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
@endif
--}}
<!------------------------ ------------------------------------------------->
@if ($data['service_data']['cleaning_supplies'])
    <tr style="background: #FFF;">
        <td colspan="3">
            <table border="1"
                style="width: 100%;border-color: #e5e2e2;font-family: Tahoma, Geneva, sans-serif;color: #555;text-align: center;">
                <tr style="background: #efefef;">
                    <th colspan="4" style="font-weight: bold;">Cleaning Supplies</th>
                </tr>
                <tr style="background: #fafafa;">
                    <th>Name</th>
                    <th>Rate</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                </tr>
                @foreach ($data['service_data']['cleaning_supplies'] as $key => $cleaning_supply)
                    <tr>
                        <td>{{ $cleaning_supply['cleaning_supply_name'] }}</td>
                        <td>{{ $cleaning_supply['cleaning_supply_unit_rate'] }}</td>
                        <td>{{ $cleaning_supply['cleaning_supply_quantity'] }}</td>
                        <td>{{ $cleaning_supply['cleaning_supply_unit_rate'] * $cleaning_supply['cleaning_supply_quantity'] }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
@endif
<!------------------------ ------------------------------------------------->
@if ($data['service_data']['extra_services'])
    <tr style="background: #FFF;">
        <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Extra Services</p></td>
        <td style="border-top:1px solid #e5e2e2;">:</td>
        <td>
            <p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
            @php
                $extra_services = array_column($data['service_data']['extra_services'],'extra_service_name');
                echo join(' and ', array_filter(array_merge(array(join(', ', array_slice($extra_services, 0, -1))), array_slice($extra_services, -1)), 'strlen'));
            @endphp
            </p>
        </td>
    </tr>
@endif
<!------------------------ ------------------------------------------------->
{{--@if ($data['service_data']['extra_services'])
    <tr style="background: #FFF;">
        <td colspan="3">
            <table border="1"
                style="width: 100%;border-color: #e5e2e2;font-family: Tahoma, Geneva, sans-serif;color: #555;text-align: center;">
                <tr style="background: #efefef;">
                    <th colspan="5" style="font-weight: bold;">Extra Services</th>
                </tr>
                <tr style="background: #fafafa;">
                    <th>Service</th>
                    <th>Duration</th>
                    <th>Rate</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                </tr>
                @foreach ($data['service_data']['extra_services'] as $key => $extra_service)
                    <tr>
                        <td>{{ $extra_service['extra_service_name'] }}</td>
                        <td>{{ $extra_service['extra_service_duration'] }} Mins.</td>
                        <td>{{ $extra_service['extra_service_unit_rate'] }}</td>
                        <td>{{ $extra_service['extra_service_quantity'] }}</td>
                        <td>{{ $extra_service['extra_service_total_amount'] }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
@endif--}}
<!------------------------ ------------------------------------------------->
