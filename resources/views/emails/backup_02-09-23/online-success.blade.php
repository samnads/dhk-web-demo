<html>
<head>
</head>

<body>

<div class="main-wrapper" style="width: 800px; margin:0 auto;">

     <div style="width: 800px;"><img src="{{asset('images/dhk-banner.jpg')}}"  width="800" height="303" alt="" /></div>
     
     <div style="border-left: 1px solid #ccc; border-right: 1px solid #ccc; width: 798px;">
	 <div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
     
     <div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear {{$customer['customer_name']}},</p>
		  <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Thank you for your Payment!</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Welcome to Dubai Housekeeping Cleaning Services. You can find your booking ID below. We hope you have a great experience with our services...</p>
		  @if(isset($online) && ($online != null ) && ($online['transaction_id'] != '0' && $online['transaction_id'] !='' ))
		  <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; font-weight: bold; margin:0px;">Payment transaction id - {{$online['transaction_id']}}</p>
		  @endif
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:15px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px;">
          <span style="font-size:15px; background: #62a6c9; padding: 12px 30px; border-radius: 50px; margin:0px; font-weight: bold;">Booking Reference ID - {{$online['reference_id']}}</span>
          </p>
     </div>
     
     
     <div style="width: 560px; height:auto; background: #fafafa; border:1px solid #e5e2e2; padding: 20px 20px; ">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:0px 0px 0px 0px; font-weight: bold; margin:0px;">Address Summary</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:5px 0px 0px 0px; margin:0px;">Address : @if($customerAddress !=''){{$customerAddress['customer_address']}}@else NA @endif<br />
            
             Area -  {{$areaName}}</p>
     </div>
     
     
     
     <div style="width: 600px; height:auto; background: #fafafa; border:1px solid #e5e2e2; border-top:0px; padding:0px 0px;">
          <table width="600" border="0" cellspacing="0" cellpadding="0" bordercolor="#FFF">
		 
            
            <tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Payment Method</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">Card</p></td>
            </tr>
		
			<tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Total Cost</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED {{number_format(@$online['amount'],2,'.','')}}</p></td>
            </tr>
			
			 <tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Transaction Charge</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED {{number_format(@$online['transaction_charge'],2,'.','')}}</p></td>
            </tr> 
           
            <tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Net Payable</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <?php 
              $total = $online['amount'] + $online['transaction_charge'];
              ?>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; font-weight: bold; padding: 12px 10px; margin:0px;">AED {{number_format($total,2,'.','')}}</p></td>
            </tr>
          </table>

     </div>
     
     
     
     
     <div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">
          <strong>Grace Quest Cleaning Services</strong><br />
          P. O. Box : 236887, Office #4201B,<br />
          ASPiN Commercial Tower,Sheikh Zayed Road,<br />
		  Dubai, United Arab Emirates<br />
          For Bookings : +971 4 263 1976<br />
          Email : office@dubaihousekeeping.com</p> 
     </div>
     
     

     
     </div>
     
<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #fafafa; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #555; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> Dubai Housekeeping All Rights Reserved.</p>
     </div>

</div>
	 
	 </div>


</body>
</html>
