<html>
<head>
</head>

<body>
<div class="main-wrapper" style="width: 800px; margin:0 auto; background:#ddf8ed;">
	<div style="width: 800px;"><img src="{{asset('images/email-banner-new.jpg')}}"  width="800" height="303" alt=""/></div>
	
	<div style="width: 798px; background:#ddf8ed;">
	
		<div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
			<div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear {{$data['customer_name']}},</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Your booking with below reference id has been cancelled successfully...</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:15px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px;"> <span style="font-size:15px; background: #00bff3; padding: 12px 30px; border-radius: 50px; margin:0px; font-weight: bold;">Your Reference ID - {{$data['reference_id']}}</span> </p>
			</div>
			<div style="width: 560px; height:auto; background: #fafafa; border:1px solid #e5e2e2; padding: 20px 20px; ">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:0px 0px 0px 0px; font-weight: bold; margin:0px;">Address Summary</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:5px 0px 0px 0px; margin:0px;"> Address - {{$data['address']?:"NA"}} </br>
					Area - {{$data['area']}} </br>
					Email ID - {{$data['customer_email']}} </br>
					Mobile Number - {{$data['phone_number']}} </br>
				</p>
			</div>
			<div style="width: 600px; height:auto; background: #fafafa; border:1px solid #e5e2e2; border-top:0px; padding:0px 0px;">
				<table width="600" border="0" cellspacing="0" cellpadding="0" bordercolor="#FFF">
					<?php 	$start = date("g:i a", strtotime(@$data['time_from'])); 
											$end = date("g:i a", strtotime(@$data['time_to'])); ?>
					<tr style="  ">
						<td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Service Date & Time</p></td>
						<td style="">:</td>
						<td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$data['service_start_date']}}<br/>
								{{$start}} - {{$end}}</p></td>
					</tr>
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Payment Method</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$data['pay_by']}}</p></td>
					</tr>
				</table>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Please read our <a href="https://dubaihousekeeping.com/terms-conditions/">Cancellation Policy</a></p>
			</div>
			
			<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
				<div style="width: 100%; text-align: center;"><img src="{{asset('images/dhk-logo.png')}}" width="300" height="33" alt="" style="margin:0 auto;" /></div>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 0px 15px 0px; margin:0px;">Grace Quest Cleaning Services<br />
					Office 4201B, ASPiN Commercial Tower,<br />
					Sheikh Zayed Road, Dubai, U.A.E<br />
					Phone : +971 4 263 1976<br />
					Email : office@dubaihousekeeping.com</p>
			</div>
			
		</div>
		<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #1cac51; text-align: center;">
			<p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #FFF; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> Dubai Housekeeping All Rights Reserved.</p>
		</div>
	</div>
</div>
</body>
</html>
