<html>
<head>
</head>
<body>
<div class="main-wrapper" style="width: 800px; margin:0 auto; background:#ddf8ed;">
	<div style="width: 800px;"><img src="{{asset('images/email-banner-new.jpg')}}" width="800" height="303" alt="" /></div>
	<div style="width: 798px; background:#ddf8ed;">
		<div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
			<div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear {{$name}},</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Thank you for registration...</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">You successfully registered on Dubai Housekeeping Cleaning Services.</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Please use the following details to login our portal.</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">User Name -  {{$email}}<br>
					Password  -  {{$password}}</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Please read our <a href="https://dubaihousekeeping.com/terms-conditions/">Cancellation Policy</a></p>
			</div>
			
			<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
				<div style="width: 100%; text-align: center;"><img src="{{asset('images/dhk-logo.png')}}" width="300" height="33" alt="" style="margin:0 auto;" /></div>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 0px 15px 0px; margin:0px;">Grace Quest Cleaning Services<br />
					Office 4201B, ASPiN Commercial Tower,<br />
					Sheikh Zayed Road, Dubai, U.A.E<br />
					Phone : +971 4 263 1976<br />
					Email : office@dubaihousekeeping.com</p>
			</div>
			
		</div>
		<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #1cac51; text-align: center;">
			<p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #FFF; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> Dubai Housekeeping All Rights Reserved.</p>
		</div>
	</div>
</div>
</body>
</html>
