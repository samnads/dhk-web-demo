<!doctype html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KT5TTZ2');</script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KT5TTZ2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @include('includes.headerAssets')
</head>
    <body class="bg-color">
    @if(strpos($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],'dhk-web-demo') !== false)
    <div class="demo-ribbon"><span>DEMO</span></div>
    @endif
     <div class="wrapper-main">
    @include('includes.header')
        <div id="preLoader">
            <div class="loader">
                <div class="loader_shadow"></div>
                <div class="box"></div>
            </div>
        </div>
        <div class="preloader">
            <div class="loader">
                <div class="loader_shadow"></div>
                <div class="box"></div>
            </div>
        </div>
        @yield('content')
        <div class="col-sm-12 popup-main login-popup" id="login-popup">
            <form id="login_form" method="post">
            <div class="row min-vh-100 d-flex flex-column justify-content-center">
                <div class="col-lg-4 col-md-7 col-sm-4 popup-content em-booking-det-cont mx-auto signin-set-main shadow">
                        <h5 class="">Login <span class="em-login-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
                        <span id="error_message_p" style="display:none;"></span> 
                        <div class="row ml-0 mr-0">
                            <div class="col-md-6 col-sm-12 login-left-set pl-0 pr-0 pb-2">
                                <div class="col-sm-12 em-field-main pl-0 pr-0">
                                    <p>Username / Email ID</p>
                                    <div class="col-12 em-text-field-main pl-0 pr-0">
                                        <input name="emailUser" class="text-field" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-12 em-field-main pl-0 pr-0">
                                    <p>Password</p>
                                    <div class="col-12 em-text-field-main password-set pl-0 pr-0">
                                        <input name="passwordUser" class="text-field" type="password">
                                        <div class="password-set-btn fa fa-fw fa-eye-slash"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 em-field-main pl-0 pr-0">
                                    <p><a onclick="viewForgetCommon();" href="#" class="cursor">Did you forget your password?</a></p>
                                </div>
                                <div class="col-sm-12 em-field-main  log-remb pl-0 pr-0">
                                    <input value="" id="login" class="" type="checkbox">
                                    <label for="login"> <span></span> &nbsp; Remember Me</label>
                                </div>
                            </div>                     
                            <div class="col-md-6 login-right-set pr-0 d-none d-md-block d-xl-block">
                                <div class="col-sm-12 login-right-image pr-0"><img src="{{asset('images/login.png')}}" alt=""></div>
                            </div>
                            </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                            <input value="Login" class="text-field-button show6-step" id="button_submit_login" type="submit">
                    </div>      
                </div>
            </div>
            </form>
        </div><!--popup-main end-->
        <!-- forgot password-->
<div class="col-sm-12 popup-main forgot-password-popup" id="forgot_password_common">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-3 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
               <form id="forgotFormCommon" method="post">
               {{ csrf_field() }}
                    <h5 class="">Forgot Password <span class="em-forgot-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
                    <p id="success_message">Please enter a valid email address with which you created your emaid account.</p>

                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                         <div class="row m-0">
                              <div class="col-12 em-text-field-main pl-0 pr-0">
                                   <input name="emailId" plaecholder="Email id" class="text-field" type="text">
                              </div>
                         </div>
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                         <input value="Send" id="submit_email" class="text-field-button show6-step col-sm-6" type="submit">
                    </div>  
               </form>    
          </div>
     </div>
</div><!--popup-main end-->

	<!--popup-otp-->
    <div class="col-sm-12 popup-main otp-popup" id="verify_mobilel" style="display: none">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-3 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <h5 class="">OTP Verification <span class="em-otp-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
                <p>Enter the OTP you received to<br>
                <span id="mobile_spanl">+971 2345 678</span></p>
                <p>OTP is being sent as email &amp; SMS</p>
                <p id="resend_messagel"></p>
                <form id="verify_otpForml" method="post">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <!-- {{ csrf_field() }} -->
                <!-- <input type="hidden" value="{{@$bookingId}}" id="booking_id" name="booking_id">  -->
                <input type="hidden" value="{{session('customerId')}}" id="customerIdl" name="customerId"> 
                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                         <div class="row m-0">
                              <div class="col-3 em-text-field-main pl-0 pr-0">
                                   <input name="first" id="codeBox1l" type="tel" class="text-field border-right-0" maxlength="1" onkeyup="onKeyUpEventl(1, event)" onfocus="onFocusEventl(1)">
                              </div>
                              <div class="col-3 em-text-field-main pl-0 pr-0">
                                   <input name="second" id="codeBox2l" type="tel" class="text-field border-right-0" maxlength="1" onkeyup="onKeyUpEventl(2, event)" onfocus="onFocusEventl(2)">
                              </div>
                              <div class="col-3 em-text-field-main pl-0 pr-0">
                                   <input name="third" id="codeBox3l" type="tel" class="text-field border-right-0" maxlength="1" onkeyup="onKeyUpEventl(3, event)" onfocus="onFocusEventl(3)">
                              </div>
                              <div class="col-3 em-text-field-main pl-0 pr-0">
                                   <input name="fourth" id="codeBox4l" type="tel" class="text-field " maxlength="1" onkeyup="onKeyUpEventl(4, event)" onfocus="onFocusEventl(4)">
                              </div>
                         </div>
                    </div>
                    <span id="otp_errorl" style="color:red;font-size:13px;" ></span>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                         <button type="button" onclick="resendOTP();" style="display:none" id="resend_button" class="text-field-button show6-step">Resend OTP</button>
                         <input value="Continue" id="otp_submitl" class="text-field-button show6-step col-sm-6" type="submit">
                    </div>  
               </form>    
          </div>
     </div>
</div><!--popup-main end-->

<!-- forgot password success-->
<div class="col-sm-12 popup-main forgot-password-popup" id="success_password">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-3 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                    <h5 class="">Forgot Password <span class="em-forgot-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
                    <span class="message" id="message">Please enter a valid email address with which you created your emaid account.</span> 
          </div>
     </div>
</div><!--popup-main end-->

<div class="col-sm-12 popup-main forgot-password-popup" id="errormessagepopup" style="display: none;">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-3 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
               
			<h5 class="" onclick="closeerrorpop()">Error <span class="em-forgot-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
			<p id="success_message">Cuurently we dont have free maids for the selected area. Please try later.</p>

			<div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
				 <input value="Close" id="closeerror" class="text-field-button show6-step col-sm-6" type="button" onclick="closeerrorpop()">
			</div>
          </div>
     </div>
</div><!--popup-main end-->
<div class="col-sm-12 popup-main forgot-password-popup" id="paymenterrormessagepopup" style="display: none;">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-3 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
               
			<h5 class="" onclick="closepaymenterrorpop()">Error <span class="em-forgot-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
			<p id="success_pay_message">Cuurently we dont have free maids for the selected area. Please try later.</p>

			<div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
				 <input value="Close" id="closepaymenterror" class="text-field-button show6-step col-sm-6" type="button" onclick="closepaymenterrorpop()">
			</div>
          </div>
     </div>
</div><!--popup-main end-->
        @include('includes.footer')
        @include('includes.footerAssets')
        </div>
    </body>
    
    @if (isWebView())
    <style>
        .em-booking-content-section {
            padding-top: 50px;
            min-height: 100vh;
        }
        .book-det-cont-set {
            margin-top: 0;
        }
    </style>
    @endif

    <style>
        #timeSelected-error{
            margin-left: 15px;
        }
       
    </style>
</html>