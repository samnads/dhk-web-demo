<?php if(!isset($_GET['mobile'])) { ?>
<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">
    <div class="col-md-12 col-sm-12 em-step-head em-head no-left-right-padding">
        <h2>Book Your Service</h2>
        <ul>
            <li class="active"><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
        </ul>
    </div>
</div>
<?php } ?>
<form id="step1Carpet" method="post">
{{ csrf_field() }}
<input type="hidden" value="" id="noOfHours" name="noOfHours">
    <div class="col-12 em-booking-content-set pl-0 pr-0">
        <div class="row em-booking-content-set-main ml-0 mr-0">
            <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
                <div class="col-12 step1 pl-0 pr-0">
                    @include('common.services')
                    <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0">
                        <div class="col-12 book-det-cont-set-main carpet-cleaning-service  ser-cont4 ser-cont" style="display:block;" id="cont4">
                            <h5>Carpet Cleaning Services</h5>
                            <input type="hidden" id="service_category" value="0">

                                <div class="col-sm-12 em-field-main-set"> 
                                    <div class="row m-0">
                                    
                                        <div class="col-sm-12 em-often-section-box pt-1 pl-0 pr-0">
                                            <div class="row m-0">
                                               

                                                <div id="office_div" class="col-sm-6 pt-3">
                                                        <p>What is the size of carpet?</p>
                                                        <select class="js-select2" name="carpet_count" id="carpet_count" onChange="amountCalculation();">
                                                            <option value="">Select Square meter</option>
                                                            @for ($i = 1; $i <= 40; $i++)
                                                            <option value="{{$i}}" @if($i == '10') selected @endif>{{$i}} square meter</option>
                                                            @endfor
                                                        </select>
                                                </div>
                                            </div>
                                           

                                            

                                        </div>
                                    
                                    </div> 
                                </div>
                        </div>
                        <div class="col-sm-12 em-terms-and-condition-main"> 
                            <h6>What to expect from the service?</h6>
                            <div class="col-sm-12 em-terms-and-condition">
                                <ul class="">
                                    <li>
                                        <div class="em-num">1</div>
                                        <div class="em-num-cont"><p>Carpet shampooing</p></div>
                                        <div class="clear"></div>
                                    </li>
                                    <li>
                                        <div class="em-num">2</div>
                                        <div class="em-num-cont"><p>Removal of stains and watermarks</p></div>
                                        <div class="clear"></div>
                                    </li>
                                    <li>
                                        <div class="em-num">3</div>
                                        <div class="em-num-cont"><p>Carpet sanitation</p></div>
                                        <div class="clear"></div>
                                    </li>
                                    <li>
                                        <div class="em-num">4</div>
                                        <div class="em-num-cont"><p>Prevention of potential contaminants</p></div>
                                        <div class="clear"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <!--<span class="em-back-arrow " title="Previous Step"></span>-->
                                <div class="col-12 sp-total-price-set pl-0 pr-0">
                                    Total<br>
                                    <span>AED <b class="mobile_net_p"></b></span> 
                                </div>
                            
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                <input value="Next" class="text-field-button show2-step" id="submit_button" type="submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('includes.steps.summary')
        </div>
    </div>
</form>
