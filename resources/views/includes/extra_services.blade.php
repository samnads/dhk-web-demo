@if ($data['service_extra_services'])
    <div class="col-sm-12 checkbox-extra-service">
        <p>Extra services within booked time</p>
        <div class="row">
            @foreach ($data['service_extra_services'] as $key => $extra_service)
                <div class="col-lg-6 col-md-6 col-sm-6 col-6 n-extra-ser">
                    <div class="row service">
                        <input id="extra_service_{{ $key }}" value="{{ $extra_service['id'] }}"
                            name="extra_services[]" type="checkbox"
                            data-show-from-hours="{{ $extra_service['show_on_min_hour'] }}"
                            data-show-to-hours="{{ $extra_service['show_on_max_hour'] }}"
                            data-cost="{{ $extra_service['cost'] }}"
                            data-duration="{{ $extra_service['duration'] }}">
                        <label for="extra_service_{{ $key }}">
                            <div class="row em-extra-ser-tmb m-0">
                                <div class="col-sm-4 em-extra-ser-img"><img src="{{ asset('images/extra-services/'.($extra_service['image_url'] ?: 'extra-service-no-image.jpg').'?v='. config('version.img')) }}"
                                        alt="{{ @$extra_service['imgAltTag'] }}"></div>
                                <div class="col-sm-8 em-extra-ser-cont">
                                    {{ $extra_service['service'] }}<br>
                                    {{ $extra_service['duration'] }} min {{ $extra_service['cost'] }} AED
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif