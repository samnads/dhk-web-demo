<script  type="text/javascript" src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/jquery.validate.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/owl.carousel.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/moment.latest.min.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/pignose.calendar.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/select2.full.min.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/intlTelInput.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/jquery.query-object.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('js/custom.js?v=').config('version.js')}}"></script>
<script type="text/javascript">
var _base_url = "{{url('')}}/";
var _current_url = "{{strtok(url()->full(), '?')}}";
var _full_url = "{!!urldecode(url()->full())!!}";
var _is_webview = {{isWebView() ? 'true' : 'false'}};
var redirect_to_url = null;
var _no_of_hours = 0;
var no_of_maids = 1;
var service_rate_per_hour = 0;
var tools_rate = 0;
var hourly_material_fee = 0;
var _materials_rate = 0;
var _hourly_supervisor_fee = 60;
var _supervisor_fee = 0;
var _total_payable = 0;
var _total_without_vat = 0;
var _vat_charge = 0;
var _service_only_charge = 0;
var _vat_percentage = {{Config::get('values.vat_percentage')}};
var _coupon_discount = 0;
var _total_discount = 0;
var _discount = 0;
var _total_before_tax = 0;
var _taxable_amount = 0;
var _coupon_code = {!! Request::segment(2) ? '"'.Request::segment(2).'"' : "undefined" !!};
function scrollToElement(element){
	if($(element).is(":visible")){
		$("html").animate(
			{
				scrollTop: element.offset().top-50
			},
			800 //speed
			);
		}
		else{
			// some element not visible so use parent
			$("html").animate(
				{
					scrollTop: element.parent().offset().top-50
				},
				800 //speed
			);
		}
}
function viewForgetCommon()
{
     $('#forgot_password_common').show(500);
     $('#emailId').val('');
}
$("#forgotFormCommon").validate({
	 ignore: [],
     rules: {
          emailId: {
               customemail:true,
               required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                },
            },
          },
    },
    messages: {
     emailId: "Please enter your email id.",
     },
    errorPlacement: function (error, element)
    {
            error.insertAfter(element.parent().parent());
    },
     submitHandler: function (form,event) {
          var form_data = new FormData($("#forgotFormCommon")[0]);
          form_data.append('_token', '{{csrf_token()}}');
          $('#forgot_password_common').hide(500);
          $("#submit_email").attr( "disabled", "disabled" );
          $(".preloader").show();
          $.ajax({
               method: 'post',
               url: '{{url('forgot-password')}}',
               data: form_data,
               cache : false,
               contentType: false,
               processData: false,
               success: function (result) {
                    $(".preloader").hide();
                    $('#forgotFormCommon')[0].reset();
                    if (result.status == 'success') {
                         $("#submit_email").attr( "disabled", false );
						 $('#error_message_p').show();
                         $('#error_message_p').text('Mail send successfully.')
                    } else {
						$('#error_message_p').show();
                         $("#submit_email").attr( "disabled", false );
                         $('#error_message_p').text('Failed!Please try again.')
                    }
               }
          }); 
     }  
}); 
$(document).ready(function() {
	var customersession = '<?php echo session('customerId')?>';
	var customerAddress = `<?php echo session('addressName');?>`;
	var areaName = `<?php echo session('areaName');?>`;

	if(customersession == '') {
		localStorage.clear(); 
		$('.login').show();
		$('.logout').hide();
		$('.userPersonel').hide();
		$('.make_pay_head').hide();
	} else{
		localStorage.setItem('customerId',customersession);
		if(customerAddress!='' && areaName!='' ) {
			localStorage.setItem('address',customerAddress);
			localStorage.setItem('area',areaName);
		}  else {
			localStorage.removeItem("address"); 
			localStorage.removeItem("area"); 
		}
	}

  var customerId = localStorage.getItem('customerId');
  if ((customerId) && customerId != '' && localStorage.getItem('address') !=null) {
	$('.login').hide();
	$('.logout').show();
	$('.make_pay_head').show();
	$('.userPersonel').show();
	$('.address_div').show();
	$('#typeLogin').val('login');
	$('#customer_id').val(localStorage.getItem('customerId')); 
	$('#customerid').val(localStorage.getItem('customerId')); 

	$('.address_p').text(localStorage.getItem('address')); 
	$('.area_p').text(localStorage.getItem('area')); 
  } else if((customerId) && customerId != '') {
	$('.login').hide();
	$('.logout').show();
	$('.make_pay_head').show();
	$('.userPersonel').show();
	$('#customer_id').val(localStorage.getItem('customerId')); 
	$('#customerid').val(localStorage.getItem('customerId')); 
  }
  var loc = window.location.href; // returns the full URL
  if(/mobile=active/.test(loc)) {
	$('.em-booking-content-section').addClass('top_length');
  }
  @if(Request::segment(2))
  console.log("Apply coupon...")
  applyCoupon();
@endif
});
function viewLogin()
{
	$('#login-popup').show(500);
}
$('.em-login-close-btn').click(function(){
		$('.login-popup').hide(500);
});
$('.close-pay-poup').click(function(){
		$('#payment_popup').hide(500);
});
$("#login_form").validate({
     ignore: [],
     rules: {
        emailUser: {
		  required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                },
            },
			customemail:true,
		},
		passwordUser: {
			required:true,
			minlength:6  
		},
    },
    messages: {
		emailUser:"Please enter your registered email id.",
		passwordUser:{
			required: "Please enter your password.",
			minlength: "Password must contain atleast 6 characters."
		}, 
     },
    errorPlacement: function (error, element)
    {
            error.insertAfter(element.parent().parent());
    },
     submitHandler: function (form,event) {
		$('#error_message_p').hide();
		var form_data = new FormData($("#login_form")[0]);
		form_data.append('_token', '{{csrf_token()}}');
		$("#button_submit_login").attr( "disabled", "disabled" );
		$(".preloader").fadeIn(300);
		$.ajax({
			method: 'post',
			url: '{{url('login-user')}}',
			data: form_data,
			cache : false,
			contentType: false,
			processData: false,
			success: function (result) {
				$(".preloader").fadeOut(300);
				if (result.status == 'success') {
					$(".preloader").fadeIn(300);
					$('#login_form')[0].reset();
					$("#button_submit_login").attr( "disabled", false );
					localStorage.setItem('customerId',result.data['id']);
					
					var address = result.data['address'];
					if(address.length != 0 ) {
						localStorage.setItem('address',result.data['address']['addressName']);
					    localStorage.setItem('area',result.data['address']['Area']);
					}
					$('#error_message_p').hide();
					$('.login-popup').hide(500);
					if(redirect_to_url != null){
						window.location.href = redirect_to_url;
						return false;
					}
					afterPopupLoginSuccess(result,true);
				} else if(result.data && result.data.mobile_verified!=undefined && !result.data['mobile_verified']){
					$('#login-popup').hide(500);
					$('#verify_mobilel').show(500);
					$('#customerIdl').val(result.data['id']);
					$('#mobile_spanl').html(result.data['mobile']);
					$('#error_message_p').text(result.messages);
					$('#error_message_p').show();
					amountCalculation();
				}else {
					$("#button_submit_login").attr( "disabled", false );
					$('#error_message_p').show();
					$('#error_message_p').addClass("text-danger");
					$('#error_message_p').text(result.messages);
				}
			}
		}); 
    }  
});
window.onload = function () {
    $('#preLoader').fadeOut(100);
}
function weekDayChanged(week_day,old_week_day){
	var booking_type = $('input[name="how_often"]:checked').val();
	if(week_day != old_week_day){
		$('input[name="service_week_days[]"]').prop('checked', false).attr("disabled", true);
	}
	if(booking_type != 'OD'){
		$('#week_selection').show(200);
		$('input[name="service_week_days[]"]').attr("disabled", false);
		$('input[name="service_week_days[]"][value='+week_day+']').prop('checked', true).attr("disabled", false); // always check selected week
		$('.info-price-we-bw').show();
	}
	else{
		$('.info-price-we-bw').hide();
	}
}
$('input[name="how_often"]').change(function() {
	// change booking type (OD,WE,BW)
	var booking_type = this.value;
	var cleaning_date = $('#cleaning_date').val();
	var week_day = moment(cleaning_date,'YYYY-MM-DD').day();
	$('input[name="service_week_days[]"]').prop('checked', false).attr("disabled", true);
	/************* manage week selection div start ******************/
	if(booking_type != 'OD'){
		$('#week_selection').show(200);
		$('input[name="service_week_days[]"]').attr("disabled", false);
		$('input[name="service_week_days[]"][value='+week_day+']').prop('checked', true).attr("disabled", false); // always check selected week
		$('.info-price-we-bw').show();
	}
	else{
		$('#week_selection').hide(200);
		$('.info-price-we-bw').hide();
	}
	/************* manage week selection div end *******************/
});
$(function () {
		var couponvall = location.pathname.split('/').pop();
        var tdate = moment().format('YYYY-MM-DD');
		var calculate_availability_request = null;
		weekDayChanged(moment().day(),moment($('#cleaning_date').val(),'YYYY-MM-DD').day());
		$('#cleaning_date').val(tdate);
        $('.toggle-calendar1').pignoseCalendar({
                minDate:tdate,
				disabledWeekdays: [
					{{implode (@$data['weekends'] ?: [])}}
				],
				disabledDates: [
					{!! @$data['holiday_dates'] ? "'".implode ( "','",@$data['holiday_dates'] )."'" : ''!!}
				],
                select: function(date, context) {
					weekDayChanged(date[0].day(),moment($('#cleaning_date').val(),'YYYY-MM-DD').day());
                    var dates = date[0] === null ? '' : date[0].format('YYYY-MM-DD');
					var dateFormat = date[0] === null ? '' : date[0].format('DD/MMMM/YYYY');
					$('.date_p').html(dateFormat);
					$('#availableTime').html(date[0].format('DD-MM-YYYY'));
					$('#cleaning_date').val(dates);
					$('#timeSelected').val('');
					$('.time_span1').text('');
					$('.time_span2').text('');
					$('.to_p').text('');
					$('#timeSelected-error').hide();
					var numberHours =$('#noOfHours').val();
					
					$('.timeSelection').html(`<div class="d-flex justify-content-center">
						<div class="spinner-border" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						</div>`);
						calculate_availability_request = $.ajax({
							method: 'post',
							url: '{{url('calculate-availability')}}',
							data:{
								'bookedDate':dates,
								'service_id':$('#service_id').val(),
								'numberHours':numberHours,
								_token:'{{csrf_token()}}'
							},
							beforeSend : function()    {           
								if(calculate_availability_request != null) {
									calculate_availability_request.abort();
								}
							},
							success: function (result) {
								if (result.status == 'success') {
									$('.timeSelection').html(result.data['html']);
       								$('.cleaning_date').val(result.data['date']);

									$('.toggle-calendar1').pignoseCalendar('set', result.data['date']);
									var monthNames = [
										"January", "February", "March",
										"April", "May", "June", "July",
										"August", "September", "October",
										"November", "December"
									];
									var selDate = new Date(result.data['date']);
									var day = selDate.getDate();
									var monthIndex = selDate.getMonth();
									var year = selDate.getFullYear();
									var avail = day + '/' + monthNames[monthIndex] + '/' + year;
									$('.date_p').html(moment(selDate).format("DD-MM-YYYY"));
									$('#availableTime').html(moment(selDate).format("DD-MM-YYYY"));
									$('.timeSelection li').on('click', function(){
										$(this).siblings().removeClass('selected');
										$(this).addClass('selected');
										var target=  $(this).children('.tick-text').attr("id");
										$('#timeSelected').val(target);
										$('#timeSpan1').val(target);
										$('.time_span1').text(target);
										$('.to_p').html('&nbsp;-&nbsp;');

											function D(J) { 
												return (J<10? '0':'') + J;
												};
											var piece = target.split(':');
											var mins = piece[0]*60 + +piece[1] + +(numberHours*60);
											var totalTime = D(mins%(24*60)/60 | 0) + ':' + D(mins%60);  
										$('.time_span2').text(totalTime+':00')
										$('#timeSpan2').val(totalTime+':00');
										$('#timeSelected-error').hide();
										//amountCalculation();
										amountCalculationSecond();
									}); 
									$('.timeSelection li:first').trigger("click");
								}
								//amountCalculation();
								amountCalculationSecond();
							}
						}); 
                }
	});
});
	function logout()
	{
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
     	});
		$(".preloader").fadeIn(100);
		$.ajax({
			method: 'get',
			url: '{{url('logout')}}',
			data:{_token:'{{csrf_token()}}'},
			success: function (result) {
				if (result.status == 'success') {
					$(".preloader").fadeOut(100);
					localStorage.clear(); 
					$('.login').show();
					$('.logout').hide();
					$('.userPersonel').hide();
					$('.make_pay_head').hide();
					window.location.href = '{{url('/')}}';
				}
			}
    	}); 
	}
	
	$(".couponClass").keyup(function(){
		var titles = $('input[name^=couponCode]').map(function(idx, elem) {
			return $(elem).val();
		}).get();
		$('input[name^=couponCode]').each(function(index, value) {
			var divId = $("div.items").not(":hidden").prop("id");
			if(divId == 'commonStep2') {
				this.value = titles[1];
			}
			if(divId == 'commonStep3') {
				this.value = titles[2];
			}
			if(divId == 'commonStep4') {
				this.value = titles[3];
			}
			if(divId == 'commonStep5') {
				this.value = titles[4];
			}
			if(divId == 'carpetStep1') {
				this.value = titles[0];
			}
			if(divId == 'sofaStep1') {
				this.value = titles[0];
			}
			if(divId == 'deepCleaning1') {
				this.value = titles[0];
			} 
			if(divId == 'commonStep1') {
				this.value = titles[0];
			}
			if(divId == 'houseCleaningStep1') {
				this.value = titles[0];
			}
			if(divId == 'mattressStep1') {
				this.value = titles[0];
			}
    	});
		var coupon = $('.couponClass').val();
		if(coupon == '') { 
			$('.couponClass').val('');
			$('.coupon_message').hide();
			amountCalculation();
		}
	});

	function applyCoupon(clicked) {
		if(clicked == true){
			_coupon_code = $('.couponClass').val();
		}
		if (_coupon_code == '') {
			return false;
		} else {
			var customerId = $('#customer_id').val();
			var bookedDate = $('#cleaning_date').val();
			var serviceId = $('#service_id').val();
			var cleaningStatus = $('#cleaningMaterialStatus').val();
			$.ajax({
				method: 'post',
				url: "{{url('check-coupon?new')}}",
				data: {
					'customerId': customerId,
					'coupon': _coupon_code,
					'numberHours': _no_of_hours,
					'bookedDate': bookedDate,
					'serviceId': serviceId,
					'selectedAmount': _taxable_amount || 0,
					'no_maids': _no_of_maids,
					'cleaningStatus': cleaningStatus,
					token: '{{csrf_token()}}'
				},
				success: function(result) {
					if (result.status == 'success') {
						//$(".preloader").hide();
						/***************************/
						// show coupon success
						$('.coupon_message').show();
						var imageUrl = "{{asset('images/tickmark1.png')}}";
						$(".coupon_message").css("background-image", "url(" + imageUrl + ")");
						$(".coupon_message").css("color", "green");
						$('.coupon_message').html(result.message)
						/***************************/
						$('.couponClass').val(_coupon_code);
						$('#discount_price').val(result.data['discount']);
						$('#coupon_id').val(result.data['coupon_id']);
						// show divs
						$('.discount_div').show();
						$('.discount_value_div').show();
						// show new values
						$('.discount_p').text(result.data['net_amount']);
						$('.discount_v').text(result.data['discount']);
						$('.vat_p').text(result.data['vat_charge']);
						$('.net_p').text(result.data['gross_amount']);
						$('.mobile_net_p').text(result.data['gross_amount']);
						_coupon_discount = parseFloat(result.data['net_amount']);
					} else {
						/***************************/
						$('.coupon_message').show();
						$(".coupon_message").css("color", "red");
						$('.coupon_message').html(result.message);
						var imageUrl = "{{asset('images/el-warring.png')}}";
						$(".coupon_message").css("background-image", "url(" + imageUrl + ")");
						/***************************/
						$('.total_p').html(parseFloat(_taxable_amount).toFixed(2));
						$('.vat_p').text(((_vat_percentage / 100) * _taxable_amount).toFixed(2));
						_vat_charge = (_vat_percentage / 100) * _taxable_amount;
						_total_payable = _taxable_amount + _vat_charge;
						$('.net_p,.mobile_net_p').html(parseFloat(_total_payable).toFixed(2));
						//
						$('#discount_price').val(0);
						$('#coupon_id').val(0);
						// hide divs
						$('.discount_div').hide();
						$('.discount_value_div').hide();
						// show new values
						$('.discount_p').text("0.00");
						$('.discount_v').text("0.00");
						_coupon_discount = 0;
					}
				}
			});
		}
	}
	function checkCoupon()
	{
		var coupon = $('.couponClass').val();
		if(coupon == '') {
			$('.coupon_message').show();
			$(".coupon_message").css("color", "red");
			$('.coupon_message').html('Please enter a coupon.');
		} else {
			var numberHours = $('#number_hours').val();
			var customerId = $('#customer_id').val();
			var bookedDate = $('#cleaning_date').val();
			var serviceId = $('#service_id').val();
			if(serviceId == 1) {
				var no_maids = parseInt($('#noOfMaids').val());
				var cleaningStatus = $('#cleaningMaterialStatus').val();
			} else {
				var no_maids = 0;
				var cleaningStatus =0;
			}
			var selectedAmount = $('.total_p').html();
		$.ajax({
			method: 'post',
			url: '{{url('check-coupon')}}',
			data:{'booking_type': $('.howOften:checked').val() || 'OD','customerId':customerId,'coupon':coupon,'numberHours':numberHours,'bookedDate':bookedDate,'serviceId':serviceId,'selectedAmount':$('.taxable_amount').html() || _service_only_charge,'no_maids':no_maids,'cleaningStatus':cleaningStatus,token:'{{csrf_token()}}'},
			success: function (result) {
				if (result.status == 'success') {
					$(".preloader").hide();
					$('.vat_p').text(result.data['vat_charge']);
					$('.net_p').text(result.data['gross_amount']);
					$('.mobile_net_p').text(result.data['gross_amount']);
					$('.discount_div').show();
					$('.discount_p').text(result.data['net_amount']);
					$('.couponClass').val(coupon);
					$('#discount_price').val(result.data['discount']);
					$('#coupon_id').val(result.data['coupon_id']);
					$('.coupon_message').show();
                    $(".coupon_message").css("background-image", "url({{asset('images/tickmark1.png')}})");
					$(".coupon_message").css("color", "green");
					$('.coupon_message').html(result.message)
				} else {
					$('.coupon_message').show();
					$(".coupon_message").css("background-image", "url({{ asset('images/el-warning.png') }})");
					$(".coupon_message").css("color", "red");
					$('.coupon_message').html(result.message);
					$('.discount_div').hide();
					$('#discount_price').val(0);
					$('#coupon_id').val(0);
					$('.vat_p').text(result.data['vat_charge']);
					$('.net_p').text(result.data['gross_amount']);
					$('.mobile_net_p').text(result.data['gross_amount']);
				}
			}
		}); 
		}
	}
$(document).ready(function() {
	var owl = $(".services-scroll");
	owl.owlCarousel({
		items : 3, //10 items above 1000px browser width
		itemsDesktop : [1000,4], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
		itemsTablet: [475,2], //2 items between 600 and 0;
		itemsMobile : [320,2], // itemsMobile disabled - inherit from itemsTablet option
		autoPlay : false,
    });
	$(".next").click(function(){
       //owl.trigger('owl.next');
    });
    $(".prev").click(function(){
      //owl.trigger('owl.prev');
    });
	/********************************************************/
	// show currrent service by scolling
	var service_count_active = parseInt($('.services-scroll .item input.active').attr("data-service-count"));
	//console.log(owl);
	for(var i=0; i<service_count_active;i++){
		//owl.trigger('owl.next');
	}
	/********************************************************/
	$('.sp-total-price-set').click(function(){
		$('.em-booking-content-right').show(500);
	});
	$('.sp-sum-rem-btn').click(function(){
		$('.em-booking-content-right').hide(500);
	});
	$('.show-register-btn').click(function(){
		$('.login-section').hide(500);
		$('.register-section').show(500);
		$('#typeLogin').val('register');
		$("#button_submit_login").attr( "disabled", false );
		$("#button_submit_login").val('Register');
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	$('.show-login-btn').click(function(){
		$('.register-section').hide(500);
		$('.login-section').show(500);
		$('#typeLogin').val('login');
		$("#button_submit_login").attr( "disabled", false );
		$("html").animate({ scrollTop: 0 }, "slow");
		$("#button_submit_login").val('Login');
	});
	$('.show-disinfection-for-office-btn').click(function(){
		$('.disinfection-cleaning-home').hide(500);
		$('.disinfection-cleaning-car').hide(500);
		$('.disinfection-cleaning-office').show(500);
	});
	$('.show-disinfection-for-home-btn').click(function(){
		$('.disinfection-cleaning-office').hide(500);
		$('.disinfection-cleaning-home').show(500);
		$('.disinfection-cleaning-car').hide(500);

	});
	$('.show-disinfection-for-car-btn').click(function(){
		$('.disinfection-cleaning-office').hide(500);
		$('.disinfection-cleaning-home').hide(500);
		$('.disinfection-cleaning-car').show(500);

	});
	$('.show-deep-for-office-btn').click(function(){
		$('.deep-cleaning-home').hide(500);
		$('.deep-cleaning-office').show(500);
	});
	$('.show-deep-for-home-btn').click(function(){
		$('.deep-cleaning-office').hide(500);
		$('.deep-cleaning-home').show(500);
	});
	
	$('.em-otp-close-btn').click(function(){
		$('.otp-popup').hide(500);
	});
	$('.em-forgot-close-btn').click(function(){
		$('.forgot-password-popup').hide(500);
	});
<!--pro-but8 pro-but-->
$(".ser-cont").each(function(){
    if($(this).attr('id') == 'cont1') {	   
    }
});
$('.ser-but').on( "click", function(e) {
    e.preventDefault();
	    $('.ser-but').removeClass('active');
	    $(this).addClass('active');
    var id = $(this).attr('data-id'); 
    $(".ser-cont").each(function(){
           $(this).hide(500);
        if($(this).attr('id') == id) {
           $(this).show(500);
        }
    });
});
});
function showPosition(position) {
    $('.us3-lat').val(position.coords.latitude);
    $('.us3-lon').val(position.coords.longitude);
    locationPickr(position.coords.latitude,position.coords.longitude);
}
function showError(error) {
  switch(error.code) {
    case error.PERMISSION_DENIED:
        $('.us3').locationpicker({
				location: {
					latitude: 25.055277,
					longitude: 55.1586003
				},
               radius: 0,
               inputBinding: {
                   latitudeInput: $('.us3-lat'),
                   longitudeInput: $('.us3-lon'),
                   radiusInput: $('.us3-radius'),
                   locationNameInput: $('.us3-address')
               },
               enableAutocomplete: true,
               onchanged: function (currentLocation,radius,isMarkerDropped) {
                   // Uncomment line below to show alert on each Location Changed event
                   //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
               }
           });  
      break;
    case error.POSITION_UNAVAILABLE:
     console.log("Location information is unavailable.");
      break;
    case error.TIMEOUT:
     console.log("The request to get user location timed out.");
      break;
    case error.UNKNOWN_ERROR:
      console.log("An unknown error occurred.");
      break;
  }
}
function locationPickr(latitude,longitude)
{
    $('.us3').locationpicker({
        location: {
            latitude: latitude,
        longitude: longitude
        },
        
        radius: 0,
        inputBinding: {
            latitudeInput: $('.us3-lat'),
            longitudeInput: $('.us3-lon'),
            radiusInput: $('.us3-radius'),
            locationNameInput: $('.us3-address')
        },
        // markerIcon: '{{URL::asset("images/pin_sm.png")}}',
        enableAutocomplete: true,
        onchanged: function (currentLocation,radius,isMarkerDropped) {
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    }); 
}

$("#verify_otpForml").validate({
     ignore: [],
//      rules: {
         
//           first: {
//                number:true,
//                required: true,
//                maxlength:1
//           },
//           second: {
//                number:true,
//                required: true,
//                maxlength:1
//           }, third: {
//                number:true,
//                required: true,
//                maxlength:1
//           }, fourth: {
//                number:true,
//                required: true,
//                maxlength:1
//           },
          
//     },
//      messages: {
//           first:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//             second:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//             third:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//             fourth:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//      },
//     errorPlacement: function (error, element)
//     {
//             error.insertAfter(element.parent().parent());
//     },
     submitHandler: function (form,event) {   
          if (($('#codeBox1l').val() == '' ||  isNaN($('#codeBox1l').val())) &&  ($('#codeBox2l').val() == '' ||  isNaN($('#codeBox2l').val())) &&
          ($('#codeBox3l').val() == '' ||  isNaN($('#codeBox3l').val())) && ($('#codeBox4l').val() == '' ||  isNaN($('#codeBox4l').val()))) 
          {
             $('#otp_errorl').text("Please enter valid digit");
             return false;
          }
          $('#otp_errorl').text();
          // $('#resend_button').show();
          var form_data = new FormData($("#verify_otpForml")[0]);
          form_data.append('_token', '{{csrf_token()}}');
          // $('#verify_mobile').hide(500);
          $("#submit_emaill").attr( "disabled", "disabled" );
          $(".preloader").show();
          $.ajax({
               method: 'post',
               url: '{{url('verify-mobile')}}',
               data: form_data,
               cache : false,
               contentType: false,
               processData: false,
               success: function (result) {
                    $(".preloader").hide();
                    $('#verify_mobilel').hide();
                    $('#verify_otpForml')[0].reset();
                    if (result.status == 'success') {
                         $("#submit_emaill").attr( "disabled", false );
                         var customerid = localStorage.setItem('customerId',result.data);
                        $('#customer_id').val(result.data);
                        $('.login').hide();
                        $('.logout').show();
                        // $('.make_pay_head').show();
	                    // $('.userPersonel').show();
                        //  $('#commonStep3').fadeOut(300);
                        //  $('#commonStep4').fadeIn(300);
                         $("html").animate({ scrollTop: 0 }, "slow");
                        
                         if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition, showError);
                        } else { 
                            x.innerHTML = "Geolocation is not supported by this browser.";
                        }
                       
                    } else {
                        
                         $("#submit_emaill").attr( "disabled", false );
                         // $('#success_password').show(500);
                         // $('#message').html(result.messages);
                         // $('#success_password').hide(500);
                         $('#verify_mobilel').show();
                         $('#resend_messagel').html(result.messages);
                    }
               }
          }); 
     }  
});   
function onKeyUpEventl(index, event) {
  const eventCode = event.which || event.keyCode;
  if (getCodeBoxElementl(index).value.length === 1) {
	 if (index !== 4) {
		getCodeBoxElementl(index+ 1).focus();
	 } else {
		getCodeBoxElementl(index).blur();
		// Submit code
		console.log('submit code ');
	 }
  }
  if (eventCode === 8 && index !== 1) {
	 getCodeBoxElementl(index - 1).focus();
  }
}
function getCodeBoxElementl(index) {
  return document.getElementById('codeBox' + index + 'l');
}
function onFocusEventl(index) {
  for (item = 1; item < index; item++) {
	 const currentElement = getCodeBoxElementl(item);
	 if (!currentElement.value) {
		  currentElement.focus();
		  break;
	 }
  }
}

$('.em-bok-his-show-btn').click(function(){
	$('.booking-history-popup').show(500);
});
$('.em-bok-his-close-btn').click(function(){
	$('.booking-history-popup').hide(500);
});
</script>
<script>
      var input = document.querySelector("#PHONE_NUMBER");
      window.intlTelInput(input, {
        allowDropdown: true,
        // autoInsertDialCode: true,
        // autoPlaceholder: "off",
        // dropdownContainer: document.body,
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        // geoIpLookup: function(callback) {
        //   fetch("https://ipapi.co/json")
        //     .then(function(res) { return res.json(); })
        //     .then(function(data) { callback(data.country_code); })
        //     .catch(function() { callback("us"); });
        // },
        hiddenInput: "countryCode",
        initialCountry: "ae",
        // localizedCountries: { 'de': 'Deutschland' },
        //nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        preferredCountries: ['ae'],
        // separateDialCode: true,
        // showFlags: false,
        utilsScript: "build/js/utils.js"
      });
    </script>
<style>
    .top_length {
       padding-top: 0.5rem!important;
    }
</style>
@stack('scripts')
