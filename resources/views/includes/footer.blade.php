@if(!isWebView())
<footer>
  <div class="container">
      <div class="row footer-main m-0">
          <div class="col-lg-12 col-md-12 col-sm-12 footer-copy-right pl-0 pr-0">
              <div class="row ml-0 mr-0">
                  <div class="col-12 footer-left p-0"><p class="copy-right text-center">dubaihousekeeping.com © 2014 - <?php echo date('Y'); ?> All Rights Reserved.</p></div>
                  <!--<div class="col-6 footer-right p-0">
                       <div class="design">
                            <a href="https://emaid.info/" target="_blank">
                               <div class="azinova-logo">&nbsp;</div>
                            </a>
                            <p class="no-padding">Powered by </p>
                            <div class="clear"></div>
                       </div>
                  </div>-->
              </div>
          </div>          
      </div>
  </div>
</footer>
@endif