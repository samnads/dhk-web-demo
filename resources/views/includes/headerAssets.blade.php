<title>@if(View::hasSection('title'))@yield('title') @else Dubai Housekeeping @endif</title>
<meta name="description" content="@if(View::hasSection('description'))@yield('description') @else Dubai Housekeeping @endif">
<link rel="canonical" href="@if(View::hasSection('canonical'))@yield('canonical') @else https://booking.dubaihousekeeping.com/house-cleaning-dubai @endif" />
<!-- Facebook Meta Tags -->
<meta property="og:url" content="@if(View::hasSection('canonical'))@yield('canonical') @else https://booking.dubaihousekeeping.com/house-cleaning-dubai @endif">
<meta property="og:type" content="website">
<meta property="og:title" content="@if(View::hasSection('title'))@yield('title') @else Dubai Housekeeping @endif">
<meta property="og:description" content="@if(View::hasSection('description'))@yield('description') @else Dubai Housekeeping @endif">
<meta property="og:image" content="@if(View::hasSection('tagimage'))@yield('tagimage') @else https://booking.dubaihousekeeping.com/images/house-cleaning-new.jpg?v=1.5 @endif">

<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta property="twitter:domain" content="booking.dubaihousekeeping.com">
<meta property="twitter:url" content="@DubaiHousekeep">
<meta name="twitter:title" content="@if(View::hasSection('title'))@yield('title') @else Dubai Housekeeping @endif">
<meta name="twitter:description" content="@if(View::hasSection('description'))@yield('description') @else Dubai Housekeeping @endif">
<meta name="twitter:image" content="@if(View::hasSection('tagimage'))@yield('tagimage') @else https://booking.dubaihousekeeping.com/images/house-cleaning-new.jpg?v=1.5 @endif">

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link rel="icon" type="image/png" href="{{asset('images/favicon.ico')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css?v=').Config::get('version.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/owl.carousel.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/css_change.css?v=').Config::get('version.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/pignose.calendar.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/font-awesome.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/intlTelInput.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/gijgo.min.css')}}"/>