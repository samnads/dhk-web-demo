@extends('layouts.app')

@section('title'){{'Book ironing services in Dubai | cleaning services Dubai'}}@endsection
@section('description'){{'Book ironing service in Dubai from the best cleaning company in Dubai at an affordable rate. Best professional maid cleaning service company in Dubai'}}@endsection
@section('canonical'){{'https://booking.dubaihousekeeping.com/ironing-service'}}@endsection
@section('tagimage'){{'https://booking.dubaihousekeeping.com/images/ironing-new.jpg?v=1.5'}}@endsection

@section('content')
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0">
                    <input type="hidden" id="number_hours" value="2">
                    <input type="hidden" id="serviceNamePay" value="HouseCleaning">
                    @include('Ironing.steps')
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
@endsection
@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $(document).ready(function() {
            getAvailability();

            $('.frequency_p').text('-');
            $('.maid_id_p').text($('#noOfMaids').val());
            $('.hrs_p').text($('#noOfHours').val());
            var clean_status = $('#cleaningMaterialStatus').val();
            if (clean_status == 'N') {
                $('.cleaning_material_p').text('No');
                $('#mat_success_p').text('without cleaning materials');
            } else if (clean_status == 'Y') {
                $('.cleaning_material_p').text('Yes');
                $('#mat_success_p').text('with cleaning materials');
            }
            $('.hrs_p').text($('#noOfHours').val());



            setTimeout(function() {
                $('.mob-coupon_message').hide(500);
            }, 7000);

            $('.small-close-btn').click(function() {
                $('.mob-coupon_message').hide(500);
            });



        });
        $("#step1Home").validate({
            ignore: [],

            rules: {
                noOfHours: {
                    required: true
                },
                noOfMaids: {
                    required: true,
                    min: 1,
                    max: 10
                },
                cleaningMaterialStatus: {
                    required: true
                },
            },
            messages: {
                noOfHours: "Please select number of hours.",
                noOfMaids: {
                    required: "Please select number of maids.",
                    min: "Please enter a value greater than 0.",
                    max: "Please enter a value less than 10.",
                    number: "Only numbers are allowed."
                },
                cleaningMaterialStatus: "Please select about cleaning material.",
            },
            errorPlacement: function(error, element) {
                // if(element.attr("name") == "role"){
                //      error.insertAfter(element.parent());
                // } else if(element.attr("name") == "permissionCheck[]"){
                //      error.insertAfter(element.parent().parent().parent().parent().parent().parent());
                // }else{
                error.insertAfter(element.parent().parent());
                // }
            },
            submitHandler: function(form, event) {
                var matstatus = $('#cleaningMaterialStatus').val();
                var tolstat = $('#cleaningSupplytatus').val();
                if (matstatus == 'Y' && $('input[name="plan_based_supply"]').is(':checked') == false) {
                    $("#cleaningSupplytatus-error").css('display', 'block');
                    $("#cleaningSupplytatus-error").text('Please select cleaning supply.');
                    return false;
                } else {
                    $("#cleaningSupplytatus-error").css('display', 'none');
                }
                var form_data = new FormData($("#step1Home")[0]);
                form_data.append('_token', '{{ csrf_token() }}');

                $('#houseCleaningStep1').fadeOut(300);
                $('#commonStep2').fadeIn(300);
                $('.timeSelection li:first').trigger("click");

                $('header .logo').addClass('logo_left');
                $('.date_time').show();
                $('.frequency_p').text('One Time');
                $("html").animate({
                    scrollTop: 0
                }, "slow");
            }
        });

        function localCalculate() {
            no_of_hours = parseInt($('#noOfHours').val());
            no_of_maids = parseInt($('#noOfMaids').val());
            service_only_charge = 0
            materials_rate = 0;
            tools_rate = 0;
            vat_charge = 0;
            supervisor_fee = 0;
            hourly_supervisor_fee = 60;
            _total_payable = 0;
            /********************************************/
            // hourly rate based on hours
            if (no_of_hours == 2) {
                service_rate_per_hour = 110 / no_of_hours;
            } else if (no_of_hours == 3) {
                service_rate_per_hour = 140 / no_of_hours;
            } else if (no_of_hours == 4) {
                service_rate_per_hour = 160 / no_of_hours;
            } else {
                service_rate_per_hour = 40;
            }
            /********************************************/
            if ($('#supervisorStatus').val() == "Y") {
                if (no_of_hours > 2 && no_of_maids > 1) {
                    supervisor_fee = hourly_supervisor_fee * no_of_hours;
                    $('#supervisordiv li[data-value="Y"]').addClass("selected");
                    $('.supervisorcharge_div').show();
                    $('input[name="supervisorStatus"').val("Y");
                    $('.super_visor_p').text("Yes");
                } else {
                    supervisor_fee = 0;
                    $('#supervisordiv li[data-value="N"]').addClass("selected");
                    $('input[name="supervisorStatus"').val("N");
                    $('.super_visor_p').text("No");
                    $('.supervisorcharge_div').hide();
                }
                $(".supervisorcharge_div_p").html(supervisor_fee.toFixed(2));
            } else {
                $(".supervisorcharge_div_p").html("0.00");
            }
            /********************************************/
            if ($('#supervisorStatus').val() == "Y") {
                if (no_of_hours > 2 && no_of_maids > 1) {
                    supervisor_fee = hourly_supervisor_fee * no_of_hours;
                    $('#supervisordiv li[data-value="Y"]').addClass("selected");
                    $('.supervisorcharge_div').show();
                    $('input[name="supervisorStatus"').val("Y");
                    $('.super_visor_p').text("Yes");
                } else {
                    supervisor_fee = 0;
                    $('#supervisordiv li[data-value="N"]').addClass("selected");
                    $('input[name="supervisorStatus"').val("N");
                    $('.super_visor_p').text("No");
                    $('.supervisorcharge_div').hide();
                }
                $(".supervisorcharge_div_p").html(supervisor_fee.toFixed(2));
            } else {
                $(".supervisorcharge_div_p").html("0.00");
            }
            /********************************************/
            $('input[name="custom_supplies[]"').each(function(index, element) {
                if ($(element).is(':checked')) {
                    tools_rate += (parseFloat($(element).attr("data-amount")) * no_of_hours);
                    $('.cleaning_tools_p').text('Yes');
                    $('.cleaningtools_div').show();
                    $('.cleaningtoolsdiv_p').html(tools_rate.toFixed(2));
                }
            });
            /********************************************/
            _total_payable = (service_rate_per_hour * no_of_hours * no_of_maids) + tools_rate;
            _vat_charge = (_total_payable * _vat_percentage) / (100 + _vat_percentage);
            _total_without_vat = _total_payable - _vat_charge;
            _service_only_charge = (_total_without_vat - tools_rate);
            // _total_without_vat = _total_without_vat + tools_rate + supervisor_fee;
            _total_without_vat = _total_without_vat + supervisor_fee;
            _vat_charge = (_vat_percentage / 100) * _total_without_vat;
            _total_payable = _total_without_vat + _vat_charge;
            //alert();
            $('.total_p').html(_service_only_charge.toFixed(2));
            $('.cleaningmaterialdiv_p').html(0);
            $('.vat_p').html(_vat_charge.toFixed(2));
            $(".taxable_amount").html(_total_without_vat.toFixed(2));
            $(".taxable_amount_div").show();
            $('.net_p,.mobile_net_p').html(_total_payable.toFixed(2));
            $('#hour_rate').val(_total_payable / no_of_hours);
            console.log('localCalculate() - called !');
            if ($('.couponClass').val() != "") {
                $.ajax({
                    method: 'post',
                    url: '{{ url('check-coupon') }}?ok',
                    data: {
                        'booking_type': $('.howOften:checked').val() || 'OD',
                        'customerId': $('#customer_id').val(),
                        'coupon': $('.couponClass').val(),
                        'numberHours': no_of_hours,
                        'bookedDate': $('#cleaning_date').val(),
                        'serviceId': $('#service_id').val(),
                        'selectedAmount': _total_without_vat,
                        'no_maids': no_of_maids,
                        'cleaningStatus': $('#cleaningMaterialStatus').val(),
                        token: '{{ csrf_token() }}'
                    },
                    success: function(result) {
                        if (result.status == 'success') {
                            $(".preloader").hide();
                            $('.vat_p').text(result.data['vat_charge']);
                            $('.net_p,.mobile_net_p').text(result.data['gross_amount']);
                            $('.discount_div').show();
                            $('.discount_p').text(result.data['net_amount']);
                            $('.couponClass').val(coupon);
                            $('#discount_price').val(result.data['discount']);
                            $('#coupon_id').val(result.data['coupon_id']);


                            $('.coupon_message').show();
                            var imageUrl = "{{ asset('images/tickmark1.png') }}";
                            $(".coupon_message").css("background-image", "url(" + imageUrl + ")");
                            $(".coupon_message").css("color", "green");
                            $('.coupon_message').html(result.message)
                        } else {
                            $('.coupon_message').show();
                            $(".coupon_message").css("color", "red");
                            $('.coupon_message').html(result.message);
                            $('.discount_div').hide();
                            $('#discount_price').val(0);
                            $('#coupon_id').val(0);
                            $('.vat_p').text(result.data['vat_charge']);
                            $('.net_p,.mobile_net_p').text(result.data['gross_amount']);
                        }
                    }
                });
            }
        }

        function amountCalculation() {
            var no_hrs = parseInt($('#noOfHours').val());
            var no_maids = parseInt($('#noOfMaids').val());
            var cleaningStatus = $('#cleaningMaterialStatus').val();

            /*if(no_hrs >= 3 && no_maids >= 2)
            {
            	$('#mainSupervisorDiv').show();
            } else {
            	$('#mainSupervisorDiv').hide();
            }*/

            var supervisorVal = $('#supervisorStatus').val();

            var cleaningVacuumStatus = $('#cleaningVacuumStatus').val();
            var cleaningLadderStatus = $('#cleaningLadderStatus').val();
            var cleaningMopStatus = $('#cleaningMopStatus').val();
            var cleaningIronStatus = $('#cleaningIronStatus').val();
            var cleaningSupplytatus = $('#cleaningSupplytatus').val();

            var interior = $('#interior').val();
            var ironing = $('#ironing').val();
            var fridge = $('#fridge').val();
            var oven = $('#oven').val();
            var coupon = $('.couponClass').val();
            // console.log(coupon)
            var customerId = '';
            var bookedDate = $('#cleaning_date').val();
            //console.log(bookedDate);
            var monthDuration = '';
            var serviceId = $('#service_id').val();
            var visitType = 0;
            var service_types = parseInt('1');
            var visitType = 'OD';
            var vat_percentage = parseInt('5');
            var no_visits = 0;
            var bookingdatess = $('#cleaning_date').val();
            var day = new Date(bookingdatess).getDay();
            if (visitType == 'OD') {
                no_visits = 1;
            }
            if (customerId != "") {
                var cust_id = customerId;
            } else {
                var cust_id = "";
            }
            var time_from = $('.time_span1').text();
            var perHour = 40;
            if (no_hrs == 2) {
                var totPrice = (110 * no_maids);
            } else if (no_hrs == 3) {
                var totPrice = (140 * no_maids);
            } else if (no_hrs == 4) {
                var totPrice = (160 * no_maids);
            } else if (no_hrs == 5) {
                var totPrice = (200 * no_maids);
            } else if (no_hrs == 6) {
                var totPrice = (240 * no_maids);
            } else {
                var totPrice = ((no_hrs * perHour) * no_maids);
            }
            if (supervisorVal == "Y") {
                //var supervisorrate = (60 * no_hrs);
                //totPrice = (totPrice + supervisorrate);
                //$(".supervisorcharge_div_p").text(supervisorrate);
                //$(".supervisorcharge_div").show();
            } else {
                //var supervisorrate = 0;
                //$(".supervisorcharge_div").hide();
            }
            totPrice = totPrice + supervisor_fee;
            $(".supervisorcharge_div_p").text(supervisor_fee);
            if (cleaningStatus == "N") {
                if (cleaningVacuumStatus == 'Y' || cleaningLadderStatus == 'Y' || cleaningMopStatus == 'Y' ||
                    cleaningIronStatus == 'Y') {
                    //var toolRate = 10;
                    //$('.cleaningtoolsdiv_p').text(toolRate);
                    //$('.cleaningtools_div').show();
                } else {
                    //var toolRate = 0;
                    //$('.cleaningtoolsdiv_p').text('');
                    //$('.cleaningtools_div').hide();
                    //$('.cleaning_tools_p').text('No');
                }
                var cleaning_material_rate = (tools_rate);
                var totPrice = (totPrice + cleaning_material_rate);
                $('.cleaningmaterial_div').hide();
            } else {
                //$('.cleaningtools_div').hide();
                if (cleaningSupplytatus == 0) {
                    var supplyRate = 10;
                } else if (cleaningSupplytatus == 1) {
                    var supplyRate = 20;
                } else {
                    var supplyRate = 0;
                }
                var cleaning_material_rate = (materials_rate);
                var totPrice = (totPrice + cleaning_material_rate);
                //$('.cleaningmaterialdiv_p').text(cleaning_material_rate);
                $('.cleaningmaterial_div').show();
            }

            var vat_charge = (totPrice * 5) / (100 + 5);
            var service_rate_with_material = (totPrice - vat_charge);
            var service_rate = (service_rate_with_material - cleaning_material_rate);

            var per_hour_rate = (service_rate / no_hrs);
            per_hour_rate = per_hour_rate.toFixed(2);
            vat_charge = vat_charge.toFixed(2);
            var gross_amount = totPrice.toFixed(2);
            var a_service_rate = service_rate;
            var cleaning_rates = cleaning_material_rate;
            service_rate = service_rate.toFixed(2);
            var price_to_show = per_hour_rate;
            var total_service_rate = 0;
            var discount = 0;
            $('.total_p').text(service_rate);
            if (coupon != '' && coupon != 'undefined') {
                checkCoupon();
            } else {
                $('.vat_p').text(vat_charge);
                $('.net_p').text(gross_amount);
                $('.mobile_net_p').text(gross_amount);
                $('.discount_div').hide();
                $('#coupon_message').hide();
            }
        }

        function getEventTarget(e) {
            e = e || window.event;
            return e.target || e.srcElement;
        }

        var ul = document.getElementById('noHours');
        ul.onclick = function(event) {
            var target = getEventTarget(event);
            $('#noOfHours').val(target.innerHTML).trigger('change');
            $('#number_hours').val(target.innerHTML);
            $('.hrs_p').text(target.innerHTML);
            $('#noOfHours-error').hide();
            //amountCalculation();
            //getAvailability();
            localCalculate();

        };
        // var increment = 2;
        function addNumber() {
            increment = $('#noOfMaids').val();
            if (increment <= 0) {
                increment++;
                $('#noOfMaids').val(increment).trigger('change');
                $('.maid_id_p').text(increment);
                $('#noOfMaids-error').hide();
                //amountCalculation();
                localCalculate();
            }
        }
        // console.log(dec);
        function noofmaidchange() {
            var change = $('#noOfMaids').val();
            if (change < 1 && change != '')
                change = 1;
            else if (change > 10)
                change = 10;
            $('#noOfMaids').val(change).trigger('change');
            $('.maid_id_p').text(change);
            $('#noOfMaids-error').hide();
            if (change != '')
                localCalculate();
            //amountCalculation();
        }

        function removeNumber() {
            var dec = $('#noOfMaids').val();
            // console.log(dec);
            if (dec > 1) {
                dec--;
                $('#noOfMaids').val(dec).trigger('change');
                $('.maid_id_p').text(dec);
                $('#noOfMaids-error').hide();
                //amountCalculation();
                localCalculate();
            }
        }


        /*var ul = document.getElementById('cleaningMaterial');
        ul.onclick = function(event) {
             var target = getEventTarget(event);
             if(target.innerHTML == 'YES, PLEASE')
             {
                  var x = 'Y';
                  $('.cleaning_material_p').text('Yes');
        		  $('.tools-yes').show();
        		  $('.tools-no').hide();
        		  triggerToolsUnSelect();
        		  $("#cleaningSupplytatus").val('2');
        		  $('.supplyClass').removeClass('selected');
             }
             if(target.innerHTML == 'NO, I HAVE THEM')
             {
                  var x = 'N';
                  $('.cleaning_material_p').text('No');
        		  $('.tools-yes').hide();
        		  $('.tools-no').show();
        		  triggerToolsUnSelect();
        		  $("#cleaningSupplytatus").val('2');
        		  $('.supplyClass').removeClass('selected');
             }
             $('#cleaningMaterialStatus').val(x);

             $('#cleaningMaterialStatus-error').hide();
                  amountCalculation();

        };*/

        /*var ul = document.getElementById('supervisordiv');
        ul.onclick = function(event) {
             var target = getEventTarget(event);
             if(target.innerHTML == 'YES')
             {
                  var x = 'Y';
                  $('.super_visor_p').text('Yes');
                  $('.supervisorcharge_div').show();
             }
             else if(target.innerHTML == 'NO')
             {
                  var x = 'N';
                  $('.super_visor_p').text('No');
                  $('.supervisorcharge_div').hide();
             }
             $('#supervisorStatus').val(x).trigger('change');
                  //amountCalculation();

        };*/

        function triggerToolsUnSelect() {
            $("#cleaningVacuumStatus").val('N');
            $("#cleaningLadderStatus").val('N');
            $("#cleaningMopStatus").val('N');
            $("#cleaningIronStatus").val('N');
            $(".tools-no > .tool-select").removeClass("selected").addClass("unselect");
            amountCalculation();
        }

        function getInterior(id) {
            $('#interior').val(id);
            amountCalculation();
        }

        function getFridge(id) {
            $('#fridge').val(id);
            amountCalculation();
        }

        function getIroning(id) {
            $('#ironing').val(id);
            amountCalculation();
        }

        function getOven(id) {
            $('#oven').val(id);
            amountCalculation();
        }
        $('#noHours li').on('click', function() {
            $(this).siblings().removeClass('selected');
            $(this).addClass('selected');
        });
        $('#cleaningMaterial li').on('click', function() {
            $(this).siblings().removeClass('selected');
            $(this).addClass('selected');
        });
        /*$('#supervisordiv li').on('click', function(){
             $(this).siblings().removeClass('selected');
             $(this).addClass('selected');
        });*/

        function toggleSupplyClass(element, item) {
            $("#cleaningSupplytatus-error").css('display', 'none');
            $('.supplyClass').removeClass('selected');
            $(element).addClass('selected');
            // var classe = 'col-md-6 col-sm-12 tool-select p-0 supplyClass';
            // element.className = classe.add('selected');
            checkSupplyItem(item);
        }

        function checkSupplyItem(item) {
            if (item == 'standard') {
                $("#cleaningSupplytatus").val('0');
                amountCalculation();
            } else if (item == 'eco') {
                $("#cleaningSupplytatus").val('1');
                amountCalculation();
            } else {
                $("#cleaningSupplytatus").val('2');
                amountCalculation();
            }
        }



        function toggleClass(element, item) {
            var classe = 'row ml-0 mr-0 em-extra-ser-tmb unselect';

            if (element.className == classe) {
                element.className = classe.replace('unselect', 'selected');
                checkItem(item);
            } else {
                element.className = classe;
                unCheckItem(item);
            }
        }

        function checkToolsItem(item) {
            if (item == 'vaccum') {
                getVaccum('Y');
            }
            if (item == 'ladder') {
                getLadder('Y');
            }
            if (item == 'mop') {
                getMop('Y');
            }
            if (item == 'electriciron') {
                getElectriciron('Y');
            }
        }

        function unCheckToolsItem(item) {
            if (item == 'vaccum') {
                getVaccum('N');
            }
            if (item == 'ladder') {
                getLadder('N');
            }
            if (item == 'mop') {
                getMop('N');
            }
            if (item == 'electriciron') {
                getElectriciron('N');
            }
        }

        function getVaccum(id) {
            $('#cleaningVacuumStatus').val(id);
            amountCalculation();
        }

        function getLadder(id) {
            $('#cleaningLadderStatus').val(id);
            amountCalculation();
        }

        function getMop(id) {
            $('#cleaningMopStatus').val(id);
            amountCalculation();
        }

        function getElectriciron(id) {
            $('#cleaningIronStatus').val(id);
            amountCalculation();
        }

        function checkItem(item) {
            if (item == 'fridge') {
                getFridge(1);
            }
            if (item == 'oven') {
                getOven(3);
            }
            if (item == 'ironing') {
                getIroning(2);
            }
            if (item == 'window') {
                getInterior(4);
            }
        }

        function unCheckItem(item) {
            console.log(item);
            if (item == 'fridge') {
                getFridge(0);
            }
            if (item == 'oven') {
                getOven(0);
            }
            if (item == 'ironing') {
                getIroning(0);
            }
            if (item == 'window') {
                getInterior(0);
            }
        }
    </script>
@endpush
