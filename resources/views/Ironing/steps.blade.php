<div id="houseCleaning">
    <input type="hidden" value="{{@$data['service']['service_type_id']}}" id="service_id">
    <input type="hidden" id="discount_price" >
    <input type="hidden" id="coupon_id" >
    <input type="hidden" id="checkout_secret" value="{{Config::get('values.checkout_primary_key')}}">
    <input type="hidden" id="apple_merchant_id" value="{{Config::get('values.apple_merchant_id')}}">
    <div id="houseCleaningStep1" class="items">
        @include('Ironing.step1')
    </div>
    <div id="commonStep2" style="display: none" class="items">
        @include('common.step2')
    </div>
    <div id="commonStep3" style="display: none" class="items">
        @include('common.step3')
    </div>
    <div id="commonStep4" style="display: none" class="items">
        @include('common.step4')
    </div>
    <div id="commonStep5" style="display: none" class="items">
        @include('common.step5')
    </div>
    
</div>

@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5VeXXoHzVpT6ZWQN4Af5gibJYSPdoWRw&amp;libraries=places"></script>
<script type="text/javascript"  src="{{URL::asset('js/locationpicker.jquery.min.js')}}"></script>
<!--<script type="text/javascript"  src="{{URL::asset('js/checkout_frames.min.js')}}"></script>-->
<script type="text/javascript" src="https://cdn.checkout.com/js/framesv2.min.js"></script>
<script async src="https://pay.google.com/gp/p/js/pay.js"></script>
<script src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"></script>

<script>
	var debug = true;
	var merchantIdentifier = $('#apple_merchant_id').val();
	var checkoutpkey = $('#checkout_secret').val();
	if (window.ApplePaySession)
	{
		//var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser';
		var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
		promise.then(function (canMakePayments) {
			if (canMakePayments) {
				///document.getElementById("applePaybtn").style.display = "block";
				logit('hi, I can do ApplePay');
			} else {
				logit('ApplePay is possible on this browser, but not currently activated.');
			}
		}); 
	} else {
		logit('ApplePay is not available on this browser');
	}
	
	
	document.getElementById("applePaybtn").onclick = function(evt) {
		$("#payment_popup_applepay").hide();
		var ap_amount = $("#ap_amount").val();
		
		var runningAmount 	= ap_amount;
		var runningPP		= 0;
		getShippingCosts('domestic_std', true);
		var runningTotal	= function() { return runningAmount + runningPP; }
		var shippingOption = "";
	 
		var subTotalDescr	= "Payment against Cleaning Service";
	 
		function getShippingOptions(shippingCountry)
		{
			logit('getShippingOptions: ' + shippingCountry );
			if( shippingCountry.toUpperCase() == "AE" ) {
				shippingOption = [{label: 'Standard Shipping', amount: getShippingCosts('domestic_std', true), detail: '3-5 days', identifier: 'domestic_std'},{label: 'Expedited Shipping', amount: getShippingCosts('domestic_exp', false), detail: '1-3 days', identifier: 'domestic_exp'}];
			} else {
				shippingOption = [{label: 'International Shipping', amount: getShippingCosts('international', true), detail: '5-10 days', identifier: 'international'}];
			}
		}
	 
		function getShippingCosts(shippingIdentifier, updateRunningPP )
		{
			var shippingCost = 0;
			
				 switch(shippingIdentifier) {
			case 'domestic_std':
				shippingCost = 0;
				break;
			case 'domestic_exp':
				shippingCost = 0;
				break;
			case 'international':
				shippingCost = 0;
				break;
			default:
				shippingCost = 0;
			}
		
			if (updateRunningPP == true) {
				runningPP = shippingCost;
			}
				
			logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost +"|"+ runningPP );
			
			return shippingCost;
		}

		var paymentRequest = {
		   currencyCode: 'AED',
		   countryCode: 'AE',
		   requiredShippingContactFields: ['postalAddress'],
		   lineItems: [{label: subTotalDescr, amount: runningAmount }, {label: 'P&P', amount: runningPP }],
		   total: {
			  label: 'Elitemaids',
			  amount: runningTotal()
		   },
		   supportedNetworks: ['amex', 'masterCard', 'visa' ],
		   merchantCapabilities: [ 'supports3DS', 'supportsCredit', 'supportsDebit' ]
		};
	
		var session = new ApplePaySession(1, paymentRequest);
	
		// Merchant Validation
		session.onvalidatemerchant = function (event) {
			logit(event);
			var promise = performValidation(event.validationURL);
			promise.then(function (merchantSession) {
				session.completeMerchantValidation(merchantSession);
			}); 
		}
	

		function performValidation(valURL) {
			return new Promise(function(resolve, reject) {
				var xhr = new XMLHttpRequest();
				xhr.onload = function() {
					var data = JSON.parse(this.responseText);
					logit(data);
					resolve(data);
				};
				xhr.onerror = reject;
				xhr.open('GET', 'https://elitemaids.emaid.info/applepay/apple_pay_comm.php?u=' + valURL);
				xhr.send();
			});
		}

		session.onshippingcontactselected = function(event) {
			logit('starting session.onshippingcontactselected');
			logit('NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.');
			logit(event);
			
			getShippingOptions( event.shippingContact.countryCode );
			
			var status = ApplePaySession.STATUS_SUCCESS;
			var newShippingMethods = shippingOption;
			var newTotal = { type: 'final', label: 'Elitemaids', amount: runningTotal() };
			var newLineItems =[{type: 'final',label: subTotalDescr, amount: runningAmount }, {type: 'final',label: 'P&P', amount: runningPP }];
			
			session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems );
		}
	
		session.onshippingmethodselected = function(event) {
			logit('starting session.onshippingmethodselected');
			logit(event);
			
			getShippingCosts( event.shippingMethod.identifier, true );
			
			var status = ApplePaySession.STATUS_SUCCESS;
			var newTotal = { type: 'final', label: 'Elitemaids', amount: runningTotal() };
			var newLineItems =[{type: 'final',label: subTotalDescr, amount: runningAmount }, {type: 'final',label: 'P&P', amount: runningPP }];
			
			session.completeShippingMethodSelection(status, newTotal, newLineItems );
			
			
		}
	
		session.onpaymentmethodselected = function(event) {
			logit('starting session.onpaymentmethodselected');
			logit(event);
			
			var newTotal = { type: 'final', label: 'Elitemaids', amount: runningTotal() };
			var newLineItems =[{type: 'final',label: subTotalDescr, amount: runningAmount }, {type: 'final',label: 'P&P', amount: runningPP }];
			
			session.completePaymentMethodSelection( newTotal, newLineItems );
			
			
		}
	
		session.onpaymentauthorized = function (event) {

			logit('starting session.onpaymentauthorized');
			logit('NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object');
			logit(event.payment.token);
			var eventToken = JSON.stringify(event.payment.token.paymentData);
			
			var ap_amount = $("#ap_amount").val();
			var ap_billing_name = $("#ap_billing_name").val();
			var ap_billing_tel = $("#ap_billing_tel").val();
			var ap_billing_email = $("#ap_billing_email").val();
			var ap_order_id = $("#ap_order_id").val();
			$.ajax({
				method: 'post',
				url: '{{url('apple-process-pay')}}',
				data:{'paymentToken':eventToken,'ap_order_id':ap_order_id,'ap_amount':ap_amount,'ap_billing_name':ap_billing_name,'ap_billing_tel':ap_billing_tel,'ap_billing_email':ap_billing_email,_token:'{{csrf_token()}}'},
				
				success: function (result) {
						console.log(result);
						if (result.status == 'success') {
							$(".preloader").fadeOut(1000);
							session.completePayment(ApplePaySession.STATUS_SUCCESS);
							window.location.href = '{{url('apple-payment-success')}}'+'/'+result.refId+'/'+result.id;
						} else {
							$(".preloader").fadeOut(1000);
							session.completePayment(ApplePaySession.STATUS_FAILURE);
							$("#payment_button").hide();
							$("#success_pay_message").text("Something went wrong. Try again.");
							$("#paymenterrormessagepopup").show();
						}
				}
			});
		}

		function sendPaymentToken(paymentToken) {
			return new Promise(function(resolve, reject) {
				logit('starting function sendPaymentToken()');
				
				logit("this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;");
				logit("defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like");
				if ( debug == true )
				resolve(true);
				else
				reject;
			});
		}
	
		session.oncancel = function(event) {
			$("#payment_button").hide();
			$("#success_pay_message").text("Cancelled. Try again.");
			$("#paymenterrormessagepopup").show();
		}
	
		session.begin();

	};

    // var payButton = document.getElementById("payment_button");
    var payButton = document.getElementById("pay-button");

    var form = document.getElementById("payment-form");
    Frames.init($('#checkout_secret').val());
    Frames.addEventHandler(
      Frames.Events.CARD_VALIDATION_CHANGED,
      function (event) {
        // console.log("CARD_VALIDATION_CHANGED: %o", event);

        payButton.disabled = !Frames.isCardValid();
      }
    );
   $('#book_payment_button').click(function() {
    $('#payment_popup').show(500);
   }) 
$(".howOften").click(function(){ 
    var selValue = $("input[type='radio']:checked").val();
    console.log(selValue);
    if (selValue != 'OD') {
        $('#month_div').show();
        if(selValue == 'WE') {
            $('.frequency_p').text('Weekly');
            $('#need_cleaner_error').text('This will repeat every week.');
        } else if(selValue == 'BW') {
            $('.frequency_p').text('BI-weekly');
            $('#need_cleaner_error').text('This will repeat every alternative weeks.');
        }
    } else {
        $('#month_div').hide();
        $('.frequency_p').text('One Time');
        $('#need_cleaner_error').text('');
    }
    amountCalculationSecond();
});

$('#back_houseCleaningStep1').click(function(){
$('#commonStep2').fadeOut(300);
$('#houseCleaningStep1').fadeIn(300);
$('.date_time').hide();
$('.frequency_p').text('-');
$('header .logo').removeClass('logo_left');
$("html").animate({ scrollTop: 0 }, "slow");
})

function getAvailability()
{

    var dateA = $('#cleaning_date').val();

    var numberHours = $('#noOfHours').val();

    // var datess = dateA=== null ? '' : dateA.format('YYYY-MM-DD');
    // var dateFormat1 = dateA=== null ? '' : dateA.format('DD/MMMM/YYYY');
    // $('#availableTime').html(dateFormat1);

    // $(".preloader").show();
    $.ajax({
    method: 'post',
    url: '{{url('calculate-availability')}}',
    data:{'bookedDate':dateA,'service_id':$('#service_id').val(),'numberHours':numberHours,_token:'{{csrf_token()}}'},
    success: function (result) {
        if (result.status == 'success') {
            // $(".preloader").hide();
            $('.timeSelection').html(result.data['html']);
            $('.cleaning_date').val(result.data['date']);
                        amountCalculationSecond();
            $('.toggle-calendar1').pignoseCalendar('set', result.data['date']);
            var monthNames = [
                "January", "February", "March",
                "April", "May", "June", "July",
                "August", "September", "October",
                "November", "December"
            ];
            var selDate = new Date(result.data['date']);

            var day = selDate.getDate();
            var monthIndex = selDate.getMonth();
            var year = selDate.getFullYear();
            var avail = day + '/' + monthNames[monthIndex] + '/' + year;

            $('#availableTime').html(moment(selDate).format("DD-MM-YYYY"));
            $('.date_p').html(moment(selDate).format("DD-MM-YYYY"));
            $('#timeSelection li').on('click', function(){
                let number_of_hours = $('#noOfHours').val();
                $(this).siblings().removeClass('selected');
                    $(this).addClass('selected');
                    var target=  $(this).children('.tick-text').attr("id");
                    $('#timeSelected').val(target);
                    $('#timeSpan1').val(target);
                    $('.time_span1').text(target);
                    $('.to_p').html('&nbsp;-&nbsp;');

                        function D(J) { 
                            return (J<10? '0':'') + J;
                            };
                        var piece = target.split(':');
                        var mins = piece[0]*60 + +piece[1] + +(number_of_hours*60);
                        var totalTime = D(mins%(24*60)/60 | 0) + ':' + D(mins%60);  
                    $('.time_span2').text(totalTime+':00');
                    $('#timeSpan2').val(totalTime+':00');
            }); 
        }
    }
    }); 
}

$.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$("#submit_form").click(function(){
    if($("#commonStep2").is(':visible'))
    amountCalculationSecond();
})
function amountCalculationSecond()
{
    localCalculate();
    return false;
    var numberHours = Number($('#noOfHours').val());
    //var time_from = $('.time_span1').text();
    var time_from = $('#timeSelection li.selected div.tick-text').attr('id'); // i can only found 00:00:00 time format here
    var startHour = time_from ? Number(time_from.split(":")[0]) : 0;
    var endHour = startHour + numberHours;
    var normalHours = 0;
    var specialHours = 0;
    if(startHour >= 16){
        //specialHours = (endHour-16) > numberHours ? numberHours : (endHour-16);
        normalHours = Math.max(numberHours-specialHours,0);
    }
    else{
        var normalHours = numberHours;
        var specialHours = 0;
    }
    //var bookedDate = $('#cleaning_date').val();
    var bookedDate = $('.pignose-calendar-unit-first-active').attr('data-date');
    console.log(time_from);
    console.log(bookedDate);
    var numberMaids = $('#noOfMaids').val();
	
	/*if(numberHours >= 3 && numberMaids >= 2)
	{
		$('#mainSupervisorDiv').show();
	} else {
		$('#mainSupervisorDiv').hide();
		$('.super_visor_p').text('No');
		$('#supervisorStatus').val('N');
		$('#supervisordiv li').removeClass('selected');
		$('#supervisordiv .noselectli').addClass('selected');
	}*/
	
	var supervisorVal = $('#supervisorStatus').val();
    
	var cleaningStatus = $('#cleaningMaterialStatus').val();
    var cleaningVacuumStatus = $('#cleaningVacuumStatus').val();
    var cleaningLadderStatus = $('#cleaningLadderStatus').val();
    var cleaningMopStatus = $('#cleaningMopStatus').val();
    var cleaningIronStatus = $('#cleaningIronStatus').val();
    var cleaningSupplytatus = $('#cleaningSupplytatus').val();
	
    var interior = $('#interior').val();
    var ironing = $('#ironing').val();
    var fridge = $('#fridge').val();
    var oven = $('#oven').val(); 
    var coupon = $('.couponClass').val();
    var customerId = '';
    var monthDuration = $('#monthDurationId').val();;
    var visitType = $("input[name='how_often']:checked").val();
    var serviceId = $('#service_id').val();
    var day = new Date(bookedDate).getDay();
	
	if(cleaningStatus == "N")
	{
		if(cleaningVacuumStatus == 'Y' || cleaningLadderStatus == 'Y' || cleaningMopStatus == 'Y' || cleaningIronStatus == 'Y')
		{
			//var toolRate = 10;
		} else {
			//var toolRate = 0;
		}
		if(numberHours == 2)
		{
			var per_hour_rate_normal = 52.38;
		} else if(numberHours == 3)
		{
			var per_hour_rate_normal = 44.44;
		} else if(numberHours == 4)
		{
			var per_hour_rate_normal = 38.10;
		} else {
			if(bookedDate == "2023-04-20" || bookedDate == "2023-04-23" || bookedDate == "2023-04-24")
			{
				var per_hour_rate_normal = 38.10;
			} else {
				var per_hour_rate_normal = 38.10;
			}
		}
	} else {
		//var toolRate = 0;
		
		if(cleaningSupplytatus == 0)
		{
			//var supplyRate = 10;
		} else if(cleaningSupplytatus == 1){
			//var supplyRate = 20;
		} else {
			//var supplyRate = 0;
		}
		
		if(numberHours == 2)
		{
			var per_hour_rate_normal = 52.38;
		} else if(numberHours == 3)
		{
			var per_hour_rate_normal = 44.44;
		} else if(numberHours == 4)
		{
			var per_hour_rate_normal = 38.10;
		} else {
			if(bookedDate == "2023-04-20" || bookedDate == "2023-04-23" || bookedDate == "2023-04-24")
			{
				var per_hour_rate_normal = 38.10;
			} else {
				var per_hour_rate_normal = 38.10;
			}
		}
	}
	
    $.ajax({
        method: 'post',
        url: '{{url('calculate-price')}}',
        data:{'customerId':customerId,'numberHours':numberHours,'numberMaids':numberMaids,'cleaningStatus':cleaningStatus,
        'interior':interior,'ironing':ironing,'fridge':fridge,'oven':oven,'bookedDate':bookedDate,'monthDuration':monthDuration,'serviceId':serviceId,
        'visitType':visitType,'price_per_hr':per_hour_rate_normal,'tool_rate':tools_rate,'supply_rate':hourly_material_fee,'supervisorVal':supervisorVal,_token:'{{csrf_token()}}'},
        
        success: function (result) {
            console.log(result);
                if (result.status == 'success') {
                    $(".preloader").hide();
					if(supervisorVal == "Y")
					{
						$(".supervisorcharge_div_p").text(result.data['supervisorrate']);
						$(".supervisorcharge_div").show();
					} else {
						$(".supervisorcharge_div").hide();
					}
					if(cleaningSupplytatus == 0 || cleaningSupplytatus == 1)
					{
						//$('.cleaningmaterialdiv_p').text(result.data['cleaningrate']);
						//$('.cleaningmaterial_div').show();
						//$('.cleaningtools_div').hide();
					} else {
						$('.cleaningmaterialdiv_p').text('0.00');
						//$('.cleaningmaterial_div').hide();
						//$('.cleaningtools_div').hide();
					}
                    $('.cleaningmaterialdiv_p').text(result.data['cleaningrate']);
					if(cleaningStatus == "N")
					{
						//$('.cleaningmaterial_div').hide();
                        $('.cleaningtoolsdiv_p').text(result.data['cleaningrate']);
						if(cleaningVacuumStatus == 'Y' || cleaningLadderStatus == 'Y' || cleaningMopStatus == 'Y' || cleaningIronStatus == 'Y')
						{
							//$('.cleaningtoolsdiv_p').text(result.data['cleaningrate']);
							//$('.cleaningtools_div').show();
						} else {
							//$('.cleaningtoolsdiv_p').text('');
							//$('.cleaningtools_div').hide();
						}
					}
					
                  //  $("#perhr").val(result.data['hour_rate'])
                    $('#hour_rate').val(result.data['hour_rate']);
                    $('#discount').val(result.data['discount']);
                    $('#service_charge').val(result.data['serv_rate']);
                    $('#vat_charge').val(result.data['vat_charge']);
                    $('#total_amount').val(result.data['net_amount']);
                    $('#net_cleaning_fee').val(result.data['cleaningrate']);
                    $('#net_service_charge').val(result.data['gross_amount']);
                    $('#net_discount').val(result.data['discount']);
                    $('#net_vat_charge').val(result.data['vat_charge']);
                    $('#total_net_amount').val(result.data['price_to_show']);
                    $('#cleaning_material_fee').val(10);
                    var total =  result.data['service_rate'];
                    $('.total_p').text(total.toFixed(2));
                    if(coupon != '' && coupon != 'undefined') {
                        checkCoupon();
                    } else {
                        $('.vat_p').text(result.data['vat_charge'].toFixed(2));
                        $('.net_p').text(result.data['gross_amount'].toFixed(2));
                        $('.mobile_net_p').text(result.data['gross_amount']);

                        $('.discount_div').hide();
                        $('#coupon_message').hide();
                    }                   
                }
        }
    }); 
}
$("#secondForm").validate({
    ignore: [],
    rules: {
    how_often: {
    required:true
    },
    timeSelected: {
        required:true
    },
    monthDurations: {
        required: function () {
                return (($("input[type='radio']:checked").val() == 'WE') || ($("input[type='radio']:checked").val()));
        },
    },
    },
    messages: {
    how_often: "Please select how often do you need your cleaner.",
    timeSelected: "Please select the time.",
    monthDurations: "Please select month duration.",
    },
    errorPlacement: function (error, element)
    {
        if(element.attr("name") == "timeSelected") {
        error.insertAfter(element.parent());
    } else {
        error.insertAfter(element.parent().parent());
    }
        },
    submitHandler: function (form,event) {
        var customerId = localStorage.getItem('customerId');
        var customerAddress = localStorage.getItem('address');
        if ((customerId) && customerId != ''&& localStorage.getItem('address') !=null) {
            $('#commonStep2').fadeOut(300);
            $('#commonStep5').fadeIn(300);
            $("html").animate({ scrollTop: 0 }, "slow");
        } else if ((customerId) && customerId != '' && !customerAddress) {
            $('#commonStep2').fadeOut(300);
            $('#commonStep4').fadeIn(300);
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else { 
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
            $("html").animate({ scrollTop: 0 }, "slow");
        } else {
            $('#commonStep2').fadeOut(300);
            $('#commonStep3').fadeIn(300);
            $("html").animate({ scrollTop: 0 }, "slow");
        }
    }
});
// Month DUration
$('#monthDuration li').on('click', function(){
$(this).siblings().removeClass('selected');
$(this).addClass('selected');

});

function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement; 
}

var ul = document.getElementById('timeSelection');
ul.onclick = function(event) {
  var target = getEventTarget(event);
  $('#timeSelected').val(target.innerHTML);
  $('#timeSelected-error').hide();
  amountCalculationSecond();
}; 
var ul = document.getElementById('monthDuration');
ul.onclick = function(event) {
  var target = getEventTarget(event);
  $('#monthDurationId').val(target.innerHTML);
  $('#monthDurationId-error').hide();
  amountCalculationSecond();
};


////step 3////
$('#back_houseCleaningStep2').click(function(){
    $('#commonStep3').fadeOut(300);
    $('#commonStep2').fadeIn(300);
    $('.address_div').hide();
    $("html").animate({ scrollTop: 0 }, "slow");
})
function resendOTP()
{
          $.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
     });
     var customerId = $('#customerId').val();
     $(".preloader").fadeIn(100);
     $.ajax({
        method: 'post',
        url: '{{url('resend-otp')}}',
        data:{'customerId':customerId,_token:'{{csrf_token()}}'},
        cache : false,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result.status == 'success') {
               $(".preloader").fadeOut(100);
                 $('#resend_message').html(result.messages);
               //  window.location.href = '{{url('cleaning-fourth')}}'+'/'+result.data['id'];
            }
        }
    }); 
}
function viewForget()
{
     $('#forgot_password').show(500);
     $('#emailId').val('');
}
// $('.em-forgot-close-btn').click(function(){
//      $('.popup-main').removeClass('modal_show');
// });
 $.validator.addMethod("customemail",
        function(value, element) {
            return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
        },
        "Please enter a valid email id. "
    );

    $("#forgotForm").validate({
     ignore: [],
     rules: {
         
          emailId: {
               customemail:true,
               required: true
          },
          
    },
    messages: {
     emailId: "Please enter your email id.",
     },
    errorPlacement: function (error, element)
    {
            error.insertAfter(element.parent().parent());
    },
     submitHandler: function (form,event) {
          var form_data = new FormData($("#forgotForm")[0]);
          form_data.append('_token', '{{csrf_token()}}');
          $('#forgot_password').hide(500);
          $("#submit_email").attr( "disabled", "disabled" );
          $(".preloader").show();
          $.ajax({
               method: 'post',
               url: '{{url('forgot-password')}}',
               data: form_data,
               cache : false,
               contentType: false,
               processData: false,
               success: function (result) {
                    $(".preloader").hide();
                    $('#forgotForm')[0].reset();
                    if (result.status == 'success') {
                         $("#submit_email").attr( "disabled", false );
                         $('#success_password').show(500);
                         $('#message').html('Mail send successfully.')
                    } else {
                         $("#submit_email").attr( "disabled", false );
                         $('#success_password').show(500);
                         $('#message').html('Failed!Please try again.')
                    }
               }
          }); 
     }  
}); 
$("#verify_otpForm").validate({
     ignore: [],
//      rules: {
         
//           first: {
//                number:true,
//                required: true,
//                maxlength:1
//           },
//           second: {
//                number:true,
//                required: true,
//                maxlength:1

//           }, third: {
//                number:true,
//                required: true,
//                maxlength:1

//           }, fourth: {
//                number:true,
//                required: true,
//                maxlength:1

//           },
          
//     },
//      messages: {
//           first:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//             second:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//             third:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//             fourth:{
//                 required: "Please enter the digit.",
//                 maxlength: "One digit is allowed.",
//                 number:"Please enter valid number."
//             },
//      },
//     errorPlacement: function (error, element)
//     {
//             error.insertAfter(element.parent().parent());
//     },
     submitHandler: function (form,event) {   
          if (($('#codeBox1').val() == '' ||  isNaN($('#codeBox1').val())) &&  ($('#codeBox2').val() == '' ||  isNaN($('#codeBox2').val())) &&
          ($('#codeBox3').val() == '' ||  isNaN($('#codeBox3').val())) && ($('#codeBox4').val() == '' ||  isNaN($('#codeBox4').val()))) 
          {
             $('#otp_error').text("Please enter valid digit");
             return false;
          }
          $('#otp_error').text();
          // $('#resend_button').show();
          var form_data = new FormData($("#verify_otpForm")[0]);
          form_data.append('_token', '{{csrf_token()}}');
          // $('#verify_mobile').hide(500);
          $("#submit_email").attr( "disabled", "disabled" );
          $(".preloader").show();
          $.ajax({
               method: 'post',
               url: '{{url('verify-mobile')}}',
               data: form_data,
               cache : false,
               contentType: false,
               processData: false,
               success: function (result) {
                    $(".preloader").hide();
                    $('#verify_mobile').hide();
                    $('#verify_otpForm')[0].reset();
                    if (result.status == 'success') {
                         $("#submit_email").attr( "disabled", false );
                         var customerid = localStorage.setItem('customerId',result.data['id']);
                        $('#customer_id').val(result.data['id']);
                        $('.login').hide();
                        $('.logout').show();
                        $('.make_pay_head').show();
	                    $('.userPersonel').show();
                         $('#commonStep3').fadeOut(300);
                         $('#commonStep4').fadeIn(300);
                         $("html").animate({ scrollTop: 0 }, "slow");
                        
                         if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition, showError);
                        } else { 
                            x.innerHTML = "Geolocation is not supported by this browser.";
                        }
                       
                    } else {
                        
                         $("#submit_email").attr( "disabled", false );
                         // $('#success_password').show(500);
                         // $('#message').html(result.messages);
                         // $('#success_password').hide(500);
                         $('#verify_mobile').show();
                         $('#resend_message').html(result.messages);
                    }
               }
          }); 
     }  
});     

// $('.us3').locationpicker({
       
//         radius: 0,
//         inputBinding: {
//             latitudeInput: $('.us3-lat'),
//             longitudeInput: $('.us3-lon'),
//             radiusInput: $('.us3-radius'),
//             locationNameInput: $('.us3-address')
//         },
//         // markerIcon: '{{URL::asset("images/pin_sm.png")}}',
//         enableAutocomplete: true,
//         onchanged: function (currentLocation,radius,isMarkerDropped) {
//             // Uncomment line below to show alert on each Location Changed event
//             //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
//         }
//     }); 
$('.em-forgot-close-btn').click(function(){
		$('#success_password').hide(500);
});
$(document).ready(function() {
    amountCalculationSecond();
});    
$("#step3form").validate({
     ignore: [],
     rules: {
          firstName: {
               required: function () {
                    return ($("#typeLogin").val() == 'register');
               },
          },
          lastName: {
               required: function () {
                    return ($("#typeLogin").val() == 'register');
               },
          },
          phoneNumber: {
               required: function () {
                    return ($("#typeLogin").val() == 'register');
               },
               minlength:{{Config::get('form.phone_min_digit')}},
               maxlength:{{Config::get('form.phone_max_digit')}},
               number:true,
          },
          email: {
               customemail:function () {
                    return ($("#typeLogin").val() == 'register');
               },
               required: function () {
                    return ($("#typeLogin").val() == 'register');
               },
          },
          password: {
               required: function () {
                    return ($("#typeLogin").val() == 'register');
               },
               minlength:6  
          },
          emailUser: {
               required: function () {
                    return ($("#typeLogin").val() == 'login');
               },
               customemail:function () {
                    return ($("#typeLogin").val() == 'login');
               },
          },
          passwordUser: {
               required: function () {
                    return ($("#typeLogin").val() == 'login');
               },
               minlength:6  
          },
    },
    messages: {
          firstName: "Please enter your First Name.",
          lastName: "Please enter your Last Name.",
          email: "Please enter your email id.",
          phoneNumber:{
                required: "Please enter your phone number.",
                minlength: "Phone number must contain atleast {{Config::get('form.phone_min_digit')}} digits.",
                maxlength: "Only {{Config::get('form.phone_max_digit')}} digits is allowed.",
                number:"Only numbers are allowed."
            },
          password:{
                required: "Please enter password.",
                minlength: "Password must contain atleast 6 characters."
            },
            emailUser:"Please enter your registered email id.",
            passwordUser:{
                required: "Please enter your password.",
                minlength: "Password must contain atleast 6 characters."
            }  , 
     },
    errorPlacement: function (error, element)
    {
            error.insertAfter(element.parent().parent());
    },
     submitHandler: function (form,event) {
          var type = $('#typeLogin').val();
          if(type == 'login') {
               $('#error_message').text('');
               var form_data = new FormData($("#step3form")[0]);
               form_data.append('_token', '{{csrf_token()}}');
                $("#button_submit_login").attr( "disabled", "disabled" );
                $(".preloader").show();
               $.ajax({
                    method: 'post',
                    url: '{{url('login-user')}}',
                    data: form_data,
                    cache : false,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                         $(".preloader").hide();
                         $("#button_submit_login").attr( "disabled", false );
                         if (result.status == 'success') {
                              $('#step3form')[0].reset();
                              var customerid = localStorage.setItem('customerId',result.data['id']);
                              $('.login').hide();
                              $('.logout').show();
                              $('.make_pay_head').show();
	                          $('.userPersonel').show();
                              $('#customer_id').val(result.data['id']); 
                              $('#error_message').text('');
                              
                              var address = result.data['address'];
                              if(address.length != 0 ) {
                                   $('.address_p').text(result.data['address']['addressName']);
                                   $('.area_p').text(result.data['address']['Area']);
                                   $('.address_div').show();
                                   localStorage.setItem('address',result.data['address']['addressName']);
					               localStorage.setItem('area',result.data['address']['Area']);
                                   $('#commonStep3').fadeOut(300);
                              $('#commonStep5').fadeIn(300);
                              $("html").animate({ scrollTop: 0 }, "slow");
                              } else {
                                   $('#commonStep3').fadeOut(300);
                                   $('#commonStep4').fadeIn(300);
                                   if (navigator.geolocation) {
                                        navigator.geolocation.getCurrentPosition(showPosition, showError);
                                    } else { 
                                        x.innerHTML = "Geolocation is not supported by this browser.";
                                    }
                                   $("html").animate({ scrollTop: 0 }, "slow");
                              }
                              $('#name_p').text(result.data['name']);
                              $('#mail_p').text(result.data['emailId']);
                              $('#contact_p').text(result.data['phone']);
                              $('#phonepay').val(result.data['phone']);
                              amountCalculationSecond();
                              afterStepLoginSuccess(result);
                         } else if(result.status != 'success' && result.data && result.data.mobile_verified!=undefined && !result.data['mobile_verified']){
                            $('#verify_mobile').show();
                            $('#customerId').val(result.data['id']);
                            $('#mobile_span').html(result.data['mobile']);
                            $('#error_message').text(result.messages);
                            amountCalculationSecond();
                         } else {
                              $('#error_message').text(result.messages);
                         }
                    }
               }); 
          } else {
               $('#error_message').text('');
               var form_data = new FormData($("#step3form")[0]);
               form_data.append('_token', '{{csrf_token()}}');
               var phone= $('#phoneNumber').val();
                $("#button_submit_login").attr( "disabled", "disabled" );
                $(".preloader").show();
               $.ajax({
                    method: 'post',
                    url: '{{url('customer-save')}}',
                    data: form_data,
                    cache : false,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                         $('#step3form')[0].reset();
                         $(".preloader").hide();
                         $("#button_submit_login").attr( "disabled", false );
                         if (result.status == 'success') {
                              $('#customerId').val(result.data['id']);
                              $('#customer_id').val(result.data['id']);
                              $('#verify_mobile').show();
                              $('#mobile_span').html(phone);
                              $('#error_message').text('');
                              $('#name_p').text(result.data['name']);
                              $('#mail_p').text(result.data['emailId']);
                              $('#contact_p').text(result.data['phone']);
							  $('#phonepay').val(result.data['phone']);
                              amountCalculationSecond();

                         } else {
                              $('#error_message').text(result.messages);
                         }
                    }
               });  
          }
     }  
});


$(".password-set-btn").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    input = $(this).parent().find("input");
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
function getCodeBoxElement(index) {
  return document.getElementById('codeBox' + index);
}
function onKeyUpEvent(index, event) {
  const eventCode = event.which || event.keyCode;
  if (getCodeBoxElement(index).value.length === 1) {
	 if (index !== 4) {
		getCodeBoxElement(index+ 1).focus();
	 } else {
		getCodeBoxElement(index).blur();
	 }
  }
  if (eventCode === 8 && index !== 1) {
	 getCodeBoxElement(index - 1).focus();
  }
}
function onFocusEvent(index) {
  for (item = 1; item < index; item++) {
	 const currentElement = getCodeBoxElement(item);
	 if (!currentElement.value) {
		  currentElement.focus();
		  break;
	 }
  }
}
////step 4//



$("#addressForm").validate({
    ignore: [],
    rules: {
        area: {
            required:true
        },
        address: {
            required:true
        },
        address_opt: {
            required:true
        },
    },
    messages: {
        area: "Please select your area.",
        address: "Please enter your address.",
        address_opt: "Please select address type.",
    },
    errorPlacement: function (error, element)
    {
        if(element.attr("name") == "address" || element.attr("name") == "area"){
             error.insertAfter(element.parent());
        } else {
            error.insertAfter(element.parent().parent());
        }
    },
    submitHandler: function (form,event) {
        var form_data = new FormData($("#addressForm")[0]);
        form_data.append('_token', '{{csrf_token()}}');
        form_data.append('customerId', $('#customer_id').val());
         $("#button_submit").attr( "disabled", "disabled" );
         $(".preloader").show();
        $.ajax({
            method: 'post',
            url: '{{url('save-address')}}',
            data: form_data,
            cache : false,
            contentType: false,
            processData: false,
            success: function (result) {
                    if (result.status == 'success') {
                        $("#button_submit").attr( "disabled", false );
                        $(".preloader").hide();
                        $('#commonStep4').fadeOut(300);
                         $('#commonStep5').fadeIn(300);
                         var address = result.data['address'];
                        if(address.length != 0 ) {
                            $('.address_p').text(result.data['address']['addressName']);
                            $('.area_p').text(result.data['address']['Area']);
                            localStorage.setItem('address',result.data['address']['addressName']);
					        localStorage.setItem('area',result.data['address']['Area']);
                            $('.address_div').show();
                        }
                         $('.address_div').show();
                         $("html").animate({ scrollTop: 0 }, "slow");
                        // window.location.href = '{{url('cleaning-fifth')}}'+'/'+result.data['bookingId'];
                    }
            }
        }); 
    }  
});
$('#back_houseCleaningStep3').click(function(){
    var customerId = localStorage.getItem('customerId');
    if ((customerId) && customerId != '') {
        $('#commonStep2').fadeIn(300);
    } else {
        $('#commonStep3').fadeIn(300);
        localStorage.clear();
    }
    $('#commonStep4').fadeOut(300);
    $("html").animate({ scrollTop: 0 }, "slow");
});

function goPrevious()
{
    var bookingId = $('#bookingId');
    $(".preloader").fadeIn(1000);
    $.ajax({
        method: 'post',
        url: '{{url('go-four')}}',
        data:{'bookingId':bookingId,_token:'{{csrf_token()}}'},
        cache : false,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result.status == 'success') {
                window.location.href = '{{url('cleaning-fourth')}}'+'/'+result.data['id'];
            }
        }
    }); 
}
$("#payment_form").validate({
    ignore: [],
    rules: {
        payment_mode: {
            required:true
        },
        crew_in: {
            required:true
        },
        // instructions: {
        //     required:true
        // },
        terms: {
            required:true
        }
    },
    messages: {
        payment_mode: "Please select payment mode.",
        crew_in: "Please select option.",
        // instructions: "Please enter instructions.",
        terms:"Please accept the terms and conditions."
    },
    showErrors: function(errorMap, errorList) {
        // trick to only focus one error at a time and scoll to it
        if (errorList.length) {
            this.errorList = [errorList[0]];
            var elements = $(errorList[0].element);
            var element = $('.FindMe', elements);
            scrollToElement(element.prevObject);
        }
        this.defaultShowErrors();
    },
    errorPlacement: function (error, element)
    {
        if(element.attr("name") == "crew_in"){
             error.insertAfter(element.parent());
        } else {
            error.insertAfter(element.parent());
        }
		if($("#payment_mode").val() == 'applepay')
		{
			$("#applepaytmb").removeClass("selected"); 
			$("#payment_mode").val(""); 
		}
    },
    submitHandler: function (form,event) {
        var form_data = new FormData($("#payment_form")[0]);
        form_data.append('hour_rate', $('#hour_rate').val());
        // form_data.append('discount', $('#discount').val());
        form_data.append('vat_charge', $('.vat_p').html());  
        form_data.append('service_charge', $('.total_p').html());  
        form_data.append('total_amount', $('.net_p').html());
        // form_data.append('net_cleaning_fee', $('#net_cleaning_fee').val());
        // form_data.append('net_service_charge', $('#net_service_charge').val());
        // form_data.append('net_discount', $('#net_discount').val());
        // form_data.append('net_vat_charge', $('#net_vat_charge').val());
        // form_data.append('total_net_amount', $('#total_net_amount').val());
        form_data.append('cleaning_material_fee', $('#cleaning_material_fee').val());
        form_data.append('noOfHours', $('#noOfHours').val());
        form_data.append('noOfMaids', $('#noOfMaids').val());
        form_data.append('cleaningMaterialStatus', $('#cleaningMaterialStatus').val());
        form_data.append('coupon_code', $('#coupon_id').val());
        form_data.append('discount_price', $('#discount_price').val());  
        form_data.append('serviceId', $('#service_id').val());
        form_data.append('cleaning_date', $('#cleaning_date').val());
        form_data.append('monthDurationId', $('#monthDurationId').val());
        form_data.append('visitType',$("input[name='how_often']:checked").val());
        form_data.append('fromTime',$('#timeSpan1').val());
        form_data.append('toTime', $('#timeSpan2').val());
        form_data.append('customerId', $('#customer_id').val());  
        form_data.append('interior', $('#interior').val());  
        form_data.append('ironing', $('#ironing').val());  
        form_data.append('fridge', $('#fridge').val());  
        form_data.append('oven', $('#oven').val());  
        form_data.append('cate_first','0');  
        form_data.append('cate_second', '0');  
        form_data.append('cate_third', '0');  
        form_data.append('cate_fourth', '0'); 
        form_data.append('cate_fifth', '0');  
        form_data.append('cate_sixth', '0');
        form_data.append('officesqure', '0');  
        form_data.append('serviceNamePay', $('#serviceNamePay').val());
        /**********************************************************************/
        // custom tools data
        $('input[name="custom_supplies[]"').each(function(index, element) {
            // check anything is selected
            if ($(element).is(':checked')) {
                form_data.append('custom_supplies[]',  $(element).val());
                form_data.append('custom_supplies_rate[]',  parseFloat($(element).attr("data-amount")));
            }
        });
        /**********************************************************************/
        // plan based suppplies
        if($('input[name="plan_based_supply"]').is(':checked')) {
            form_data.append('plan_based_supply',  $('input[name="plan_based_supply"]:checked').val());
            form_data.append('plan_based_supply_hourly_rate',  $('input[name="plan_based_supply"]:checked').attr("data-hourly-rate"));
            form_data.append('plan_based_supply_total_amount',  ($('input[name="plan_based_supply"]:checked').attr("data-hourly-rate") * $('#noOfHours').val()) * $('#noOfMaids').val());
        }
        /**********************************************************************/
        form_data.append('supervisor_status',  $('input[name="supervisorStatus"]').val());
        form_data.append('supervisor_charge_hourly',  $('input[name="supervisor_charge_hourly"').val());
        form_data.append('supervisor_charge',  $('input[name="supervisor_charge"').val());
        /**********************************************************************/
        // selected week days
        $.each($("input[name='service_week_days[]']:checked"), function(){            
            form_data.append('service_week_days['+ $(this).val()+']', $(this).attr("data-day-name"));
        });
        /**********************************************************************/
        $("#button_submit").attr( "disabled", "disabled" );
        $(".preloader").fadeIn(1000);
        $.ajax({
            method: 'post',
            url: '{{url('make-payment')}}',
            data: form_data,
            cache : false,
            contentType: false,
            processData: false,
            success: function (result) {    
                //console.log(result.form_params);return false;
                $("#button_submit").attr( "disabled", false );
                if (result.status == 'success') {
                    $(".preloader").fadeIn(1000); 
                    if(result.data['payment_mode'] == 'card') {
                        $('#order_id').val(result.data['bookings']['reference_id']);
                        var total_amount_payable = parseFloat(result.data['transaction_charge']) + parseFloat(result.data['bookings']['total_amount']);
                        total_amount_payable = total_amount_payable.toFixed(2);
                        // if(result.data['customer']['email_address'] == 'jose.robert@azinova.info') {
                        //     $('#amount').val(1);
                        //     $('#amount_span').html(1);    
                        // } else {
                            $('#amount').val(total_amount_payable);  
                            $('#amount_span').html(total_amount_payable);  
                        // }
                        $(".preloader").fadeOut(1000); 
                        $('#billing_name').val(result.data['customer']['customer_name']);
                        $('#billing_tel').val(result.data['customer']['mobile_number_1']);
                        $('#billing_email').val(result.data['customer']['email_address']);
                        $('#billing_address').val(result.data['customer_address']['customer_address']);
                        //$('#payment_popup').show(500);
                        /******************************************************************/
                        // for cc-avenue form only
                        $("#ccavenue_form input[name='order_id']").val(result.data['bookings']['reference_id']);
                        $("#ccavenue_form input[name='amount']").val($('.trans_net_p').html());
                        $("#ccavenue_form input[name='redirect_url']").val();
                        $("#ccavenue_form input[name='cancel_url']").val();
                        $("#ccavenue_form input[name='billing_name']").val(result.data['customer']['customer_name']);
                        $("#ccavenue_form input[name='billing_address']").val(result.data['customer_address']['customer_address']);
                        //$("#ccavenue_form input[name='billing_city']").val();
                        //$("#ccavenue_form input[name='billing_state']").val();
                        //$("#ccavenue_form input[name='billing_zip']").val();
                        $("#ccavenue_form input[name='billing_country']").val("United Arab Emirates");
                        $("#ccavenue_form input[name='billing_tel']").val(result.data['customer']['phone_number']);
                        $("#ccavenue_form input[name='billing_email']").val(result.data['customer']['email_address']);
                        $("#ccavenue_form").submit();
                        /******************************************************************/
                    } else if (result.data['payment_mode'] == 'cash') {
                        if($('#merchant_param2').val() != '') {
                            window.location.href = '{{url('cash-payment-success')}}'+'/'+result.data['bookings']['reference_id']+'?mobile=active';
                        } else{
                            window.location.href = '{{url('cash-payment-success')}}'+'/'+result.data['bookings']['reference_id'];
                        }
                    } else if (result.data['payment_mode'] == 'tamara') {
						window.location.href = result.url;
					} else if (result.data['payment_mode'] == 'gpay') {
						var total_amount_payable = parseFloat(result.data['transaction_charge']) + parseFloat(result.data['bookings']['total_amount']);
                        total_amount_payable = total_amount_payable.toFixed(2);
						var refId = result.data['bookings']['reference_id'];
						var email = result.data['customer']['email_address'];
						var name = result.data['customer']['customer_name'];
						const baseRequest = {
						  apiVersion: 2,
						  apiVersionMinor: 0
						};
						
						const tokenizationSpecification = {
						  type: 'PAYMENT_GATEWAY',
						  parameters: {
							'gateway': 'checkoutltd',
							'gatewayMerchantId': checkoutpkey
						  }
						};
						
						const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];
						
						const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];
						
						const baseCardPaymentMethod = {
						  type: 'CARD',
						  parameters: {
							allowedAuthMethods: allowedCardAuthMethods,
							allowedCardNetworks: allowedCardNetworks
						  }
						};
						
						const cardPaymentMethod = Object.assign(
						  {tokenizationSpecification: tokenizationSpecification},
						  baseCardPaymentMethod
						);
						
						// const paymentsClient = new google.payments.api.PaymentsClient({environment: 'TEST'});
						const paymentsClient = new google.payments.api.PaymentsClient({environment: 'PRODUCTION'});
						
						const isReadyToPayRequest = Object.assign({}, baseRequest);
						isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];
						
						paymentsClient.isReadyToPay(isReadyToPayRequest)
						.then(function(response) {
						  if (response.result) {
							const button =
								paymentsClient.createButton({onClick: () => console.log('TODO: click handler'),
								allowedPaymentMethods: []}); // make sure to provide an allowed payment method
							//document.getElementById('gpaybtn').appendChild(button);
						  }
						})
						.catch(function(err) {
							$(".preloader").fadeOut(1000);
							$("#success_pay_message").text("Something went wrong. Try again.");
							$("#paymenterrormessagepopup").show();
						});
						
						const paymentDataRequest = Object.assign({}, baseRequest);
						paymentDataRequest.allowedPaymentMethods = [cardPaymentMethod];
						paymentDataRequest.transactionInfo = {
						  totalPriceStatus: 'FINAL',
						  totalPrice: total_amount_payable,
						  currencyCode: 'AED',
						  countryCode: 'AE'
						};
						paymentDataRequest.merchantInfo = {
						  merchantName: 'Elite Advance Building Cleaning LLC',
						  merchantId: 'BCR2DN4TXLQ3FHS6'
						};
						
						paymentsClient.loadPaymentData(paymentDataRequest).then(function(paymentData){
						  paymentToken = paymentData.paymentMethodData.tokenizationData.token;
						  $.ajaxSetup({
								headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								}
							});
							
							$.ajax({
								method: 'post',
								url: '{{url('make-gpay-payment')}}',
								data:{'amount':total_amount_payable,'referenceId':refId,'paymentToken':paymentToken,'email':email,'name':name,_token:'{{csrf_token()}}'},
								success: function (result) { 
									if (result.status == 'success')
									{
										$(".preloader").fadeOut(1000);
										window.location.href = result.url;
									} else if (result.status == 'failed')
									{
										$(".preloader").fadeOut(1000);
										$("#success_pay_message").text(result.messages);
										$("#paymenterrormessagepopup").show();
									}
								}
							});
						}).catch(function(err){
							console.log(err);
							$(".preloader").fadeOut(1000);
							$("#success_pay_message").text("Something went wrong. Try again.");
							$("#paymenterrormessagepopup").show();
						});
						
						//paymentsClient.prefetchPaymentData(paymentDataRequest);
					} else if (result.data['payment_mode'] == 'applepay') {
						$('#ap_order_id').val(result.data['bookings']['reference_id']);
                        var total_amount_payable = parseFloat(result.data['transaction_charge']) + parseFloat(result.data['bookings']['total_amount']);
                        total_amount_payable = total_amount_payable.toFixed(2);
                        // if(result.data['customer']['email_address'] == 'vishnu.m@azinova.info') {
                            // $('#ap_amount').val(10);
                            // $('#amount_span').html(10);    
                        // } else {
                             
                        // }
						$('#ap_amount').val(total_amount_payable);  
						$('#amount_span').html(total_amount_payable); 						
                        $('#ap_billing_name').val(result.data['customer']['customer_name']);
                        $('#ap_billing_tel').val(result.data['customer']['mobile_number_1']);
                        $('#ap_billing_email').val(result.data['customer']['email_address']);
                        $('#ap_billing_address').val(result.data['customer_address']['customer_address']);
						console.log(result.data['transaction_charge']);
						$(".preloader").fadeOut(1000);
						$("#payment_button").hide();
						$("#applePaybtn").show();
						
						//$("#payment_popup_applepay").show();
					}
                } else {
					$(".preloader").fadeOut(1000);
                    $("#errormessagepopup p").text(result.messages);
					$("#errormessagepopup").show();
				}
            }
        }); 
    }  
});
function logit( data ){
	
	if( debug == true ){
		console.log(data);
	}	

};
// function card(){
// Frames.init("pk_test_7b3235b1-ad25-4061-85df3-09e851fe87b4");
Frames.addEventHandler(
Frames.Events.CARD_TOKENIZED,
function (event) {
    $('#token_req').val(event.token);
    $("#payment-form").submit();
}
);
if(form) {
    form.addEventListener("submit", function (event) {
        event.preventDefault();
        Frames.submitCard();
    });
}
// }
function getPayment(id)
{
	$("#payment_button").show();
	$("#applePaybtn").hide();
   
    $('#payment_mode').val(id);
    $('#payment_mode-error').hide();
    if(id== 'cash') {
        $('.transaction_div').show();
        $('.transaction_total_div').show();
        $('.total_summary_div').hide();
        var netamount = $('.net_p').html();
        var transaction_charge = {{Config::get('values.pay_by_cash_charge')}};
        $('.transaction_p').html(transaction_charge);
        totalAmount =  parseFloat(transaction_charge)+parseFloat(netamount);
        totalAmount = totalAmount.toFixed(2);
        $('.trans_net_p').html(totalAmount);
        $('.mobile_net_p').html(totalAmount);

        $('#payment_button').val('Confirm');
    }
    else if((id== 'card') || (id== 'gpay'))
    {
        // $('.transaction_div').hide();
        // $('.transaction_total_div').hide();
        // $('.total_summary_div').show();
        // $('.mobile_net_p').html($('.net_p').html());

        $('.transaction_div').show();
        $('.transaction_total_div').show();
        $('.total_summary_div').hide();
       // $('.mobile_net_p').html($('.net_p').html());

        var netamount = $('.net_p').html();
        var transaction_charge = netamount * 0.03;
        //var transaction_charge = 0;
        var transaction_charge = transaction_charge.toFixed(2);
        $('.transaction_p').html(transaction_charge);
        totalAmount =  parseFloat(transaction_charge)+parseFloat(netamount);
        totalAmount = totalAmount.toFixed(2);
        $('.trans_net_p').html(totalAmount);
        $('.mobile_net_p').html(totalAmount);

        // var netamount = $('.net_p').html();
        // var transaction_charg = (((netamount * 0.03) + 1) * 1.05);
        // var transaction_charge = transaction_charg.toFixed(2);
        // $('.transaction_p').html(transaction_charge);
        // totalAmount =  parseFloat(transaction_charge)+parseFloat(netamount);
        // totalAmount = totalAmount.toFixed(2);
        // $('.trans_net_p').html(totalAmount);
        $('#payment_button').val('Make Payment');
    } else if(id== 'tamara')
	{
        $('.transaction_div').show();
        $('.transaction_total_div').show();
        $('.total_summary_div').hide();

        var netamount = $('.net_p').html();
        // var transaction_charge = netamount * 0.03;
        var transaction_charge = 0;
		var transaction_charge = transaction_charge.toFixed(2);
        $('.transaction_p').html(transaction_charge);
        totalAmount =  parseFloat(transaction_charge)+parseFloat(netamount);
        totalAmount = totalAmount.toFixed(2);
        $('.trans_net_p').html(totalAmount);
        $('.mobile_net_p').html(totalAmount);

        $('#payment_button').val('Make Payment');
	}
}

function getApplePayment(id)
{
	if (window.ApplePaySession)
	{
		//var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser';
		var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
		promise.then(function (canMakePayments) {
			if (canMakePayments) {
			} else {
				$(".preloader").fadeOut(1000);
				$("#payment_button").hide();
				$("#applePaybtn").hide();
				$("#success_pay_message").text('ApplePay is possible on this browser, but not currently activated.');
				$("#paymenterrormessagepopup").show();
				return false;
			}
		}); 
	} else {
		$(".preloader").fadeOut(1000);
		$("#payment_button").hide();
		$("#applePaybtn").hide();
		$("#success_pay_message").text('ApplePay is not available on this browser');
		$("#paymenterrormessagepopup").show();
		return false;
	}
	$("#terms").attr("checked", true);
	$('#payment_mode').val(id);
    $('#payment_mode-error').hide();
	$('.transaction_div').show();
	$('.transaction_total_div').show();
	$('.total_summary_div').hide();

	var netamount = $('.net_p').html();
	// var transaction_charge = netamount * 0.03;
	var transaction_charge = 0;
	var transaction_charge = transaction_charge.toFixed(2);
	$('.transaction_p').html(transaction_charge);
	totalAmount =  parseFloat(transaction_charge)+parseFloat(netamount);
	totalAmount = totalAmount.toFixed(2);
	$('.trans_net_p').html(totalAmount);
	$('.mobile_net_p').html(totalAmount);
	
	$("#payment_form").submit();
	
	//$('#payment_button').val('Make Payment');
}

function getTamaraPayment(id)
{
	$("#payment_button").show();
	$("#applePaybtn").hide();
	$('#payment_mode').val(id);
    $('#payment_mode-error').hide();
	$(".preloader").fadeIn(1000);
	$('.transaction_div').hide();
	$('.transaction_total_div').show();
	$('.total_summary_div').hide();
	var netamount = $('.net_p').html();
	//var transaction_charge = netamount * 0.03;
	//$('.transaction_p').html(transaction_charge.toFixed(2));
	// totalAmount =  parseFloat(transaction_charge)+parseFloat(netamount);
	totalAmount =  parseFloat(netamount);
	totalAmount = totalAmount.toFixed(2);
	$('.trans_net_p').html(totalAmount);
	$('.mobile_net_p').html(totalAmount);
	var phonepay = $('#phonetamarapay').val();
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$.ajax({
		method: 'post',
		url: '{{url('make-tamara-payment')}}',
		data:{'amount':totalAmount,'phn':phonepay,_token:'{{csrf_token()}}'},
		success: function (result) { 
			if (result.status == 'success')
			{
				$(".preloader").fadeOut(1000);
				$('#payment_button').val('Make Payment');
			} else if (result.status == 'failed')
			{
				$(".preloader").fadeOut(1000);
				$("#payment_button").hide();
				$("#applePaybtn").hide();
				$("#success_pay_message").text(result.messages);
				$("#paymenterrormessagepopup").show();
			}
		}
	});
	
}
$('#payment_div .em-extra-ser-tmb').on('click', function() {
        $('.selected').removeClass('selected');
        $(this).addClass('selected');
});
$('#back_houseCleaningStep4').click(function(){
    var customerId = localStorage.getItem('customerId');
    if ((customerId) && customerId != '' && $('#typeLogin').val() == 'register') {
        $('#commonStep4').fadeIn(300);
        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition, showError);
                        } else { 
                            x.innerHTML = "Geolocation is not supported by this browser.";
                        }
    } else if ((customerId) && customerId != '') {
        $('#commonStep2').fadeIn(300);
    } 
    $('#commonStep5').fadeOut(300);
    $("html").animate({ scrollTop: 0 }, "slow");
})

function closeerrorpop()
{
	window.location.href = '{{url('house-cleaning-dubai')}}';
}

function closepaymenterrorpop()
{
	$("#success_pay_message").text("");
	$("#paymenterrormessagepopup").hide();
}
</script>
@endpush