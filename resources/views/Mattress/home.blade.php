@extends('layouts.app')@section('title') {{'Mattress Cleaning & Sanitization Service Dubai'}} @endsection
@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
     <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
          <input type="hidden" id="all_amount" value="{{serialize($allAmount)}}">
          <input type="hidden" id="number_hours" value="0">		  		  <input type="hidden" id="serviceNamePay" value="MattressCleaning">
               @include('Mattress.steps')
          </div>
     </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')
<script>
 $.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});
$(document).ready(function() {
	getAvailability();
    amountCalculation();

});
function checkCheckBox(id,val) {
  var checkBox = document.getElementById(id);
  if (checkBox.checked == true ){
    // $('#threeSeat_div').show();
    $('#mattress_hidden1').val(val);
    $('#single_div').show();
    } else {
        // $('#threeSeat_div').hide();
        $('#mattress_hidden1').val('0');
        $('#single_div').hide();
  }
  amountCalculation();
}
function checkCheckBox2(id,val) {
  var checkBox = document.getElementById(id);
  if (checkBox.checked == true ){
    // $('#fourSeat_div').show();
    $('#mattress_hidden2').val(val);
    $('#queen_div').show();
    } else {
    // $('#fourSeat_div').hide();
    $('#mattress_hidden2').val('0');
    $('#queen_div').hide();
  }
  amountCalculation();

}
function checkCheckBox3(id,val) {
  var checkBox = document.getElementById(id);
  if (checkBox.checked == true ){
    // $('#threeSeatL_div').show();
    $('#mattress_hidden3').val(val);
    $('#king_div').show();

    } else {
    // $('#threeSeatL_div').hide();
    $('#mattress_hidden3').val('0');
    $('#king_div').hide();
  }
  amountCalculation();

}
// function checkCheckBox4(id,val) {
//   var checkBox = document.getElementById(id);
//   if (checkBox.checked == true ){
//     // $('#fourSeatL_div').show();
//     $('#mattress_hidden4').val(val);
//     $('#double_div').show();
//     } else {
//     // $('#fourSeatL_div').hide();
//     $('#mattress_hidden4').val('0');
//     $('#double_div').hide();
//   }
//   amountCalculation();

// }
function amountCalculation()
{
    var furnished =  0;
    var service_id = $('#service_id').val();
    var is_scrubbing = 0;
    var sub_category = 0;
    var categorySingle = $('#mattress_hidden1').val();
    var categoryQueen = $('#mattress_hidden2').val();
    var categoryKing = $('#mattress_hidden3').val();
    var singleArray = [];
    var kingArray = [];
    var queenArray = [];

    if(categorySingle != 0 ) {
       var mattressCount = $('#single_hidden').val();
       singleArray.push({service_id : service_id,category_id : categorySingle,sub_category_id : sub_category,name : mattressCount,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
    }
    if(categoryKing != 0 ) {
       var mattressCount = $('#king_hidden').val();
       kingArray.push({service_id : service_id,category_id : categoryKing,sub_category_id : sub_category,name : mattressCount,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
    }
    if(categoryQueen != 0 ) {
       var mattressCount = $('#queen_hidden').val();
       queenArray.push({service_id : service_id,category_id : categoryQueen,sub_category_id : sub_category,name : mattressCount,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
    }
  
    var intialArray = $('#all_amount').val();
    var coupon = $('.couponClass').val();

    $.ajax({
          method: 'post',
          url: '{{url('calculate-mattress')}}',
          data:{'singleArray':singleArray,'kingArray':kingArray,'queenArray':queenArray,'intialArray':intialArray,'officesqure':0,_token:'{{csrf_token()}}'},  
         
          success: function (result) {
               if (result.status == 'success') {
                   $('.total_p').text(result.data['amount']);
                   if(coupon != '' && coupon != 'undefined') {
                        checkCoupon();
                    } else {
                        $('.vat_p').text(result.data['vat_amount']);
                        $('.net_p').text(result.data['totalAmount']);
                        $('.mobile_net_p').text(result.data['totalAmount']);
                        $('.discount_div').hide();
                        $('#coupon_message').hide();
                    }

                   $('#single').val(result.data['single']);
                   $('#queen').val(result.data['queen']);
                   $('#king').val(result.data['king']);
               }
          }
     }); 
}
function toggleClass( element ) {
    var classe = 'col-lg-6 col-md-6 col-sm-6 col-6 unselect';

    if ( element.className == classe ){
        element.className = classe.replace('unselect', 'selected');
    } else {
        element.className = classe;
    }
}
</script>
@endpush