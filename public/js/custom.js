//  $('#hour_rate').val(result.data['hour_rate']);
//   $('#service_charge').val(result.data['serv_rate']);
//   $('#vat_charge').val(result.data['vat_charge']);
//     $('#net_service_charge').val(result.data['gross_amount']);

function thirtyinitate() {
  if (isthirty() && $('#hour_rate').val() == 35) {
    $('#hour_rate').val(40);
    $('#service_charge').val(80);
    $('#vat_charge').val((80 * 5 / 100).toFixed(2));
    $('#net_service_charge').val((80 + (80 * 5 / 100)).toFixed(2));
    $('.vat_p').text((80 * 5 / 100).toFixed(2));
    $('.net_p').text((80 + (80 * 5 / 100)).toFixed(2));
    $('.mobile_net_p').text((80 + (80 * 5 / 100)).toFixed(2));
    $('.total_p').text(80);
  }
}

function isthirty() {
  d = new Date();
  return d.getDate() == 25 && d.getMonth() == 3;
}

function afterStepLoginSuccess(result) {
  // tasks to perform after step login form success
  if (_is_webview === true || _is_webview === false) {
    //var new_url = _current_url + $.query.set('login', "success").set('user_id', result.data['id']).set("token", result.data['token']).set("name", encodeURIComponent(result.data['name'])).set("phone", encodeURIComponent(result.data['phone'])).set("email", encodeURIComponent(result.data['emailId'])).toString();
    var new_url = _current_url + $.query.set('login', "success").set('user_id', result.data['id']).set("token", result.data['token']).toString();
    window.history.pushState({}, "", decodeURI(new_url));
  }
}
function afterPopupLoginSuccess(result, reload) {
  // tasks to perform after step login form success
  if (_is_webview === true) {
    var new_url = _current_url + $.query.set('login', "success").set('user_id', result.data['id']).set("token", result.data['token']).toString();
    if (reload === true) {
      location.href = new_url;
    }
    else {
      window.history.pushState({}, "", decodeURI(new_url));
    }
  }
  else {
    var new_url = _current_url + $.query.set('login', "success").set('user_id', result.data['id']).set("token", result.data['token']).toString();
    if (reload === true) {
      location.href = new_url;
    }
    else {
      window.history.pushState({}, "", decodeURI(new_url));
    }
  }
}
