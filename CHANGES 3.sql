ALTER TABLE `config_cleaning_supplies`
ADD `option_description_html` varchar(150) NULL;
ALTER TABLE `config_cleaning_supplies`
ADD `quanity_method` enum('H','M','S') COLLATE 'utf8mb4_general_ci' NULL DEFAULT 'S' COMMENT 'H - Hourly, M - Maidly, S - Servicely';