---------------------------------------------------------------------------------------------
CREATE TABLE `config_cleaning_supplies` (
  `id` bigint NOT NULL,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `amount` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
INSERT INTO `config_cleaning_supplies` (`id`, `type`, `name`, `amount`) VALUES
(1, 'PlanBased', 'Standard Cleaning Supplies', 10),
(2, 'PlanBased', 'Ecological Green Clean Supplies', 20),
(3, 'Custom', 'VACUUM CLEANER', 10),
(4, 'Custom', 'LADDER', 10),
(5, 'Custom', 'MOP BUCKET', 10),
(6, 'Custom', ' ELECTRIC IRON', 10);
ALTER TABLE `config_cleaning_supplies`
  ADD PRIMARY KEY (`id`);
  ALTER TABLE `config_cleaning_supplies`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
---------------------------------------------------------------------------------------------
  CREATE TABLE `booking_cleaning_supplies` (
  `id` bigint UNSIGNED NOT NULL,
  `booking_id` bigint UNSIGNED NOT NULL,
  `cleaning_supply_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `booking_cleaning_supplies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `cleaning_supply_id` (`cleaning_supply_id`);
  ALTER TABLE `booking_cleaning_supplies`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  ALTER TABLE `booking_cleaning_supplies`
  ADD CONSTRAINT `booking_cleaning_supplies_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `booking_cleaning_supplies_ibfk_2` FOREIGN KEY (`cleaning_supply_id`) REFERENCES `config_cleaning_supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
---------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `plan_based_supplies` tinyint(1) unsigned NULL DEFAULT '0' COMMENT '1 - Selected any plan based supplies',
ADD `custom_supplies` tinyint(1) unsigned NULL DEFAULT '0' COMMENT '1 - Selected any custom supplies' AFTER `plan_based_supplies`;
---------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `plan_based_supplies_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `plan_based_supplies`,
ADD `custom_supplies_amount` decimal(10,2) unsigned NULL DEFAULT '0';
---------------------------------------------------------------------------------------------
ALTER TABLE `booking_cleaning_supplies`
ADD UNIQUE `booking_id_cleaning_supply_id` (`booking_id`, `cleaning_supply_id`);
---------------------------------------------------------------------------------------------
ALTER TABLE `booking_cleaning_supplies`
ADD `total_amount` decimal(10,2) NOT NULL;
---------------------------------------------------------------------------------------------
ALTER TABLE `booking_cleaning_supplies`
ADD `unit_rate` decimal(10,2) NOT NULL AFTER `cleaning_supply_id`;
---------------------------------------------------------------------------------------------
ALTER TABLE `booking_cleaning_supplies`
ADD `quantity` int(5) unsigned NOT NULL DEFAULT '1' AFTER `unit_rate`;
---------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `supervisor_selected` enum('N','Y') NULL DEFAULT 'N' COMMENT 'Y - opted for supervisor',
ADD `supervisor_charge_hourly` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `supervisor_selected`,
ADD `supervisor_charge_total` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `supervisor_charge_hourly`;
---------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `url_slug` varchar(150) COLLATE 'latin1_swedish_ci' NULL AFTER `service_type_name`;
---------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD UNIQUE `url_slug` (`url_slug`);
---------------------------------------------------------------------------------------------
CREATE TABLE `service_cleaning_supplies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `cleaning_supply_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id_cleaning_supply_id` (`service_type_id`,`cleaning_supply_id`),
  KEY `cleaning_supply_id` (`cleaning_supply_id`),
  CONSTRAINT `service_cleaning_supplies_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`),
  CONSTRAINT `service_cleaning_supplies_ibfk_2` FOREIGN KEY (`cleaning_supply_id`) REFERENCES `config_cleaning_supplies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
---------------------------------------------------------------------------------------------
INSERT INTO `service_cleaning_supplies` (`id`, `service_type_id`, `cleaning_supply_id`) VALUES
(1,	1,	1),
(2,	1,	2),
(3,	1,	3),
(4,	1,	4),
(6,	1,	5),
(7,	1,	6),
(8,	49,	2);
---------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `web_order_id` int(5) unsigned NULL AFTER `order_id`;
---------------------------------------------------------------------------------------------
ALTER TABLE `servicecategorycosts`
ADD `hours` decimal(5) NULL AFTER `name`;
---------------------------------------------------------------------------------------------
ALTER TABLE `extra_services`
ADD `service_type_id` int(11) NULL AFTER `id`,
ADD FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`);
---------------------------------------------------------------------------------------------
CREATE TABLE `service_what_to_expect_points` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `order_id` int(5) DEFAULT NULL,
  `point_html` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_what_to_expect_points_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
---------------------------------------------------------------------------------------------
ALTER TABLE `extra_services`
ADD `service_type_id` int(11) NULL AFTER `id`,
ADD FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`);
---------------------------------------------------------------------------------------------
ALTER TABLE `extra_services`
ADD `show_on_min_hour` int(3) unsigned NULL AFTER `image_url`,
ADD `show_on_max_hour` int(3) unsigned NULL AFTER `show_on_min_hour`;
---------------------------------------------------------------------------------------------
INSERT INTO `extra_services` (`id`, `service_type_id`, `service`, `duration`, `cost`, `image_url`, `show_on_min_hour`, `show_on_max_hour`, `status`, `app_image_url`) VALUES
(5,	1,	'Tydying Up',	'30',	'0',	NULL,	2,	8,	'0',	NULL),
(6,	1,	'Ironing',	'30',	'0',	NULL,	2,	8,	'0',	NULL),
(7,	1,	'Interior Windows',	'30',	'0',	NULL,	3,	8,	'0',	NULL),
(8,	49,	'Tydying Up',	'30',	'0',	NULL,	2,	8,	'0',	NULL),
(9,	49,	'Ironing',	'30',	'0',	NULL,	2,	8,	'0',	NULL),
(10,	49,	'Interior Windows',	'30',	'0',	NULL,	3,	8,	'0',	NULL);
---------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `extra_services` tinyint(3) unsigned NULL DEFAULT '0' COMMENT '1 - Extra services selected',
ADD `extra_services_amount` decimal(10,2) unsigned NULL DEFAULT '0.00' AFTER `extra_services`;
---------------------------------------------------------------------------------------------
CREATE TABLE `booking_extra_services` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `extra_service_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT 0 COMMENT 'in minutes',
  `unit_rate` decimal(10,2) NOT NULL,
  `quantity` int(5) unsigned NOT NULL DEFAULT 1,
  `total_amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `extra_service_id` (`extra_service_id`),
  KEY `booking_id` (`booking_id`),
  CONSTRAINT `booking_extra_services_ibfk_1` FOREIGN KEY (`extra_service_id`) REFERENCES `extra_services` (`id`),
  CONSTRAINT `booking_extra_services_ibfk_2` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
---------------------------------------------------------------------------------------------
ALTER TABLE `bookingservicemapping`
ADD `quantity` int(1) NOT NULL DEFAULT '1' AFTER `service_cost`;
---------------------------------------------------------------------------------------------
ALTER TABLE `bookingservicemapping`
CHANGE `booking_id` `booking_id` bigint(20) unsigned NOT NULL AFTER `id`,
ADD FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`);
---------------------------------------------------------------------------------------------
ALTER TABLE `bookingservicemapping`
ADD FOREIGN KEY (`service_id`) REFERENCES `service_types` (`service_type_id`);
---------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `property_details` text NULL;
---------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `service_week_days` varchar(255) COLLATE 'latin1_swedish_ci' NULL;
---------------------------------------------------------------------------------------------
CREATE TABLE `crew_in_options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) DEFAULT NULL COMMENT 'NULL - for all services',
  `text` varchar(155) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `crew_in_options_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `crew_in_options` (`id`, `service_type_id`, `text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	NULL,	'Key is with security',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(2,	NULL,	'Key under the mat',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(3,	NULL,	'Buzz the intercom',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(4,	NULL,	'Door open',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(5,	NULL,	'Others',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(6,	4,	'I will be there',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(7,	1,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(8,	5,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(9,	47,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(10,	53,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(11,	55,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(12,	54,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(13,	56,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(14,	52,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(15,	62,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(17,	57,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(18,	59,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(19,	58,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(20,	61,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL),
(21,	60,	'At home',	'2023-09-19 13:37:18',	'2023-09-19 13:37:18',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `booking_common_id` bigint unsigned NULL AFTER `booking_id`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD FOREIGN KEY (`booking_common_id`) REFERENCES `bookings` (`booking_id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `package`
ADD `strikethrough_amount` double(10,2) NOT NULL DEFAULT '0' AFTER `booking_type`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `package`
ADD `promotion_image_web` varchar(255) COLLATE 'utf8_general_ci' NULL AFTER `promotion_image`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_types` (
  `booking_type` varchar(6) NOT NULL,
  `booking_type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`booking_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `booking_types` (`booking_type`, `booking_type_name`) VALUES
('BW',	'Bi-Weekly'),
('OD',	'One TIme'),
('WE',	'Weekly');
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `sort_order_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `faqs` (`id`, `question`, `answer`, `sort_order_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Can I get same Maid assigned on a recurring basis?',	'Absolutely! The My Maid Plan offers you the same housekeeper on every service. We want you to enjoy the kind of continuity in your maid service that allows you peace of mind.',	10,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(2,	'What is the nationality of your Maids?',	'Based on the most common requests from clients, our team is about 90% Filipina and 10% Nepalese & Indonesians.',	20,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(3,	'How do I get started?',	'You could reach us by Call, WhatsApp Helpline, email, request a call back, or directly book online. We will understand your expectations and give you a free estimate. Together we create a work order tailored to your specific requests, and you receive a confirmation with details of the cleaning service booked.',	30,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(4,	'Do I have to sign a contract?',	'Not necessarily. We are happy to earn your business each time we clean. However, if you would like to set up the same housekeeper on a fixed day & time, at a discounted monthly plan, this is called My Maid Plan and it is arranged with an agreement. You can suspend your cleaning service with just one day’s notice, pay only for the service taken, and resume when you are ready.',	40,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(5,	'Will I have any liability or need to pay any hidden charges?',	'No. Dubai Housekeeping’s goal is to reduce the number of responsibilities in your life; certainly not add to them!',	50,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(6,	'What if I have a pet?',	'No worries! We work well around pets too. We will only need to be notified of this beforehand, and of course, we would like to know their names. If you have any special requests regarding your pets, or there are any special circumstances we should be aware of, include this information in your work order and we will take care of it.',	60,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(7,	'What should I expect from my first appointment?',	'We will arrive at your home on the scheduled day & time. Your first cleaning service appointment generally takes the longest, as we will need time to get acquainted with a new environment.',	70,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(8,	'What if the Maid damaged something?',	'In the unfortunate event that a something is damaged, we will promptly make every effort to have the item repaired or replaced, please notify us within 24 hours of the cleaning service date. Our liability limits are set at a maximum of 200% of your one-time service fee. In the event the damage is not within that limit, we will initiate a claim through the insurance channel according to the terms & conditions.',	80,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(9,	'How much do you charge?',	'Cleaning service charges depend on various factors such as whether you require recurring or one-off services, how many hours you expect per service, if you provide the cleaning supplies, etc.\r\n<br>\r\nThe most popular, discounted recurring service (that is at least once a week) is for 3 hrs at AED 123 and 4 hrs is AED 141, inclusive of VAT. Ask for your recurring service discount so this can be adjusted on your invoice. \r\n<br>\r\nFor standard rates & other details, please call us or visit our website.',	90,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(10,	'What is the payment mode?',	'You may pay by cash, check, online transfer, Visa, or Mastercard (T & C apply).',	100,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(11,	'Do I have to be home when the Maid arrives?',	'While it is recommended that you are present at the time of cleaning service, many of our busy customers provide us with a key to their home and (when necessary) their alarm codes so our crew can execute the service without them having to alter their plans.',	110,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(12,	'Do you guarantee your work?',	'Absolutely. We have a 100% satisfaction guaranteed policy. Our goal is to give you the best cleaning possible each time. If something is not done to your satisfaction, call us within 24 hours and we will correct it for free, or give you a credit on your next cleaning inquiry.',	120,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(13,	'When do I pay for my Maid service?',	'For an on-call service, payment is due the day the cleaning service is performed. Clients with the My Maid plan, pay before the start of the month.',	130,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(14,	'Which areas do you cover?',	'Presently we cover all communities along Sheikh Zayed Road from World Trade Centre to Marina, along Al Kahil Road from Al Quoz to JVC, along Bin Zayed Road Arabian Ranches to JVT, along Umm Sequim street from Burj Al Arab to Mira. Along Al Wasl from Satwa to Umm Sequim street, Palm Jumeirah, JBR, Blue Water Island and more.',	140,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(15,	'Do you offer Deep Cleaning services?',	'Yes, we do. Call us so we know exactly what you are expecting and we can plan accordingly.',	150,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(16,	'Do you provide ironing services?',	'Yes, you can hire us for an Ironing service.',	160,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(17,	'Do you provide babysitting services?',	'Certainly! We accept babysitting inquiries for children over the age of 3.',	170,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(18,	'Do you offer full-time housekeepers?',	'No, we do not offer full time / live-in housekeepers.',	180,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(19,	'What are your working hours?',	'We work Mondays to Saturdays from 8 am to 6 pm.',	190,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL),
(20,	'Do you have any promotions?',	'We do! You can claim a Welcome discount of <b>20%</b> on your First service. However, if you book the <i>My Maid Plan</i>, you can avail <b>12% discount on every service</b>.',	200,	'2023-09-26 15:06:29',	'2023-09-26 15:06:29',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `booking_id` bigint(20) unsigned DEFAULT NULL,
  `service_date` date DEFAULT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `read_at` datetime DEFAULT NULL,
  `cleared_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `booking_id` (`booking_id`),
  CONSTRAINT `customer_notifications_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `customer_notifications_ibfk_2` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_coupons`
ADD `booking_id` bigint(20) unsigned NULL AFTER `coupon_id`,
ADD FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
CHANGE `coupon_id` `coupon_id` bigint(20) NULL AFTER `payment_type`,
ADD FOREIGN KEY (`coupon_id`) REFERENCES `coupon_code` (`coupon_id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_coupons`
CHANGE `discount` `discount` float(10,2) NOT NULL DEFAULT '0' AFTER `reference_id`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_coupons`
CHANGE `customer_id` `customer_id` bigint(20) unsigned NOT NULL AFTER `id`,
ADD FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
ADD FOREIGN KEY (`coupon_id`) REFERENCES `coupon_code` (`coupon_id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_coupons`
ADD `coupon_name` varchar(200) NULL AFTER `coupon_id`;