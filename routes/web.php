<?php

// header('Access-Control-Allow-Origin: *');
// header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
// header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\RegistrationSuccessMail;
/************************************************************************** */
// service page routes
Route::get('/','ServiceController@getServices')->name('home');
Route::get('house-cleaning-dubai/{coupon?}','ServiceController@getServices'); // Home Cleaning Service
Route::get('organic-cleaning-service/{coupon?}','ServiceController@getOrganicCleaning'); // Organic Cleaning Service
Route::get('office-cleaning-service/{coupon?}','ServiceController@getOfficeCleaning'); // Office Cleaning Service
Route::get('ironing-service/{coupon?}','ServiceController@getIroning'); // Ironing Service
Route::get('baby-sitting-service/{coupon?}','ServiceController@getBabySitting'); // Baby Sitting Service
Route::get('deep-cleaning-service/{coupon?}','ServiceController@getDeepCleaning'); // Deep Cleaning Service
Route::get('move-out-cleaning-service/{coupon?}','ServiceController@getMoveOutCleaning'); // Move out Cleaning Service
Route::get('move-in-cleaning-service/{coupon?}','ServiceController@getMoveInCleaning'); // Move in Cleaning Service
Route::get('steam-cleaning-service/{coupon?}','ServiceController@getSteamCleaning'); // Steam Cleaning Service
Route::get('sofa-cleaning-service/{coupon?}','ServiceController@getSofaCleaning'); // Sofa Cleaning Service
Route::get('mattress-cleaning-service/{coupon?}','ServiceController@getMattressCleaning'); // Mattress Cleaning Services
Route::get('carpet-cleaning-service/{coupon?}','ServiceController@getCarpetCleaning'); // Carpet Cleaning Service
Route::get('floor-scrubbing-service/{coupon?}','ServiceController@getFloorScrubbing'); // Floor Scrubbing Service
Route::get('sanitization-service/{coupon?}','ServiceController@getSanitization'); // Sanitization
Route::get('declutter-and-organize-service/{coupon?}','ServiceController@getDeclutterAndOrganize'); // Declutter & Organize
Route::get('mold-cleaning-service/{coupon?}','ServiceController@getMoldCleaning'); // Mold Cleaning Service
Route::get('holiday-home-cleaning-service/{coupon?}','ServiceController@getHolidayHomeCleaning'); // Holiday Home Cleaning
/************************************************************************** */
// routes for payments
Route::post('ccavRequestHandler','CCAvenueController@ccavRequestHandler');
Route::post('ccavResponseHandler','CCAvenueController@ccavResponseHandler');
Route::get('payment-success/{refId}/{trackId}','ServiceController@paymentSuccess');
Route::get('payment-error/{refId}/{trackId}','ServiceController@paymentFailed');
/************************************************************************** */
// static pages
Route::get('about-us','WebviewController@about_us');
/************************************************************************** */
Route::post('make-payment','ServiceController@makePayment'); 
Route::get('view-bookings','UserController@viewBookings');
Route::get('my-accounts', 'UserController@myAccounts');


















Route::get('get-user-data','UserController@getUserData');
Route::get('cleaning-second/{id}','ServiceController@cleaningSecond');
Route::get('cleaning-third/{id}','ServiceController@cleaningThird');
Route::get('cleaning-fourth/{id}','ServiceController@cleaningFourth');
Route::get('cleaning-fifth/{id}','ServiceController@cleaningFifth');
Route::post('save-step1','ServiceController@saveStep1');
Route::post('save-step2','ServiceController@saveStep2');
Route::post('login-user','ServiceController@loginUser');
Route::post('customer-save','ServiceController@registerUser');
Route::post('save-address','ServiceController@saveAddress');
Route::get('go-four/{id}','ServiceController@goFour');
Route::get('go-three/{id}','ServiceController@goThree');
Route::get('go-two/{id}','ServiceController@goTwo');
Route::get('go-one/{id}','ServiceController@goOne');
Route::post('forgot-password','ServiceController@forgotPassword');
Route::post('verify-mobile','ServiceController@verifyMobile');
Route::post('resend-otp','ServiceController@resendOTP');

Route::post('calculate-price','ServiceController@calculatePrice');
Route::post('calculate-availability','ServiceController@calculateAvailability');
Route::post('check-coupon','ServiceController@checkCoupon');
Route::get('disinfection-service-dubai','ServiceController@loadDisinfection');
Route::get('disinfection-service-dubai/{coupon}','ServiceController@loadDisinfection');
Route::get('deep-cleaning-dubai','ServiceController@loadDeep');
Route::get('deep-cleaning-dubai/{coupon}','ServiceController@loadDeep');
Route::get('carpet-cleaning-dubai','ServiceController@loadCarpet');
Route::get('carpet-cleaning-dubai/{coupon}','ServiceController@loadCarpet');

Route::get('sofa-cleaning-dubai','ServiceController@sofaCleaning');
Route::get('sofa-cleaning-dubai/{coupon}','ServiceController@sofaCleaning');

Route::get('mattress-cleaning-dubai','ServiceController@mattressCleaning');
Route::get('mattress-cleaning-dubai/{coupon}','ServiceController@mattressCleaning');
Route::get('floor-scrubbing-dubai','ServiceController@floorScrubbing');
Route::get('floor-scrubbing-dubai/{coupon}','ServiceController@floorScrubbing');

Route::post('calculate-load','ServiceController@calculateLoad');
Route::post('calculate-sofa','ServiceController@calculateSofa');
Route::post('calculate-mattress','ServiceController@calculateMattress');
Route::post('calculate-carpet','ServiceController@calculateCarpet');
// Route::get('payment-success/{refId}/{trackId}','ServiceController@paymentSuccess');
// Route::get('payment-error/{refId}/{trackId}','ServiceController@paymentFailed');
Route::get('cash-payment-success/{refId}','ServiceController@cashPaymentSuccess');
Route::get('payment','PaymentController@payLink');
Route::get('invoice-payment','InvoiceController@invoivePayLink');
Route::post('save-online-pay','PaymentController@saveOnlinePay');
Route::post('save-invoice-pay','InvoiceController@saveInvoicePay');
Route::get('online-payment-success/{payId}/{trackId}','PaymentController@onlinePaymentSuccess');
Route::get('online-payment-error/{payId}/{trackId}','PaymentController@onlinePaymentFailed');
Route::get('invoice-payment-success','InvoiceController@invoicePaymentSuccess');
Route::get('invoice-payment-error','InvoiceController@invoicePaymentFailed');
Route::post('edit-user-data','UserController@editUserData');
Route::post('update-customer-avatar','UserController@updateCustomerAvatar');


Route::get('manage-address','UserController@manageAddress');
Route::get('edit-address-data/{addressId}','UserController@editAddressData');
Route::post('save-address-data','UserController@saveAddressData');
Route::get('add-address','UserController@addAddressData');
Route::post('delete-customer-address','UserController@deleteCustomerAddress');
Route::post('set-default','UserController@setDefault');
Route::post('update-user-password','UserController@updateUserPassword');

Route::post('bookinglist-payment','PaymentController@bookinglistPayment');
Route::post('cancel-booking','PaymentController@cancelBooking');
Route::get('logout','UserController@logOut');
Route::post('make-pay-checkout','PaymentController@payment');
Route::get('payment-success','PaymentController@paymentSuccess');
Route::get('payment-error', 'PaymentController@paymentFailed');
Route::get('payment-test', function () {
    return view('payment_view');
});
Route::get('rating/{date}/{serviceId}/{bookingId}','RatingController@rating');
Route::post('submit-rate','RatingController@submitRate');

Route::get('payment-failed/{ref}/{id}', 'PaymentController@errorFailed');
Route::get('online-payment-failed/{ref}/{id}', 'PaymentController@onlineErrorFailed');
Route::get('payment-invoice-failed/{ref}/{id}', 'InvoiceController@onlineErrorFailed');
Route::get('terms-and-conditions', 'WebviewController@terms_and_conditions');
//Route::get('terms-and-conditions', 'ServiceController@termsAndConditions');
Route::get('contact-us', 'WebviewController@contact_form');
Route::post('contact-us', 'WebviewController@contact_form_send');
Route::get('check-odoo','InvoiceController@checkOdoo');

Route::get('test-call','PaymentController@testcall');
Route::post('make-tamara-payment','ServiceController@makeTamaraPayment');
Route::post('make-retry-payment','ServiceController@makeRetryTamaraPayment');

Route::post('make-gpay-payment','PaymentController@makeGpayPayment');
Route::get('gpay-payment-failed/{ref}/{id}', 'PaymentController@gpayPaymentFailed');

Route::post('make-invoice-gpay-payment','PaymentController@makeInvoiceGpayPayment');
Route::get('invoice-gpay-payment-success','InvoiceController@invoiceGpayPaymentSuccess');
Route::get('invoice-gpay-payment-failed/{ref}/{id}', 'PaymentController@invoiceGpayPaymentFailed');

Route::get('payment-tamara-success','PaymentController@paymentTamaraSuccess'); 
Route::get('payment-tamara-error','PaymentController@paymentTamaraError');
Route::post('payment-tamara-hook','PaymentController@paymentTamaraHook');

Route::post('apple-process-pay','PaymentController@appleProcessPay');
Route::get('apple-payment-success/{ref}/{id}', 'PaymentController@applePaymentSuccess');
Route::get('apple-payment-failed/{ref}', 'PaymentController@applePaymentFailed');

Route::post('apple-invoice-process-pay','PaymentController@appleInvoiceProcessPay');
Route::get('apple-invoice-payment-failed/{ref}', 'PaymentController@appleInvoicePaymentFailed');
Route::get('apple-invoice-payment-success/{ref}/{id}', 'InvoiceController@appleInvoicePaymentSuccess');

Route::get('test-mail', function(){
    $email='vishnu.m@azinova.info';
    $name='Karthika';
    $password='karthika@123';
     Mail::send(new RegistrationSuccessMail($email,$name,$password));
});

// package module routes
// Route::get('packages', 'PackageController@list_packages');
Route::get('packages', 'PackageController@list_packages_web');
Route::get('package/view/{package_id}', 'PackageController@view_package');
Route::get('package/payment/success/{reference_id}/{tracking_id?}', 'PackagePaymentController@paymentSuccess');
Route::get('package/payment/failed/{reference_id}/{tracking_id?}', 'PackagePaymentController@paymentFailed');
Route::post('package/payment/ccavenue_response_handler', 'PackagePaymentController@ccavenue_response_handler');
Route::post('package/payment/card/checkout', 'PackagePaymentController@cardCheckout');
Route::post('package/payment/gpay/checkout', 'PackagePaymentController@gpayCheckout');
Route::post('package/payment/applepay/checkout', 'PackagePaymentController@applepayCheckout');
Route::get('package/payment/retry/{reference_id}', 'PackagePaymentController@retryPayment');